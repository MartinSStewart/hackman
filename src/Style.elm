module Style exposing (attentionBackground, button, checkboxIcon, hSplit, highlightBackground, highlightColor, menuBackground, menuBackgroundColor, menuFormButton, menuFormButton_, menuFormPadding, menuLink, tableNameColumnFontColor, tableNameColumnFontSize)

import Color exposing (Color)
import Color.Manipulate
import Draw
import Element exposing (Element, px)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Geometry.Svg as Svg
import Helper exposing (..)
import Html.Attributes
import Html.Events
import Json.Decode
import Point2d
import Polygon2d
import Svg exposing (Svg)
import Svg.Attributes
import Vector2i exposing (Vector2i)


type alias WindowSize a =
    { a | windowSize : Vector2i }


menuLink : List (Element.Attr () msg)
menuLink =
    [ Font.color <| Element.rgb 0.5 0.5 1
    , Element.mouseOver [ Font.color <| Element.rgb 0.5 1 0.5 ]
    ]


checkboxIcon : Color -> Bool -> Element msg
checkboxIcon color isChecked =
    if isChecked then
        Element.el
            [ Element.width <| px 20
            , Element.height <| px 20
            , Border.width 2
            , Border.color <| Helper.colorToElement color
            ]
            (checkSvg color)

    else
        Element.el
            [ Element.width <| px 20
            , Element.height <| px 20
            , Border.width 2
            , Border.color <| Helper.colorToElement color
            ]
            Element.none


checkSvg : Color -> Element msg
checkSvg color =
    [ Point2d.fromCoordinates ( 17, 2 )
    , Point2d.fromCoordinates ( 17, 6 )
    , Point2d.fromCoordinates ( 6, 17 )
    , Point2d.fromCoordinates ( 5, 17 )
    , Point2d.fromCoordinates ( 2, 14 )
    , Point2d.fromCoordinates ( 2, 10 )
    , Point2d.fromCoordinates ( 5.5, 13.5 )
    ]
        |> Polygon2d.singleLoop
        |> Draw.polygon color
        |> List.singleton
        |> Svg.svg
            [ Svg.Attributes.viewBox "0 0 19 19"
            , Html.Attributes.style "pointer-events" "none"
            ]
        |> Element.html


menuFormButton : List (Element.Attribute msg)
menuFormButton =
    [ Element.paddingXY 20 10
    , Element.mouseOver <| [ highlightBackground ]
    , Font.color <| Element.rgb 1 1 1
    ]


menuFormButton_ : Bool -> List (Element.Attribute msg)
menuFormButton_ highlight =
    [ Element.paddingXY 20 10
    , if highlight then
        highlightBackground

      else
        Background.color <| Element.rgba 0 0 0 0
    , Font.color <| Element.rgb 1 1 1
    ]


highlightBackground : Element.Attr decorative msg
highlightBackground =
    Background.color <| Helper.colorToElement highlightColor


highlightColor : Color
highlightColor =
    Color.rgba 1 1 1 0.5


menuFormPadding : Element.Attribute msg
menuFormPadding =
    Element.padding 16


menuBackground : Element.Attribute msg
menuBackground =
    Background.color <| Helper.colorToElement menuBackgroundColor


menuBackgroundColor : Color
menuBackgroundColor =
    Color.rgba 0 0 0 0.35


attentionBackground : Color -> Float -> Element.Attribute msg
attentionBackground color timeInSeconds =
    let
        mixT =
            sin (timeInSeconds * 3) |> (*) 0.5 |> (+) 0.5
    in
    Background.color <| Helper.colorToElement <| Color.Manipulate.weightedMix (Color.rgba 1 0.7 0.1 0.7) color mixT


tableNameColumnFontColor : Color
tableNameColumnFontColor =
    Color.rgba 0.9 0.9 0.9 1


tableNameColumnFontSize : Element.Attr decorative msg
tableNameColumnFontSize =
    Font.size 30


hSplit color =
    Element.el
        [ Border.color color
        , Border.width 1
        , Element.width Element.fill
        ]
        Element.none


button : List (Element.Attribute msg) -> { onPress : msg, onMouseMove : Maybe msg, onMouseEnter : Maybe msg, onMouseLeave : Maybe msg, label : Element msg } -> Element msg
button attributes { onPress, onMouseMove, onMouseEnter, onMouseLeave, label } =
    Input.button
        (attributes
            ++ [ Element.htmlAttribute <| Html.Attributes.tabindex -1
               , case onMouseEnter of
                    Just msg ->
                        Element.htmlAttribute <| Html.Events.onMouseEnter msg

                    Nothing ->
                        Helper.noneAttribute
               , case onMouseLeave of
                    Just msg ->
                        Element.htmlAttribute <| Html.Events.onMouseOut msg

                    Nothing ->
                        Helper.noneAttribute
               , case onMouseMove of
                    Just msg ->
                        Element.htmlAttribute <| Html.Events.on "mousemove" (Json.Decode.succeed msg)

                    Nothing ->
                        Helper.noneAttribute
               ]
        )
        { onPress = Just onPress, label = label }
