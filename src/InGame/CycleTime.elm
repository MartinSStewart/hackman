module InGame.CycleTime exposing (CycleTime(..), from, inSeconds, seconds)

{-| Time relative to the start of a chip cycle.
-}

import InGameTime exposing (InGameTime)
import Quantity exposing (Quantity)


type CycleTime
    = CycleTime Never


seconds : number -> Quantity number CycleTime
seconds value =
    Quantity.Quantity value


inSeconds : Quantity number CycleTime -> number
inSeconds (Quantity.Quantity value) =
    value


from : Quantity number InGameTime -> Quantity number InGameTime -> Quantity number CycleTime
from cycleStartTime time =
    time |> Quantity.minus cycleStartTime |> InGameTime.inSeconds |> seconds
