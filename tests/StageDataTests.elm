module StageDataTests exposing (..)

import AssocList
import Expect
import StageData exposing (CircuitData(..))
import Test exposing (Test, describe, test)


all : Test
all =
    describe "Stage data"
        [ test "Check that all stages parse" <|
            \_ ->
                StageData.allStages
                    |> AssocList.values
                    |> List.all
                        (\stage ->
                            case stage.circuit of
                                Data _ _ ->
                                    True

                                FailedToParse ->
                                    False
                        )
                    |> Expect.true "All stages should parse"
        ]
