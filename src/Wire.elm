module Wire exposing
    ( EndPoint(..)
    , WirePart(..)
    , averageDirection
    , clampT
    , detachedEndPoints
    , direction
    , endPoint
    , init
    , intersections
    , isEnd
    , isStart
    , length
    , moveT
    , nextAndPreviousPoints
    , nodePoints
    , partAtT
    , reverse
    , reverseT
    , split
    , splitNodes
    , startPoint
    , tDistance
    , tMax
    , tMin
    , tPoint
    , tSignedDistance
    , wireChipEndWorldPosition
    , wireChipEndWorldPosition_
    )

import Basics.Extra exposing (flip)
import Chip
import Circuit exposing (Chip, ChipEdgePoint, Wire, WireDirection(..), WiresAndChips)
import Direction2d exposing (Direction2d)
import Helper
import LineSegment2d exposing (LineSegment2d)
import List.Extra as List
import ListHelper as List
import Maybe.Extra as Maybe
import Point2d exposing (Point2d)
import Quantity exposing (Quantity)
import T exposing (T(..))
import Vector2d


init : Maybe ChipEdgePoint -> List Point2d -> Maybe ChipEdgePoint -> Wire
init start nodes end =
    { start = start
    , end = end
    , nodes = nodes
    }


clampT : WiresAndChips a -> Wire -> Quantity Float T -> Quantity Float T
clampT circuit wire t =
    Quantity.clamp tMin (tMax circuit wire) t


tMin : Quantity number T
tMin =
    Quantity.zero


tMax : WiresAndChips a -> Wire -> Quantity Float T
tMax circuit wire =
    nodePoints circuit wire |> List.length |> (+) -1 |> max 0 |> toFloat |> T.t


nodePoints : WiresAndChips a -> Wire -> List Point2d
nodePoints circuit wire =
    let
        start =
            wire.start |> Maybe.andThen (wireChipEndWorldPosition circuit)

        end =
            wire.end |> Maybe.andThen (wireChipEndWorldPosition circuit)
    in
    Maybe.toList start ++ wire.nodes ++ Maybe.toList end


wireChipEndWorldPosition : WiresAndChips a -> ChipEdgePoint -> Maybe Point2d
wireChipEndWorldPosition circuit { chipId, side, offset } =
    Circuit.getChip circuit chipId
        |> Maybe.map (\chip -> Chip.edgePointWorldPosition chip { side = side, offset = offset })


wireChipEndWorldPosition_ : WiresAndChips a -> ChipEdgePoint -> Maybe Point2d
wireChipEndWorldPosition_ circuit { chipId, side, offset } =
    Circuit.getChip circuit chipId
        |> Maybe.map (\chip -> Chip.edgePointWorldPosition chip { side = side, offset = offset })


moveT : WiresAndChips a -> Float -> Wire -> Quantity Float T -> Quantity Float T
moveT circuit worldSpeed wire t =
    let
        t1 =
            clampT circuit wire t

        nodes =
            nodePoints circuit wire

        moveOnLine : ( Point2d, Point2d ) -> ( Float, Quantity Float T ) -> Maybe ( Float, Quantity Float T )
        moveOnLine ( start, end ) ( speedLeft, Quantity.Quantity tStart ) =
            let
                distance =
                    Point2d.distanceFrom start end

                distanceLeft =
                    distance * (1 - Helper.frac tStart)
            in
            if speedLeft == 0 then
                Nothing

            else if distanceLeft <= speedLeft then
                Just ( speedLeft - distanceLeft, floor tStart + 1 |> toFloat |> T.t )

            else
                Just ( 0, tStart + speedLeft / distance |> T.t )
    in
    if worldSpeed > 0 then
        nodes
            |> List.drop (t1 |> Quantity.floor |> T.inT)
            |> List.pairwise
            |> List.foldFast moveOnLine ( worldSpeed, t1 )
            |> Tuple.second

    else if worldSpeed < 0 then
        let
            ( reverseNodes, reverseT_ ) =
                reverse_ nodes t1
        in
        reverseNodes
            |> List.drop (reverseT_ |> Quantity.floor |> T.inT)
            |> List.pairwise
            |> List.foldFast moveOnLine ( -worldSpeed, reverseT_ )
            |> Tuple.second
            |> reverse_ nodes
            |> Tuple.second

    else
        t1


{-| Distance you'd have to travel along a wire to get from the start to the end.
-}
length : WiresAndChips a -> Wire -> Float
length circuit wire =
    nodePoints circuit wire |> List.pairwise |> List.map (\( a, b ) -> Point2d.distanceFrom a b) |> List.sum


{-| Distance you'd have to travel along a wire to move from the first point to the second.
-}
tDistance : WiresAndChips a -> Quantity Float T -> Quantity Float T -> Wire -> Float
tDistance circuit tStart tEnd wire =
    let
        maxT =
            Quantity.max tStart tEnd

        minT =
            Quantity.min tStart tEnd

        minTFixed =
            if Quantity.floor minT == Quantity.floor maxT then
                let
                    minTFrac =
                        T.inT minT |> Helper.frac

                    maxTFrac =
                        T.inT maxT |> Helper.frac

                    ratio =
                        minTFrac / maxTFrac
                in
                if isNaN ratio || isInfinite ratio then
                    Quantity.floor minT |> Quantity.toFloatQuantity

                else
                    Quantity.floor minT |> Quantity.toFloatQuantity |> Quantity.plus (T.t ratio)

            else
                minT

        ( splitWire, _ ) =
            split circuit wire maxT
    in
    split circuit splitWire minTFixed |> Tuple.second |> length circuit


{-| Signed distance you'd have to travel along a wire to move from the first point to the second.
If tStart is less than tEnd then it's positive. Otherwise it's negative.
-}
tSignedDistance : WiresAndChips a -> Quantity Float T -> Quantity Float T -> Wire -> Float
tSignedDistance circuit tStart tEnd wire =
    let
        distance =
            tDistance circuit tStart tEnd wire
    in
    if tStart |> Quantity.lessThan tEnd then
        distance

    else
        -distance


reverse_ : List Point2d -> Quantity Float T -> ( List Point2d, Quantity Float T )
reverse_ nodes t =
    ( List.reverse nodes, List.length nodes - 1 |> toFloat |> T.t |> Quantity.plus (Quantity.negate t) )


reverse : Wire -> Wire
reverse wire =
    init wire.end (List.reverse wire.nodes) wire.start


reverseT : WiresAndChips a -> Wire -> Quantity Float T -> Quantity Float T
reverseT circuit wire t =
    reverse_ (nodePoints circuit wire) t |> Tuple.second


{-| Bisects a list of points a value t
-}
splitNodes : List Point2d -> Quantity Float T -> ( List Point2d, List Point2d )
splitNodes nodes (Quantity.Quantity t) =
    let
        startList =
            List.take (floor t + 1) nodes

        endList =
            List.drop (floor t + 1) nodes
    in
    if toFloat (floor t) == t then
        ( startList, List.drop (floor t) nodes )

    else
        case ( List.last startList, List.head endList ) of
            ( Just a, Just b ) ->
                let
                    splitPoint =
                        Point2d.interpolateFrom a b (Helper.frac t)
                in
                ( startList ++ [ splitPoint ], splitPoint :: endList )

            _ ->
                ( startList, endList )


split : WiresAndChips a -> Wire -> Quantity Float T -> ( Wire, Wire )
split circuit wire t =
    let
        ( startNodes, endNodes ) =
            nodePoints circuit wire |> flip splitNodes t

        startNodes_ =
            case wire.start of
                Just _ ->
                    List.drop 1 startNodes

                Nothing ->
                    startNodes

        endNodes_ =
            case wire.end of
                Just _ ->
                    List.dropLast endNodes

                Nothing ->
                    endNodes
    in
    ( init wire.start startNodes_ Nothing
    , init Nothing endNodes_ wire.end
    )


tPoint : WiresAndChips a -> Wire -> Quantity Float T -> Maybe Point2d
tPoint circuit wire ((Quantity.Quantity tValue) as t) =
    let
        t1 =
            clampT circuit wire t
    in
    case nextAndPreviousPoints circuit wire t1 of
        ( previous :: _, next :: _ ) ->
            Point2d.interpolateFrom previous next (Helper.frac tValue) |> Just

        ( previous :: _, [] ) ->
            Just previous

        ( [], next :: _ ) ->
            Just next

        ( [], [] ) ->
            Nothing


{-| Returns a tuple containing the points ahead and behind the given T value.
The behind points are reversed so that the nearest point is at the head of the list.
-}
nextAndPreviousPoints : WiresAndChips a -> Wire -> Quantity Float T -> ( List Point2d, List Point2d )
nextAndPreviousPoints circuit wire (Quantity.Quantity t) =
    let
        nodes =
            nodePoints circuit wire

        t1 =
            t |> floor |> (+) 1
    in
    ( List.take t1 nodes |> List.reverse, List.drop t1 nodes )


direction : WiresAndChips a -> Wire -> Quantity Float T -> WireDirection -> Maybe Direction2d
direction circuit wire t wireDirection =
    let
        ( wire2, t2 ) =
            case wireDirection of
                Forward ->
                    ( wire, t )

                Backward ->
                    ( reverse wire, reverseT circuit wire t )
    in
    case nextAndPreviousPoints circuit wire2 t2 of
        ( previous :: _, next :: _ ) ->
            Direction2d.from previous next

        ( prev2 :: prev1 :: _, [] ) ->
            Direction2d.from prev1 prev2

        ( [], next1 :: next2 :: _ ) ->
            Direction2d.from next1 next2

        _ ->
            Nothing


averageDirection : List Point2d -> Maybe Direction2d
averageDirection points =
    points
        |> List.pairwise
        |> List.map (\( start, end ) -> Vector2d.from start end)
        |> List.foldr Vector2d.sum Vector2d.zero
        |> Vector2d.lengthAndDirection
        |> Maybe.map Tuple.second


intersections : WiresAndChips a -> Wire -> Point2d -> Point2d -> List (Quantity Float T)
intersections circuit wire start end =
    let
        line =
            LineSegment2d.from start end
    in
    nodePoints circuit wire
        |> List.pairwise
        |> List.indexedMap Tuple.pair
        |> List.filter (\( _, ( a, b ) ) -> Point2d.distanceFrom a b |> (/=) 0)
        |> List.map (intersectT line >> Maybe.toList)
        |> List.concat


intersectT : LineSegment2d -> ( Int, ( Point2d, Point2d ) ) -> Maybe (Quantity Float T)
intersectT line ( index, ( a, b ) ) =
    LineSegment2d.from a b
        |> LineSegment2d.intersectionPoint line
        |> Maybe.map
            (Point2d.distanceFrom a
                >> flip (/) (Point2d.distanceFrom a b)
                >> (+) (toFloat index)
                >> T.t
            )


type WirePart
    = WireStart
    | WireEnd
    | WireMiddle


partAtT : WiresAndChips a -> Wire -> Quantity Float T -> WirePart
partAtT circuit wire t =
    if isStart circuit wire t then
        WireStart

    else if isEnd circuit wire t then
        WireEnd

    else
        WireMiddle


{-| Is a given T value at the start of a wire?
-}
isStart : WiresAndChips a -> Wire -> Quantity Float T -> Bool
isStart circuit wire t =
    let
        t1 =
            clampT circuit wire t
    in
    t1 == tMin


{-| Is a given T value at the end of a wire?
-}
isEnd : WiresAndChips a -> Wire -> Quantity Float T -> Bool
isEnd circuit wire t =
    let
        t1 =
            clampT circuit wire t
    in
    t1 == tMax circuit wire


{-| Get the start and end point if they are not attached to a chip.
-}
detachedEndPoints : Wire -> { start : Maybe Point2d, end : Maybe Point2d }
detachedEndPoints wire =
    { start =
        case ( wire.start, wire.nodes ) of
            ( Nothing, head :: _ ) ->
                Just head

            _ ->
                Nothing
    , end =
        case ( wire.end, List.reverse wire.nodes ) of
            ( Nothing, head :: _ ) ->
                Just head

            _ ->
                Nothing
    }


type EndPoint
    = DetachedEndPoint Point2d
    | ChipEndPoint ChipEdgePoint


startPoint : Wire -> EndPoint
startPoint wire =
    case wire.start of
        Just chipEdgePoint ->
            ChipEndPoint chipEdgePoint

        Nothing ->
            wire.nodes |> List.head |> Maybe.withDefault Point2d.origin |> DetachedEndPoint


endPoint : Wire -> EndPoint
endPoint wire =
    case wire.end of
        Just chipEdgePoint ->
            ChipEndPoint chipEdgePoint

        Nothing ->
            wire.nodes |> List.reverse |> List.head |> Maybe.withDefault Point2d.origin |> DetachedEndPoint
