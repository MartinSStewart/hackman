module Draw exposing
    ( arc
    , arrow
    , circle
    , circleWithOutline
    , lineStrip
    , opacity
    , polygon
    , polygonWithOutline
    , rectangle
    , rectangleWithOutline
    , sprite
    )

import Arc2d exposing (Arc2d)
import BoundingBox2d exposing (BoundingBox2d)
import Circle2d
import Color exposing (Color)
import Geometry.Svg as Svg
import Html.Attributes
import Mesh
import Point2d exposing (Point2d)
import Polygon2d exposing (Polygon2d)
import Polyline2d exposing (Polyline2d)
import Sprite exposing (Sprite)
import Svg exposing (Svg)
import Svg.Attributes
import Vector2d exposing (Vector2d)


circleWithOutline : Color -> Color -> Float -> Float -> Point2d -> Svg msg
circleWithOutline fillColor strokeColor radius strokeThickness position =
    position
        |> Circle2d.withRadius radius
        |> Svg.circle2d
            [ Svg.Attributes.stroke <| Color.toCssString <| strokeColor
            , Svg.Attributes.fill <| Color.toCssString <| fillColor
            , Svg.Attributes.strokeWidth <| String.fromFloat <| strokeThickness
            ]


circle : Color -> Float -> Point2d -> Svg msg
circle fillColor radius position =
    position
        |> Circle2d.withRadius radius
        |> Svg.circle2d
            [ Svg.Attributes.fill <| Color.toCssString <| fillColor
            , Svg.Attributes.overflow "visible"
            ]


arc : Color -> Float -> Arc2d -> Svg msg
arc color thickness arc_ =
    Svg.arc2d
        [ Svg.Attributes.stroke <| Color.toCssString <| color
        , Svg.Attributes.fill <| Color.toCssString <| Color.rgba 0 0 0 0
        , Svg.Attributes.strokeWidth <| String.fromFloat <| thickness
        , Svg.Attributes.overflow "visible"
        ]
        arc_


lineStrip : Color -> Float -> List Point2d -> Svg msg
lineStrip color thickness points =
    points
        |> Polyline2d.fromVertices
        |> Svg.polyline2d
            [ Svg.Attributes.stroke <| Color.toCssString <| color
            , Svg.Attributes.fill "none"
            , Svg.Attributes.strokeWidth <| String.fromFloat <| thickness
            , Svg.Attributes.overflow "visible"
            ]


rectangle : Color -> BoundingBox2d -> Svg msg
rectangle color box =
    Svg.boundingBox2d [ Svg.Attributes.fill <| Color.toCssString <| color ] box


rectangleWithOutline : Color -> Color -> Float -> BoundingBox2d -> Svg msg
rectangleWithOutline color strokeColor thickness box =
    Svg.boundingBox2d
        [ Svg.Attributes.fill <| Color.toCssString <| color
        , Svg.Attributes.stroke <| Color.toCssString <| strokeColor
        , Svg.Attributes.strokeWidth <| String.fromFloat <| thickness
        ]
        box


{-| Draw a polygon
-}
polygon : Color -> Polygon2d -> Svg msg
polygon color polygon_ =
    Svg.polygon2d
        [ Svg.Attributes.fill <| Color.toCssString <| color ]
        polygon_


polygonWithOutline : Color -> Color -> Float -> Polygon2d -> Svg msg
polygonWithOutline color strokeColor thickness polygon_ =
    Svg.polygon2d
        [ Svg.Attributes.fill <| Color.toCssString <| color
        , Svg.Attributes.stroke <| Color.toCssString <| strokeColor
        , Svg.Attributes.strokeWidth <| String.fromFloat <| thickness
        ]
        polygon_


arrow : Color -> Float -> Float -> Point2d -> Point2d -> Svg msg
arrow color innerDiameter outerDiameter start end =
    Mesh.arrow innerDiameter outerDiameter start end |> polygon color


sprite : Float -> Sprite -> Point2d -> Svg msg
sprite scale sprite_ position =
    let
        ( x, y ) =
            Point2d.coordinates position
    in
    Svg.image
        [ Svg.Attributes.xlinkHref sprite_.image
        , Svg.Attributes.x <| String.fromFloat x
        , Svg.Attributes.y <| String.fromFloat y
        , Html.Attributes.id "pixel-art"
        , Html.Attributes.width sprite_.size.width
        , Html.Attributes.height sprite_.size.height
        ]
        []
        |> Svg.scaleAbout position scale


opacity : Float -> Svg msg -> Svg msg
opacity opacity_ svg =
    Svg.g [ Svg.Attributes.opacity <| String.fromFloat opacity_ ] [ svg ]
