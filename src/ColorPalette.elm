module ColorPalette exposing (ColorPalette, ColorSelection(..), almostEqual, codec, defaultColors, getColor, setColor)

import Codec.Serialize as Codec exposing (Codec)
import Color exposing (Color)


type alias ColorPalette =
    { background : Color
    , wireMiddle : Color
    , wireEdge : Color
    , chip : Color
    , chipLegs : Color
    , chipHacked : Color
    , passiveAgent : Color
    , chaserAgent : Color
    , player : Color
    , playerDamage : Color
    , movementArrows : Color
    , mapWire : Color
    , mapChip : Color
    , mapChipHacked : Color
    }


defaultColors : ColorPalette
defaultColors =
    { background = Color.rgb255 79 187 107
    , wireMiddle = Color.rgb (197 / 255) (206 / 255) (137 / 255)
    , wireEdge = Color.rgb (7 / 255) (62 / 255) (43 / 255)
    , chip = Color.rgb 0.3 0.3 0.3
    , chipLegs = Color.rgb 0.8 0.8 0.8
    , chipHacked = Color.rgb 0 1 0
    , passiveAgent = Color.white
    , chaserAgent = Color.red
    , player = Color.yellow
    , playerDamage = Color.rgb (237 / 400) (212 / 400) 0
    , movementArrows = Color.rgb 1 1 0
    , mapWire = Color.rgb 1 1 0
    , mapChip = Color.rgb 1 0 0
    , mapChipHacked = Color.rgb 0 1 0
    }


almostEqual : ColorPalette -> ColorPalette -> Bool
almostEqual palette0 palette1 =
    (Codec.toBytes colorPalletteV0Codec palette0 |> Codec.fromBytes colorPalletteV0Codec)
        == (Codec.toBytes colorPalletteV0Codec palette1 |> Codec.fromBytes colorPalletteV0Codec)


type ColorPaletteVersion
    = ColorPaletteV0 ColorPalette


codec : Codec ColorPalette
codec =
    Codec.customType
        (\v0Encoder value ->
            case value of
                ColorPaletteV0 value_ ->
                    v0Encoder value_
        )
        |> Codec.variant1 ColorPaletteV0 colorPalletteV0Codec
        |> Codec.finishCustomType
        |> Codec.map
            (\value ->
                case value of
                    ColorPaletteV0 value_ ->
                        value_
            )
            ColorPaletteV0


colorPalletteV0Codec : Codec ColorPalette
colorPalletteV0Codec =
    Codec.record ColorPalette
        |> Codec.field .background colorCodec
        |> Codec.field .wireMiddle colorCodec
        |> Codec.field .wireEdge colorCodec
        |> Codec.field .chip colorCodec
        |> Codec.field .chipLegs colorCodec
        |> Codec.field .chipHacked colorCodec
        |> Codec.field .passiveAgent colorCodec
        |> Codec.field .chaserAgent colorCodec
        |> Codec.field .player colorCodec
        |> Codec.field .playerDamage colorCodec
        |> Codec.field .movementArrows colorCodec
        |> Codec.field .mapWire colorCodec
        |> Codec.field .mapChip colorCodec
        |> Codec.field .mapChipHacked colorCodec
        |> Codec.finishRecord


colorCodec : Codec Color
colorCodec =
    Codec.record Color.rgb
        |> Codec.field (Color.toRgba >> .red) colorChannelCodec
        |> Codec.field (Color.toRgba >> .green) colorChannelCodec
        |> Codec.field (Color.toRgba >> .blue) colorChannelCodec
        |> Codec.finishRecord


colorChannelCodec : Codec Float
colorChannelCodec =
    Codec.byte |> Codec.map (toFloat >> (*) (1 / 255)) ((*) 255 >> round >> clamp 0 255)


type ColorSelection
    = PaletteBackground
    | PaletteWireMiddle
    | PaletteWireEdge
    | PaletteChip
    | PaletteChipLegs
    | PaletteChipHacked
    | PalettePassiveAgent
    | PaletteChaserAgent
    | PalettePlayer
    | PalettePlayerDamage
    | PaletteMovementArrows
    | PaletteMapWire
    | PaletteMapChip
    | PaletteMapChipHacked


getColor : ColorSelection -> (ColorPalette -> Color)
getColor selection =
    case selection of
        PaletteBackground ->
            .background

        PaletteWireMiddle ->
            .wireMiddle

        PaletteWireEdge ->
            .wireEdge

        PaletteChip ->
            .chip

        PaletteChipLegs ->
            .chipLegs

        PaletteChipHacked ->
            .chipHacked

        PalettePassiveAgent ->
            .passiveAgent

        PaletteChaserAgent ->
            .chaserAgent

        PalettePlayer ->
            .player

        PalettePlayerDamage ->
            .playerDamage

        PaletteMovementArrows ->
            .movementArrows

        PaletteMapWire ->
            .mapWire

        PaletteMapChip ->
            .mapChip

        PaletteMapChipHacked ->
            .mapChipHacked


setColor : ColorSelection -> (Color -> ColorPalette -> ColorPalette)
setColor selection =
    \color a ->
        case selection of
            PaletteBackground ->
                { a | background = color }

            PaletteWireMiddle ->
                { a | wireMiddle = color }

            PaletteWireEdge ->
                { a | wireEdge = color }

            PaletteChip ->
                { a | chip = color }

            PaletteChipLegs ->
                { a | chipLegs = color }

            PaletteChipHacked ->
                { a | chipHacked = color }

            PalettePassiveAgent ->
                { a | passiveAgent = color }

            PaletteChaserAgent ->
                { a | chaserAgent = color }

            PalettePlayer ->
                { a | player = color }

            PalettePlayerDamage ->
                { a | playerDamage = color }

            PaletteMovementArrows ->
                { a | movementArrows = color }

            PaletteMapWire ->
                { a | mapWire = color }

            PaletteMapChip ->
                { a | mapChip = color }

            PaletteMapChipHacked ->
                { a | mapChipHacked = color }
