module MenuState exposing
    ( Game(..)
    , MainMenuIntro(..)
    , MainMenuPage(..)
    , MainMenu_
    , MsgConfig
    , StageSelectButton(..)
    , StageSelectPage_
    , Stage_
    , TitlePageButton(..)
    , TitlePage_
    , gotoMainMenu
    , gotoStageSelect
    , init
    , initStage
    , menuIsTransitioning
    , menuTransitionLength
    , pressStageTile
    , resetStageSelectSelectedButton
    , setBackgroundGameplay
    , setColorSelection
    , setShowTrailer
    , setStageSelectButtonSelection
    , setTitlePageButtonSelection
    , step
    , transitionMenuPage
    )

import Basics.Extra exposing (flip)
import Circuit exposing (Circuit)
import ColorPalette exposing (ColorPalette, ColorSelection)
import ColorSettingsPage
import Either exposing (Either)
import HighscoreTable
import InGame
import Input
import Jump
import Keyboard exposing (Key)
import KeyboardHelper as Keyboard
import List.Extra as List
import List.Nonempty
import Mission exposing (Mission)
import Point2d exposing (Point2d)
import Quantity exposing (Quantity)
import RealTime exposing (RealTime)
import Replay exposing (CompleteReplay)
import ReplayViewer
import Sound
import SpriteSheet exposing (SpriteSheet)
import StageData exposing (CircuitData, StageId)
import Vector2i exposing (Vector2i)


type Game
    = MainMenu MainMenu_
    | Stage Stage_
    | LevelEditor
    | ReplayViewer ReplayViewer.Model


type alias MainMenu_ =
    { backgroundGameplay : Maybe BackgroundGameplay
    , menuPage : MainMenuPage
    , transitionStart : Maybe ( Quantity Float RealTime, MainMenuPage )
    , showTrailer : Bool
    , showReplayParseError : Bool
    , mainMenuIntro : MainMenuIntro
    }


type TitlePageButton
    = StartButton
    | EditorButton
    | SettingsButton


type StageSelectButton
    = StageButton Int
    | StageSelectBackButton
    | StageSelectStartButton


setTitlePageButtonSelection : TitlePageButton -> MainMenu_ -> MainMenu_
setTitlePageButtonSelection selectedButton mainMenu =
    case mainMenu.menuPage of
        TitlePage titlePage ->
            setMenuPage (TitlePage { titlePage | selectedButton = selectedButton }) mainMenu

        _ ->
            mainMenu


setStageSelectButtonSelection : StageSelectButton -> MainMenu_ -> MainMenu_
setStageSelectButtonSelection stageSelectButton mainMenu =
    case mainMenu.menuPage of
        StageSelectPage stageSelect ->
            setMenuPage (StageSelectPage { stageSelect | selectedButton = stageSelectButton }) mainMenu

        _ ->
            mainMenu


resetStageSelectSelectedButton : Config a -> MainMenu_ -> MainMenu_
resetStageSelectSelectedButton config mainMenu =
    case mainMenu.menuPage of
        StageSelectPage stageSelect ->
            setMenuPage
                (StageSelectPage
                    { stageSelect
                        | selectedButton =
                            StageData.playableStageIndex config.stageSelectId
                                |> Maybe.withDefault 0
                                |> StageButton
                    }
                )
                mainMenu

        _ ->
            mainMenu


type alias BackgroundGameplay =
    { inGameModel : InGame.InGameModel
    , replay : Maybe CompleteReplay
    }


type MainMenuIntro
    = MainMenuIntro (Quantity Float RealTime)
    | MainMenuIntroOver


type MainMenuPage
    = SettingsPage
    | TitlePage TitlePage_
    | StageSelectPage StageSelectPage_
    | ColorSettingsPage ColorSettingsPage.Model


type alias TitlePage_ =
    { selectedButton : TitlePageButton }


type alias StageSelectPage_ =
    { stageTilePressTime : Maybe (Quantity Float RealTime)
    , selectedButton : StageSelectButton
    }


type alias Stage_ =
    { stageId : StageId
    , inGameModel : InGame.InGameModel
    }


setColorSelection : ColorSelection -> MainMenu_ -> MainMenu_
setColorSelection colorSelection mainMenu =
    case mainMenu.menuPage of
        ColorSettingsPage colorSettings ->
            { mainMenu
                | menuPage =
                    ColorSettingsPage
                        { colorSettings
                            | colorSelection =
                                if colorSettings.colorSelection == Just colorSelection then
                                    Nothing

                                else
                                    Just colorSelection
                        }
            }

        _ ->
            mainMenu


initStage : Quantity Float RealTime -> StageId -> Mission -> Circuit -> Jump.LookupTable -> Stage_
initStage time stageId mission circuit lookupTable =
    { stageId = stageId
    , inGameModel =
        InGame.init
            True
            (StageData.playableStageOrder |> List.Nonempty.toList |> List.findIndex ((==) stageId) |> Maybe.withDefault 0)
            time
            mission
            { circuit = circuit, lookupTable = Just lookupTable }
    }


setMenuPage : MainMenuPage -> MainMenu_ -> MainMenu_
setMenuPage menuPage mainMenu =
    { mainMenu | menuPage = menuPage }


init : Quantity Float RealTime -> MainMenu_
init introTime =
    case StageData.menuReplay of
        StageData.ReplayData replay lookupTable ->
            { backgroundGameplay = Nothing
            , menuPage = TitlePage { selectedButton = StartButton }
            , transitionStart = Nothing
            , showTrailer = False
            , showReplayParseError = False
            , mainMenuIntro = MainMenuIntro introTime
            }
                |> setBackgroundGameplay (Either.Left replay) lookupTable True

        StageData.FailedToParseReplay _ ->
            { backgroundGameplay = Nothing
            , menuPage = TitlePage { selectedButton = StartButton }
            , transitionStart = Nothing
            , showTrailer = False
            , showReplayParseError = False
            , mainMenuIntro = MainMenuIntro introTime
            }


mainMenuButtons =
    [ StartButton
    , EditorButton
    , SettingsButton
    ]


buttonToIndex : TitlePageButton -> Int
buttonToIndex button =
    List.findIndex ((==) button) mainMenuButtons |> Maybe.withDefault 0


indexToButton : Int -> TitlePageButton
indexToButton =
    modBy (List.length mainMenuButtons) >> flip List.getAt mainMenuButtons >> Maybe.withDefault StartButton


type alias MsgConfig msg =
    { noOp : msg
    , pressedStartButton : msg
    , pressedEditorButton : msg
    , pressedSettingsButton : msg
    , keyPressedStageSelectBackButton : msg
    , keyPressedStageSelectStartButton : msg
    , keyPressedStageSelectTile : { stageId : StageId } -> msg
    , selectedStageSelecTileWithArrowKeys : { stageId : StageId } -> msg
    , pressedSettingsBackButton : msg
    , pressedColorSettingsBackButton : msg
    }


type alias Config a =
    { a
        | windowSize : Vector2i
        , time : Quantity Float RealTime
        , stepDelta : Quantity Float RealTime
        , mouseDown : Bool
        , previousMouseDown : Bool
        , mousePosition : Point2d
        , previousMousePosition : Point2d
        , spriteSheet : SpriteSheet
        , sounds : Sound.Model
        , antialias : Bool
        , highscoreState : HighscoreTable.Model
        , colorPalette : ColorPalette
        , keys : List Keyboard.Key
        , previousKeys : List Keyboard.Key
        , stageSelectId : StageId
    }


step : MsgConfig msg -> Config a -> MainMenu_ -> ( MainMenu_, msg )
step msgConfig config mainMenu =
    let
        backgroundGameplay =
            case mainMenu.backgroundGameplay of
                Just ({ inGameModel, replay } as backgroundGameplay_) ->
                    let
                        input =
                            replay
                                |> Maybe.map (flip Replay.getInput inGameModel.replayable.coreState)
                                |> Maybe.withDefault Input.noInput

                        step_ =
                            InGame.step
                                InGame.noOpMsgConfig
                                config
                                (replay /= Nothing)
                                input
                                inGameModel
                    in
                    Just { backgroundGameplay_ | inGameModel = step_.model }

                Nothing ->
                    mainMenu.backgroundGameplay

        ( mainMenu2, msg ) =
            if mainMenu.showTrailer then
                if Keyboard.isPressed config Keyboard.Escape then
                    ( { mainMenu | showTrailer = False }, msgConfig.noOp )

                else
                    ( mainMenu, msgConfig.noOp )

            else
                case mainMenu.menuPage of
                    SettingsPage ->
                        if Keyboard.isPressed config Keyboard.Escape then
                            ( mainMenu, msgConfig.pressedSettingsBackButton )

                        else
                            ( mainMenu, msgConfig.noOp )

                    TitlePage { selectedButton } ->
                        if Keyboard.isPressed config Keyboard.ArrowDown then
                            ( { selectedButton = buttonToIndex selectedButton + 1 |> indexToButton } |> TitlePage |> flip setMenuPage mainMenu
                            , msgConfig.noOp
                            )

                        else if Keyboard.isPressed config Keyboard.ArrowUp then
                            ( { selectedButton = buttonToIndex selectedButton - 1 |> indexToButton } |> TitlePage |> flip setMenuPage mainMenu
                            , msgConfig.noOp
                            )

                        else if Keyboard.buttonPressed config then
                            case selectedButton of
                                StartButton ->
                                    ( mainMenu, msgConfig.pressedStartButton )

                                EditorButton ->
                                    ( mainMenu, msgConfig.pressedEditorButton )

                                SettingsButton ->
                                    ( mainMenu, msgConfig.pressedSettingsButton )

                        else
                            ( mainMenu, msgConfig.noOp )

                    StageSelectPage stageSelectPage ->
                        if Keyboard.isPressed config Keyboard.Escape then
                            ( mainMenu, msgConfig.keyPressedStageSelectBackButton )

                        else if Keyboard.buttonPressed config then
                            case stageSelectPage.selectedButton of
                                StageButton stageIndex ->
                                    case stageIndexToStageId stageIndex of
                                        Just stageId ->
                                            ( mainMenu, msgConfig.keyPressedStageSelectTile { stageId = stageId } )

                                        Nothing ->
                                            ( mainMenu, msgConfig.noOp )

                                StageSelectBackButton ->
                                    ( mainMenu, msgConfig.keyPressedStageSelectBackButton )

                                StageSelectStartButton ->
                                    ( mainMenu, msgConfig.keyPressedStageSelectStartButton )

                        else
                            let
                                newSelectedButton =
                                    stageSelectArrowKeyMovement config stageSelectPage.selectedButton
                            in
                            if newSelectedButton == stageSelectPage.selectedButton then
                                ( mainMenu, msgConfig.noOp )

                            else
                                ( { stageSelectPage | selectedButton = newSelectedButton }
                                    |> StageSelectPage
                                    |> flip setMenuPage mainMenu
                                , case newSelectedButton of
                                    StageButton stageIndex ->
                                        msgConfig.selectedStageSelecTileWithArrowKeys
                                            { stageId = stageIndexToStageId stageIndex |> Maybe.withDefault StageData.tutorialId }

                                    _ ->
                                        msgConfig.noOp
                                )

                    ColorSettingsPage _ ->
                        if Keyboard.isPressed config Keyboard.Escape then
                            ( mainMenu, msgConfig.pressedColorSettingsBackButton )

                        else
                            ( mainMenu, msgConfig.noOp )
    in
    ( { mainMenu2 | backgroundGameplay = backgroundGameplay }, msg )


stageIndexToStageId : Int -> Maybe StageId
stageIndexToStageId stageIndex =
    StageData.playableStageOrder |> List.Nonempty.toList |> List.getAt stageIndex


stageSelectArrowKeyMovement : Config a -> StageSelectButton -> StageSelectButton
stageSelectArrowKeyMovement config selectedButton =
    let
        playableStages =
            StageData.playableStageOrder
                |> List.Nonempty.toList

        stageAvailable stageId =
            HighscoreTable.stageStatus stageId config.highscoreState /= HighscoreTable.StageLocked
    in
    if Keyboard.isPressed config Keyboard.ArrowUp then
        case selectedButton of
            StageButton stageIndex ->
                playableStages
                    |> List.indexedMap Tuple.pair
                    |> List.reverse
                    |> List.find
                        (\( index, stageId ) ->
                            (index < stageIndex)
                                && (modBy StageData.stagesPerRow (stageIndex - index) == 0)
                                && stageAvailable stageId
                        )
                    |> Maybe.map Tuple.first
                    |> Maybe.withDefault stageIndex
                    |> StageButton

            _ ->
                config.stageSelectId |> StageData.playableStageIndex |> Maybe.withDefault 0 |> StageButton

    else if Keyboard.isPressed config Keyboard.ArrowDown then
        case selectedButton of
            StageButton stageIndex ->
                playableStages
                    |> List.indexedMap Tuple.pair
                    |> List.findIndex
                        (\( index, stageId ) ->
                            (index > stageIndex)
                                && (modBy StageData.stagesPerRow (stageIndex - index) == 0)
                                && stageAvailable stageId
                        )
                    |> Maybe.map StageButton
                    |> Maybe.withDefault StageSelectStartButton

            StageSelectBackButton ->
                StageSelectBackButton

            StageSelectStartButton ->
                StageSelectStartButton

    else if Keyboard.isPressed config Keyboard.ArrowLeft then
        case selectedButton of
            StageButton stageIndex ->
                StageData.playableStageRow (stageIndex // StageData.stagesPerRow)
                    |> List.reverse
                    |> List.find
                        (\( index, stageId ) ->
                            (index < stageIndex)
                                && stageAvailable stageId
                        )
                    |> Maybe.map Tuple.first
                    |> Maybe.withDefault stageIndex
                    |> StageButton

            StageSelectBackButton ->
                StageSelectBackButton

            StageSelectStartButton ->
                StageSelectBackButton

    else if Keyboard.isPressed config Keyboard.ArrowRight then
        case selectedButton of
            StageButton stageIndex ->
                StageData.playableStageRow (stageIndex // StageData.stagesPerRow)
                    |> List.find
                        (\( index, stageId ) ->
                            (index > stageIndex)
                                && stageAvailable stageId
                        )
                    |> Maybe.map Tuple.first
                    |> Maybe.withDefault stageIndex
                    |> StageButton

            StageSelectBackButton ->
                StageSelectStartButton

            StageSelectStartButton ->
                StageSelectStartButton

    else
        selectedButton


gotoMainMenu : Either CompleteReplay Circuit -> Jump.LookupTable -> MainMenuPage -> Bool -> Game
gotoMainMenu replayOrCircuit lookupTable menuPage firstMenuIntro =
    { backgroundGameplay = Nothing
    , menuPage = menuPage
    , transitionStart = Nothing
    , showTrailer = False
    , showReplayParseError = False
    , mainMenuIntro = MainMenuIntroOver
    }
        |> setBackgroundGameplay replayOrCircuit lookupTable firstMenuIntro
        |> MainMenu


pressStageTile : Int -> Quantity Float RealTime -> MainMenu_ -> MainMenu_
pressStageTile stageIndex time mainMenu =
    { mainMenu | menuPage = StageSelectPage { stageTilePressTime = Just time, selectedButton = StageButton stageIndex } }


gotoStageSelect : Int -> Either CompleteReplay Circuit -> Jump.LookupTable -> Bool -> Game
gotoStageSelect stageIndex replayOrCircuit lookupTable firstMenuIntro =
    gotoMainMenu
        replayOrCircuit
        lookupTable
        (StageSelectPage { stageTilePressTime = Nothing, selectedButton = StageButton stageIndex })
        firstMenuIntro


setBackgroundGameplay : Either CompleteReplay Circuit -> Jump.LookupTable -> Bool -> MainMenu_ -> MainMenu_
setBackgroundGameplay replayOrCircuit lookupTable firstMenuIntro mainMenu =
    let
        circuit =
            case replayOrCircuit of
                Either.Left replay ->
                    Replay.getInitialState replay |> .circuit

                Either.Right circuit_ ->
                    circuit_

        backgroundGameplay =
            InGame.init
                False
                0
                Quantity.zero
                (Mission.menuMissionInit firstMenuIntro circuit)
                { circuit = circuit
                , lookupTable = Just lookupTable
                }
    in
    { mainMenu | backgroundGameplay = Just { inGameModel = backgroundGameplay, replay = Either.leftToMaybe replayOrCircuit } }


transitionMenuPage : MainMenuPage -> Quantity Float RealTime -> MainMenu_ -> MainMenu_
transitionMenuPage menuPage time mainMenu =
    { mainMenu
        | menuPage = menuPage
        , transitionStart = Just ( time, mainMenu.menuPage )
    }


setShowTrailer : Bool -> MainMenu_ -> MainMenu_
setShowTrailer showTrailer mainMenu =
    { mainMenu | showTrailer = showTrailer, mainMenuIntro = MainMenuIntroOver }


menuIsTransitioning : Quantity Float RealTime -> MainMenu_ -> Bool
menuIsTransitioning time mainMenu =
    case mainMenu.transitionStart of
        Just ( startTime, _ ) ->
            time |> Quantity.lessThan (startTime |> Quantity.plus menuTransitionLength)

        Nothing ->
            False


menuTransitionLength : Quantity Float RealTime
menuTransitionLength =
    RealTime.seconds 0.1
