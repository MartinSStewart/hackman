module Chip exposing
    ( WireConnection
    , connectedWires
    , edgeBounds
    , edgePointLocalPosition
    , edgePointWorldPosition
    , hackPosition
    , init
    , isAlmostHacked
    , isHacked
    , localEdgePoints
    , localToWorld
    , localToWorld_
    , tileBounds
    , tileCount
    , tilesHacked
    , totalTilesNeededToHack
    , wireChipEndLocalPosition
    , worldBounds
    , worldToLocal
    )

import BoundingBox2d exposing (BoundingBox2d)
import BoundingBox2i exposing (BoundingBox2i)
import Cardinal exposing (Cardinal(..))
import Circuit exposing (Chip, ChipEdgePoint, EdgePoint, WiresAndChips)
import Helper
import Point2d exposing (Point2d)
import Point2dEx as Point2d
import Point2i exposing (Point2i)
import Set exposing (Set)
import Vector2d
import Vector2i exposing (Vector2i)


type alias WireConnection =
    { wireId : Circuit.WireId
    , edgePoint : EdgePoint
    , isWireStart : Bool
    }


init : Point2d -> Vector2i -> Set ( Int, Int ) -> Chip
init position size hackedPoints =
    { position =
        Point2d.map2
            (\a b -> Helper.ifElse (b < 0) (a + b) a)
            position
            (Vector2i.toPoint size |> Point2i.toPoint2d)
    , size = Vector2i.map (abs >> max 2) size
    , hackedPoints = hackedPoints
    }


connectedWires : WiresAndChips a -> Circuit.ChipId -> List WireConnection
connectedWires circuit chipId_ =
    Circuit.getWires circuit
        |> List.concatMap
            (\( wireId, wire ) ->
                let
                    mapValue : Bool -> Maybe ChipEdgePoint -> Maybe WireConnection
                    mapValue isWireStart wireEnd =
                        case wireEnd of
                            Just { chipId, side, offset } ->
                                if chipId_ == chipId then
                                    Just
                                        { wireId = wireId
                                        , edgePoint = { side = side, offset = offset }
                                        , isWireStart = isWireStart
                                        }

                                else
                                    Nothing

                            Nothing ->
                                Nothing
                in
                [ mapValue True wire.start, mapValue False wire.end ] |> List.filterMap identity
            )


edgePointLocalPosition : Chip -> EdgePoint -> Point2i
edgePointLocalPosition chip { side, offset } =
    let
        { width, height } =
            chip.size
    in
    case side of
        Left ->
            { x = 0, y = Helper.between 0 height offset }

        Right ->
            { x = width, y = Helper.between 0 height offset }

        Top ->
            { x = Helper.between 0 width offset, y = 0 }

        Bottom ->
            { x = Helper.between 0 width offset, y = height }


edgePointWorldPosition : Chip -> EdgePoint -> Point2d
edgePointWorldPosition chip edgePoint =
    edgePointLocalPosition chip edgePoint |> localToWorld chip


localToWorld : Chip -> Point2i -> Point2d
localToWorld chip localPosition =
    Point2d.translateBy (Vector2i.from Point2i.origin localPosition |> Vector2i.toVector2d) chip.position


localToWorld_ : Chip -> Point2d -> Point2d
localToWorld_ chip localPosition =
    Point2d.translateBy (chip.position |> Vector2d.from Point2d.origin) localPosition


worldToLocal : Chip -> Point2d -> Point2d
worldToLocal chip worldPoint =
    Point2d.translateBy (chip.position |> Vector2d.from Point2d.origin |> Vector2d.scaleBy -1) worldPoint


localEdgePoints : Chip -> List EdgePoint
localEdgePoints chip =
    let
        { width, height } =
            chip.size

        row =
            if width > 0 then
                List.range 1 (width - 1)

            else
                List.range (width + 1) -1

        column =
            if height > 0 then
                List.range 1 (height - 1)

            else
                List.range (height + 1) -1
    in
    [ List.map (EdgePoint Top) row
    , List.map (EdgePoint Bottom) row
    , List.map (EdgePoint Left) column
    , List.map (EdgePoint Right) column
    ]
        |> List.concat


wireChipEndLocalPosition : WiresAndChips a -> ChipEdgePoint -> Maybe Point2i
wireChipEndLocalPosition circuit { chipId, side, offset } =
    Circuit.getChip circuit chipId
        |> Maybe.map
            (\chip ->
                edgePointLocalPosition chip { side = side, offset = offset }
            )


ratioNeededToHack : Float
ratioNeededToHack =
    0.5


ratioNeededToAlmostHack =
    0.4


hackRatio : Chip -> Float
hackRatio chip =
    let
        hackedPoints =
            Set.size chip.hackedPoints |> toFloat

        area =
            tileCount chip
    in
    if area == 0 then
        0

    else
        hackedPoints / toFloat area


totalTilesNeededToHack : Chip -> Int
totalTilesNeededToHack chip =
    tileCount chip |> toFloat |> (*) ratioNeededToHack |> ceiling


tilesHacked : Chip -> Int
tilesHacked chip =
    Set.size chip.hackedPoints


tileCount chip =
    (chip.size.width - 1) * (chip.size.height - 1)


isHacked : Chip -> Bool
isHacked chip =
    hackRatio chip >= ratioNeededToHack


isAlmostHacked : Chip -> Bool
isAlmostHacked chip =
    hackRatio chip
        < ratioNeededToHack
        && hackRatio chip
        >= ratioNeededToAlmostHack


hackPosition : Point2i -> Chip -> Chip
hackPosition position chip =
    { chip
        | hackedPoints =
            if BoundingBox2i.contains position (tileBounds chip) then
                Set.insert (Point2i.tuple position) chip.hackedPoints

            else
                chip.hackedPoints
    }


{-| Boundary in local coordinates for hackable tiles.
-}
tileBounds : Chip -> BoundingBox2i
tileBounds chip =
    Vector2i.sub chip.size { width = 1, height = 1 }
        |> Vector2i.toPoint
        |> BoundingBox2i.from { x = 1, y = 1 }


{-| Boundary in local coordinates for the edge of the chip.
-}
edgeBounds : Chip -> BoundingBox2i
edgeBounds chip =
    chip.size
        |> Vector2i.toPoint
        |> BoundingBox2i.from Point2i.origin


{-| Boundary in world coordinates for the edge of the chip.
-}
worldBounds : Chip -> BoundingBox2d
worldBounds chip =
    let
        offset =
            chip.position |> Vector2d.from Point2d.origin
    in
    edgeBounds chip |> BoundingBox2i.toBoundingBox2d |> BoundingBox2d.translateBy offset
