module BoundingBox2i exposing (BoundingBox2i(..), area, contains, dimensions, extrema, from, fromExtrema, maxX, maxY, minX, minY, singleton, toBoundingBox2d)

import BoundingBox2d exposing (BoundingBox2d)
import Point2i exposing (Point2i)


type BoundingBox2i
    = BoundingBox2i { minX : Int, maxX : Int, minY : Int, maxY : Int }


{-| Construct a bounding box from its minimum and maximum X and Y values:

    exampleBox =
        BoundingBox2i.fromExtrema
            { minX = 3
            , maxX = 8
            , minY = 2
            , maxY = 6
            }

If the minimum and maximum values are provided in the wrong order (for example
if <code>minX&nbsp;>&nbsp;maxX</code>), then they will be swapped so that the
resulting bounding box is valid.

-}
fromExtrema : { minX : Int, maxX : Int, minY : Int, maxY : Int } -> BoundingBox2i
fromExtrema extrema_ =
    if extrema_.minX <= extrema_.maxX && extrema_.minY <= extrema_.maxY then
        BoundingBox2i extrema_

    else
        BoundingBox2i
            { minX = min extrema_.minX extrema_.maxX
            , maxX = max extrema_.minX extrema_.maxX
            , minY = min extrema_.minY extrema_.maxY
            , maxY = max extrema_.minY extrema_.maxY
            }


{-| Get the minimum and maximum X and Y values of a bounding box in a single
record.

    BoundingBox2i.extrema exampleBox
    --> { minX = 3
    --> , maxX = 8
    --> , minY = 2
    --> , maxY = 6
    --> }

Can be useful when combined with record destructuring, for example


    { minX, maxX, minY, maxY } =
        BoundingBox2i.extrema exampleBox

    --> minX = 3
    --> maxX = 8
    --> minY = 2
    --> maxY = 6

-}
extrema : BoundingBox2i -> { minX : Int, maxX : Int, minY : Int, maxY : Int }
extrema (BoundingBox2i extrema_) =
    extrema_


{-| Construct a zero-width bounding box containing a single point.

    point =
        Point2i.fromCoordinates ( 2, 3 )

    BoundingBox2i.singleton point
    --> BoundingBox2i.fromExtrema
    -->     { minX = 2
    -->     , maxX = 2
    -->     , minY = 3
    -->     , maxY = 3
    -->     }

-}
singleton : Point2i -> BoundingBox2i
singleton point =
    let
        ( x, y ) =
            Point2i.tuple point
    in
    fromExtrema { minX = x, maxX = x, minY = y, maxY = y }


{-| Construct a bounding box with the two given points as two of its corners.
The points can be given in any order and don't have to represent the 'primary'
diagonal of the bounding box.

    firstPoint =
        Point2i.fromCoordinates ( 2, 3 )

    secondPoint =
        Point2i.fromCoordinates ( -1, 5 )

    BoundingBox2i.from firstPoint secondPoint
    --> BoundingBox2i.fromExtrema
    -->     { minX = -1
    -->     , maxX = 2
    -->     , minY = 3
    -->     , maxY = 5
    -->     }

-}
from : Point2i -> Point2i -> BoundingBox2i
from firstPoint secondPoint =
    let
        ( x1, y1 ) =
            Point2i.tuple firstPoint

        ( x2, y2 ) =
            Point2i.tuple secondPoint
    in
    fromExtrema
        { minX = min x1 x2
        , maxX = max x1 x2
        , minY = min y1 y2
        , maxY = max y1 y2
        }


{-| Get the minimum X value of a bounding box.

    BoundingBox2i.minX exampleBox
    --> 3

-}
minX : BoundingBox2i -> Int
minX (BoundingBox2i boundingBox) =
    boundingBox.minX


{-| Get the maximum X value of a bounding box.

    BoundingBox2i.maxX exampleBox
    --> 8

-}
maxX : BoundingBox2i -> Int
maxX (BoundingBox2i boundingBox) =
    boundingBox.maxX


{-| Get the minimum Y value of a bounding box.

    BoundingBox2i.minY exampleBox
    --> 2

-}
minY : BoundingBox2i -> Int
minY (BoundingBox2i boundingBox) =
    boundingBox.minY


{-| Get the maximum Y value of a bounding box.

    BoundingBox2i.maxY exampleBox
    --> 6

-}
maxY : BoundingBox2i -> Int
maxY (BoundingBox2i boundingBox) =
    boundingBox.maxY


{-| Get the X and Y dimensions (width and height) of a bounding box.


    ( width, height ) =
        BoundingBox2i.dimensions exampleBox

    --> width = 5
    --> height = 4

-}
dimensions : BoundingBox2i -> ( Int, Int )
dimensions boundingBox =
    ( maxX boundingBox - minX boundingBox
    , maxY boundingBox - minY boundingBox
    )


{-| Check if a bounding box contains a particular point.

    point =
        Point2i.fromCoordinates ( 4, 3 )

    BoundingBox2i.contains point exampleBox
    --> True

    BoundingBox2i.contains Point2i.origin exampleBox
    --> False

-}
contains : Point2i -> BoundingBox2i -> Bool
contains point boundingBox =
    let
        ( x, y ) =
            Point2i.tuple point
    in
    (minX boundingBox <= x && x <= maxX boundingBox)
        && (minY boundingBox <= y && y <= maxY boundingBox)


area : BoundingBox2i -> Int
area boundingBox =
    let
        ( width, height ) =
            dimensions boundingBox
    in
    width * height


toBoundingBox2d : BoundingBox2i -> BoundingBox2d
toBoundingBox2d boundingBox =
    BoundingBox2d.fromExtrema
        { minX = minX boundingBox |> toFloat
        , minY = minY boundingBox |> toFloat
        , maxX = maxX boundingBox |> toFloat
        , maxY = maxY boundingBox |> toFloat
        }
