module Mission exposing (MenuMission_, Mission(..), Mission1_, menuMissionInit, randomMenuMission, tutorialMissionInit)

import Agent exposing (ChipData, WireData)
import Chip
import Circuit exposing (Circuit, CircuitPosition(..))
import Direction2d exposing (Direction2d)
import InGameTime exposing (InGameTime)
import LineSegment2d exposing (LineSegment2d)
import Point2d exposing (Point2d)
import Quantity exposing (Quantity)
import Random
import T
import Vector2d
import Wire


type Mission
    = TutorialMission Mission1_
    | NormalMission
    | CustomMission
    | MenuMission MenuMission_


type alias MenuMission_ =
    { firstMenuIntro : Bool
    , startTime : Quantity Float InGameTime
    , cameraPath : LineSegment2d
    , cameraRotation : Direction2d
    }


randomMenuMission : Bool -> Quantity Float InGameTime -> Circuit -> Maybe (CircuitPosition WireData ChipData) -> Random.Generator MenuMission_
randomMenuMission firstMenuIntro startTime circuit followPlayer =
    Random.map3
        (\midpoint direction rotation ->
            let
                getStartAndEnd : Float -> Point2d -> LineSegment2d
                getStartAndEnd length center =
                    LineSegment2d.fromEndpoints
                        ( Point2d.translateBy
                            (Vector2d.withLength (-length * 3 / 4) direction)
                            center
                        , Point2d.translateBy
                            (Vector2d.withLength (length / 4) direction)
                            center
                        )

                defaultPath =
                    { firstMenuIntro = firstMenuIntro
                    , startTime = startTime
                    , cameraPath = getStartAndEnd 10 midpoint
                    , cameraRotation = rotation
                    }
            in
            case followPlayer of
                Just playerPosition ->
                    case playerPosition of
                        WirePosition wirePosition ->
                            case Circuit.getWire circuit wirePosition.wireId of
                                Just wire ->
                                    { firstMenuIntro = firstMenuIntro
                                    , startTime = startTime
                                    , cameraPath =
                                        Wire.moveT circuit 10 wire wirePosition.t
                                            |> Wire.tPoint circuit wire
                                            |> Maybe.withDefault midpoint
                                            |> getStartAndEnd 10
                                    , cameraRotation = rotation
                                    }

                                Nothing ->
                                    defaultPath

                        ChipPosition chipPosition ->
                            case Circuit.getChip circuit chipPosition.chipId of
                                Just chip ->
                                    { firstMenuIntro = firstMenuIntro
                                    , startTime = startTime
                                    , cameraPath = chipPosition.position |> Chip.localToWorld chip |> getStartAndEnd 8
                                    , cameraRotation = rotation
                                    }

                                Nothing ->
                                    defaultPath

                Nothing ->
                    defaultPath
        )
        (case Circuit.getWires circuit of
            head :: rest ->
                Random.uniform head rest
                    |> Random.andThen
                        (\( _, wire ) ->
                            wire
                                |> Wire.length circuit
                                |> Random.float 0
                                |> Random.map
                                    (\distance ->
                                        Wire.moveT circuit distance wire Quantity.zero
                                            |> Wire.tPoint circuit wire
                                            |> Maybe.withDefault Point2d.origin
                                    )
                        )

            _ ->
                Random.constant Point2d.origin
        )
        (Random.float 0 (2 * pi) |> Random.map Direction2d.fromAngle)
        (Random.float 0 (2 * pi) |> Random.map Direction2d.fromAngle)


type alias Mission1_ =
    { introductionStart : Maybe (Quantity Float InGameTime)
    , controlsStart : Maybe (Quantity Float InGameTime)
    }


menuMissionInit : Bool -> Circuit -> Mission
menuMissionInit firstMenuIntro circuit =
    Random.step (randomMenuMission firstMenuIntro (InGameTime.seconds 0) circuit Nothing) (Random.initialSeed 123) |> Tuple.first |> MenuMission


tutorialMissionInit : Mission
tutorialMissionInit =
    TutorialMission { introductionStart = Nothing, controlsStart = Nothing }
