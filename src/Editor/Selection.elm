module Editor.Selection exposing
    ( Selection
    , WireSelection(..)
    , agentSelected
    , chipSelected
    , empty
    , from
    , isEmpty
    , merge
    , negateAgentSelection
    , negateWireSelection
    , playerSelected
    , selectAll
    , selectRegion
    , selectWire
    , selectedWireNodes
    , selectedWireSegments
    , wireSelected
    )

import AssocList
import BoundingBox2d exposing (BoundingBox2d)
import Chip
import Circuit exposing (AgentStartType, Chip, ChipId(..), Wire, WireId, WiresAndChips)
import Dict
import Editor.Circuit exposing (AgentStartId, Circuit)
import Editor.Helper as Helper
import EverySet exposing (EverySet)
import ListHelper as List
import Set exposing (Set)
import Wire


type alias Selection =
    { chips : List ChipId
    , wireSegments : List ( WireId, WireSelection )
    , agents : EverySet AgentStartId
    , player : Bool
    }


from : List ChipId -> List ( WireId, WireSelection ) -> EverySet AgentStartId -> Bool -> Selection
from chips wireSegments agents player =
    { chips = chips
    , wireSegments = wireSegments
    , agents = agents
    , player = player
    }


empty : Selection
empty =
    { chips = []
    , wireSegments = []
    , agents = EverySet.empty
    , player = False
    }


isEmpty : Selection -> Bool
isEmpty selection =
    selection == empty


chipSelected : Selection -> ChipId -> Bool
chipSelected model chipId =
    List.any ((==) chipId) model.chips


{-| Returns true if any node or segment is selected on a given wire.
-}
wireSelected : Selection -> WireId -> Bool
wireSelected selection wireId =
    selection.wireSegments |> List.any (Tuple.first >> (==) wireId)


agentSelected : Selection -> AgentStartId -> Bool
agentSelected selection agentIndex =
    EverySet.member agentIndex selection.agents


playerSelected : Selection -> Bool
playerSelected selection =
    selection.player


merge : Bool -> Selection -> Selection -> Selection
merge addSelection { chips, wireSegments, agents, player } selection =
    { selection
        | chips =
            if addSelection then
                chips ++ selection.chips

            else
                chips
        , wireSegments =
            if addSelection then
                wireSegments ++ selection.wireSegments

            else
                wireSegments
        , agents =
            if addSelection then
                EverySet.union agents selection.agents

            else
                agents
        , player =
            if addSelection then
                player && selection.player

            else
                player
    }


type WireSelection
    = WireSegmentSelection Int
    | WireNodeSelection Int


selectedWireNodes : List WireSelection -> Set Int
selectedWireNodes wireSelection =
    wireSelection
        |> List.concatMap
            (\wireSelection_ ->
                case wireSelection_ of
                    WireNodeSelection nodeIndex ->
                        [ nodeIndex ]

                    WireSegmentSelection segmentIndex ->
                        [ segmentIndex, segmentIndex + 1 ]
            )
        |> Set.fromList


selectedWireSegments : List WireSelection -> Set Int
selectedWireSegments wireSelection =
    wireSelection
        |> List.concatMap
            (\wireSelection_ ->
                case wireSelection_ of
                    WireNodeSelection nodeIndex ->
                        [ nodeIndex - 1, nodeIndex ]

                    WireSegmentSelection segmentIndex ->
                        [ segmentIndex ]
            )
        |> Set.fromList


selectAll : Circuit -> Selection
selectAll circuit =
    { chips = Circuit.getChips circuit |> List.map Tuple.first
    , wireSegments =
        Circuit.getWires circuit
            |> List.concatMap
                (\( wireId, wire ) ->
                    List.range 0 (Wire.nodePoints circuit wire |> List.length |> (+) -1)
                        |> List.map (\index -> ( wireId, WireNodeSelection index ))
                )
    , agents = Editor.Circuit.getAgents circuit |> AssocList.keys |> EverySet.fromList
    , player = circuit.playerStart /= Nothing
    }


selectWire : Circuit -> Wire -> List WireSelection
selectWire circuit wire =
    Wire.nodePoints circuit wire |> List.length |> (+) -1 |> List.range 0 |> List.map WireSegmentSelection


negateWireSelection : Circuit -> Wire -> List WireSelection -> List WireSelection
negateWireSelection circuit wire wireSelection =
    let
        wireSegments =
            Wire.nodePoints circuit wire |> List.length |> (+) -1 |> List.range 0 |> Set.fromList
    in
    Set.diff wireSegments (selectedWireSegments wireSelection) |> Set.toList |> List.map WireSegmentSelection


negateAgentSelection : Circuit -> Selection -> Selection
negateAgentSelection circuit selection =
    let
        agentIds =
            Editor.Circuit.getAgents circuit |> AssocList.keys |> EverySet.fromList
    in
    { selection | agents = EverySet.diff agentIds selection.agents }


selectRegion : Circuit -> BoundingBox2d -> Selection
selectRegion circuit selectRegion_ =
    let
        selectedWireSegments_ =
            Circuit.getWires circuit
                |> List.map
                    (\( wireId, wire ) ->
                        wireSegmentsInsideRegion circuit selectRegion_ wire
                            |> List.map (Tuple.pair wireId)
                    )
                |> List.concat

        selectedChips : List ChipId
        selectedChips =
            Dict.filter
                (\_ chip -> chipInsideRegion selectRegion_ chip)
                circuit.chips
                |> Dict.toList
                |> List.map (Tuple.first >> ChipId)

        selectedAgentStart =
            Editor.Circuit.getAgents circuit
                |> AssocList.toList
                |> List.filterMap
                    (\( id, start ) ->
                        case Circuit.getAgentStartPosition start |> Helper.circuitWorldPosition circuit of
                            Just position ->
                                if BoundingBox2d.contains position selectRegion_ then
                                    Just id

                                else
                                    Nothing

                            Nothing ->
                                Nothing
                    )
                |> EverySet.fromList

        selectedPlayerStart =
            circuit.playerStart
                |> Maybe.map
                    (\start ->
                        case Helper.circuitWorldPosition circuit start of
                            Just position ->
                                BoundingBox2d.contains position selectRegion_

                            Nothing ->
                                False
                    )
                |> Maybe.withDefault False
    in
    from
        selectedChips
        selectedWireSegments_
        selectedAgentStart
        selectedPlayerStart


wireSegmentsInsideRegion : WiresAndChips a -> BoundingBox2d -> Wire -> List WireSelection
wireSegmentsInsideRegion circuit region wire =
    Wire.nodePoints circuit wire
        |> List.pairwise
        |> List.indexedMap Tuple.pair
        |> List.filterMap
            (\( index, ( first, second ) ) ->
                if BoundingBox2d.contains first region && BoundingBox2d.contains second region then
                    Just (WireSegmentSelection index)

                else
                    Nothing
            )


chipInsideRegion : BoundingBox2d -> Chip -> Bool
chipInsideRegion region chip =
    Chip.worldBounds chip |> BoundingBox2d.isContainedIn region
