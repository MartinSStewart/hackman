module Editor.WirePlacer exposing
    ( ChipSnapPoint
    , Model
    , SnapPoint(..)
    , addNode
    , alongWirePosition
    , chipSnapPoints
    , endOnChipEdge
    , endOnNothing
    , endOnWire
    , getChipSnapPoints
    , getSnapPoint
    , lastPlacedPosition
    , startDisconnected
    , startOnChipEdge
    , startOnWire
    , view
    )

import Basics.Extra as Basics exposing (flip)
import Chip
import Circle2d
import Circuit exposing (AgentStartType, Chip, ChipEdgePoint, ChipId(..), PlayerStart, Trigger, Wire, WireId, WiresAndChips)
import Color
import Direction2d exposing (Direction2d)
import Draw
import Editor.Circuit as Circuit exposing (Circuit)
import Editor.Helper as Helper
import Editor.SnapSettings as SnapSettings exposing (SnapSettings)
import Geometry.Svg as Svg
import Helper exposing (ifElse)
import LineSegment2d exposing (LineSegment2d)
import List.Extra as List
import ListHelper as List
import Maybe.Extra as Maybe
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Svg exposing (Svg)
import Svg.Attributes
import Vector2d exposing (Vector2d)
import Wire


type alias Model_ =
    { start : StartType

    -- The head item is the most recently placed node.
    , nodes : List Point2d
    }


type Model
    = Model Model_


type alias ChipSnapPoint =
    { chipEdgePoint : ChipEdgePoint, position : Point2d }


type StartType
    = NodeStart Point2d
    | ChipEdgeStart ChipEdgePoint
    | WireStart { wireId : WireId, isEnd : Bool }


startOnWire : WireId -> Bool -> Model
startOnWire wireId isEnd =
    WireStart { wireId = wireId, isEnd = isEnd } |> init


startOnChipEdge : ChipEdgePoint -> Model
startOnChipEdge chipEdgePoint =
    ChipEdgeStart chipEdgePoint |> init


startDisconnected : Point2d -> Model
startDisconnected worldPoint =
    NodeStart worldPoint |> init


init : StartType -> Model
init startType =
    { start = startType
    , nodes = []
    }
        |> Model


endOnWire : WireId -> Bool -> Model -> Circuit -> Maybe Circuit
endOnWire wireId isEnd wirePlacer editorModel =
    placeWire wirePlacer ({ wireId = wireId, isEnd = isEnd } |> WireStart |> Just) editorModel


endOnChipEdge : ChipEdgePoint -> Model -> Circuit -> Maybe Circuit
endOnChipEdge chipEdgePoint wirePlacer editorModel =
    placeWire wirePlacer (chipEdgePoint |> ChipEdgeStart |> Just) editorModel


endOnNothing : Model -> Circuit -> Maybe Circuit
endOnNothing wirePlacer editorModel =
    placeWire wirePlacer Nothing editorModel


isValidWireEndpoint : WireId -> Model -> Bool
isValidWireEndpoint endpoint (Model model) =
    case model.start of
        WireStart { wireId } ->
            wireId /= endpoint

        _ ->
            True


placeWire : Model -> Maybe StartType -> Circuit -> Maybe Circuit
placeWire (Model model) end circuit =
    let
        chipEnd : StartType -> Maybe ChipEdgePoint
        chipEnd startType =
            case startType of
                ChipEdgeStart chipEdgeStart ->
                    Just chipEdgeStart

                _ ->
                    Nothing

        wireNodeEnd : StartType -> List Point2d
        wireNodeEnd startType =
            case startType of
                NodeStart point ->
                    [ point ]

                _ ->
                    []

        ( circuit2, newWireId ) =
            Circuit.addWire
                (Wire.init
                    (Maybe.andThen chipEnd end)
                    ((end |> Maybe.map wireNodeEnd |> Maybe.withDefault [])
                        ++ model.nodes
                        ++ wireNodeEnd model.start
                    )
                    (chipEnd model.start)
                )
                circuit

        startWire : ( Circuit, WireId ) -> Maybe ( Circuit, WireId )
        startWire ( circuit_, wireId_ ) =
            case model.start of
                WireStart { wireId, isEnd } ->
                    if not isEnd then
                        circuit_ |> Circuit.mergeWires wireId_ wireId

                    else
                        circuit_ |> Circuit.reverseWire wireId |> Circuit.mergeWires wireId_ wireId

                _ ->
                    Just ( circuit_, wireId_ )

        endWire : ( Circuit, WireId ) -> Maybe ( Circuit, WireId )
        endWire ( circuit_, wireId_ ) =
            case end of
                Just (WireStart { wireId, isEnd }) ->
                    if not isEnd then
                        circuit_ |> Circuit.reverseWire wireId |> Circuit.mergeWires wireId wireId_

                    else
                        circuit_ |> Circuit.mergeWires wireId wireId_

                _ ->
                    Just ( circuit_, wireId_ )
    in
    ( circuit2, newWireId )
        |> startWire
        |> Maybe.andThen endWire
        |> Maybe.andThen
            (\( circuit_, wireId ) ->
                case Circuit.getWire circuit_ wireId of
                    Just wire ->
                        if wireIsValid wire then
                            Circuit.reverseWire wireId circuit_ |> Just

                        else
                            Nothing

                    Nothing ->
                        Nothing
            )


wireIsValid : Wire -> Bool
wireIsValid wire =
    let
        a =
            ifElse (wire.start == Nothing) 0 1

        b =
            ifElse (wire.end == Nothing) 0 1

        c =
            List.length wire.nodes
    in
    a + b + c >= 2


addNode : Point2d -> Model -> Model
addNode point (Model model) =
    { start = model.start
    , nodes = point :: model.nodes
    }
        |> Model


getChipSnapPoints : ( ChipId, Chip ) -> List ChipSnapPoint
getChipSnapPoints ( chipId, chip ) =
    chip
        |> Chip.localEdgePoints
        |> List.map
            (\edgePoint ->
                Chip.edgePointWorldPosition chip edgePoint
                    |> ChipSnapPoint
                        { chipId = chipId
                        , side = edgePoint.side
                        , offset = edgePoint.offset
                        }
            )


chipSnapPoints : ( ChipId, Chip ) -> Maybe Model -> List ChipSnapPoint
chipSnapPoints (( chipId, _ ) as tuple) maybeModel =
    case maybeModel of
        Just (Model { start, nodes }) ->
            case start of
                ChipEdgeStart start_ ->
                    if start_.chipId == chipId && List.isEmpty nodes then
                        []

                    else
                        getChipSnapPoints tuple

                WireStart _ ->
                    getChipSnapPoints tuple

                NodeStart _ ->
                    getChipSnapPoints tuple

        Nothing ->
            getChipSnapPoints tuple


lastPlacedPosition : Circuit -> Model -> Maybe Point2d
lastPlacedPosition editorModel (Model model) =
    case model.nodes of
        [] ->
            startPoint editorModel model.start

        nodes ->
            List.head nodes


startPoint : Circuit -> StartType -> Maybe Point2d
startPoint editorModel start =
    case start of
        ChipEdgeStart chipEdge ->
            Wire.wireChipEndWorldPosition_ editorModel chipEdge

        NodeStart node ->
            Just node

        WireStart { wireId, isEnd } ->
            case ( Circuit.getWire editorModel wireId, isEnd ) of
                ( Just wire, False ) ->
                    Wire.nodePoints editorModel wire |> List.head

                ( Just wire, True ) ->
                    Wire.nodePoints editorModel wire |> List.last

                ( Nothing, _ ) ->
                    Nothing


view : Float -> SnapSettings -> Circuit -> Maybe Point2d -> Maybe Model -> Svg msg
view viewZoom snapSettings editorModel mouseWorldPosition maybeModel =
    case maybeModel of
        Just (Model { start, nodes }) ->
            let
                ( points, highlights ) =
                    case mouseWorldPosition of
                        Just mousePos ->
                            let
                                snapPoint : SnapPoint
                                snapPoint =
                                    getSnapPoint viewZoom snapSettings editorModel maybeModel mousePos

                                points_ =
                                    snapPointPosition editorModel maybeModel snapPoint |> Maybe.toList |> flip (++) nodes

                                highlights_ =
                                    case snapPoint of
                                        AlongWire { adjacentLine } ->
                                            Draw.lineStrip
                                                Color.lightBlue
                                                (1.5 / viewZoom)
                                                [ LineSegment2d.startPoint adjacentLine, LineSegment2d.endPoint adjacentLine ]

                                        _ ->
                                            Svg.g [] []
                            in
                            ( points_, highlights_ )

                        Nothing ->
                            ( nodes, Svg.g [] [] )

                allPoints : List Point2d
                allPoints =
                    points ++ Maybe.toList (startPoint editorModel start)
            in
            Svg.g
                []
                [ Draw.lineStrip Color.black (2 / viewZoom) allPoints
                , case List.reverse allPoints |> List.head of
                    Just head ->
                        head
                            |> Circle2d.withRadius (4 / viewZoom)
                            |> Svg.circle2d [ Svg.Attributes.fill "black" ]

                    Nothing ->
                        Svg.g [] []
                , highlights
                ]

        Nothing ->
            case mouseWorldPosition of
                Just mousePos ->
                    let
                        snapPoint : SnapPoint
                        snapPoint =
                            getSnapPoint viewZoom snapSettings editorModel maybeModel mousePos
                    in
                    case snapPointPosition editorModel Nothing snapPoint of
                        Just pos ->
                            pos
                                |> Circle2d.withRadius (4 / viewZoom)
                                |> Svg.circle2d [ Svg.Attributes.fill "black" ]

                        Nothing ->
                            Svg.g [] []

                Nothing ->
                    Svg.g [] []


type alias WireDetachedEndpoint =
    { wireId : WireId, isEnd : Bool, position : Point2d }


type SnapPoint
    = GridPosition Point2d
    | ChipEdge ChipEdgePoint
    | WireEndpoint WireDetachedEndpoint
    | LastPlacedPosition
    | AlongWire AlongWire_


type alias AlongWire_ =
    { adjacentLine : LineSegment2d
    , position : Point2d
    }


snapToAngle : Bool -> Point2d -> Point2d -> Point2d
snapToAngle snapToGrid start currentPos =
    let
        point =
            Vector2d.from start currentPos
    in
    case Vector2d.lengthAndDirection point of
        Just ( length, direction ) ->
            let
                angle =
                    Direction2d.toAngle direction |> Helper.roundBy (pi / 8)
            in
            angle
                |> Direction2d.fromAngle
                |> Vector2d.withLength
                    (if snapToGrid then
                        let
                            ( startX, startY ) =
                                Point2d.coordinates start

                            nearestLength : Float -> Float -> Maybe Float
                            nearestLength angle_ startValue =
                                if abs (tan angle_) < 1000 then
                                    let
                                        distance =
                                            Point2d.fromCoordinates ( 1, tan angle_ )
                                                |> Point2d.distanceFrom Point2d.origin

                                        offset =
                                            Basics.fractionalModBy 1 startValue

                                        offset_ =
                                            if angle_ |> Tuple.pair 1 |> Vector2d.fromPolarComponents |> Vector2d.xComponent |> (>) 0 then
                                                offset

                                            else
                                                1 - offset
                                    in
                                    Helper.roundByWithOffset
                                        distance
                                        (distance * offset_)
                                        length
                                        |> Just

                                else
                                    Nothing

                            nearestX =
                                nearestLength angle startX |> Maybe.toList

                            nearestY =
                                nearestLength (angle - pi / 2) startY |> Maybe.toList
                        in
                        nearestX
                            ++ nearestY
                            |> List.minimumBy (\distance -> distance - length |> abs)
                            |> Maybe.withDefault length

                     else
                        length
                    )
                |> flip Point2d.translateBy start

        Nothing ->
            Point2d.translateBy point start


getSnapPoint : Float -> SnapSettings -> Circuit -> Maybe Model -> Point2d -> SnapPoint
getSnapPoint viewZoom snapSettings circuit wirePlacer mouseWorldPosition =
    let
        snapToGrid =
            SnapSettings.getToGrid snapSettings

        snapToAngle_ =
            SnapSettings.getToAngle snapSettings

        nearestWire =
            nearestFreeWireEndpoint mouseWorldPosition wirePlacer viewZoom circuit |> Maybe.map WireEndpoint

        nearestChip =
            nearestChipEdgePoint mouseWorldPosition wirePlacer viewZoom circuit |> Maybe.map ChipEdge

        lastPlaced =
            case wirePlacer of
                Just wirePlacer_ ->
                    case lastPlacedPosition circuit wirePlacer_ of
                        Just position_ ->
                            Helper.snapToNearest viewZoom mouseWorldPosition [ { position = position_ } ]
                                |> Maybe.map (always LastPlacedPosition)
                                |> Maybe.toList

                        Nothing ->
                            []

                Nothing ->
                    []

        gridSnapPos : Point2d
        gridSnapPos =
            if snapToGrid then
                Point2i.roundFrom mouseWorldPosition |> Point2i.toPoint2d

            else
                mouseWorldPosition

        nearestAlongWire : List SnapPoint
        nearestAlongWire =
            if SnapSettings.getAlongWire snapSettings && nearestChip == Nothing && nearestWire == Nothing then
                Circuit.getWires circuit
                    |> List.concatMap
                        (\( _, wire ) ->
                            alongWirePoints
                                snapSettings
                                viewZoom
                                circuit
                                wirePlacer
                                mouseWorldPosition
                                (Wire.nodePoints circuit wire)
                        )
                    |> List.map AlongWire

            else
                []

        allSnapPoints =
            List.sortBy
                (snapPointPosition circuit wirePlacer >> Maybe.map (Point2d.distanceFrom mouseWorldPosition) >> Maybe.withDefault 9999)
                (Maybe.toList nearestWire ++ Maybe.toList nearestChip ++ lastPlaced ++ nearestAlongWire)
    in
    case ( List.head allSnapPoints, wirePlacer ) of
        ( Just snapPoint, _ ) ->
            snapPoint

        ( Nothing, Just wirePlacer_ ) ->
            case ( snapToAngle_, lastPlacedPosition circuit wirePlacer_ ) of
                ( True, Just firstNode ) ->
                    snapToAngle snapToGrid firstNode mouseWorldPosition
                        |> GridPosition

                _ ->
                    GridPosition gridSnapPos

        ( Nothing, Nothing ) ->
            GridPosition gridSnapPos


snapPointPosition : Circuit -> Maybe Model -> SnapPoint -> Maybe Point2d
snapPointPosition circuit maybeWirePlacer snapPoint =
    case snapPoint of
        GridPosition position_ ->
            Just position_

        ChipEdge { chipId, side, offset } ->
            case Circuit.getChip circuit chipId of
                Just chip ->
                    Chip.edgePointWorldPosition chip { side = side, offset = offset }
                        |> Just

                Nothing ->
                    Nothing

        WireEndpoint wireEndpoint ->
            Just wireEndpoint.position

        LastPlacedPosition ->
            case maybeWirePlacer of
                Just wirePlacer ->
                    lastPlacedPosition circuit wirePlacer

                Nothing ->
                    Nothing

        AlongWire adjacentToWire ->
            alongWirePosition adjacentToWire |> Just


alongWirePosition : AlongWire_ -> Point2d
alongWirePosition alongWire =
    alongWire.position


nearestFreeWireEndpoint : Point2d -> Maybe Model -> Float -> Circuit -> Maybe WireDetachedEndpoint
nearestFreeWireEndpoint clickWorldPosition maybeWirePlacer viewZoom model =
    Circuit.getWires model
        |> List.concatMap
            (\( wireId, wire ) ->
                let
                    endPoints =
                        Wire.detachedEndPoints wire

                    validWire =
                        case maybeWirePlacer of
                            Just (Model wirePlacer) ->
                                case wirePlacer.start of
                                    WireStart wireStart ->
                                        wireStart.wireId /= wireId

                                    _ ->
                                        True

                            Nothing ->
                                True
                in
                if validWire then
                    case ( endPoints.start, endPoints.end ) of
                        ( Just start_, Just end_ ) ->
                            [ { wireId = wireId, isEnd = False, position = start_ }
                            , { wireId = wireId, isEnd = True, position = end_ }
                            ]

                        ( Just start_, Nothing ) ->
                            [ { wireId = wireId, isEnd = False, position = start_ } ]

                        ( Nothing, Just end_ ) ->
                            [ { wireId = wireId, isEnd = True, position = end_ } ]

                        ( Nothing, Nothing ) ->
                            []

                else
                    []
            )
        |> Helper.snapToNearest viewZoom clickWorldPosition


nearestChipEdgePoint : Point2d -> Maybe Model -> Float -> WiresAndChips a -> Maybe ChipEdgePoint
nearestChipEdgePoint clickWorldPosition maybeWirePlacer viewZoom circuit =
    Circuit.getChips circuit
        |> List.concatMap (flip chipSnapPoints maybeWirePlacer)
        |> Helper.snapToNearest viewZoom clickWorldPosition
        |> Maybe.map .chipEdgePoint


alongWirePoints : SnapSettings -> Float -> Circuit -> Maybe Model -> Point2d -> List Point2d -> List AlongWire_
alongWirePoints snapSettings viewZoom circuit maybeWirePlacer mouseWorldPosition wire =
    case maybeWirePlacer of
        Just wirePlacer ->
            case lastPlacedPosition circuit wirePlacer of
                Just start ->
                    let
                        snapToGrid =
                            SnapSettings.getToGrid snapSettings

                        pos =
                            if SnapSettings.getToAngle snapSettings then
                                snapToAngle False start mouseWorldPosition

                            else if snapToGrid then
                                mouseWorldPosition |> Point2i.roundFrom |> Point2i.toPoint2d

                            else
                                mouseWorldPosition
                    in
                    case Direction2d.from start pos of
                        Just direction ->
                            alongWirePoints_ viewZoom start direction pos wire

                        Nothing ->
                            []

                Nothing ->
                    []

        Nothing ->
            []


alongWirePoints_ : Float -> Point2d -> Direction2d -> Point2d -> List Point2d -> List AlongWire_
alongWirePoints_ viewZoom start direction end wire =
    let
        errorMargin =
            0.00001

        isParallel angle =
            angle < errorMargin && angle > -errorMargin

        offsetEdge : LineSegment2d -> List LineSegment2d
        offsetEdge line =
            case LineSegment2d.perpendicularDirection line of
                Just dir ->
                    [ LineSegment2d.translateIn dir 1 line
                    , LineSegment2d.translateIn (Direction2d.reverse dir) 1 line
                    ]

                Nothing ->
                    []

        currentLine =
            LineSegment2d.from
                (Point2d.translateIn direction -100 start)
                (Point2d.translateIn direction 100 start)

        lineSegments : List LineSegment2d
        lineSegments =
            List.pairwise wire
                |> List.concatMap
                    (\( prev, next ) ->
                        let
                            line =
                                LineSegment2d.fromEndpoints ( prev, next )

                            lineScaled =
                                line |> LineSegment2d.scaleAbout (LineSegment2d.midpoint line) 5
                        in
                        case LineSegment2d.direction lineScaled of
                            Just lineDir ->
                                if
                                    (Direction2d.angleFrom lineDir direction |> isParallel |> not)
                                        && (Direction2d.angleFrom (Direction2d.rotateBy pi lineDir) direction |> isParallel |> not)
                                then
                                    offsetEdge lineScaled

                                else
                                    []

                            Nothing ->
                                []
                    )
    in
    lineSegments
        |> List.filterMap
            (\line ->
                case LineSegment2d.intersectionPoint currentLine line of
                    Just intersection ->
                        if Point2d.distanceFrom intersection end < Helper.snapDistance viewZoom then
                            Just { adjacentLine = line, position = intersection }

                        else
                            Nothing

                    Nothing ->
                        Nothing
            )
