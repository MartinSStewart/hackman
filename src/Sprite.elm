module Sprite exposing (Sprite)

import Vector2i exposing (Vector2i)


type alias Sprite =
    { image : String
    , size : Vector2i
    }
