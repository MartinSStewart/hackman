module Editor.Hand exposing
    ( draw
    , mouseMove
    , mouseUp
    , removeSelected
    )

import AssocList
import BoundingBox2d exposing (BoundingBox2d)
import Chip
import Circuit exposing (AgentStartType(..), Chip, ChipEdgePoint, ChipId(..), CircuitPosition(..), Trigger, Wire, WireDirection(..), WireId(..), WiresAndChips)
import Dict exposing (Dict)
import Editor.Circuit as Circuit exposing (Circuit)
import Editor.DragState as DragState
import Editor.Helper as Helper
import Editor.Selection as Selection exposing (Selection, WireSelection(..))
import Editor.UndoModel as UndoModel exposing (UndoModel)
import EverySet
import Keyboard
import KeyboardHelper as Keyboard exposing (KeyboardState)
import List.Extra as List
import ListHelper as List
import Maybe.Extra as Maybe
import Point2d exposing (Point2d)
import Set exposing (Set)
import Svg exposing (Svg)
import Svg.Attributes
import UndoList exposing (UndoList)
import Vector2d
import Wire


type alias EditorModel a =
    { a
        | undoList : UndoList UndoModel
        , dragState : DragState.DragState
    }


mouseUp : KeyboardState a -> Point2d -> EditorModel b -> EditorModel b
mouseUp keyboardState mouseWorldPosition editorModel =
    let
        undoList =
            editorModel.undoList.present

        newSelection : Selection
        newSelection =
            case editorModel.dragState of
                DragState.DragNone ->
                    Selection.empty

                DragState.DragReady _ ->
                    clickSelect undoList mouseWorldPosition

                DragState.Dragging dragStart ->
                    Selection.selectRegion undoList.circuit (BoundingBox2d.from dragStart mouseWorldPosition)

        newUndoList : UndoModel
        newUndoList =
            { undoList
                | selection =
                    Selection.merge
                        (Keyboard.isDown keyboardState Keyboard.Shift)
                        newSelection
                        undoList.selection
            }
    in
    if Selection.isEmpty newSelection && Selection.isEmpty undoList.selection then
        { editorModel | dragState = DragState.DragNone }

    else
        { editorModel
            | undoList =
                UndoList.new
                    newUndoList
                    editorModel.undoList
            , dragState = DragState.DragNone
        }


clickSelect : UndoModel -> Point2d -> Selection
clickSelect undoState mouseWorldPosition =
    let
        selectedChip =
            Circuit.getChips undoState.circuit
                |> List.find
                    (Tuple.second >> worldPointInsideChip mouseWorldPosition)
                |> Maybe.map Tuple.first

        selectedWireNode =
            Circuit.getWires undoState.circuit
                |> List.concatMap
                    (\( wireId, wire ) ->
                        wire
                            |> Wire.nodePoints undoState.circuit
                            |> List.indexedMap
                                (\index a ->
                                    { wireId = wireId
                                    , nodeId = index
                                    , position = a
                                    }
                                )
                    )
                |> Helper.snapToNearest undoState.viewZoom mouseWorldPosition
                |> Maybe.map (\{ wireId, nodeId } -> Tuple.pair wireId nodeId)

        selectedAgentStart =
            Circuit.getAgents undoState.circuit
                |> AssocList.toList
                |> List.filterMap
                    (\( id, start ) ->
                        case Circuit.getAgentStartPosition start |> Helper.circuitWorldPosition undoState.circuit of
                            Just position ->
                                Just { position = position, id = id }

                            Nothing ->
                                Nothing
                    )
                |> Helper.snapToNearest undoState.viewZoom mouseWorldPosition
                |> Maybe.filter
                    (.position
                        >> Point2d.distanceFrom mouseWorldPosition
                        >> (>) (Helper.viewScalarToWorld undoState 15)
                    )
                |> Maybe.map .id

        selectedPlayerStart =
            undoState.circuit.playerStart
                |> Maybe.map
                    (\start ->
                        case Helper.circuitWorldPosition undoState.circuit start of
                            Just position ->
                                Point2d.distanceFrom mouseWorldPosition position < Helper.viewScalarToWorld undoState 15

                            Nothing ->
                                False
                    )
                |> Maybe.withDefault False

        emptySelection =
            Selection.empty
    in
    case ( selectedChip, selectedWireNode, ( selectedAgentStart, selectedPlayerStart ) ) of
        ( _, _, ( _, True ) ) ->
            { emptySelection | player = True }

        ( _, _, ( Just a, False ) ) ->
            { emptySelection | agents = EverySet.fromList [ a ] }

        ( _, Just ( wireId, segmentIndex ), ( Nothing, False ) ) ->
            { emptySelection | wireSegments = [ ( wireId, WireSegmentSelection segmentIndex ) ] }

        ( Just a, Nothing, ( Nothing, False ) ) ->
            { emptySelection | chips = [ a ] }

        ( Nothing, Nothing, ( Nothing, False ) ) ->
            Selection.empty


mouseMove : Point2d -> EditorModel a -> EditorModel a
mouseMove mouseWorldPosition editorModel =
    { editorModel
        | dragState =
            case editorModel.dragState of
                DragState.DragNone ->
                    DragState.DragNone

                DragState.DragReady dragStart ->
                    if Point2d.distanceFrom mouseWorldPosition dragStart > (10 / editorModel.undoList.present.viewZoom) then
                        DragState.Dragging dragStart

                    else
                        DragState.DragReady dragStart

                DragState.Dragging dragStart ->
                    DragState.Dragging dragStart
    }


removeSelected : UndoModel -> UndoModel
removeSelected undoModel =
    { undoModel | circuit = UndoModel.removeSelected undoModel.selection undoModel.circuit }
        |> UndoModel.resetSelection


worldPointInsideChip : Point2d -> Chip -> Bool
worldPointInsideChip point chip =
    Chip.worldBounds chip |> BoundingBox2d.contains point


draw : Float -> Maybe Point2d -> EditorModel a -> Svg msg
draw viewZoom mouseWorldPosition editorModel =
    case ( mouseWorldPosition, editorModel.dragState ) of
        ( _, DragState.DragNone ) ->
            Svg.g [] []

        ( _, DragState.DragReady _ ) ->
            Svg.g [] []

        ( Just mousePos, DragState.Dragging dragStart ) ->
            Helper.drawRectangle
                [ Svg.Attributes.stroke "blue"
                , Svg.Attributes.fill "none"
                , Svg.Attributes.strokeWidth <| String.fromFloat <| 1 / viewZoom
                ]
                dragStart
                (Vector2d.from dragStart mousePos)

        ( Nothing, DragState.Dragging _ ) ->
            Svg.g [] []
