module InGame.HudTimer exposing (TimePenaltyType(..), view)

import Element exposing (Element)
import Element.Font as Font
import Helper
import InGameTime exposing (InGameTime)
import Keyframe
import Pixels
import Quantity exposing (Quantity)
import Round
import StageScore


type alias TimePenaltyAnimation =
    { startTime : Quantity Float InGameTime
    , penaltyType : TimePenaltyType
    }


type TimePenaltyType
    = BadMovePenalty
    | DamagePenalty


fontSize : number
fontSize =
    24


offset =
    Pixels.pixels 26


updatePenaltyDelay : Quantity Float InGameTime
updatePenaltyDelay =
    InGameTime.seconds 1.5


offsetAnimation : Quantity Float InGameTime -> Keyframe.Animation InGameTime Pixels.Pixels
offsetAnimation startTime =
    Keyframe.startAt startTime offset
        |> Keyframe.add Keyframe.Linear (InGameTime.seconds 1) (offset |> Quantity.minus (Pixels.pixels 7))
        |> Keyframe.add Keyframe.InOutExpo (InGameTime.seconds 0.5) Quantity.zero


numberAlphaAnimation : Quantity Float InGameTime -> Keyframe.Animation InGameTime Quantity.Unitless
numberAlphaAnimation startTime =
    Keyframe.startAt_ startTime 0
        |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 0.1) 1
        |> Keyframe.addConstant (InGameTime.seconds 1.4)
        |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 0) 0


textAlphaAnimation : Quantity Float InGameTime -> Keyframe.Animation InGameTime Quantity.Unitless
textAlphaAnimation startTime =
    Keyframe.startAt_ startTime 0
        |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 0.1) 1
        |> Keyframe.addConstant (InGameTime.seconds 1)
        |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 0.1) 0


timePenaltyView : Quantity Float InGameTime -> TimePenaltyAnimation -> Element msg
timePenaltyView time timePenalty =
    let
        name =
            Element.el
                [ Element.alpha <| Keyframe.valueAt_ time (textAlphaAnimation timePenalty.startTime) ]
            <|
                case timePenalty.penaltyType of
                    BadMovePenalty ->
                        Element.text "Moved early"

                    DamagePenalty ->
                        Element.text "Took damage"

        number =
            penaltyAmount timePenalty
                |> InGameTime.inSeconds
                |> Round.round 0
                |> Element.text
                |> Element.el [ Element.alpha <| Keyframe.valueAt_ time (numberAlphaAnimation timePenalty.startTime) ]
    in
    Element.row
        [ penaltyFontColor
        , Element.spacing 10
        , Element.moveDown <| Pixels.inPixels <| Keyframe.valueAt time (offsetAnimation timePenalty.startTime)
        ]
        [ name, number ]


penaltyFontColor : Element.Attr decorative msg
penaltyFontColor =
    Font.color <| Element.rgb 1 0 0


penaltyAmount timePenalty =
    case timePenalty.penaltyType of
        BadMovePenalty ->
            StageScore.chipMoves 1 |> Quantity.toFloatQuantity |> Quantity.at StageScore.timePerBadChipMove

        DamagePenalty ->
            StageScore.healthPoints 1 |> Quantity.toFloatQuantity |> Quantity.at StageScore.timePerHealthPoint


view : Quantity Float InGameTime -> List TimePenaltyAnimation -> Quantity Float InGameTime -> Element msg
view time timePenalties totalTimePenalty =
    let
        maxAge : Quantity Float InGameTime
        maxAge =
            time |> Quantity.minus updatePenaltyDelay

        totalOffset =
            timePenalties
                |> List.filter (.startTime >> Quantity.greaterThan maxAge)
                |> List.map penaltyAmount
                |> Quantity.sum

        timestamp =
            time
                |> InGameTime.inMinutes
                |> Helper.timestamp
                |> Element.text

        penalty =
            totalTimePenalty
                |> Quantity.minus totalOffset
                |> InGameTime.inSeconds
                |> Round.round 0
                |> (++) "+ "
                |> Element.text
                |> Element.el
                    [ penaltyFontColor
                    , Element.width <| Element.minimum 48 Element.shrink
                    , Font.alignRight
                    ]

        timePenaltyEls =
            timePenalties |> List.map (timePenaltyView time >> Element.el [ Element.alignRight ] >> Element.inFront)
    in
    Element.row (Font.size fontSize :: Element.spacing 7 :: timePenaltyEls) [ timestamp, penalty ]
