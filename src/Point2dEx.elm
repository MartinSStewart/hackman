module Point2dEx exposing (map2)

import Point2d exposing (Point2d)


map2 : (Float -> Float -> Float) -> Point2d -> Point2d -> Point2d
map2 mapFunc p0 p1 =
    let
        ( x0, y0 ) =
            Point2d.coordinates p0

        ( x1, y1 ) =
            Point2d.coordinates p1
    in
    Point2d.fromCoordinates ( mapFunc x0 x1, mapFunc y0 y1 )
