module Benchmarks exposing (..)

import Benchmark exposing (Benchmark)
import Benchmark.Runner exposing (BenchmarkProgram, program)
import Circuit exposing (Circuit, CircuitPosition(..), WireId(..))
import Dict
import InGame.Core as Core
import Input
import Jump
import Mission
import Point2d
import Quantity
import T
import Wire


circuit : Circuit
circuit =
    { wires = [ ( 0, Wire.init Nothing [ Point2d.origin, Point2d.fromCoordinates ( 1000, 0 ) ] Nothing ) ] |> Dict.fromList
    , chips = Dict.empty
    , description = ""
    , name = ""
    , playerStart = WirePosition { wireId = WireId 0, t = Quantity.zero, data = Circuit.Forward }
    , agentsStart = []
    , triggers = Dict.empty
    }


addAgentsAlongWire agentCount circuit_ =
    { circuit_
        | agentsStart =
            List.range 1 agentCount
                |> List.map
                    (\index ->
                        Circuit.PassiveAgentStart
                            { position =
                                WirePosition { wireId = WireId 0, t = T.t (toFloat index / toFloat agentCount), data = Circuit.Forward }
                            }
                    )
    }


addWires : Int -> Circuit -> Circuit
addWires wireCount circuit_ =
    let
        wires =
            List.range 1 wireCount
                |> List.map
                    (\index ->
                        ( index
                        , Wire.init Nothing [ Point2d.origin, Point2d.fromCoordinates ( 1000, 50 + toFloat index ) ] Nothing
                        )
                    )
                |> Dict.fromList
    in
    { circuit_ | wires = Dict.union wires circuit_.wires }


addAgentsOnEndOfWire agentCount circuit_ =
    { circuit_
        | agentsStart =
            List.range 1 agentCount
                |> List.map
                    (\_ ->
                        Circuit.PassiveAgentStart
                            { position =
                                WirePosition { wireId = WireId 0, t = T.t 1, data = Circuit.Forward }
                            }
                    )
    }


agentAlongWire =
    let
        circuit_ =
            addAgentsAlongWire 1 circuit |> addWires 25
    in
    List.repeat 120 ()
        |> List.foldr (\_ state -> Core.step Input.noInput state)
            (Core.init
                { circuit = circuit_
                , lookupTable = Jump.init circuit_ |> Just
                }
            )


agentOnEndOfWire =
    let
        circuit_ =
            addAgentsOnEndOfWire 1 circuit |> addWires 25
    in
    List.repeat 120 ()
        |> List.foldr (\_ state -> Core.step Input.noInput state)
            (Core.init
                { circuit = circuit_
                , lookupTable = Jump.init circuit_ |> Just
                }
            )


main : BenchmarkProgram
main =
    program <|
        Benchmark.describe "InGame.Core.step"
            [ Benchmark.benchmark "Agents along the same wire" <|
                \() -> agentAlongWire |> Core.step Input.noInput
            , Benchmark.benchmark "Agents on the end of the same wire" <|
                \() -> agentOnEndOfWire |> Core.step Input.noInput
            ]
