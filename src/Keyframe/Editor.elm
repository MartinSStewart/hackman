module Keyframe.Editor exposing (view)

import Array
import Basics.Extra exposing (flip)
import Circle2d
import Color exposing (Color)
import Ease
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Environment
import Geometry.Svg as Svg
import Helper exposing (multVector)
import Html.Attributes
import Keyframe exposing (Transition(..))
import LineSegment2d
import ListHelper as List
import Point2d
import Polyline2d
import Quantity exposing (Quantity)
import Round
import Svg
import Svg.Attributes
import Vector2d


keyframeToPoint keyframe =
    Point2d.fromCoordinates
        ( rawQuantity keyframe.time
        , rawQuantity keyframe.value
        )


rawQuantity =
    \(Quantity.Quantity a) -> a


pointToViewCoord minX minY maxX maxY viewWidth viewHeight point =
    let
        ( x, y ) =
            Point2d.coordinates point
    in
    Point2d.fromCoordinates ( viewWidth * (x - minX) / (maxX - minX), viewHeight - viewHeight * (y - minY) / (maxY - minY) )
        |> Point2d.scaleAbout (Point2d.fromCoordinates ( viewWidth * 0.5, viewHeight * 0.5 )) 0.8


viewSingle : Float -> Float -> Float -> Float -> Float -> Float -> Float -> ( Color, Keyframe.Animation independentUnit a ) -> Svg.Svg msg
viewSingle uiScale minX minY maxX maxY viewWidth viewHeight ( color, animation ) =
    let
        keyframes =
            Keyframe.getKeyframes animation |> Array.toList

        pointToViewCoord_ =
            pointToViewCoord minX minY maxX maxY viewWidth viewHeight

        points =
            keyframes
                |> List.map
                    (\keyframe ->
                        keyframeToPoint keyframe
                            |> pointToViewCoord_
                            |> Circle2d.withRadius (uiScale / 100)
                            |> Svg.circle2d
                                [ Svg.Attributes.fill <| Color.toCssString color
                                , Svg.Attributes.overflow "visible"
                                ]
                    )

        edges =
            keyframes
                |> List.pairwise
                |> List.map
                    (\( previous, next ) ->
                        let
                            ( x0, _ ) =
                                previousPoint |> Point2d.coordinates

                            previousPoint =
                                keyframeToPoint previous

                            nextPoint =
                                keyframeToPoint next

                            xDelta =
                                Point2d.xCoordinate nextPoint - x0

                            delta =
                                Vector2d.from (keyframeToPoint previous) (keyframeToPoint next)

                            detail =
                                ceiling (50 * xDelta)

                            easeSvg easeFunc =
                                List.range 0 detail
                                    |> List.map (toFloat >> (*) (1 / toFloat detail))
                                    |> List.map
                                        (\x ->
                                            Vector2d.fromComponents ( x, easeFunc x )
                                                |> multVector delta
                                                |> flip Point2d.translateBy previousPoint
                                                |> pointToViewCoord_
                                        )
                                    |> Polyline2d.fromVertices
                                    |> Svg.polyline2d
                                        [ Svg.Attributes.stroke <| Color.toCssString color
                                        , Svg.Attributes.fill "none"
                                        , Svg.Attributes.strokeWidth <| String.fromFloat <| uiScale / 150
                                        , Svg.Attributes.overflow "visible"
                                        ]
                        in
                        case next.transition of
                            Constant ->
                                Svg.polyline2d
                                    [ Svg.Attributes.stroke <| Color.toCssString <| color
                                    , Svg.Attributes.fill "none"
                                    , Svg.Attributes.strokeWidth <| String.fromFloat <| uiScale / 150
                                    , Svg.Attributes.overflow "visible"
                                    ]
                                    (Polyline2d.fromVertices
                                        [ Point2d.fromCoordinates ( x0, Point2d.yCoordinate nextPoint ) |> pointToViewCoord_
                                        , pointToViewCoord_ nextPoint
                                        ]
                                    )

                            Linear ->
                                Svg.polyline2d
                                    [ Svg.Attributes.stroke <| Color.toCssString color
                                    , Svg.Attributes.fill "none"
                                    , Svg.Attributes.strokeWidth <| String.fromFloat <| uiScale / 150
                                    , Svg.Attributes.overflow "visible"
                                    ]
                                    (Polyline2d.fromVertices [ pointToViewCoord_ previousPoint, pointToViewCoord_ nextPoint ])

                            InExpo ->
                                easeSvg Ease.inExpo

                            OutExpo ->
                                easeSvg Ease.outExpo

                            InOutExpo ->
                                easeSvg Ease.inOutExpo

                            InBack ->
                                easeSvg Ease.inBack

                            OutBack ->
                                easeSvg Ease.outBack

                            InOutBack ->
                                easeSvg Ease.inOutBack

                            Bezier { t0, v0, t1, v1 } ->
                                let
                                    startTangent =
                                        Vector2d.fromComponents ( rawQuantity t0, rawQuantity v0 )

                                    endTangent =
                                        Vector2d.fromComponents ( rawQuantity t1, rawQuantity v1 )

                                    handle0 =
                                        startTangent |> flip Point2d.translateBy previousPoint

                                    handle1 =
                                        endTangent |> flip Point2d.translateBy previousPoint

                                    curveSvg =
                                        List.range 0 detail
                                            |> List.map (toFloat >> (*) (1 / toFloat detail))
                                            |> List.map
                                                (Keyframe.bezierCurve
                                                    previousPoint
                                                    startTangent
                                                    endTangent
                                                    nextPoint
                                                    >> pointToViewCoord_
                                                )
                                            |> Polyline2d.fromVertices
                                            |> Svg.polyline2d
                                                [ Svg.Attributes.stroke <| Color.toCssString color
                                                , Svg.Attributes.fill "none"
                                                , Svg.Attributes.strokeWidth <| String.fromFloat <| uiScale / 150
                                                , Svg.Attributes.overflow "visible"
                                                ]

                                    handles =
                                        [ handle0, handle1 ]
                                            |> List.map
                                                (pointToViewCoord_
                                                    >> Circle2d.withRadius (uiScale / 100)
                                                    >> Svg.circle2d
                                                        [ Svg.Attributes.stroke <| Color.toCssString color
                                                        , Svg.Attributes.overflow "visible"
                                                        , Svg.Attributes.strokeWidth <| flip (++) "px" <| String.fromFloat <| uiScale * 0.005
                                                        , Svg.Attributes.fill "none"
                                                        , Svg.Attributes.r <| flip (++) "px" <| String.fromFloat <| uiScale * 0.01
                                                        ]
                                                )

                                    handleLines =
                                        [ ( previousPoint, handle0 ), ( nextPoint, handle1 ) ]
                                            |> List.map
                                                (Tuple.mapBoth pointToViewCoord_ pointToViewCoord_
                                                    >> LineSegment2d.fromEndpoints
                                                    >> Svg.lineSegment2d
                                                        [ Svg.Attributes.stroke <| Color.toCssString color
                                                        , Svg.Attributes.strokeWidth <| String.fromFloat <| uiScale / 200
                                                        , Svg.Attributes.overflow "visible"
                                                        ]
                                                )
                                in
                                Svg.g [] (curveSvg :: handles ++ handleLines)

                            SineWave { amplitude, count } ->
                                List.range 0 detail
                                    |> List.map (toFloat >> (*) (1 / toFloat detail))
                                    |> List.map
                                        (\x ->
                                            let
                                                x1 =
                                                    x * 2 * pi / count

                                                offset =
                                                    Vector2d.fromComponents ( 0, sin x1 * rawQuantity amplitude )
                                            in
                                            Vector2d.fromComponents ( x, Ease.linear x )
                                                |> multVector delta
                                                |> flip Point2d.translateBy previousPoint
                                                |> Point2d.translateBy offset
                                                |> pointToViewCoord_
                                        )
                                    |> Polyline2d.fromVertices
                                    |> Svg.polyline2d
                                        [ Svg.Attributes.stroke <| Color.toCssString color
                                        , Svg.Attributes.fill "none"
                                        , Svg.Attributes.strokeWidth <| String.fromFloat <| uiScale / 150
                                        , Svg.Attributes.overflow "visible"
                                        ]
                    )
    in
    Svg.g [] (points ++ edges)


graphView : Maybe (Quantity Float independentUnit) -> List ( Color, Keyframe.Animation independentUnit a ) -> Element.Element msg
graphView currentTime animations =
    let
        currentTimeLine =
            case currentTime of
                Just currentTime_ ->
                    let
                        ( currentTimeX, _ ) =
                            Point2d.fromCoordinates ( rawQuantity currentTime_, 0 ) |> pointToViewCoord minX minY maxX maxY viewWidth viewHeight |> Point2d.coordinates
                    in
                    LineSegment2d.from
                        (Point2d.fromCoordinates ( currentTimeX, 0 ))
                        (Point2d.fromCoordinates ( currentTimeX, 1 ))
                        |> Svg.lineSegment2d
                            [ Svg.Attributes.stroke <| Color.toCssString Color.black
                            , Svg.Attributes.fill "none"
                            , Svg.Attributes.strokeWidth <| String.fromFloat <| uiScale / 250
                            ]

                Nothing ->
                    Svg.g [] []

        allKeyframes =
            animations |> List.concatMap (Tuple.second >> Keyframe.getKeyframes >> Array.toList)

        minX =
            animations
                |> List.map (Tuple.second >> Keyframe.getStartTime >> rawQuantity)
                |> List.minimum
                |> Maybe.withDefault 0

        maxX =
            allKeyframes |> List.map (.time >> rawQuantity) |> List.maximum |> Maybe.withDefault 0

        minY =
            allKeyframes |> List.map (.value >> rawQuantity) |> List.minimum |> Maybe.withDefault 0

        maxY =
            allKeyframes |> List.map (.value >> rawQuantity) |> List.maximum |> Maybe.withDefault 0

        viewWidth =
            1.5

        viewHeight =
            1

        width =
            600

        uiScale =
            400 / width

        height =
            width * viewHeight / viewWidth
    in
    Svg.svg
        [ Html.Attributes.style "width" "100%"
        , Html.Attributes.style "height" "100%"
        , Html.Attributes.style "pointer-events" "none"
        , Svg.Attributes.viewBox <| "0 0 " ++ String.fromFloat viewWidth ++ " " ++ String.fromFloat viewHeight
        ]
        (animations |> List.map (viewSingle uiScale minX minY maxX maxY viewWidth viewHeight) |> (::) currentTimeLine)
        |> Element.html
        |> Element.el
            [ Element.width <| Element.px width, Element.height <| Element.px <| round height ]


view :
    (Quantity Float independentUnit -> Float)
    -> (Quantity Float a -> Float)
    -> Maybe (Quantity Float independentUnit)
    -> List ( Color, Keyframe.Animation independentUnit a )
    -> Element msg
view independentValueToFloat quantityToFloat currentTime animations =
    if Environment.isDebug then
        Element.row
            []
            [ graphView currentTime animations
            , sidePanelView independentValueToFloat quantityToFloat currentTime animations
            ]

    else
        Element.none


sidePanelView :
    (Quantity Float independentUnit -> Float)
    -> (Quantity Float a -> Float)
    -> Maybe (Quantity Float independentUnit)
    -> List ( Color, Keyframe.Animation independentUnit a )
    -> Element msg
sidePanelView independentValueToFloat quantityToFloat currentTime animations =
    let
        attributes =
            [ Element.width <| Element.px 90
            , Element.height Element.fill
            , Element.padding 2
            , Element.spacingXY 5 0
            ]
    in
    case currentTime of
        Just currentTime_ ->
            Element.table
                attributes
                { data =
                    List.map
                        (\( color, animation ) ->
                            ( color
                            , Keyframe.valueAt currentTime_ animation |> quantityToFloat
                            )
                        )
                        animations
                , columns =
                    [ { header = Element.text "⏱️"
                      , width = Element.shrink
                      , view =
                            \( color, _ ) ->
                                Element.el
                                    [ Element.padding 2 ]
                                <|
                                    Element.el
                                        [ Element.width <| Element.px 16
                                        , Element.height <| Element.px 16
                                        , Background.color <| Helper.colorToElement color
                                        , Border.rounded 999
                                        , Element.moveUp 2
                                        ]
                                        Element.none
                      }
                    , { header = Element.text <| Round.round 2 <| independentValueToFloat currentTime_
                      , width = Element.px 70
                      , view = Tuple.second >> Round.round 2 >> Element.text
                      }
                    ]
                }

        Nothing ->
            Element.column attributes []
