module Cardinal exposing
    ( Cardinal(..)
    , all
    , direction
    , fromValue
    , mirrorX
    , mirrorY
    , reverse
    , rotateLeft
    , rotateRight
    , toValue
    , toVector2i
    )

import Direction2d exposing (Direction2d)
import Vector2i exposing (Vector2i)


type Cardinal
    = Left
    | Right
    | Top
    | Bottom


all =
    [ Top, Right, Bottom, Left ]


rotateLeft : Cardinal -> Cardinal
rotateLeft side =
    case side of
        Left ->
            Bottom

        Right ->
            Top

        Top ->
            Left

        Bottom ->
            Right


rotateRight : Cardinal -> Cardinal
rotateRight side =
    case side of
        Left ->
            Top

        Right ->
            Bottom

        Top ->
            Right

        Bottom ->
            Left


reverse : Cardinal -> Cardinal
reverse side =
    case side of
        Left ->
            Right

        Right ->
            Left

        Top ->
            Bottom

        Bottom ->
            Top


direction : Cardinal -> Direction2d
direction side =
    case side of
        Left ->
            Direction2d.unsafe ( -1, 0 )

        Right ->
            Direction2d.unsafe ( 1, 0 )

        Top ->
            Direction2d.unsafe ( 0, -1 )

        Bottom ->
            Direction2d.unsafe ( 0, 1 )


toValue : Cardinal -> number
toValue cardinal =
    case cardinal of
        Left ->
            2

        Top ->
            1

        Right ->
            0

        Bottom ->
            3


fromValue : Int -> Cardinal
fromValue int =
    case modBy 4 int of
        0 ->
            Right

        1 ->
            Top

        2 ->
            Left

        _ ->
            Bottom


mirrorX : Cardinal -> Cardinal
mirrorX cardinal =
    case cardinal of
        Left ->
            Right

        Right ->
            Left

        Top ->
            Top

        Bottom ->
            Bottom


mirrorY : Cardinal -> Cardinal
mirrorY cardinal =
    case cardinal of
        Left ->
            Left

        Right ->
            Right

        Top ->
            Bottom

        Bottom ->
            Top


toVector2i : Cardinal -> Vector2i
toVector2i cardinal =
    case cardinal of
        Left ->
            Vector2i.xy ( -1, 0 )

        Right ->
            Vector2i.xy ( 1, 0 )

        Top ->
            Vector2i.xy ( 0, -1 )

        Bottom ->
            Vector2i.xy ( 0, 1 )
