module StageSelect exposing (sounds, view)

import AssocList
import Color
import Element exposing (Element)
import Element.Background as Background
import Element.Font as Font
import Helper
import Hex
import HighscoreTable exposing (StageStatus(..))
import List.Extra as List
import List.Nonempty
import MenuState exposing (StageSelectButton(..), StageSelectPage_)
import Model exposing (Model, Msg(..))
import Quantity exposing (Quantity)
import Random
import RealTime exposing (RealTime)
import Sound
import StageData exposing (StageId)
import Style
import Tangram


view : Model -> StageSelectPage_ -> Element Msg
view model stageSelectPage =
    Element.el
        [ Element.padding 8
        , Element.width Element.fill
        , Element.height Element.fill
        ]
    <|
        Element.column
            [ Element.width Element.fill
            , Element.height Element.fill
            , Style.menuBackground
            , Style.menuFormPadding
            , Element.spacing 16
            ]
            [ stageName model
            , Element.row
                [ Element.width Element.fill, Element.height Element.fill ]
                [ Element.column
                    [ Element.width Element.fill, Element.height Element.fill ]
                    [ stageTilesView model stageSelectPage
                    , Element.row [ Element.spacing 16, Element.alignBottom ] [ backButton stageSelectPage, startButton model stageSelectPage ]
                    ]
                , Helper.filler
                , HighscoreTable.view
                    { typedHighscoreName = TypedHighscoreName
                    , pressedAcceptedHighscoreName = PressedAcceptHighscoreName
                    , pressedHighscoreSortBy = PressedHighscoreSortBy
                    }
                    model
                    model.stageSelectId
                    Quantity.zero
                    (Quantity.Quantity 1000)
                    |> Element.el [ Element.alignRight ]
                ]
            ]


backButton stageSelectPage =
    Style.button
        (Font.size 40
            :: Element.mouseOver [ Style.highlightBackground ]
            :: Style.menuFormButton_ (stageSelectPage.selectedButton == StageSelectBackButton)
        )
        { onPress = PressedStageSelectBack
        , onMouseEnter = MouseEnterStageSelectButton StageSelectBackButton |> Just
        , onMouseMove = Nothing
        , onMouseLeave = Nothing
        , label = Element.text "Back"
        }


onlyTutorialUnlocked : HighscoreTable.Model -> Bool
onlyTutorialUnlocked highscoreState =
    StageData.playableStageOrder
        |> List.Nonempty.any (\stageId -> not (stageId == StageData.tutorialId) && HighscoreTable.stageStatus stageId highscoreState /= StageLocked)
        |> not


startButton : Model -> StageSelectPage_ -> Element Msg
startButton model stageSelectPage =
    let
        isHighlighted =
            stageSelectPage.selectedButton == StageSelectStartButton

        background =
            if onlyTutorialUnlocked model.highscoreState then
                if isHighlighted then
                    [ Style.highlightBackground ]

                else
                    [ Style.attentionBackground (Color.rgba 1 1 1 0) (RealTime.inSeconds model.time) ]

            else
                []
    in
    Style.button
        (Font.size 40
            :: Element.alignBottom
            :: Element.mouseOver [ Style.highlightBackground ]
            :: Style.menuFormButton_ isHighlighted
            ++ background
        )
        { onPress = PressedStageSelectStartHacking
        , onMouseEnter = MouseEnterStageSelectButton StageSelectStartButton |> Just
        , onMouseMove = Nothing
        , onMouseLeave = Nothing
        , label = Element.text "Start Hacking"
        }


stageName : Model -> Element msg
stageName model =
    let
        stageId =
            model.stageSelectId

        stageIndex =
            StageData.playableStageOrder |> List.Nonempty.toList |> List.findIndex ((==) stageId) |> Maybe.withDefault 0

        stageName_ =
            AssocList.get stageId StageData.allStages
                |> Maybe.map .name
                |> Maybe.withDefault ""

        stageName__ =
            if stageId == StageData.tutorialId && (model.tutorialIntroShown || not (onlyTutorialUnlocked model.highscoreState)) then
                "Tutorial"

            else
                stageName_
    in
    stageName__
        |> (++) (toHex stageIndex ++ ": ")
        |> Element.text
        |> Element.el [ Font.size 40, Font.color <| Element.rgb 1 1 1 ]


stageTilesView : Model -> StageSelectPage_ -> Element Msg
stageTilesView model stageSelectPage =
    let
        stages =
            StageData.playableStageOrder
                |> List.Nonempty.toList
                |> List.indexedMap (\index stageId -> stageTileView model stageSelectPage stageId index)
    in
    List.greedyGroupsOf 4 stages
        |> List.map (Element.row [])
        |> Element.column [ Element.centerX, Element.centerY ]


toHex : Int -> String
toHex value =
    value
        |> Hex.toString
        |> String.padLeft 2 '0'
        |> String.toUpper


lockedColor =
    Color.rgb 0.3 0.3 0.3


stageTileView : Model -> StageSelectPage_ -> StageId -> Int -> Element Msg
stageTileView model stageSelectPage stageId index =
    let
        hexText =
            toHex index
                |> Element.text
                |> Element.el [ Element.centerX, Element.centerY ]

        stageStatus =
            HighscoreTable.stageStatus stageId model.highscoreState

        isHighlighted =
            case stageSelectPage.selectedButton of
                StageButton stageIndex ->
                    stageIndex == index

                _ ->
                    False

        isCurrentStage =
            model.stageSelectId == stageId

        backgroundColor =
            case stageStatus of
                StageLocked ->
                    lockedColor

                StageUnlocked ->
                    if isHighlighted then
                        Color.rgb 0.7 0.7 0.7

                    else if isCurrentStage then
                        Color.rgb 0.55 0.55 0.55

                    else
                        Color.rgb 0.5 0.5 0.5

                StageCompleted ->
                    if isHighlighted then
                        Color.rgb 0.7 1 0.7

                    else if isCurrentStage then
                        Color.rgb 0.55 0.89 0.55

                    else
                        Color.rgb 0.5 0.8 0.5

        mouseOverColor =
            case stageStatus of
                StageLocked ->
                    lockedColor

                StageUnlocked ->
                    Color.rgb 0.7 0.7 0.7

                StageCompleted ->
                    Color.rgb 0.7 1 0.7

        backgroundAttr =
            backgroundColor |> Helper.colorToElement |> Background.color

        label =
            Element.el
                ([ backgroundAttr
                 , Element.inFront <|
                    case model.stageUnlockAnimation of
                        Model.NoStageUnlocked ->
                            Element.none

                        Model.PendingStageUnlock _ ->
                            Element.none

                        Model.StageUnlocking { unlockedStages, startTime } ->
                            case unlockedStages |> List.findIndex ((==) stageId) of
                                Just unlockIndex ->
                                    Tangram.view
                                        100
                                        100
                                        (Random.initialSeed index)
                                        (stageUnlockAnimationStart startTime unlockIndex)
                                        model.time
                                        lockedColor

                                Nothing ->
                                    Element.none
                 , Element.inFront <|
                    Element.el
                        [ Font.size 70
                        , Element.centerX
                        , Element.centerY
                        , Font.color <| Element.rgba 0 0 0 0.3
                        ]
                        hexText
                 , Element.mouseOver [ Background.color <| Helper.colorToElement mouseOverColor ]
                 ]
                    ++ (if isCurrentStage then
                            [ Element.width <| Element.px 108
                            , Element.height <| Element.px 108
                            , Element.moveLeft 2
                            , Element.moveUp 54
                            ]

                        else
                            [ Element.width <| Element.px 100
                            , Element.height <| Element.px 100
                            , Element.moveRight 2
                            , Element.moveUp 50
                            ]
                       )
                )
                Element.none
    in
    Style.button
        [ Element.width <| Element.px 104
        , Element.height <| Element.px 104
        ]
        { onPress = PressedStageTile { stageId = stageId }
        , onMouseEnter = MouseEnterStageSelectButton (StageButton index) |> Just
        , onMouseMove = Nothing
        , onMouseLeave = Nothing
        , label = Element.el [ Element.inFront label ] Element.none
        }


stageUnlockAnimationStart startTime unlockIndex =
    Quantity.sum [ startTime, RealTime.seconds 1, toFloat unlockIndex * 0.2 |> RealTime.seconds ]


sounds : Model -> Sound.Model -> Sound.Model
sounds model sound =
    case model.stageUnlockAnimation of
        Model.NoStageUnlocked ->
            sound

        Model.PendingStageUnlock _ ->
            sound

        Model.StageUnlocking { unlockedStages, startTime } ->
            unlockedStages
                |> List.zip [ Sound.StageUnlock0, Sound.StageUnlock1, Sound.StageUnlock0, Sound.StageUnlock1 ]
                |> List.foldl
                    (\( soundEffect, _ ) ( index, state ) ->
                        ( index + 1, state |> Sound.play soundEffect (stageUnlockAnimationStart startTime index) )
                    )
                    ( 0, sound )
                |> Tuple.second
