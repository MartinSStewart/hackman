module InGame.Shaders exposing
    ( AgentUniforms
    , AgentVertex
    , BaseUniforms
    , ChipCapsUniforms
    , ChipMovementArrowsUniforms
    , ChipTileUniforms
    , ChipTileVertex
    , CircuitUniforms
    , GlowParticleVertex
    , GlowParticlesUniforms
    , HackSpriteUniforms
    , MapOverlayUniforms
    , PlayerUniforms
    , PlayerVertex
    , Uniforms
    , UvVertex
    , agentVS
    , chipCapsVS
    , chipMovementArrows
    , chipMovementArrowsVS
    , chipTileVS
    , circuitVS
    , fragmentShader
    , glowParticlesVS
    , hackSpriteFS
    , hackSpriteVS
    , mapOverlayVS
    , overlayFS
    , overlayVS
    , playerFS
    , playerVS
    , vertexShader
    )

import Cardinal
import Geometry.Interop.LinearAlgebra.Point2d as Point2d
import Math.Matrix4 exposing (Mat4)
import Math.Vector2 exposing (Vec2)
import Math.Vector3 exposing (Vec3)
import Math.Vector4 exposing (Vec4)
import Mesh exposing (ChipCapVertex, CircuitVertex, MapOverlayVertex, MeshPart, Vertex)
import Point2d
import Vector2d
import Vector2i
import WebGL exposing (Mesh, Shader)
import WebGL.Texture exposing (Texture)


type alias BaseUniforms a =
    { a | perspective : Mat4 }


type alias Uniforms =
    { rotation : Mat4
    , perspective : Mat4
    }


type alias CircuitUniforms =
    { perspective : Mat4
    , chipColor : Vec4
    , chipLegColor : Vec4
    , wireMiddleColor : Vec4
    , wireEdgeColor : Vec4
    }


type alias AgentUniforms =
    { perspective : Mat4
    , color : Vec4
    , instanceOffset0 : Vec3
    , instanceOffset1 : Vec3
    , instanceOffset2 : Vec3
    , instanceOffset3 : Vec3
    , instanceOffset4 : Vec3
    , instanceOffset5 : Vec3
    , instanceOffset6 : Vec3
    , instanceOffset7 : Vec3
    , instanceOffset8 : Vec3
    , instanceOffset9 : Vec3
    , instanceOffset10 : Vec3
    , instanceOffset11 : Vec3
    , instanceOffset12 : Vec3
    , instanceOffset13 : Vec3
    , instanceOffset14 : Vec3
    , instanceOffset15 : Vec3
    , instanceOffset16 : Vec3
    , instanceOffset17 : Vec3
    , instanceOffset18 : Vec3
    , instanceOffset19 : Vec3
    }


type alias GlowParticlesUniforms =
    { perspective : Mat4
    , color : Vec4
    , instanceOffset0 : Vec4
    , instanceOffset1 : Vec4
    , instanceOffset2 : Vec4
    , instanceOffset3 : Vec4
    , instanceOffset4 : Vec4
    , instanceOffset5 : Vec4
    , instanceOffset6 : Vec4
    , instanceOffset7 : Vec4
    , instanceOffset8 : Vec4
    , instanceOffset9 : Vec4
    , instanceOffset10 : Vec4
    , instanceOffset11 : Vec4
    , instanceOffset12 : Vec4
    , instanceOffset13 : Vec4
    , instanceOffset14 : Vec4
    , instanceOffset15 : Vec4
    , instanceOffset16 : Vec4
    , instanceOffset17 : Vec4
    , instanceOffset18 : Vec4
    , instanceOffset19 : Vec4
    }


type alias ChipCapsUniforms =
    { perspective : Mat4
    , previousChipId : Int
    , currentChipId : Int
    , chipEnterTime : Float
    , chipLeaveTime : Float
    , color : Vec4
    , time : Float
    }


overlayVS : Shader { position : Vec2 } { color : Vec4 } { vcolor : Vec4 }
overlayVS =
    [glsl|
        attribute vec2 position;
        uniform vec4 color;
        varying vec4 vcolor;
        void main () {
            gl_Position = vec4(position.xy, 0.0, 1.0);
            vcolor = color;
        }
    |]


overlayFS : Shader {} { color : Vec4 } { vcolor : Vec4 }
overlayFS =
    [glsl|
        precision mediump float;
        varying vec4 vcolor;
        void main () {
            gl_FragColor = vcolor;
        }
    |]


vertexShader : Shader Vertex Uniforms { vcolor : Vec4 }
vertexShader =
    [glsl|
        attribute vec3 position;
        attribute vec4 color;
        uniform mat4 perspective;
        uniform mat4 rotation;
        varying vec4 vcolor;
        void main () {
            gl_Position = perspective * rotation * vec4(position, 1.0);
            vcolor = color;
        }
    |]


circuitVS : Shader CircuitVertex CircuitUniforms { vcolor : Vec4 }
circuitVS =
    [glsl|
        attribute vec3 position;
        attribute float colorIndex;
        attribute float colorLightness;
        attribute float colorAlpha;
        uniform mat4 perspective;
        uniform vec4 chipColor;
        uniform vec4 chipLegColor;
        uniform vec4 wireMiddleColor;
        uniform vec4 wireEdgeColor;
        varying vec4 vcolor;
        void main () {
            mediump int colorIndex_ = int(colorIndex);

            gl_Position = perspective * vec4(position, 1.0);
            vcolor =
                (chipColor * float(colorIndex_ == 0)) +
                (chipLegColor * float(colorIndex_ == 1)) +
                (wireMiddleColor * float(colorIndex_ == 2)) +
                (wireEdgeColor * float(colorIndex_ == 3)) +
                (vec4(0, 0, 0, 1) * float(colorIndex_ > 3 || colorIndex_ < 0));

            vcolor *= colorAlpha;
            vcolor.xyz *= colorLightness;
        }
    |]


chipCapsVS : Shader ChipCapVertex ChipCapsUniforms { vcolor : Vec4 }
chipCapsVS =
    [glsl|
        attribute vec3 position;
        attribute float colorLightness;
        attribute float chipId;
        uniform mat4 perspective;
        uniform int previousChipId;
        uniform int currentChipId;
        uniform float chipEnterTime;
        uniform float chipLeaveTime;
        uniform float time;
        uniform vec4 color;
        varying vec4 vcolor;
        void main () {
            gl_Position = perspective * vec4(position, 1.0);
            vcolor = vec4(color.xyz * colorLightness, color.w);

            mediump float fadeLength = 0.1;

            mediump float fadeOut = int(chipId) == previousChipId
                ? clamp((time - chipLeaveTime) / fadeLength, 0.0, 1.0)
                : 1.0;

            vcolor *= int(chipId) == currentChipId
                ? clamp(1.0 - (time - chipEnterTime) / fadeLength, 0.0, 1.0)
                : fadeOut;
        }
    |]


type alias AgentVertex =
    { position : Vec3
    , instanceIndex : Float
    }


type alias GlowParticleVertex =
    { position : Vec3
    , instanceIndex : Float
    , alpha : Float
    }


type alias PlayerVertex =
    { position : Vec3
    , alpha : Float
    , uv : Vec2
    }


agentVS : Shader GlowParticleVertex AgentUniforms { vcolor : Vec4 }
agentVS =
    [glsl|
        attribute vec3 position;
        attribute float instanceIndex;
        attribute float alpha;
        uniform mat4 perspective;
        uniform vec3 instanceOffset0;
        uniform vec3 instanceOffset1;
        uniform vec3 instanceOffset2;
        uniform vec3 instanceOffset3;
        uniform vec3 instanceOffset4;
        uniform vec3 instanceOffset5;
        uniform vec3 instanceOffset6;
        uniform vec3 instanceOffset7;
        uniform vec3 instanceOffset8;
        uniform vec3 instanceOffset9;
        uniform vec3 instanceOffset10;
        uniform vec3 instanceOffset11;
        uniform vec3 instanceOffset12;
        uniform vec3 instanceOffset13;
        uniform vec3 instanceOffset14;
        uniform vec3 instanceOffset15;
        uniform vec3 instanceOffset16;
        uniform vec3 instanceOffset17;
        uniform vec3 instanceOffset18;
        uniform vec3 instanceOffset19;
        uniform vec4 color;
        varying vec4 vcolor;

        void main () {
            mediump int index = int(instanceIndex);
            mediump vec3 offset =
                (instanceOffset0 * float(index == 0)) +
                (instanceOffset1 * float(index == 1)) +
                (instanceOffset2 * float(index == 2)) +
                (instanceOffset3 * float(index == 3)) +
                (instanceOffset4 * float(index == 4)) +
                (instanceOffset5 * float(index == 5)) +
                (instanceOffset6 * float(index == 6)) +
                (instanceOffset7 * float(index == 7)) +
                (instanceOffset8 * float(index == 8)) +
                (instanceOffset9 * float(index == 9)) +
                (instanceOffset10 * float(index == 10)) +
                (instanceOffset11 * float(index == 11)) +
                (instanceOffset12 * float(index == 12)) +
                (instanceOffset13 * float(index == 13)) +
                (instanceOffset14 * float(index == 14)) +
                (instanceOffset15 * float(index == 15)) +
                (instanceOffset16 * float(index == 16)) +
                (instanceOffset17 * float(index == 17)) +
                (instanceOffset18 * float(index == 18)) +
                (instanceOffset19 * float(index == 19));
            gl_Position = perspective * vec4(position * offset.z + vec3(offset.xy, 0), 1.0);
            vcolor = color * alpha;
        }
    |]


glowParticlesVS : Shader GlowParticleVertex GlowParticlesUniforms { vcolor : Vec4 }
glowParticlesVS =
    [glsl|
        attribute vec3 position;
        attribute float instanceIndex;
        attribute float alpha;
        uniform mat4 perspective;
        // The xy components are offset, the z component is the particles scale, and the w compontent is alpha.
        uniform vec4 instanceOffset0;
        uniform vec4 instanceOffset1;
        uniform vec4 instanceOffset2;
        uniform vec4 instanceOffset3;
        uniform vec4 instanceOffset4;
        uniform vec4 instanceOffset5;
        uniform vec4 instanceOffset6;
        uniform vec4 instanceOffset7;
        uniform vec4 instanceOffset8;
        uniform vec4 instanceOffset9;
        uniform vec4 instanceOffset10;
        uniform vec4 instanceOffset11;
        uniform vec4 instanceOffset12;
        uniform vec4 instanceOffset13;
        uniform vec4 instanceOffset14;
        uniform vec4 instanceOffset15;
        uniform vec4 instanceOffset16;
        uniform vec4 instanceOffset17;
        uniform vec4 instanceOffset18;
        uniform vec4 instanceOffset19;
        uniform vec4 color;
        varying vec4 vcolor;

        void main () {
            mediump int index = int(instanceIndex);
            mediump vec4 offset =
                (instanceOffset0 * float(index == 0)) +
                (instanceOffset1 * float(index == 1)) +
                (instanceOffset2 * float(index == 2)) +
                (instanceOffset3 * float(index == 3)) +
                (instanceOffset4 * float(index == 4)) +
                (instanceOffset5 * float(index == 5)) +
                (instanceOffset6 * float(index == 6)) +
                (instanceOffset7 * float(index == 7)) +
                (instanceOffset8 * float(index == 8)) +
                (instanceOffset9 * float(index == 9)) +
                (instanceOffset10 * float(index == 10)) +
                (instanceOffset11 * float(index == 11)) +
                (instanceOffset12 * float(index == 12)) +
                (instanceOffset13 * float(index == 13)) +
                (instanceOffset14 * float(index == 14)) +
                (instanceOffset15 * float(index == 15)) +
                (instanceOffset16 * float(index == 16)) +
                (instanceOffset17 * float(index == 17)) +
                (instanceOffset18 * float(index == 18)) +
                (instanceOffset19 * float(index == 19));

            gl_Position = perspective * vec4(position * offset.z + vec3(offset.xy, 0), 1.0);
            vcolor = alpha * color * offset.w;
        }
    |]


type alias ChipTileVertex =
    { position : Vec3
    , instanceIndex : Float
    }


type alias ChipTileUniforms =
    { perspective : Mat4
    , time : Float
    , color : Vec4
    , instanceOffset0 : Vec3
    , instanceOffset1 : Vec3
    , instanceOffset2 : Vec3
    , instanceOffset3 : Vec3
    , instanceOffset4 : Vec3
    , instanceOffset5 : Vec3
    , instanceOffset6 : Vec3
    , instanceOffset7 : Vec3
    , instanceOffset8 : Vec3
    , instanceOffset9 : Vec3
    , instanceOffset10 : Vec3
    , instanceOffset11 : Vec3
    , instanceOffset12 : Vec3
    , instanceOffset13 : Vec3
    , instanceOffset14 : Vec3
    , instanceOffset15 : Vec3
    , instanceOffset16 : Vec3
    , instanceOffset17 : Vec3
    , instanceOffset18 : Vec3
    , instanceOffset19 : Vec3
    }


chipTileVS : Shader ChipTileVertex ChipTileUniforms { vcolor : Vec4 }
chipTileVS =
    [glsl|
        attribute vec3 position;
        attribute float instanceIndex;
        uniform mat4 perspective;
        uniform vec4 color;
        uniform vec3 instanceOffset0;
        uniform vec3 instanceOffset1;
        uniform vec3 instanceOffset2;
        uniform vec3 instanceOffset3;
        uniform vec3 instanceOffset4;
        uniform vec3 instanceOffset5;
        uniform vec3 instanceOffset6;
        uniform vec3 instanceOffset7;
        uniform vec3 instanceOffset8;
        uniform vec3 instanceOffset9;
        uniform vec3 instanceOffset10;
        uniform vec3 instanceOffset11;
        uniform vec3 instanceOffset12;
        uniform vec3 instanceOffset13;
        uniform vec3 instanceOffset14;
        uniform vec3 instanceOffset15;
        uniform vec3 instanceOffset16;
        uniform vec3 instanceOffset17;
        uniform vec3 instanceOffset18;
        uniform vec3 instanceOffset19;
        uniform float time;
        varying vec4 vcolor;

        void main () {
            mediump int index = int(instanceIndex);
            mediump vec3 offset =
                (instanceOffset0 * float(index == 0)) +
                (instanceOffset1 * float(index == 1)) +
                (instanceOffset2 * float(index == 2)) +
                (instanceOffset3 * float(index == 3)) +
                (instanceOffset4 * float(index == 4)) +
                (instanceOffset5 * float(index == 5)) +
                (instanceOffset6 * float(index == 6)) +
                (instanceOffset7 * float(index == 7)) +
                (instanceOffset8 * float(index == 8)) +
                (instanceOffset9 * float(index == 9)) +
                (instanceOffset10 * float(index == 10)) +
                (instanceOffset11 * float(index == 11)) +
                (instanceOffset12 * float(index == 12)) +
                (instanceOffset13 * float(index == 13)) +
                (instanceOffset14 * float(index == 14)) +
                (instanceOffset15 * float(index == 15)) +
                (instanceOffset16 * float(index == 16)) +
                (instanceOffset17 * float(index == 17)) +
                (instanceOffset18 * float(index == 18)) +
                (instanceOffset19 * float(index == 19));

            gl_Position = perspective * vec4(position + vec3(offset.xy, 0.0), 1.0);
            vcolor = color;
        }
    |]


type alias MapOverlayUniforms =
    { perspective : Mat4
    , hackedChips : Int
    , chipColor : Vec4
    , chipHackedColor : Vec4
    , wireColor : Vec4
    }


mapOverlayVS : Shader MapOverlayVertex MapOverlayUniforms { vcolor : Vec4 }
mapOverlayVS =
    [glsl|
        attribute vec3 position;
        attribute float colorIndex;
        attribute float chipId;
        uniform mat4 perspective;
        uniform int hackedChips;
        uniform vec4 chipColor;
        uniform vec4 chipHackedColor;
        uniform vec4 wireColor;
        varying vec4 vcolor;

        int AND(int n1, int n2){

            float v1 = float(n1);
            float v2 = float(n2);

            int byteVal = 1;
            int result = 0;

            for(int i = 0; i < 32; i++){
                bool keepGoing = v1>0.0 || v2 > 0.0;
                if(keepGoing){

                    bool addOn = mod(v1, 2.0) > 0.0 && mod(v2, 2.0) > 0.0;

                    if(addOn){
                        result += byteVal;
                    }

                    v1 = floor(v1 / 2.0);
                    v2 = floor(v2 / 2.0);
                    byteVal *= 2;
                } else {
                    return result;
                }
            }
            return result;
        }

        void main () {
            bool hacked = AND(int(chipId), hackedChips) != 0;

            gl_Position = perspective * vec4(position, 1.0);

            mediump int colorIndex = int(colorIndex);
            mediump vec4 color =
                chipColor * float(colorIndex == 0) +
                wireColor * float(colorIndex == 1);

            vcolor = hacked ? chipHackedColor : color;
        }
    |]


type alias UvVertex =
    { position : Vec3
    , uv : Vec2
    , instanceIndex : Float
    }


type alias HackSpriteUniforms =
    { perspective : Mat4
    , spriteSheet : Texture
    , uvTransform : Mat4
    , alpha : Float
    , instanceOffset0 : Mat4
    , instanceOffset1 : Mat4
    , instanceOffset2 : Mat4
    , instanceOffset3 : Mat4
    , instanceOffset4 : Mat4
    , instanceOffset5 : Mat4
    , instanceOffset6 : Mat4
    , instanceOffset7 : Mat4
    , instanceOffset8 : Mat4
    , instanceOffset9 : Mat4
    , instanceOffset10 : Mat4
    , instanceOffset11 : Mat4
    , instanceOffset12 : Mat4
    , instanceOffset13 : Mat4
    , instanceOffset14 : Mat4
    , instanceOffset15 : Mat4
    , instanceOffset16 : Mat4
    , instanceOffset17 : Mat4
    , instanceOffset18 : Mat4
    , instanceOffset19 : Mat4
    }


hackSpriteVS : Shader UvVertex HackSpriteUniforms { uvOutput : Vec2, alphaOutput : Float }
hackSpriteVS =
    [glsl|
        attribute vec3 position;
        attribute float instanceIndex;
        attribute vec2 uv;
        uniform mat4 perspective;
        uniform sampler2D spriteSheet;
        uniform mat4 uvTransform;
        uniform float alpha;
        uniform mat4 instanceOffset0;
        uniform mat4 instanceOffset1;
        uniform mat4 instanceOffset2;
        uniform mat4 instanceOffset3;
        uniform mat4 instanceOffset4;
        uniform mat4 instanceOffset5;
        uniform mat4 instanceOffset6;
        uniform mat4 instanceOffset7;
        uniform mat4 instanceOffset8;
        uniform mat4 instanceOffset9;
        uniform mat4 instanceOffset10;
        uniform mat4 instanceOffset11;
        uniform mat4 instanceOffset12;
        uniform mat4 instanceOffset13;
        uniform mat4 instanceOffset14;
        uniform mat4 instanceOffset15;
        uniform mat4 instanceOffset16;
        uniform mat4 instanceOffset17;
        uniform mat4 instanceOffset18;
        uniform mat4 instanceOffset19;
        varying vec2 uvOutput;
        varying float alphaOutput;

        void main () {
            mediump int index = int(instanceIndex);
            mediump mat4 offset =
                (instanceOffset0 * float(index == 0)) +
                (instanceOffset1 * float(index == 1)) +
                (instanceOffset2 * float(index == 2)) +
                (instanceOffset3 * float(index == 3)) +
                (instanceOffset4 * float(index == 4)) +
                (instanceOffset5 * float(index == 5)) +
                (instanceOffset6 * float(index == 6)) +
                (instanceOffset7 * float(index == 7)) +
                (instanceOffset8 * float(index == 8)) +
                (instanceOffset9 * float(index == 9)) +
                (instanceOffset10 * float(index == 10)) +
                (instanceOffset11 * float(index == 11)) +
                (instanceOffset12 * float(index == 12)) +
                (instanceOffset13 * float(index == 13)) +
                (instanceOffset14 * float(index == 14)) +
                (instanceOffset15 * float(index == 15)) +
                (instanceOffset16 * float(index == 16)) +
                (instanceOffset17 * float(index == 17)) +
                (instanceOffset18 * float(index == 18)) +
                (instanceOffset19 * float(index == 19));

            gl_Position = perspective * offset * vec4(position, 1.0);
            uvOutput = (uvTransform * vec4(uv, 0.0, 1.0)).xy;
            alphaOutput = alpha;
        }
    |]


fragmentShader : Shader {} (BaseUniforms a) { vcolor : Vec4 }
fragmentShader =
    [glsl|
        precision mediump float;
        varying vec4 vcolor;
        void main () {
            gl_FragColor = vcolor;
        }
    |]


type alias PlayerUniforms =
    { perspective : Mat4
    , offset : Vec3
    , color : Vec4
    , health : Float
    , spriteSheet : Texture
    , uvTransform : Mat4
    }


playerVS : Shader PlayerVertex PlayerUniforms { vcolor : Vec4, uvOutput : Vec2 }
playerVS =
    [glsl|
        attribute vec3 position;
        attribute float alpha;
        attribute vec2 uv;
        uniform mat4 perspective;
        uniform vec3 offset;
        uniform vec4 color;
        uniform mat4 uvTransform;
        varying vec4 vcolor;
        varying vec2 uvOutput;

        void main () {
            gl_Position = perspective * vec4(position * offset.z + vec3(offset.xy, 0), 1.0);
            vcolor = color * alpha;
            uvOutput = (uvTransform * vec4(uv, 0.0, 1.0)).xy;
        }
    |]


playerFS : Shader {} PlayerUniforms { vcolor : Vec4, uvOutput : Vec2 }
playerFS =
    [glsl|
        uniform sampler2D spriteSheet;

        precision mediump float;
        uniform float health;
        varying vec4 vcolor;
        varying vec2 uvOutput;

        void main () {
            float alpha = clamp(10000.0 * ((texture2D(spriteSheet, uvOutput).x + health - 1.0)), 0.4, 1.0);
            gl_FragColor = vcolor * vec4(alpha, alpha, alpha, min(1.0, alpha * 2.0));
        }
    |]


hackSpriteFS : Shader {} { a | spriteSheet : Texture } { uvOutput : Vec2, alphaOutput : Float }
hackSpriteFS =
    [glsl|
        uniform sampler2D spriteSheet;
    
        precision mediump float;
        varying vec2 uvOutput;
        varying float alphaOutput;
        void main () {
            gl_FragColor = texture2D(spriteSheet, uvOutput) * alphaOutput;
        }
    |]


type alias ChipMovementArrowsVertex =
    { position : Vec2
    , index : Float
    }


chipMovementArrows : Mesh ChipMovementArrowsVertex
chipMovementArrows =
    Cardinal.all
        |> List.indexedMap
            (\index cardinal ->
                let
                    offset =
                        Cardinal.toVector2i cardinal |> Vector2i.toVector2d

                    start =
                        Point2d.translateBy (Vector2d.scaleBy 0.4 offset) Point2d.origin

                    end =
                        Point2d.translateBy (Vector2d.scaleBy 0.9 offset) Point2d.origin
                in
                Mesh.arrowMesh 0.1 0.2 start end
                    |> Mesh.map (\v -> { position = Point2d.toVec2 v, index = toFloat index })
            )
        |> Mesh.Compound
        |> Mesh.toMesh


type alias ChipMovementArrowsUniforms =
    { playerPosition : Vec2
    , perspective : Mat4
    , showArrow0 : Float
    , showArrow1 : Float
    , showArrow2 : Float
    , showArrow3 : Float
    , color : Vec4
    }


chipMovementArrowsVS : Shader ChipMovementArrowsVertex ChipMovementArrowsUniforms { vcolor : Vec4 }
chipMovementArrowsVS =
    [glsl|
        attribute vec2 position;
        attribute float index;
        uniform mat4 perspective;
        uniform vec2 playerPosition;
        uniform float showArrow0;
        uniform float showArrow1;
        uniform float showArrow2;
        uniform float showArrow3;
        uniform vec4 color;
        varying vec4 vcolor;

        void main () {
            bool visible =
                (index == 0.0 && showArrow0 == 1.0) ||
                (index == 1.0 && showArrow1 == 1.0) ||
                (index == 2.0 && showArrow2 == 1.0) ||
                (index == 3.0 && showArrow3 == 1.0);

            gl_Position = perspective * vec4(position + playerPosition, 0.0, 1.0);
            vcolor = color * (visible ? 1.0 : 0.0);
        }
    |]
