module Tangram exposing (view)

import Angle
import Color exposing (Color)
import Direction2d
import Draw
import Element exposing (Element)
import Geometry.Svg as Svg
import Html.Attributes
import Keyframe
import Point2d
import Polygon2d
import Quantity exposing (Quantity)
import Random
import RealTime exposing (RealTime)
import Svg exposing (Svg)
import Svg.Attributes
import Vector2d


part0 =
    Polygon2d.singleLoop [ Point2d.origin, Point2d.fromCoordinates ( 0, 1 ), Point2d.fromCoordinates ( 0.5, 0.5 ) ]


part1 =
    Polygon2d.singleLoop [ Point2d.fromCoordinates ( 0, 1 ), Point2d.fromCoordinates ( 0.5, 0.5 ), Point2d.fromCoordinates ( 1, 1 ) ]


part2 =
    Polygon2d.singleLoop [ Point2d.origin, Point2d.fromCoordinates ( 0.5, 0 ), Point2d.fromCoordinates ( 0.75, 0.25 ), Point2d.fromCoordinates ( 0.25, 0.25 ) ]


part3 =
    Polygon2d.singleLoop [ Point2d.fromCoordinates ( 0.75, 0.25 ), Point2d.fromCoordinates ( 0.25, 0.25 ), Point2d.fromCoordinates ( 0.5, 0.5 ) ]


part4 =
    Polygon2d.singleLoop [ Point2d.fromCoordinates ( 0.5, 0.5 ), Point2d.fromCoordinates ( 0.75, 0.25 ), Point2d.fromCoordinates ( 1, 0.5 ), Point2d.fromCoordinates ( 0.75, 0.75 ) ]


part5 =
    Polygon2d.singleLoop [ Point2d.fromCoordinates ( 0.5, 0 ), Point2d.fromCoordinates ( 1, 0 ), Point2d.fromCoordinates ( 1, 0.5 ) ]


part6 =
    Polygon2d.singleLoop [ Point2d.fromCoordinates ( 0.75, 0.75 ), Point2d.fromCoordinates ( 1, 0.5 ), Point2d.fromCoordinates ( 1, 1 ) ]


parts =
    [ part0, part1, part2, part3, part4, part5, part6 ]


shatterAnimAlpha startTime =
    Keyframe.startAt_ startTime 1
        |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 1) 0


shatterAnimOffset : Float -> Quantity Float RealTime -> Keyframe.Animation RealTime Quantity.Unitless
shatterAnimOffset offset startTime =
    Keyframe.startAt_ startTime 0
        |> Keyframe.add_ Keyframe.OutCirc (RealTime.seconds 0.8) offset


shatterAnimRotationOffet : Quantity Float Angle.Radians -> Quantity Float RealTime -> Keyframe.Animation RealTime Angle.Radians
shatterAnimRotationOffet rotation startTime =
    Keyframe.startAt startTime (Angle.degrees 0)
        |> Keyframe.add Keyframe.OutCirc (RealTime.seconds 0.8) rotation


view : Int -> Int -> Random.Seed -> Quantity Float RealTime -> Quantity Float RealTime -> Color -> Element msg
view width height seed startTime time color =
    parts
        |> List.foldr
            (\part state ->
                let
                    rotationMax =
                        15 * distance

                    rotationAmount =
                        Random.step (Random.float -rotationMax rotationMax) state.seed |> Tuple.first |> Angle.degrees

                    ( offsetAmount, newSeed ) =
                        Random.step (Random.float 0.15 0.3) state.seed

                    offset =
                        shatterAnimOffset (offsetAmount * distance) startTime |> Keyframe.valueAt_ time

                    rotation =
                        shatterAnimRotationOffet rotationAmount startTime |> Keyframe.valueAt time |> Angle.inRadians

                    centroid =
                        Polygon2d.vertices part |> Point2d.centroid |> Maybe.withDefault Point2d.origin

                    distance =
                        Point2d.distanceFrom centroid trangramCenter

                    trangramCenter =
                        Point2d.fromCoordinates ( 0.5, 0.5 )

                    direction =
                        Direction2d.from trangramCenter centroid |> Maybe.withDefault (Direction2d.fromAngle 0) |> Direction2d.toAngle

                    svg_ : Svg msg
                    svg_ =
                        Draw.polygon color part
                            |> Svg.rotateAround centroid rotation
                            |> Svg.translateBy (Vector2d.fromPolarComponents ( offset, direction ))
                in
                { seed = newSeed, list = svg_ :: state.list }
            )
            { seed = seed, list = [] }
        |> .list
        |> Svg.svg
            [ Svg.Attributes.viewBox "0 0 1 1"
            , Html.Attributes.style "pointer-events" "none"
            , Svg.Attributes.overflow "visible"
            , Svg.Attributes.height <| String.fromInt width ++ "px"
            , Svg.Attributes.height <| String.fromInt height ++ "px"
            ]
        |> Element.html
        |> Element.el
            [ shatterAnimAlpha startTime |> Keyframe.valueAt_ time |> Element.alpha
            ]
