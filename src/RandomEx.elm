module RandomEx exposing (initialSeed1, initialSeed2, initialSeed3)

import Random


initialSeed1 : Float -> Random.Seed
initialSeed1 seed0 =
    round (seed0 * 123123) |> Random.initialSeed


initialSeed2 : Float -> Float -> Random.Seed
initialSeed2 seed0 seed1 =
    round (seed0 * 123123 + seed1 * 234234) |> Random.initialSeed


initialSeed3 : Float -> Float -> Float -> Random.Seed
initialSeed3 seed0 seed1 seed2 =
    round (seed0 * 123123 + seed1 * 234234 + seed2 * 345345) |> Random.initialSeed
