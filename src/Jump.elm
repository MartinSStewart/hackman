module Jump exposing (JumpPoint, LookupTable, agentJumpPoints, codec, init, jumpDistance)

import Basics.Extra exposing (flip)
import Circuit exposing (Circuit, Wire, WireDirection, WireId(..))
import Codec.Serialize as Codec
import CodecEx as Codec
import Dict exposing (Dict)
import Direction2d exposing (Direction2d)
import Helper
import LineSegment2d exposing (LineSegment2d)
import List.Extra as List
import ListHelper
import Maybe.Extra as Maybe
import Point2d exposing (Point2d)
import Point2i
import Quantity exposing (Quantity)
import RasterShapes
import Set
import T exposing (T(..))
import Vector2d
import Wire


type LookupTable
    = LookupTable (Dict ( Int, Int ) (List WireSegment))


codec : Codec.Codec LookupTable
codec =
    Codec.dict (Codec.tuple Codec.int Codec.int) (Codec.list wireSegmentCodec)
        |> Codec.map LookupTable (\(LookupTable a) -> a)


wireSegmentCodec =
    Codec.record WireSegment
        |> Codec.field .wireId wireIdCodec
        |> Codec.field .line Codec.lineSegment2d
        |> Codec.field .segmentIndex Codec.int
        |> Codec.finishRecord


wireIdCodec =
    Codec.int |> Codec.map WireId (\(WireId a) -> a)


type alias WireSegment =
    { wireId : WireId, line : LineSegment2d, segmentIndex : Int }


gridSize : number
gridSize =
    5


addLine : WireSegment -> Dict ( Int, Int ) (List WireSegment) -> Dict ( Int, Int ) (List WireSegment)
addLine segment dict =
    let
        start =
            LineSegment2d.startPoint segment.line |> Point2d.scaleAbout Point2d.origin (1 / gridSize) |> Point2i.roundFrom

        end =
            LineSegment2d.endPoint segment.line |> Point2d.scaleAbout Point2d.origin (1 / gridSize) |> Point2i.roundFrom
    in
    RasterShapes.line start end
        |> List.concatMap (\pos -> keysOffsets |> List.map (\( x, y ) -> ( pos.x + x, pos.y + y )))
        |> Set.fromList
        |> Set.foldl
            (\( x, y ) dict_ ->
                Dict.update
                    ( x, y )
                    (\value ->
                        case value of
                            Just list ->
                                segment :: list |> Just

                            Nothing ->
                                Just [ segment ]
                    )
                    dict_
            )
            dict


keysOffsets : List ( Int, Int )
keysOffsets =
    List.range -1 1 |> List.concatMap (\x -> List.range -1 1 |> List.map (Tuple.pair x))


getNearbyLines : LineSegment2d -> Dict ( Int, Int ) (List WireSegment) -> List WireSegment
getNearbyLines line dict =
    let
        start =
            line |> LineSegment2d.startPoint |> Point2d.scaleAbout Point2d.origin (1 / gridSize) |> Point2i.roundFrom

        end =
            line |> LineSegment2d.endPoint |> Point2d.scaleAbout Point2d.origin (1 / gridSize) |> Point2i.roundFrom

        keyLine =
            RasterShapes.line start end
    in
    keyLine
        |> List.concatMap (\{ x, y } -> Dict.get ( x, y ) dict |> Maybe.withDefault [])
        |> List.gatherEquals
        |> List.map Tuple.first


init : Circuit.WiresAndChips a -> LookupTable
init circuit =
    let
        segments : List WireSegment
        segments =
            Circuit.getWires circuit
                |> List.concatMap
                    (\( wireId, wire ) ->
                        Wire.nodePoints circuit wire
                            |> ListHelper.pairwise
                            |> List.indexedMap
                                (\index ( start, end ) ->
                                    { wireId = wireId, line = LineSegment2d.from start end, segmentIndex = index }
                                )
                    )

        intermediateLookup =
            segments |> List.foldl addLine Dict.empty
    in
    segments
        |> List.filterMap
            (\segment ->
                let
                    nearbySegments : List { wireId : WireId, line : LineSegment2d, segmentIndex : Int }
                    nearbySegments =
                        getNearbyLines segment.line intermediateLookup
                            |> List.filter
                                (\otherSegment ->
                                    if isSameWireSegment otherSegment segment then
                                        False

                                    else
                                        Helper.distanceBetweenLines segment.line otherSegment.line < jumpDistance
                                )

                    (WireId segmentWireId) =
                        segment.wireId
                in
                if List.isEmpty nearbySegments then
                    Nothing

                else
                    Just ( ( segmentWireId, segment.segmentIndex ), nearbySegments )
            )
        |> Dict.fromList
        |> LookupTable


isSameWireSegment : { a | wireId : WireId, segmentIndex : Int } -> { b | wireId : WireId, segmentIndex : Int } -> Bool
isSameWireSegment a b =
    a.wireId == b.wireId && a.segmentIndex == b.segmentIndex


candidateWireSegments : LookupTable -> Circuit.WirePosition_ a -> List WireSegment
candidateWireSegments (LookupTable lookupTable) wirePosition =
    let
        (WireId wireId) =
            wirePosition.wireId

        index =
            wirePosition.t |> T.inT |> floor

        indexCandidates =
            Dict.get ( wireId, index ) lookupTable |> Maybe.withDefault []

        -- We get the previous segment as well because if we are exactly on a node then we might pick the wrong segment.
        previousIndexCandidates =
            Dict.get ( wireId, index - 1 ) lookupTable |> Maybe.withDefault []
    in
    indexCandidates
        ++ previousIndexCandidates
        |> List.filter (isSameWireSegment { wireId = wirePosition.wireId, segmentIndex = index } >> not)


candidateWireNodes : LookupTable -> Circuit.WirePosition_ a -> List { wireId : WireId, nodeIndex : Int, position : Point2d }
candidateWireNodes lookupTable wirePosition =
    candidateWireSegments lookupTable wirePosition
        |> List.concatMap
            (\{ wireId, segmentIndex, line } ->
                [ { wireId = wireId, nodeIndex = segmentIndex, position = LineSegment2d.startPoint line }
                , { wireId = wireId, nodeIndex = segmentIndex + 1, position = LineSegment2d.endPoint line }
                ]
            )
        |> List.gatherEqualsBy (\{ wireId, nodeIndex } -> ( wireId, nodeIndex ))
        |> List.map Tuple.first


type alias JumpPoint =
    { wireId : WireId
    , t : Quantity Float T
    , direction : WireDirection
    }


jumpDistance : Float
jumpDistance =
    1.7


agentJumpPoints :
    Circuit.WiresAndChips a
    -> LookupTable
    -> Circuit.WirePosition_ WireDirection
    -> { left : Maybe ( Point2d, JumpPoint ), right : Maybe ( Point2d, JumpPoint ) }
agentJumpPoints circuit lookupTable wirePosition =
    case Circuit.getWire circuit wirePosition.wireId of
        Just wire ->
            case ( Wire.tPoint circuit wire wirePosition.t, Wire.direction circuit wire wirePosition.t wirePosition.data ) of
                ( Just startPoint, Just direction ) ->
                    let
                        endPoint rotate jumpRange_ =
                            direction
                                |> Direction2d.rotateBy rotate
                                |> Direction2d.toVector
                                |> Vector2d.scaleBy jumpRange_
                                |> flip Point2d.translateBy startPoint

                        intersection_ =
                            getIntersection circuit lookupTable wirePosition startPoint

                        jumpPoint angle jumpRange_ isLeft =
                            endPoint angle jumpRange_
                                |> intersection_ isLeft
                                |> Maybe.map (Tuple.pair startPoint)

                        normalAngle =
                            pi / 4
                    in
                    { left =
                        jumpPoint -normalAngle jumpDistance True
                    , right =
                        jumpPoint normalAngle jumpDistance False
                    }

                _ ->
                    { left = Nothing, right = Nothing }

        Nothing ->
            { left = Nothing, right = Nothing }


intersectT : LineSegment2d -> Int -> Point2d -> Point2d -> Maybe { t : Quantity Float T, intersection : Point2d }
intersectT line index start end =
    let
        lineStart =
            LineSegment2d.startPoint line

        lineEnd =
            LineSegment2d.endPoint line
    in
    LineSegment2d.from start end
        |> LineSegment2d.intersectionPoint line
        |> Maybe.map
            (\intersection ->
                { t =
                    intersection
                        |> Point2d.distanceFrom lineStart
                        |> flip (/) (Point2d.distanceFrom lineStart lineEnd)
                        |> (+) (toFloat index)
                        |> T.t
                , intersection = intersection
                }
            )


getIntersection :
    Circuit.WiresAndChips a
    -> LookupTable
    -> Circuit.WirePosition_ WireDirection
    -> Point2d
    -> Bool
    -> Point2d
    -> Maybe JumpPoint
getIntersection circuit lookupTable currentWirePosition startPoint isLeft endPoint =
    case Circuit.getWire circuit currentWirePosition.wireId of
        Just currentWire ->
            let
                wireIntersections : List { wireId : WireId, t : Quantity Float T, intersection : Point2d }
                wireIntersections =
                    candidateWireSegments lookupTable currentWirePosition
                        |> List.filterMap
                            (\{ wireId, segmentIndex, line } ->
                                case intersectT line segmentIndex startPoint endPoint of
                                    Just { t, intersection } ->
                                        Just { wireId = wireId, t = t, intersection = intersection }

                                    Nothing ->
                                        Nothing
                            )

                jumpMaxDistance =
                    Point2d.distanceFrom startPoint endPoint

                jumpDirection =
                    Direction2d.from startPoint endPoint |> Maybe.withDefault Direction2d.x

                wireNodes : List { wireId : WireId, t : Quantity Float T, intersection : Point2d }
                wireNodes =
                    candidateWireNodes lookupTable currentWirePosition
                        |> List.filterMap
                            (\{ wireId, nodeIndex, position } ->
                                let
                                    withinDistance =
                                        Point2d.distanceFrom startPoint position < jumpMaxDistance

                                    angleDelta =
                                        Direction2d.from startPoint position
                                            |> Maybe.withDefault Direction2d.x
                                            |> Direction2d.angleFrom jumpDirection
                                            |> abs
                                in
                                if withinDistance && (angleDelta < pi / 4.01) then
                                    Just { wireId = wireId, t = nodeIndex |> toFloat |> T.t, intersection = position }

                                else
                                    Nothing
                            )

                nearestJumpPoint : List { wireId : WireId, t : Quantity Float T, intersection : Point2d } -> Maybe JumpPoint
                nearestJumpPoint intersections =
                    intersections
                        |> List.minimumBy (.intersection >> Point2d.distanceFrom startPoint)
                        |> Maybe.andThen
                            (\{ wireId, t } ->
                                case Circuit.getWire circuit wireId of
                                    Just wire ->
                                        let
                                            nextWireDirection =
                                                Wire.direction circuit wire t Circuit.Forward

                                            currentWireDirection =
                                                Wire.direction circuit currentWire currentWirePosition.t currentWirePosition.data
                                        in
                                        case ( currentWireDirection, nextWireDirection ) of
                                            ( Just currentWireDirection_, Just nextWireDirection_ ) ->
                                                Just
                                                    { wireId = wireId
                                                    , t = t
                                                    , direction = jumpWireDirection currentWireDirection_ nextWireDirection_ isLeft
                                                    }

                                            _ ->
                                                Just
                                                    { wireId = wireId
                                                    , t = t
                                                    , direction = Circuit.Forward
                                                    }

                                    Nothing ->
                                        Nothing
                            )
            in
            nearestJumpPoint wireIntersections |> Maybe.orElseLazy (\() -> nearestJumpPoint wireNodes)

        Nothing ->
            Nothing


jumpWireDirection : Direction2d -> Direction2d -> Bool -> WireDirection
jumpWireDirection currentWireDirection nextWireDirection isLeft =
    let
        dir =
            Direction2d.rotateBy
                (Helper.ifElse isLeft (-pi / 16) (pi / 16))
                currentWireDirection
    in
    if
        Direction2d.angleFrom dir nextWireDirection
            |> abs
            |> (<) (pi / 2)
    then
        Circuit.Backward

    else
        Circuit.Forward
