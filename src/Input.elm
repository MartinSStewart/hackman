module Input exposing (Input, accelerate, cardinal, codec, fromKeyboard, moveDown, moveLeft, moveRight, moveUp, noInput, setInput, setShowMap, showMap)

import Bitwise
import Cardinal exposing (Cardinal(..))
import Codec.Serialize as Codec exposing (Codec)
import Helper exposing (ifElse)
import Keyboard
import KeyboardHelper as Keyboard exposing (KeyboardState)


type Input
    = Input Input_


type alias Input_ =
    { leftKey : Bool
    , rightKey : Bool
    , upKey : Bool
    , downKey : Bool
    , showMapKey : Bool
    , accelerate : Bool
    }


moveLeft : Input -> Bool
moveLeft (Input input) =
    input.leftKey


moveRight : Input -> Bool
moveRight (Input input) =
    input.rightKey


moveUp : Input -> Bool
moveUp (Input input) =
    input.upKey


moveDown : Input -> Bool
moveDown (Input input) =
    input.downKey


showMap : Input -> Bool
showMap (Input input) =
    input.showMapKey


accelerate : Input -> Bool
accelerate (Input input) =
    input.accelerate


{-| Get the cardinal direction from the input keys. Nothing is returned if no keys are down or more than one key is down.
-}
cardinal : Input -> Maybe Cardinal
cardinal (Input { leftKey, rightKey, upKey, downKey }) =
    case ( ( leftKey, rightKey ), ( upKey, downKey ) ) of
        ( ( True, False ), ( False, False ) ) ->
            Just Left

        ( ( False, True ), ( False, False ) ) ->
            Just Right

        ( ( False, False ), ( True, False ) ) ->
            Just Top

        ( ( False, False ), ( False, True ) ) ->
            Just Bottom

        _ ->
            Nothing


noInput : Input
noInput =
    { leftKey = False
    , rightKey = False
    , upKey = False
    , downKey = False
    , showMapKey = False
    , accelerate = False
    }
        |> Input


setInput : Bool -> Bool -> Bool -> Bool -> Bool -> Bool -> Input
setInput left right up down showMapKey accelerate_ =
    { leftKey = left
    , rightKey = right
    , upKey = up
    , downKey = down
    , showMapKey = showMapKey
    , accelerate = accelerate_
    }
        |> Input


setShowMap : Bool -> Input -> Input
setShowMap showMapKey (Input input) =
    Input { input | showMapKey = showMapKey }


fromKeyboard : KeyboardState a -> Input
fromKeyboard keyboard =
    { leftKey = Keyboard.isDown keyboard Keyboard.ArrowLeft
    , rightKey = Keyboard.isDown keyboard Keyboard.ArrowRight
    , upKey = Keyboard.isDown keyboard Keyboard.ArrowUp
    , accelerate = Keyboard.isDown keyboard Keyboard.ArrowUp
    , downKey = Keyboard.isDown keyboard Keyboard.ArrowDown
    , showMapKey = Keyboard.isDown keyboard Keyboard.Spacebar
    }
        |> Input


getBit : Int -> Int -> Bool
getBit index value =
    value |> Bitwise.and (Bitwise.shiftLeftBy index 1) |> Bitwise.shiftRightZfBy index |> (==) 1


codec : Codec Input
codec =
    Codec.byte
        |> Codec.map
            (\value ->
                setInput
                    (getBit 0 value)
                    (getBit 1 value)
                    (getBit 2 value)
                    (getBit 3 value)
                    (getBit 4 value)
                    (getBit 5 value)
            )
            (\input ->
                let
                    left_ =
                        ifElse (moveLeft input) 1 0

                    right_ =
                        ifElse (moveRight input) 2 0

                    up_ =
                        ifElse (moveUp input) 4 0

                    down_ =
                        ifElse (moveDown input) 8 0

                    showMap_ =
                        ifElse (showMap input) 16 0

                    accelerate_ =
                        ifElse (accelerate input) 32 0
                in
                left_ |> Bitwise.or right_ |> Bitwise.or up_ |> Bitwise.or down_ |> Bitwise.or showMap_ |> Bitwise.or accelerate_
            )
