module Editor.DragState exposing (DragState(..), getOffset, mouseDown, mouseMove, mouseUp)

import Point2d exposing (Point2d)
import Vector2d exposing (Vector2d)
import Vector2i


type DragState
    = DragNone
    | DragReady Point2d
    | Dragging Point2d


mouseDown : Point2d -> DragState
mouseDown mouseWorldPosition =
    DragReady mouseWorldPosition


mouseUp : DragState
mouseUp =
    DragNone


mouseMove : Float -> Point2d -> DragState -> DragState
mouseMove viewZoom mouseWorldPosition dragState =
    case dragState of
        DragNone ->
            DragNone

        DragReady startPos ->
            if Point2d.distanceFrom startPos mouseWorldPosition > 5 / viewZoom then
                Dragging startPos

            else
                DragReady startPos

        Dragging startPos ->
            Dragging startPos


getOffset : Bool -> Point2d -> DragState -> Vector2d
getOffset snapToGrid mouseWorldPosition dragState =
    case dragState of
        DragNone ->
            Vector2d.zero

        DragReady _ ->
            Vector2d.zero

        Dragging startPos ->
            if snapToGrid then
                Vector2d.from startPos mouseWorldPosition |> Vector2i.roundFrom |> Vector2i.toVector2d

            else
                Vector2d.from startPos mouseWorldPosition
