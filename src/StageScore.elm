module StageScore exposing (ChipMoves, HealthPoints, SortBy(..), StageScore, chipMoves, compare, healthPoints, penaltyTime, timePerBadChipMove, timePerHealthPoint, timePlusPenalty)

import InGameTime exposing (InGameTime)
import Quantity exposing (Quantity)


type alias StageScore =
    { time : Quantity Float InGameTime
    , health : Quantity Int HealthPoints
    , badChipMoves : Quantity Int ChipMoves
    , goodChipMoves : Quantity Int ChipMoves
    , name : String
    }


type HealthPoints
    = HealthPoints Never


type ChipMoves
    = ChipMoves Never


healthPoints : Int -> Quantity Int HealthPoints
healthPoints =
    Quantity.Quantity


chipMoves : Int -> Quantity Int ChipMoves
chipMoves =
    Quantity.Quantity


type SortBy
    = SortByTime
    | SortByHealth
    | SortByTimePlusPenalty


timePlusPenalty : StageScore -> Quantity Float InGameTime
timePlusPenalty score =
    let
        (Quantity.Quantity time) =
            score.time
    in
    Quantity.plus (penaltyTime score) (Quantity.Quantity time)


penaltyTime : { a | health : Quantity Int HealthPoints, badChipMoves : Quantity Int ChipMoves } -> Quantity Float InGameTime
penaltyTime score =
    let
        damagePenalty =
            Quantity.Quantity 3 |> Quantity.minus score.health |> Quantity.toFloatQuantity |> Quantity.at timePerHealthPoint

        timePenalty =
            score.badChipMoves |> Quantity.toFloatQuantity |> Quantity.at timePerBadChipMove
    in
    Quantity.plus timePenalty damagePenalty


compare : SortBy -> StageScore -> StageScore -> Order
compare sortBy a b =
    case sortBy of
        SortByTime ->
            a.time |> Quantity.compare b.time

        SortByHealth ->
            let
                health =
                    a.health |> Quantity.compare b.health
            in
            if health == EQ then
                b.time |> Quantity.compare a.time

            else
                health

        SortByTimePlusPenalty ->
            timePlusPenalty b |> Quantity.compare (timePlusPenalty a)


timePerHealthPoint : Quantity Float (Quantity.Rate InGameTime HealthPoints)
timePerHealthPoint =
    InGameTime.seconds 6 |> Quantity.per (healthPoints 1 |> Quantity.toFloatQuantity)


goodChipMovesRatio : StageScore -> Float
goodChipMovesRatio score =
    Quantity.plus score.goodChipMoves score.badChipMoves
        |> Quantity.toFloatQuantity
        |> Quantity.ratio (Quantity.toFloatQuantity score.goodChipMoves)


timePerBadChipMove : Quantity Float (Quantity.Rate InGameTime ChipMoves)
timePerBadChipMove =
    Quantity.Quantity 1 |> Quantity.per (chipMoves 1 |> Quantity.toFloatQuantity)
