module UrlDataTests exposing (..)

import AssocList
import Color
import ColorPalette
import Expect exposing (FloatingPointTolerance(..))
import Test exposing (..)
import Url
import Url.Builder
import UrlData


all : Test
all =
    describe "Url Data tests"
        [ test "parse Gitlab url" <|
            \_ -> Url.Builder.crossOrigin "https://martinsstewart.gitlab.io" [ "hackman" ] [] |> urlDataTest
        , test "parse localhost url" <|
            \_ -> Url.Builder.crossOrigin "https://localhost:3000" [] [] |> urlDataTest
        ]


urlDataTest : String -> Expect.Expectation
urlDataTest url =
    case Url.fromString url of
        Just baseUrl ->
            let
                urlData =
                    { colorPalette = ColorPalette.defaultColors }
            in
            case UrlData.toUrl baseUrl urlData |> Url.fromString of
                Just url_ ->
                    case UrlData.fromUrl url_ of
                        Ok urlData_ ->
                            urlData_.colorPalette.background
                                |> colorWithin urlData.colorPalette.background

                        Err _ ->
                            Expect.fail "Failed to parse url"

                Nothing ->
                    Expect.fail "This shouldn't happen either"

        Nothing ->
            Expect.fail "This shouldn't happen"


colorWithin : Color.Color -> Color.Color -> Expect.Expectation
colorWithin expected actual =
    let
        expected_ =
            Color.toRgba expected
    in
    Expect.all
        [ .red >> Expect.within (Absolute (1 / 255)) expected_.red
        , .green >> Expect.within (Absolute (1 / 255)) expected_.green
        , .blue >> Expect.within (Absolute (1 / 255)) expected_.blue
        , .alpha >> Expect.within (Absolute (1 / 255)) expected_.alpha
        ]
        (Color.toRgba actual)
