module HashKey exposing (generate, hashCoreModel, hashFloat, hashInt)

import Agent exposing (AgentType(..), ChipData, Health, Player, WireData, WireTransition)
import Bytes
import Bytes.Decode
import Bytes.Encode
import Cardinal exposing (Cardinal(..))
import Circuit exposing (Chip, ChipId(..), Circuit, CircuitPosition(..), WireDirection(..), WireId(..))
import Dict exposing (Dict)
import Direction2d exposing (Direction2d)
import InGame.Core as Core
import InGame.FrameNumber as FrameNumber exposing (FrameNumber)
import InGameTime exposing (InGameTime)
import LineSegment2d exposing (LineSegment2d)
import Mission exposing (MenuMission_, Mission(..), Mission1_)
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Quantity exposing (Quantity)
import Set exposing (Set)
import T exposing (T)
import Vector2i exposing (Vector2i)


type HashKey
    = HashKey Int


type alias HashFunction a =
    a -> HashKey -> HashKey


unsignedInt32Max =
    (2 ^ 32) - 1


generate : HashFunction a -> a -> Int
generate hashA value =
    HashKey 29 |> hashA value |> (\(HashKey hashKey) -> hashKey)


hashInt : HashFunction Int
hashInt value (HashKey hashKey) =
    hashKey * 17 + value * 23 |> modBy unsignedInt32Max |> HashKey


hashFloat : HashFunction Float
hashFloat value hashKey =
    Bytes.Encode.float64 Bytes.LE value
        |> Bytes.Encode.encode
        |> Bytes.Decode.decode
            (Bytes.Decode.map2 Tuple.pair (Bytes.Decode.signedInt32 Bytes.BE) (Bytes.Decode.signedInt32 Bytes.BE))
        |> Maybe.withDefault ( 0, 0 )
        |> (\( a, b ) -> hashKey |> hashInt a |> hashInt b)


hashCoreModel : HashFunction Core.Model
hashCoreModel model hashKey =
    hashKey
        |> hashPlayer (Core.getPlayer model)
        |> hashQuantityInt (Core.getCurrentFrame model)
        |> hashList hashAgentType (Core.getAgents model)
        |> hashCircuit (Core.getCircuit model)


hashList : HashFunction a -> HashFunction (List a)
hashList hashA list hashKey =
    List.foldr hashA hashKey list


hashCircuit : HashFunction Circuit
hashCircuit circuit hashKey =
    hashKey |> hashList (hashTuple hashChipId hashChip) (Circuit.getChips circuit)


hashChip : HashFunction Chip
hashChip chip hashKey =
    hashKey
        |> hashPoint2d chip.position
        |> hashVector2i chip.size
        |> hashSet (hashTuple hashInt hashInt) chip.hackedPoints


hashVector2i : HashFunction Vector2i
hashVector2i vector hashKey =
    hashKey |> hashInt vector.width |> hashInt vector.height


hashSet : HashFunction comparable -> HashFunction (Set comparable)
hashSet hashValue set hashKey =
    hashKey |> hashList hashValue (Set.toList set)


hashTuple : HashFunction a -> HashFunction b -> HashFunction ( a, b )
hashTuple hashFirst hashSecond_ ( first, second ) hashKey =
    hashKey |> hashFirst first |> hashSecond_ second


hashDict : HashFunction comparable -> HashFunction a -> HashFunction (Dict comparable a)
hashDict hashKey hashValue dict hash =
    hash |> hashList (hashTuple hashKey hashValue) (Dict.toList dict)


hashAgentType : HashFunction AgentType
hashAgentType agentType hashKey =
    case agentType of
        PassiveAgent { position } ->
            hashKey |> hashInt 0 |> hashCircuitPosition hashWireData hashChipData position

        ChaserAgent { position } ->
            hashKey |> hashInt 1 |> hashCircuitPosition hashWireData hashChipData position


hashMission : HashFunction Mission
hashMission mission hashKey =
    case mission of
        TutorialMission mission1_ ->
            hashKey |> hashInt 0 |> hashMission1 mission1_

        NormalMission ->
            hashKey |> hashInt 1

        CustomMission ->
            hashKey |> hashInt 3

        MenuMission menuMission_ ->
            hashKey |> hashInt 4 |> hashMenuMission menuMission_


hashMission1 : HashFunction Mission1_
hashMission1 mission1 hashKey =
    hashKey |> hashMaybe hashQuantity mission1.introductionStart |> hashMaybe hashQuantity mission1.controlsStart


hashMenuMission : HashFunction MenuMission_
hashMenuMission menuMission hashKey =
    hashKey |> hashQuantity menuMission.startTime |> hashLineSegment2d menuMission.cameraPath |> hashDirection2d menuMission.cameraRotation


hashLineSegment2d : HashFunction LineSegment2d
hashLineSegment2d lineSegment2d hashKey =
    hashKey |> hashPoint2d (LineSegment2d.startPoint lineSegment2d) |> hashPoint2d (LineSegment2d.endPoint lineSegment2d)


hashDirection2d : HashFunction Direction2d
hashDirection2d direction2d hashKey =
    hashKey |> hashFloat (Direction2d.angleFrom direction2d (Direction2d.fromAngle 0))


hashPlayer : HashFunction Player
hashPlayer player hashKey =
    hashKey |> hashHealth player.health |> hashCircuitPosition hashWireData hashChipData player.position


hashWireData : HashFunction WireData
hashWireData wireData hashKey =
    hashKey |> hashWireDirection wireData.direction |> hashMaybe hashTransition wireData.transitioning


hashChipData : HashFunction ChipData
hashChipData chipData hashKey =
    hashKey |> hashCardinal chipData.direction |> hashPoint2i chipData.previousPoint


hashWireDirection : HashFunction WireDirection
hashWireDirection wireDirection hashKey =
    case wireDirection of
        Forward ->
            hashKey |> hashInt 0

        Backward ->
            hashKey |> hashInt 1


hashCardinal : HashFunction Cardinal
hashCardinal cardinal hashKey =
    case cardinal of
        Left ->
            hashKey |> hashInt 0

        Right ->
            hashKey |> hashInt 1

        Top ->
            hashKey |> hashInt 2

        Bottom ->
            hashKey |> hashInt 3


hashMaybe : HashFunction a -> HashFunction (Maybe a)
hashMaybe hashA value hashKey =
    case value of
        Just a ->
            hashKey |> hashInt 0 |> hashA a

        Nothing ->
            hashKey |> hashInt 1


hashTransition : HashFunction WireTransition
hashTransition transition hashKey =
    hashKey |> hashQuantity transition.t |> hashPoint2d transition.start


hashCircuitPosition : HashFunction wire -> HashFunction chip -> HashFunction (CircuitPosition wire chip)
hashCircuitPosition hashWireData_ hashChipData_ circuitPos hashKey =
    case circuitPos of
        WirePosition { wireId, t, data } ->
            hashKey |> hashInt 0 |> hashWireId wireId |> hashQuantity t |> hashWireData_ data

        ChipPosition { chipId, position, chipData } ->
            hashKey |> hashInt 1 |> hashChipId chipId |> hashPoint2i position |> hashChipData_ chipData


hashWireId : HashFunction WireId
hashWireId (WireId wireId) hashKey =
    hashKey |> hashInt wireId


hashChipId : HashFunction ChipId
hashChipId (ChipId chipId) hashKey =
    hashKey |> hashInt chipId


hashQuantity : HashFunction (Quantity Float unit)
hashQuantity (Quantity.Quantity value) hashKey =
    hashKey |> hashFloat value


hashQuantityInt : HashFunction (Quantity Int unit)
hashQuantityInt (Quantity.Quantity value) hashKey =
    hashKey |> hashInt value


hashPoint2i : HashFunction Point2i
hashPoint2i point hashKey =
    hashKey |> hashInt point.x |> hashInt point.y


hashPoint2d : HashFunction Point2d
hashPoint2d point hashKey =
    hashKey |> hashFloat (Point2d.xCoordinate point) |> hashFloat (Point2d.yCoordinate point)


hashHealth : HashFunction Health
hashHealth health hashKey =
    case health of
        Agent.MaxHealth ->
            hashKey |> hashInt 0

        Agent.Damaged damage ->
            hashKey |> hashInt 1 |> hashInt damage.health |> hashQuantity damage.lastDamaged
