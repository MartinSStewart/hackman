module Tests exposing (all)

import Cardinal exposing (Cardinal)
import Circuit
    exposing
        ( Chip
        , ChipEdgePoint
        , ChipId(..)
        , Circuit
        , CircuitPosition(..)
        , Wire
        , WireId(..)
        )
import CircuitCodec
import Codec.Serialize as Codec
import Color
import ColorPalette
import Dict exposing (Dict)
import Expect
import ListHelper as List
import Point2d
import Set
import Test exposing (..)


all : Test
all =
    describe "A Test Suite"
        [ test "Decode stage inverses encode stage" <|
            \_ ->
                Codec.toBytes CircuitCodec.circuitVersion defaultCircuit
                    |> Codec.fromBytes CircuitCodec.circuitVersion
                    |> Expect.equal (Ok defaultCircuit)
        , test "Pairwise" <|
            \_ -> [ 1, 2, 3 ] |> List.pairwise |> Expect.equal [ ( 1, 2 ), ( 2, 3 ) ]
        , test "Color palette almostEqual" <|
            \_ ->
                let
                    palette =
                        ColorPalette.defaultColors

                    palette0 =
                        { palette | wireMiddle = Color.rgb 0 0 0 }

                    palette1 =
                        { palette | wireMiddle = Color.rgb 0 0 0.001 }
                in
                ColorPalette.almostEqual palette0 palette1 |> Expect.true "Colors are almost equal"
        , test "Color palette not almostEqual" <|
            \_ ->
                let
                    palette =
                        ColorPalette.defaultColors

                    palette0 =
                        { palette | wireMiddle = Color.rgb 0 0 0 }

                    palette1 =
                        { palette | wireMiddle = Color.rgb 0 0 0.1 }
                in
                ColorPalette.almostEqual palette0 palette1 |> Expect.false "Colors are not almost equal"
        ]


defaultCircuit : Circuit
defaultCircuit =
    { wires =
        [ ( 0
          , { start = Nothing
            , end = Nothing
            , nodes = [ Point2d.fromCoordinates ( 5, 2 ), Point2d.fromCoordinates ( 5, 4 ) ]
            }
          )
        ]
            |> Dict.fromList
    , chips =
        [ ( 0
          , { position = Point2d.fromCoordinates ( 1, 0 )
            , size = { width = 3, height = 4 }
            , hackedPoints = Set.empty
            }
          )
        ]
            |> Dict.fromList
    , name = "a"
    , description = ""
    , playerStart = Circuit.chipPosition (ChipId 0) { x = 1, y = 2 } Cardinal.Top
    , agentsStart = []
    , triggers = Dict.empty
    }
