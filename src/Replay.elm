module Replay exposing (CompleteReplay, Replay, addInput, codec, endReplay, fileExtension, getInitialState, getInput, getLastFrame, hashesAreEqual, init, replayLength, replayLength_)

import Array exposing (Array)
import Basics.Extra exposing (flip)
import Circuit exposing (Circuit)
import CircuitCodec
import Codec.Serialize as Codec exposing (Codec)
import CodecEx as Codec
import ColorPalette exposing (ColorPalette)
import Environment
import HashKey
import InGame.Core as Core
import InGame.FrameNumber as FrameNumber exposing (FrameNumber)
import InGameTime exposing (InGameTime)
import Input exposing (Input)
import Mission exposing (MenuMission_, Mission(..), Mission1_)
import Quantity exposing (Quantity)


type Replay
    = Replay Replay_


type CompleteReplay
    = CompleteReplay CompleteReplay_


type alias InitialState =
    { circuit : Circuit
    }


type alias CompleteReplay_ =
    { buildId : Int
    , lastFrameHash : Int
    , lastFrame : Maybe Core.Model
    , initialState : InitialState
    , colorPalette : ColorPalette
    , input : Array Input
    }


type alias Replay_ =
    { initialState : InitialState
    , input : List Input
    }


init : { a | circuit : Circuit } -> Replay
init { circuit } =
    { initialState = { circuit = circuit }
    , input = []
    }
        |> Replay


getInput : CompleteReplay -> Core.Model -> Input
getInput (CompleteReplay replay) coreState =
    replay.input
        |> Array.get (Core.getCurrentFrame coreState |> FrameNumber.value)
        |> Maybe.withDefault Input.noInput


getLastFrame : CompleteReplay -> Maybe Core.Model
getLastFrame (CompleteReplay replay) =
    replay.lastFrame


replayLength : CompleteReplay -> Quantity Float InGameTime
replayLength (CompleteReplay replay) =
    replay.input |> Array.length |> toFloat |> flip Quantity.multiplyBy Core.secondsPerFrame


replayLength_ : CompleteReplay -> Quantity Int FrameNumber
replayLength_ (CompleteReplay replay) =
    replay.input |> Array.length |> Quantity.Quantity


endReplay : ColorPalette -> Core.Model -> Replay -> CompleteReplay
endReplay colorPalette model (Replay replay) =
    { buildId = Environment.buildId
    , initialState = replay.initialState
    , input = replay.input |> List.reverse |> Array.fromList
    , colorPalette = colorPalette
    , lastFrameHash = HashKey.generate HashKey.hashCoreModel model
    , lastFrame = Nothing
    }
        |> CompleteReplay


hashesAreEqual : CompleteReplay -> CompleteReplay -> Bool
hashesAreEqual (CompleteReplay replay0) (CompleteReplay replay1) =
    replay0.lastFrameHash == replay1.lastFrameHash


getInitialState : CompleteReplay -> InitialState
getInitialState (CompleteReplay replay) =
    replay.initialState


addInput : Input -> Replay -> Replay
addInput input (Replay replay) =
    { initialState = replay.initialState
    , input = input :: replay.input
    }
        |> Replay


type CompleteReplayVersion
    = CompleteReplayV0 CompleteReplay


codec : Codec CompleteReplay
codec =
    Codec.customType
        (\v0Encoder value ->
            case value of
                CompleteReplayV0 value_ ->
                    v0Encoder value_
        )
        |> Codec.variant1 CompleteReplayV0 completeReplayV0Codec
        |> Codec.finishCustomType
        |> Codec.map
            (\value ->
                case value of
                    CompleteReplayV0 value_ ->
                        value_
            )
            CompleteReplayV0


completeReplayV0Codec : Codec CompleteReplay
completeReplayV0Codec =
    Codec.record CompleteReplay_
        |> Codec.field .buildId Codec.int
        |> Codec.field .lastFrameHash Codec.int
        |> Codec.field .lastFrame (Codec.maybe Core.codec)
        |> Codec.field .initialState initialStateCodec
        |> Codec.field .colorPalette ColorPalette.codec
        |> Codec.field .input (Codec.array Input.codec)
        |> Codec.finishRecord
        |> Codec.map CompleteReplay (\(CompleteReplay a) -> a)


initialStateCodec : Codec InitialState
initialStateCodec =
    Codec.record InitialState
        |> Codec.field .circuit CircuitCodec.circuitVersion
        |> Codec.finishRecord


missionCodec : Codec Mission
missionCodec =
    Codec.customType
        (\mission1 missionNormal customMission menuMission value ->
            case value of
                TutorialMission mission1_ ->
                    mission1 mission1_

                NormalMission ->
                    missionNormal

                CustomMission ->
                    customMission

                MenuMission menuMission_ ->
                    menuMission menuMission_
        )
        |> Codec.variant1 TutorialMission mission1Codec
        |> Codec.variant0 NormalMission
        |> Codec.variant0 CustomMission
        |> Codec.variant1 MenuMission menuMissionCodec
        |> Codec.finishCustomType


mission1Codec : Codec Mission1_
mission1Codec =
    Codec.record Mission1_
        |> Codec.field .introductionStart (Codec.maybe Codec.quantity)
        |> Codec.field .controlsStart (Codec.maybe Codec.quantity)
        |> Codec.finishRecord


menuMissionCodec =
    Codec.record MenuMission_
        |> Codec.field .firstMenuIntro Codec.bool
        |> Codec.field .startTime Codec.quantity
        |> Codec.field .cameraPath Codec.lineSegment2d
        |> Codec.field .cameraRotation Codec.direction2d
        |> Codec.finishRecord


fileExtension =
    ".replay"
