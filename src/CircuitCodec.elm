module CircuitCodec exposing (circuitVersion)

import Circuit
    exposing
        ( AgentStartType(..)
        , Chip
        , ChipEdgePoint
        , ChipId(..)
        , ChipPosition_
        , Circuit
        , CircuitPosition(..)
        , Trigger
        , Wire
        , WireDirection(..)
        , WireId(..)
        , WirePosition_
        )
import CircuitCodec.CircuitCodecV0 as CircuitCodecV0
import Codec.Serialize


type CircuitVersion_
    = CircuitV0 Circuit


circuitVersion : Codec.Serialize.Codec Circuit
circuitVersion =
    Codec.Serialize.customType
        (\circuitV0Encoder value ->
            case value of
                CircuitV0 circuit_ ->
                    circuitV0Encoder circuit_
        )
        |> Codec.Serialize.variant1 CircuitV0 CircuitCodecV0.circuit
        |> Codec.Serialize.finishCustomType
        |> Codec.Serialize.map (\(CircuitV0 circuit) -> circuit) CircuitV0
