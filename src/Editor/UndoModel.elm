module Editor.UndoModel exposing
    ( AgentToolItem(..)
    , Tool(..)
    , UndoModel
    , defaultZoom
    , detachedWireSnapsToChip
    , init
    , initWithCircuit
    , moveSelection
    , pasteClipboard
    , removeSelected
    , resetSelection
    , selectionToCircuit
    , setCircuit
    , setTool
    , setZoom
    , toolSelected
    , updateWireFromDetachedWireSnapsToChip
    )

import AssocList
import Basics.Extra exposing (flip)
import BoundingBox2i
import Cardinal exposing (Cardinal)
import Chip
import Circuit exposing (AgentStartType(..), Chip, ChipEdgePoint, ChipId(..), CircuitPosition(..), EdgePoint, Trigger, Wire, WireDirection, WireId(..))
import Dict exposing (Dict)
import Editor.Circuit exposing (AgentStartId, Circuit)
import Editor.Selection as Selection exposing (Selection, WireSelection)
import Editor.WirePlacer as WirePlacer
import Either exposing (Either(..))
import EverySet
import Helper exposing (ifElse)
import List.Extra as List
import ListHelper as List
import Maybe.Extra as Maybe
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Quantity exposing (Quantity)
import Set exposing (Set)
import T exposing (T)
import Vector2d exposing (Vector2d)
import Wire


type alias UndoModel =
    { circuit : Circuit

    -- Top left position of the view in world coordinates.
    , viewWorldPosition : Point2d

    -- Scale factor for the view in pixels per world unit. The larger the number the more zoomed in we are.
    , viewZoom : Float
    , showTriggers : Bool
    , selection : Selection
    , selectedTrigger : Maybe { name : String, editedName : String }
    , tool : Tool
    }


type Tool
    = HandTool
    | MoveTool
    | ChipTool
    | WireTool (Maybe WirePlacer.Model)
    | PlayerStartTool
    | AgentTool AgentToolItem
    | TriggerTool


type AgentToolItem
    = ItemPassiveAgent
    | ItemChaserAgent


setTool : Tool -> UndoModel -> UndoModel
setTool tool model =
    { model | tool = tool }


setZoom : Point2d -> Float -> UndoModel -> UndoModel
setZoom relativeTo newZoom undoModel =
    let
        newZoom_ =
            clamp 6 64 newZoom

        newViewWorldPosition =
            Vector2d.from relativeTo undoModel.viewWorldPosition
                |> Vector2d.scaleBy (undoModel.viewZoom / newZoom_)
                |> flip Point2d.translateBy relativeTo
    in
    { undoModel | viewZoom = newZoom_, viewWorldPosition = newViewWorldPosition }


setCircuit : Circuit -> UndoModel -> UndoModel
setCircuit circuit undoModel =
    { undoModel | circuit = circuit }


toolSelected : Tool -> UndoModel -> Bool
toolSelected tool model =
    case ( tool, model.tool ) of
        ( HandTool, HandTool ) ->
            True

        ( MoveTool, MoveTool ) ->
            True

        ( WireTool _, WireTool _ ) ->
            True

        ( ChipTool, ChipTool ) ->
            True

        ( PlayerStartTool, PlayerStartTool ) ->
            True

        ( TriggerTool, TriggerTool ) ->
            True

        ( AgentTool newItem, AgentTool item ) ->
            newItem == item

        ( _, _ ) ->
            False


resetSelection : UndoModel -> UndoModel
resetSelection undoModel =
    { undoModel | selection = Selection.empty }


init : UndoModel
init =
    { selection = Selection.empty
    , selectedTrigger = Nothing
    , circuit = Editor.Circuit.empty
    , viewWorldPosition = Point2d.origin
    , viewZoom = defaultZoom
    , showTriggers = True
    , tool = HandTool
    }


defaultZoom =
    16


initWithCircuit : Circuit.Circuit -> UndoModel
initWithCircuit circuit =
    { circuit =
        Editor.Circuit.from
            (Circuit.getWires circuit)
            (Circuit.getChips circuit)
            (Just circuit.playerStart)
            circuit.agentsStart
            circuit.triggers
    , viewWorldPosition = Point2d.origin
    , viewZoom = defaultZoom
    , showTriggers = True
    , selection = Selection.empty
    , selectedTrigger = Nothing
    , tool = HandTool
    }


selectionToCircuit : UndoModel -> Circuit
selectionToCircuit undoModel =
    let
        selection =
            undoModel.selection

        circuit =
            undoModel.circuit

        wireSegmentsToRemove =
            selection.wireSegments
                |> List.gatherEqualsBy Tuple.first
                |> List.concatMap
                    (\( ( wireId, wireSegment ), rest ) ->
                        case Circuit.getWire circuit wireId of
                            Just wire ->
                                wireSegment
                                    :: List.map Tuple.second rest
                                    |> Selection.negateWireSelection circuit wire
                                    |> List.map (\wireSelection -> ( wireId, wireSelection ))

                            Nothing ->
                                []
                    )
                |> (++) wiresToRemove

        wiresToRemove =
            Circuit.getWires circuit
                |> List.concatMap
                    (\( wireId, wire ) ->
                        if Selection.wireSelected selection wireId |> not then
                            Selection.selectWire circuit wire |> List.map (Tuple.pair wireId)

                        else
                            []
                    )

        chipsToRemove =
            Circuit.getChips circuit
                |> List.filterMap
                    (\( chipId, _ ) ->
                        if Selection.chipSelected selection chipId then
                            Nothing

                        else
                            Just chipId
                    )

        agentsToRemove =
            Selection.negateAgentSelection circuit selection

        playerToRemove =
            Selection.playerSelected selection |> not
    in
    removeSelected (Selection.from chipsToRemove wireSegmentsToRemove agentsToRemove.agents playerToRemove) circuit


moveChip : Vector2d -> Chip -> Chip
moveChip moveBy chip =
    { chip | position = chip.position |> Point2d.translateBy moveBy }


moveWireNodes : Vector2d -> List WireSelection -> Wire -> Wire
moveWireNodes moveBy wireSelection wire =
    let
        selectedNodes : Set Int
        selectedNodes =
            Selection.selectedWireNodes wireSelection
    in
    Wire.init
        wire.start
        (wire.nodes
            |> List.indexedMap
                (\index node ->
                    let
                        indexOffset =
                            ifElse (wire.start == Nothing) 0 1
                    in
                    if Set.member (index + indexOffset) selectedNodes then
                        Point2d.translateBy moveBy node

                    else
                        node
                )
        )
        wire.end


moveSelection : Vector2d -> Selection -> Circuit -> Circuit
moveSelection moveBy selection circuit =
    let
        wireSelection wireId =
            selection.wireSegments |> List.filter (\( wireId_, _ ) -> wireId == wireId_) |> List.map Tuple.second

        wireSelectionIds =
            selection.wireSegments |> List.map Tuple.first
    in
    circuit
        |> Editor.Circuit.updateChips (Tuple.second >> moveChip moveBy) selection.chips
        |> Editor.Circuit.updateAllWires
            (\( wireId, wire ) ->
                if List.any ((==) wireId) wireSelectionIds then
                    moveWireNodes moveBy (wireSelection wireId) wire

                else
                    let
                        maybeChipTuple =
                            selection.chips
                                |> List.find (Circuit.getChip circuit >> Maybe.andThen (detachedWireSnapsToChip wire) >> (/=) Nothing)
                                |> Maybe.andThen (\chipId -> Circuit.getChip circuit chipId |> Maybe.map (Tuple.pair chipId))
                    in
                    case maybeChipTuple of
                        Just ( chipId, chip ) ->
                            case detachedWireSnapsToChip wire chip of
                                Just detachedWireSnaps ->
                                    updateWireFromDetachedWireSnapsToChip chipId wire detachedWireSnaps

                                Nothing ->
                                    wire

                        Nothing ->
                            wire
            )


type alias DetachedWireSnapsToChip =
    { newWireStart : Maybe EdgePoint, newWireNodes : List Point2d, newWireEnd : Maybe EdgePoint }


updateWireFromDetachedWireSnapsToChip : ChipId -> Wire -> DetachedWireSnapsToChip -> Wire
updateWireFromDetachedWireSnapsToChip chipId wire { newWireStart, newWireNodes, newWireEnd } =
    let
        chipEdgePoint maybeEdgePoint =
            maybeEdgePoint |> Maybe.map (\edgePoint -> ChipEdgePoint chipId edgePoint.side edgePoint.offset)
    in
    Wire.init
        (chipEdgePoint newWireStart |> Maybe.or wire.start)
        newWireNodes
        (chipEdgePoint newWireEnd |> Maybe.or wire.end)


detachedWireSnapsToChip : Wire -> Chip -> Maybe DetachedWireSnapsToChip
detachedWireSnapsToChip wire chip =
    let
        chipSnapPoints =
            Chip.localEdgePoints chip

        findChipEdgePoint : List a -> Bool -> ( List a, Maybe EdgePoint )
        findChipEdgePoint wireNodes_ isStart =
            case ifElse isStart (Wire.startPoint wire) (Wire.endPoint wire) of
                Wire.DetachedEndPoint position ->
                    case List.find (Chip.edgePointWorldPosition chip >> Point2d.distanceFrom position >> (>) 0.2) chipSnapPoints of
                        Just edgePoint ->
                            ( if isStart then
                                List.drop 1 wireNodes_

                              else
                                List.dropLast wireNodes_
                            , Just edgePoint
                            )

                        Nothing ->
                            ( wireNodes_, Nothing )

                Wire.ChipEndPoint { side, offset } ->
                    ( wireNodes_, Nothing )

        ( wireNodes, startChipEdge ) =
            findChipEdgePoint wire.nodes True

        ( wireNodes2, endChipEdge ) =
            findChipEdgePoint wireNodes False
    in
    case ( startChipEdge, endChipEdge ) of
        ( Nothing, Nothing ) ->
            Nothing

        _ ->
            { newWireStart = startChipEdge, newWireNodes = wireNodes2, newWireEnd = endChipEdge } |> Just



--(selection.wireSegments |> List.map Tuple.first)


gatherAgentsByPosition : Circuit -> { wireAgents : Dict Int (List AgentStartType), chipAgents : Dict Int (List AgentStartType) }
gatherAgentsByPosition circuit =
    let
        groupFunc : (( AgentStartId, AgentStartType ) -> Maybe ( Int, AgentStartType )) -> Dict Int (List AgentStartType)
        groupFunc filterMapFunc =
            circuit
                |> Editor.Circuit.getAgents
                |> AssocList.toList
                |> List.filterMap filterMapFunc
                |> List.gatherEqualsBy Tuple.first
                |> List.map (\( ( key, _ ), list ) -> ( key, list |> List.map Tuple.second ))
                |> Dict.fromList
    in
    { wireAgents =
        groupFunc
            (\( _, agent ) ->
                case Circuit.getAgentStartWireId agent of
                    Just (WireId agentWireId) ->
                        Just ( agentWireId, agent )

                    Nothing ->
                        Nothing
            )
    , chipAgents =
        groupFunc
            (\( _, agent ) ->
                case Circuit.getAgentStartChipId agent of
                    Just (ChipId agentChipId) ->
                        Just ( agentChipId, agent )

                    Nothing ->
                        Nothing
            )
    }


pasteClipboard : Circuit -> UndoModel -> UndoModel
pasteClipboard clipboard model =
    let
        circuit =
            model.circuit

        newClipboard =
            Editor.Circuit.removeIdCollisions circuit clipboard

        newSelection =
            Selection.selectAll newClipboard

        newCircuit =
            { wires = circuit.wires |> Dict.union newClipboard.wires
            , chips = circuit.chips |> Dict.union newClipboard.chips
            , playerStart = circuit.playerStart |> Maybe.or newClipboard.playerStart
            , agentsStart = AssocList.union circuit.agentsStart newClipboard.agentsStart
            , triggers = circuit.triggers |> Dict.union newClipboard.triggers
            }
    in
    { model | circuit = newCircuit, selection = newSelection }


removeSelected : Selection -> Circuit -> Circuit
removeSelected selection circuit =
    let
        selectedWiresRemoved =
            removeWires selection.wireSegments circuit

        selectedChipsAndAgentsRemoved =
            { selectedWiresRemoved
                | chips =
                    circuit.chips
                        |> Dict.filter
                            (\id _ -> ChipId id |> Selection.chipSelected selection |> not)
                , agentsStart =
                    Editor.Circuit.getAgents selectedWiresRemoved
                        |> AssocList.filter (\id _ -> EverySet.member id selection.agents |> not)
                , playerStart =
                    if selection.player then
                        Nothing

                    else
                        selectedWiresRemoved.playerStart
            }

        fixedWires : List ( WireId, Wire )
        fixedWires =
            Circuit.getWires selectedChipsAndAgentsRemoved
                |> List.map
                    (Tuple.mapSecond (wireFixEnds selectedWiresRemoved selectedChipsAndAgentsRemoved))
    in
    { selectedChipsAndAgentsRemoved
        | wires = fixedWires |> List.map (Tuple.mapFirst (\(WireId wireId) -> wireId)) |> Dict.fromList
    }


removeWires : List ( WireId, WireSelection ) -> Circuit -> Circuit
removeWires wireSelection circuit =
    Circuit.getWires circuit
        |> List.concatMap
            (\( id, wire ) ->
                let
                    playerItem : List WireItemAndPos
                    playerItem =
                        case circuit.playerStart of
                            Just position ->
                                toWireItemAndPos PlayerStart position |> Maybe.toList

                            _ ->
                                []

                    toWireItemAndPos :
                        (WireDirection -> WireItems)
                        -> CircuitPosition WireDirection Cardinal
                        -> Maybe WireItemAndPos
                    toWireItemAndPos temp position =
                        case position of
                            WirePosition { wireId, t, data } ->
                                if wireId == id then
                                    Just { itemType = temp data, position = t }

                                else
                                    Nothing

                            ChipPosition _ ->
                                Nothing

                    agentItems : List WireItemAndPos
                    agentItems =
                        Editor.Circuit.getAgents circuit
                            |> AssocList.toList
                            |> List.filterMap
                                (\( agentStartId, agentStart ) ->
                                    case agentStart of
                                        PassiveAgentStart { position } ->
                                            toWireItemAndPos (PassiveAgentTemp >> AgentStart agentStartId) position

                                        ChaserAgentStart { position } ->
                                            toWireItemAndPos (ChaserAgentTemp >> AgentStart agentStartId) position
                                )
                in
                wireRemoveSegments
                    (playerItem ++ agentItems)
                    (wireSelection
                        |> List.filter (\( id_, _ ) -> id_ == id)
                        |> List.map Tuple.second
                    )
                    wire
            )
        |> List.foldl
            addWireItems
            { circuit
                | wires = Dict.empty
                , agentsStart =
                    Editor.Circuit.getAgents circuit
                        |> AssocList.filter
                            (\_ agent -> Circuit.getAgentStartPosition agent |> Circuit.isChipPosition)
                , playerStart = Maybe.filter Circuit.isChipPosition circuit.playerStart
            }
        |> (\circuit_ ->
                { circuit_
                    | playerStart =
                        if hasValidPlayerStart circuit_ then
                            circuit_.playerStart

                        else
                            Nothing
                }
           )


hasValidPlayerStart : Circuit -> Bool
hasValidPlayerStart model =
    case model.playerStart of
        Just (WirePosition { wireId, t }) ->
            case Circuit.getWire model wireId of
                Just wire ->
                    Wire.nodePoints model wire
                        |> List.length
                        |> (\maxT -> T.inT t > 0 && T.inT t <= toFloat maxT)

                Nothing ->
                    False

        Just (ChipPosition { chipId, position }) ->
            case Circuit.getChip model chipId of
                Just chip ->
                    localPointInsideChip position chip

                Nothing ->
                    False

        Nothing ->
            False


localPointInsideChip : Point2i -> Chip -> Bool
localPointInsideChip point chip =
    Chip.edgeBounds chip |> BoundingBox2i.contains point


type alias WireEdit =
    Dict Int WireEditNode


type WireEditNode
    = EditEndPoint ChipEdgePoint
    | EditNode Point2d


type alias WireItemAndPos =
    { itemType : WireItems, position : Quantity Float T }


wireRemoveSegments : List WireItemAndPos -> List WireSelection -> Wire -> List ( Wire, List WireItemAndPos )
wireRemoveSegments itemsOnWire wireSelection wire =
    let
        wireToWireEdit : Wire -> WireEdit
        wireToWireEdit wire_ =
            let
                getEndNode : Maybe ChipEdgePoint -> List WireEditNode
                getEndNode endNode =
                    endNode |> Maybe.map EditEndPoint |> Maybe.toList
            in
            (getEndNode wire_.start
                ++ List.map EditNode wire_.nodes
                ++ getEndNode wire_.end
            )
                |> List.indexedMap Tuple.pair
                |> Dict.fromList

        segments =
            Selection.selectedWireSegments wireSelection

        wireGroups : List ( ( WireEditNode, List WireItemAndPos ), List ( WireEditNode, List WireItemAndPos ) )
        wireGroups =
            wireToWireEdit wire
                |> Dict.map
                    (\k v ->
                        ( v
                        , List.filter (.position >> T.inT >> floor >> (==) k) itemsOnWire
                        )
                    )
                |> Dict.toList
                |> List.groupWhile (\( key0, _ ) ( key1, _ ) -> key0 + 1 == key1 && Set.member key0 segments |> not)
                |> List.filterMap
                    (\( ( key, head ), rest ) ->
                        if List.isEmpty rest then
                            Nothing

                        else
                            let
                                adjustT : ( WireEditNode, List WireItemAndPos ) -> ( WireEditNode, List WireItemAndPos )
                                adjustT =
                                    Tuple.mapSecond (List.map (\a -> { a | position = a.position |> Quantity.minus (toFloat key |> T.t) }))

                                newRest : List ( WireEditNode, List WireItemAndPos )
                                newRest =
                                    List.map (Tuple.second >> adjustT) rest
                            in
                            Just ( adjustT head, newRest )
                    )

        filterNodes : List ( WireEditNode, b ) -> List Point2d
        filterNodes nodes =
            List.filterMap
                (\( node, _ ) ->
                    case node of
                        EditEndPoint _ ->
                            Nothing

                        EditNode node_ ->
                            Just node_
                )
                nodes

        toWireAndItems :
            ( ( WireEditNode, List WireItemAndPos ), List ( WireEditNode, List WireItemAndPos ) )
            -> ( Wire, List WireItemAndPos )
        toWireAndItems =
            \( head, rest ) ->
                let
                    items : List WireItemAndPos
                    items =
                        List.concatMap Tuple.second (head :: rest)

                    wire_ : Wire
                    wire_ =
                        case ( head, List.reverse rest ) of
                            ( ( EditEndPoint startPoint, _ ), ( EditEndPoint endPoint, _ ) :: rest_ ) ->
                                { start = Just startPoint
                                , nodes = filterNodes rest_ |> List.reverse
                                , end = Just endPoint
                                }

                            ( ( EditEndPoint startPoint, _ ), ( EditNode _, _ ) :: _ ) ->
                                { start = Just startPoint
                                , nodes = filterNodes rest
                                , end = Nothing
                                }

                            ( ( EditEndPoint startPoint, _ ), [] ) ->
                                { start = Just startPoint
                                , nodes = []
                                , end = Nothing
                                }

                            ( ( EditNode startPoint, _ ), ( EditEndPoint endPoint, _ ) :: rest_ ) ->
                                { start = Nothing
                                , nodes = startPoint :: (filterNodes rest_ |> List.reverse)
                                , end = Just endPoint
                                }

                            ( ( EditNode startPoint, _ ), ( EditNode _, _ ) :: _ ) ->
                                { start = Nothing
                                , nodes = startPoint :: filterNodes rest
                                , end = Nothing
                                }

                            ( ( EditNode startPoint, _ ), [] ) ->
                                { start = Nothing
                                , nodes = [ startPoint ]
                                , end = Nothing
                                }
                in
                ( wire_, items )
    in
    List.map toWireAndItems wireGroups


wireFixEnds : Circuit -> Circuit -> Wire -> Wire
wireFixEnds oldCircuit circuit wire =
    let
        getWireEnd : Maybe ChipEdgePoint -> Either (Maybe ChipEdgePoint) Point2d
        getWireEnd wireEnd =
            case wireEnd of
                Just chipEdgePoint ->
                    case
                        ( Circuit.getChip oldCircuit chipEdgePoint.chipId
                        , Circuit.getChip circuit chipEdgePoint.chipId
                        )
                    of
                        -- If the chip still exists for this wire end then we don't do anything.
                        ( _, Just _ ) ->
                            Left wireEnd

                        -- If the chip was deleted then we use the world position for the wire end.
                        ( Just oldChip, Nothing ) ->
                            Chip.edgePointWorldPosition
                                oldChip
                                { side = chipEdgePoint.side
                                , offset = chipEdgePoint.offset
                                }
                                |> Right

                        -- This shouldn't happen.
                        ( Nothing, Nothing ) ->
                            Left Nothing

                Nothing ->
                    Left Nothing

        getNode : Maybe ChipEdgePoint -> Maybe Point2d
        getNode wireEnd =
            case getWireEnd wireEnd of
                Left _ ->
                    Nothing

                Right node ->
                    Just node
    in
    { wire
        | start =
            case getWireEnd wire.start of
                Left wireEnd ->
                    wireEnd

                Right _ ->
                    wire.start
        , end =
            case getWireEnd wire.end of
                Left wireEnd ->
                    wireEnd

                Right _ ->
                    wire.end
        , nodes =
            Maybe.toList (getNode wire.start)
                ++ wire.nodes
                ++ Maybe.toList (getNode wire.end)
    }


type AgentStartTypeTemp
    = PassiveAgentTemp WireDirection
    | ChaserAgentTemp WireDirection


type WireItems
    = PlayerStart WireDirection
    | AgentStart AgentStartId AgentStartTypeTemp


addWireItems : ( Wire, List { itemType : WireItems, position : Quantity Float T } ) -> Circuit -> Circuit
addWireItems ( wire, wireItems ) circuit =
    let
        ( circuit2, id ) =
            Editor.Circuit.addWire wire circuit
    in
    List.foldl
        (\{ itemType, position } circuit_ ->
            case itemType of
                PlayerStart wireData ->
                    { circuit_ | playerStart = Just (Circuit.wirePosition id position wireData) }

                AgentStart agentStartId agentStartType ->
                    case agentStartType of
                        PassiveAgentTemp wireData ->
                            { circuit_
                                | agentsStart =
                                    AssocList.insert
                                        agentStartId
                                        (PassiveAgentStart { position = Circuit.wirePosition id position wireData })
                                        circuit_.agentsStart
                            }

                        ChaserAgentTemp wireData ->
                            { circuit_
                                | agentsStart =
                                    AssocList.insert
                                        agentStartId
                                        (ChaserAgentStart { position = Circuit.wirePosition id position wireData })
                                        circuit_.agentsStart
                            }
        )
        circuit2
        wireItems
