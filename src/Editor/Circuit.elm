module Editor.Circuit exposing
    ( AgentStartId
    , Circuit
    , ToCircuitError(..)
    , addAgent
    , addChip
    , addTrigger
    , addWire
    , empty
    , from
    , getAgents
    , mergeWires
    , removeChip
    , removeIdCollisions
    , removeTrigger
    , removeWire
    , reverseWire
    , setPlayer
    , toCircuit
    , updateAllWires
    , updateChips
    , updateWires
    )

import AssocList
import Cardinal exposing (Cardinal)
import Circuit exposing (AgentStartType(..), Chip, ChipEdgePoint, ChipId(..), CircuitPosition(..), PlayerStart, Trigger, Wire, WireDirection(..), WireId(..))
import Dict exposing (Dict)
import Editor.Remap as Remap
import Helper exposing (ifElse)
import Maybe.Extra as Maybe
import Quantity
import Set exposing (Set)
import T exposing (T)
import Wire


type AgentStartId
    = AgentStartId Int


type alias Circuit =
    { wires : Dict Int Wire
    , chips : Dict Int Chip
    , playerStart : Maybe (CircuitPosition Circuit.WireDirection Cardinal)
    , agentsStart : AssocList.Dict AgentStartId AgentStartType
    , triggers : Dict String Trigger
    }


empty : Circuit
empty =
    { wires = Dict.empty
    , chips = Dict.empty
    , playerStart = Nothing
    , agentsStart = AssocList.empty
    , triggers = Dict.empty
    }


from :
    List ( WireId, Wire )
    -> List ( ChipId, Chip )
    -> Maybe PlayerStart
    -> List AgentStartType
    -> Dict String Trigger
    -> Circuit
from wires chips playerStart agentsStart triggers =
    let
        wires_ =
            wires |> List.map (Tuple.mapFirst (\(WireId a) -> a))

        chips_ =
            chips |> List.map (Tuple.mapFirst (\(ChipId a) -> a))
    in
    { wires = Dict.fromList wires_
    , chips = Dict.fromList chips_
    , playerStart = playerStart
    , agentsStart = agentsStart |> List.indexedMap (\index agent -> ( AgentStartId index, agent )) |> AssocList.fromList
    , triggers = triggers
    }


type ToCircuitError
    = AtLeastOneChip
    | MissingPlayerStart


toCircuit : Circuit -> Result ToCircuitError Circuit.Circuit
toCircuit circuit =
    case ( Dict.isEmpty circuit.chips, circuit.playerStart ) of
        ( False, Just playerStart ) ->
            { wires = circuit.wires
            , chips = circuit.chips
            , name = "Test Name"
            , description = "Test Description"
            , playerStart = playerStart
            , agentsStart = circuit.agentsStart |> AssocList.toList |> List.map Tuple.second
            , triggers = circuit.triggers
            }
                |> Ok

        ( False, Nothing ) ->
            Err MissingPlayerStart

        ( True, _ ) ->
            Err AtLeastOneChip


addWire : Wire -> Circuit -> ( Circuit, WireId )
addWire wire circuit =
    let
        wireId =
            getUniqueIdFromDict circuit.wires
    in
    ( { circuit | wires = Dict.insert wireId wire circuit.wires }
    , WireId wireId
    )


addChip : Chip -> Circuit -> ( Circuit, ChipId )
addChip chip circuit =
    let
        chipId =
            getUniqueIdFromDict circuit.chips
    in
    ( { circuit | chips = Dict.insert chipId chip circuit.chips }
    , ChipId chipId
    )


getUniqueIdFromDict : Dict Int a -> Int
getUniqueIdFromDict dict =
    let
        value =
            Dict.size dict

        getUniqueId_ n =
            if Dict.member n dict then
                getUniqueId_ (n + 1)

            else
                n
    in
    if Dict.member value dict then
        getUniqueId_ 0

    else
        value


getUniqueIdFromAssocList : (Int -> a) -> AssocList.Dict a b -> a
getUniqueIdFromAssocList keyContructor dict =
    let
        value =
            AssocList.size dict |> keyContructor

        getUniqueId_ n =
            if AssocList.member (keyContructor n) dict then
                getUniqueId_ (n + 1)

            else
                keyContructor n
    in
    if AssocList.member value dict then
        getUniqueId_ 0

    else
        value


addTrigger : Trigger -> Circuit -> ( Circuit, String )
addTrigger trigger circuit =
    let
        id =
            getUniqueTriggerId circuit
    in
    ( { circuit | triggers = Dict.insert id trigger circuit.triggers }
    , id
    )


removeChip : ChipId -> Circuit -> Circuit
removeChip ((ChipId chipId_) as chipId) circuit =
    { circuit
        | chips = Dict.remove chipId_ circuit.chips
        , agentsStart = circuit.agentsStart |> AssocList.filter (\_ agent -> agentPosition agent |> positionOnChip chipId |> not)
        , playerStart = circuit.playerStart |> Maybe.filter (positionOnChip chipId >> not)
    }


removeWire : WireId -> Circuit -> Circuit
removeWire ((WireId wireId_) as wireId) circuit =
    { circuit
        | wires = Dict.remove wireId_ circuit.wires
        , agentsStart = circuit.agentsStart |> AssocList.filter (\_ agent -> agentPosition agent |> positionOnWire wireId |> not)
        , playerStart = circuit.playerStart |> Maybe.filter (positionOnWire wireId >> not)
    }


removeWire_ : WireId -> Dict Int Wire -> Dict Int Wire
removeWire_ (WireId wireId) wires =
    Dict.remove wireId wires


addWire_ : WireId -> Wire -> Dict Int Wire -> Dict Int Wire
addWire_ (WireId wireId) wire wires =
    Dict.insert wireId wire wires


positionOnWire : WireId -> CircuitPosition a b -> Bool
positionOnWire wireId position =
    case position of
        Circuit.WirePosition wirePosition ->
            wireId == wirePosition.wireId

        Circuit.ChipPosition _ ->
            False


positionOnChip : ChipId -> CircuitPosition a b -> Bool
positionOnChip chipId position =
    case position of
        Circuit.WirePosition _ ->
            False

        Circuit.ChipPosition chipPosition ->
            chipId == chipPosition.chipId


agentPosition : AgentStartType -> CircuitPosition WireDirection Cardinal
agentPosition agent =
    case agent of
        PassiveAgentStart { position } ->
            position

        ChaserAgentStart { position } ->
            position


removeTrigger : String -> Circuit -> Circuit
removeTrigger triggerId circuit =
    { circuit | triggers = Dict.remove triggerId circuit.triggers }


getUniqueTriggerId : Circuit -> String
getUniqueTriggerId circuit =
    let
        getUniqueTriggerId_ index =
            let
                id =
                    ifElse (index == 0) "New Trigger" ("New Trigger " ++ String.fromInt index)
            in
            if Dict.member id circuit.triggers then
                getUniqueTriggerId_ (index + 1)

            else
                id
    in
    getUniqueTriggerId_ 0


addAgent : AgentStartType -> Circuit -> Circuit
addAgent agentStart circuit =
    let
        agentId =
            getUniqueIdFromAssocList AgentStartId circuit.agentsStart
    in
    { circuit | agentsStart = circuit.agentsStart |> AssocList.insert agentId agentStart }


getAgents : Circuit -> AssocList.Dict AgentStartId AgentStartType
getAgents circuit =
    circuit.agentsStart


setPlayer : Maybe PlayerStart -> Circuit -> Circuit
setPlayer playerStart circuit =
    { circuit | playerStart = playerStart }


reverseWire : WireId -> Circuit -> Circuit
reverseWire wireId circuit =
    case Circuit.getWire circuit wireId of
        Just wire ->
            { circuit
                | wires = circuit.wires |> removeWire_ wireId |> addWire_ wireId (Wire.reverse wire)
                , chips = circuit.chips
                , playerStart = Maybe.map (reverseWirePosition circuit wireId wire) circuit.playerStart
                , agentsStart = AssocList.map (\_ agent -> updateAgentPosition (reverseWirePosition circuit wireId wire) agent) circuit.agentsStart
            }

        Nothing ->
            circuit


reverseWirePosition : Circuit -> WireId -> Wire -> CircuitPosition WireDirection a -> CircuitPosition WireDirection a
reverseWirePosition circuit wireId wire position =
    case position of
        WirePosition wirePosition ->
            if wireId == wirePosition.wireId then
                Circuit.wirePosition
                    wirePosition.wireId
                    (Wire.reverseT circuit wire wirePosition.t)
                    (reverseWireDirection wirePosition.data)

            else
                position

        ChipPosition _ ->
            position


reverseWireDirection : WireDirection -> WireDirection
reverseWireDirection wireDirection =
    case wireDirection of
        Forward ->
            Backward

        Backward ->
            Forward


{-| Joins the end node of the first wire with the start node of the second wire.
-}
mergeWires : WireId -> WireId -> Circuit -> Maybe ( Circuit, WireId )
mergeWires firstWireId secondWireId circuit =
    case ( Circuit.getWire circuit firstWireId, Circuit.getWire circuit secondWireId ) of
        ( Just firstWire, Just secondWire ) ->
            let
                newWire =
                    Wire.init firstWire.start (firstWire.nodes ++ secondWire.nodes) secondWire.end

                offset =
                    List.length firstWire.nodes
                        + ifElse (firstWire.start == Nothing) 0 1
                        - ifElse (secondWire.start == Nothing) 0 1
                        |> toFloat
                        |> T.t

                offsetWirePosition : WireId -> CircuitPosition a b -> CircuitPosition a b
                offsetWirePosition wireId position =
                    case position of
                        WirePosition wirePosition ->
                            if wireId == wirePosition.wireId then
                                Circuit.wirePosition firstWireId (Quantity.plus wirePosition.t offset) wirePosition.data

                            else
                                position

                        ChipPosition _ ->
                            position
            in
            ( { circuit
                | wires =
                    circuit.wires
                        |> removeWire_ firstWireId
                        |> removeWire_ secondWireId
                        |> addWire_ firstWireId newWire
                , chips = circuit.chips
                , playerStart = Maybe.map (offsetWirePosition secondWireId) circuit.playerStart
                , agentsStart = AssocList.map (\_ agent -> updateAgentPosition (offsetWirePosition secondWireId) agent) circuit.agentsStart
              }
            , firstWireId
            )
                |> Just

        _ ->
            Nothing


updateAgentPosition :
    (CircuitPosition WireDirection Cardinal -> CircuitPosition WireDirection Cardinal)
    -> AgentStartType
    -> AgentStartType
updateAgentPosition updatePosition agentStartType =
    case agentStartType of
        PassiveAgentStart { position } ->
            PassiveAgentStart { position = updatePosition position }

        ChaserAgentStart { position } ->
            ChaserAgentStart { position = updatePosition position }


shiftWireIdId : Dict Int Int -> WireId -> WireId
shiftWireIdId wireIdMap (WireId wireId) =
    Dict.get wireId wireIdMap |> Maybe.withDefault 0 |> WireId


shiftChipIdId : Dict Int Int -> ChipId -> ChipId
shiftChipIdId chipIdMap (ChipId chipId) =
    Dict.get chipId chipIdMap |> Maybe.withDefault 0 |> ChipId


shiftCircuitPositionId : Dict Int Int -> Dict Int Int -> CircuitPosition a b -> CircuitPosition a b
shiftCircuitPositionId wireIdMap chipIdMap circuitPosition =
    case circuitPosition of
        WirePosition wirePosition ->
            WirePosition { wirePosition | wireId = shiftWireIdId wireIdMap wirePosition.wireId }

        ChipPosition chipPosition ->
            ChipPosition { chipPosition | chipId = shiftChipIdId chipIdMap chipPosition.chipId }


shiftAgentId : Dict Int Int -> Dict Int Int -> AssocList.Dict AgentStartId AgentStartId -> ( AgentStartId, AgentStartType ) -> ( AgentStartId, AgentStartType )
shiftAgentId wireIdMap chipIdMap agentIdMap ( agentId, agentStart ) =
    ( AssocList.get agentId agentIdMap |> Maybe.withDefault (AgentStartId 0)
    , case agentStart of
        PassiveAgentStart passiveAgent ->
            PassiveAgentStart { passiveAgent | position = shiftCircuitPositionId wireIdMap chipIdMap passiveAgent.position }

        ChaserAgentStart chaserAgent ->
            ChaserAgentStart { chaserAgent | position = shiftCircuitPositionId wireIdMap chipIdMap chaserAgent.position }
    )


shiftChipEdgePointId : Dict Int Int -> ChipEdgePoint -> ChipEdgePoint
shiftChipEdgePointId chipIdMap chipEdgePoint =
    let
        (ChipId chipId) =
            chipEdgePoint.chipId
    in
    { chipEdgePoint | chipId = Dict.get chipId chipIdMap |> Maybe.withDefault 0 |> ChipId }


shiftWireId : Dict Int Int -> Dict Int Int -> ( Int, Wire ) -> ( Int, Wire )
shiftWireId wireIdMap chipIdMap ( wireId, wire ) =
    ( Dict.get wireId wireIdMap |> Maybe.withDefault 0
    , Wire.init
        (case wire.start of
            Just chipEdgePoint ->
                shiftChipEdgePointId chipIdMap chipEdgePoint |> Just

            Nothing ->
                Nothing
        )
        wire.nodes
        (case wire.end of
            Just chipEdgePoint ->
                shiftChipEdgePointId chipIdMap chipEdgePoint |> Just

            Nothing ->
                Nothing
        )
    )


removeIdCollisions : Circuit -> Circuit -> Circuit
removeIdCollisions target circuit =
    let
        wireIdMap =
            Remap.remapDict circuit.wires target.wires

        chipIdMap =
            Remap.remapDict circuit.chips target.chips

        agentIdMap =
            Remap.remapAssocList AgentStartId circuit.agentsStart target.agentsStart
    in
    { wires = circuit.wires |> Dict.toList |> List.map (shiftWireId wireIdMap chipIdMap) |> Dict.fromList
    , chips = circuit.chips |> Dict.toList |> List.map (Tuple.mapFirst (\key -> Dict.get key chipIdMap |> Maybe.withDefault 0)) |> Dict.fromList
    , playerStart = circuit.playerStart |> Maybe.map (shiftCircuitPositionId wireIdMap chipIdMap)
    , agentsStart = circuit.agentsStart |> AssocList.toList |> List.map (shiftAgentId wireIdMap chipIdMap agentIdMap) |> AssocList.fromList
    , triggers = circuit.triggers
    }


updateChips : (( ChipId, Chip ) -> Chip) -> List ChipId -> Circuit -> Circuit
updateChips chipFunc chipsToUpdate circuit =
    let
        chipIdSet : Set Int
        chipIdSet =
            chipsToUpdate |> List.map (\(ChipId id) -> id) |> Set.fromList

        newChips : Dict Int Chip
        newChips =
            Dict.toList circuit.chips
                |> List.map
                    (\( chipId, chip ) ->
                        if Set.member chipId chipIdSet then
                            ( chipId, chipFunc ( ChipId chipId, chip ) )

                        else
                            ( chipId, chip )
                    )
                |> Dict.fromList
    in
    { circuit | chips = newChips }


updateWires : (( WireId, Wire ) -> Wire) -> List WireId -> Circuit -> Circuit
updateWires wireFunc wireNodesToUpdate circuit =
    let
        wireSet =
            wireNodesToUpdate |> List.map (\(WireId id) -> id) |> Set.fromList

        newWires =
            Dict.toList circuit.wires
                |> List.map
                    (\( wireId, wire ) ->
                        if Set.member wireId wireSet then
                            ( wireId, wireFunc ( WireId wireId, wire ) )

                        else
                            ( wireId, wire )
                    )
                |> Dict.fromList
    in
    { circuit | wires = newWires }


updateAllWires : (( WireId, Wire ) -> Wire) -> Circuit -> Circuit
updateAllWires wireFunc circuit =
    let
        newWires =
            Dict.toList circuit.wires
                |> List.map
                    (\( wireId, wire ) ->
                        ( wireId, wireFunc ( WireId wireId, wire ) )
                    )
                |> Dict.fromList
    in
    { circuit | wires = newWires }
