import './main.css';
import { Elm } from './Main.elm';
import registerServiceWorker from './registerServiceWorker';
import Pizzicato from 'pizzicato'

function mod(n, m) {
  return ((n % m) + m) % m;
}

let saveData = localStorage.getItem('saveData');

let app = Elm.Main.init({
    node: document.getElementById('root'),
    flags: {
        windowWidth: window.innerWidth,
        windowHeight: window.innerHeight,
        seed: Math.floor(Math.random() * 1000000),
        saveData: saveData,
        url: window.location.href
    }
});

window.addEventListener("beforeunload", function (e) {
    var confirmationMessage = 'It looks like you have been editing something. '
                            + 'If you leave before saving, your changes will be lost.';

    (e || window.event).returnValue = confirmationMessage; //Gecko + IE
    return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
});

let loadedSounds = [];

let voices = null;
if (window.speechSynthesis) {
    voices = window.speechSynthesis.getVoices();
}

app.ports.toJsPort.subscribe( ( message ) => {
    let messageType = message.Constructor;
    switch (messageType)
    {
        case "LoadSound":
        {
            let soundData = new Pizzicato.Sound({
                source: 'file',
                options: { path: message.soundPath, loop: message.loops }
            }, function(error) {
                if (!error) {
                    loadedSounds = loadedSounds.concat(soundData);
                    app.ports.fromJsPort.send({
                        Constructor: "SoundLoaded",
                        soundPath: message.soundPath,
                        result: { Constructor: "Ok", value: loadedSounds.length - 1 }
                    });
                }
                else {
                    app.ports.fromJsPort.send({
                        Constructor: "SoundLoaded",
                        soundPath: message.soundPath,
                        result: { Constructor: "Err", value: error }
                    });
                }
            });
            break;
        }
        case "UpdateSounds":
        {
            message.updates.forEach((item, _) => {
                let index = item.soundId
                switch (item.type)
                {
                    case 0:
                    {
                        let duration = loadedSounds[index].getRawSourceNode().buffer.duration;
                        loadedSounds[index].play(item.delay, mod(item.offset, duration));
                        break;
                    }
                    case 1:
                    {
                        loadedSounds[index].stop();
                        break;
                    }
                    case 2:
                    {
                        loadedSounds[index].volume = item.volume;
                        break;
                    }
                    case 3:
                    {
                        loadedSounds[index].sourceNode.playbackRate.value = item.playbackRate;
                        break;
                    }
                    case 4:
                    {
                        if (voices) {
                            let speech = new SpeechSynthesisUtterance(item.text);
                            speech.volume = item.volume;
                            let targetVoice = voices.find(function(voice) { return voice.name === "Karen" } );
                            if (targetVoice !== null) {
                                speech.voice = targetVoice;
                            }

                            let speak = function() {
                                window.speechSynthesis.speak(speech);
                            };
                            setTimeout(speak, item.delay * 1000);
                        }

                        break;
                    }
                    case 5:
                    {
                        //Stopping speech is not implemented yet.
                        break;
                    }
                    default:
                    {
                        debugger;
                    }
                }
            });
            break;
        }
        case "SaveSaveData":
        {
            localStorage.setItem('saveData', message.saveData);
            break;
        }
        case "SelectTextInput":
        {
            let textInput = document.getElementById(message.textInputId);
            if (textInput)
            {
                textInput.focus();
                textInput.select();
            }
            break;
        }
        default:
            console.log("Message type \"" + messageType + "\" was not handled.");
            debugger;
            break;
    }
});

registerServiceWorker();
