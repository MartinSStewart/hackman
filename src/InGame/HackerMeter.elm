module InGame.HackerMeter exposing (Model, init, step, view)

import Basics.Extra exposing (fractionalModBy)
import Chip
import Circuit exposing (ChipId, CircuitPosition(..))
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Helper
import InGame.Core as Core exposing (MissionStatus(..))
import Quantity exposing (Quantity, Unitless)


type alias Model =
    { offset : Quantity Float Unitless
    , currentHackRatio : Quantity Float Unitless
    , lastChipId : Maybe ChipId
    }


init : Model
init =
    { offset = Quantity.zero
    , currentHackRatio = Quantity.zero
    , lastChipId = Nothing
    }


step : Core.Model -> Model -> Model
step coreState hackerMeter =
    let
        ( targetHackRatio, hackRatio, ( targetOffset, newLastChipId ) ) =
            case Core.getPlayer coreState |> .position of
                ChipPosition { chipId } ->
                    let
                        hackRatio_ =
                            if Just chipId == hackerMeter.lastChipId then
                                hackerMeter.currentHackRatio

                            else
                                Quantity.zero
                    in
                    case Circuit.getChip (Core.getCircuit coreState) chipId of
                        Just chip ->
                            ( (Chip.tilesHacked chip |> toFloat)
                                / (Chip.totalTilesNeededToHack chip |> toFloat)
                                |> clamp 0 1
                                |> Quantity.float
                            , hackRatio_
                            , ( 1
                              , Just chipId
                              )
                            )

                        Nothing ->
                            ( Quantity.zero, hackRatio_, ( 0, Nothing ) )

                WirePosition _ ->
                    ( hackerMeter.currentHackRatio, hackerMeter.currentHackRatio, ( 0, hackerMeter.lastChipId ) )
    in
    { offset = Helper.stepTowards (Quantity.float 0.1) (Quantity.float targetOffset) hackerMeter.offset
    , currentHackRatio = Helper.stepTowards (Quantity.float 0.03) targetHackRatio hackRatio
    , lastChipId = newLastChipId
    }


view : Int -> Model -> Core.Model -> Element msg
view width hackerMeter coreState =
    let
        textElement =
            Element.el
                [ Font.color <| Element.rgb 1 1 1
                , Font.glow (Element.rgb 0 0 0) 5
                , Element.centerX
                , Element.centerY
                , Font.size 30
                ]

        hackText : Element msg
        hackText =
            case hackerMeter.lastChipId |> Maybe.andThen (Circuit.getChip (Core.getCircuit coreState)) of
                Just chip ->
                    let
                        tilesLeft =
                            Chip.totalTilesNeededToHack chip - Chip.tilesHacked chip
                    in
                    if Core.missionStatus coreState == MissionSuccess then
                        textElement (Element.text "all chips hacked")

                    else if tilesLeft == 1 then
                        textElement
                            (Element.text (String.fromInt tilesLeft ++ " tile left"))

                    else if tilesLeft > 0 then
                        textElement
                            (Element.text (String.fromInt tilesLeft ++ " tiles left"))

                    else if fractionalModBy 3 time < 1.5 then
                        textElement (Element.text "chip hacked")

                    else
                        textElement (Element.text "goto next")

                Nothing ->
                    Element.none

        (Quantity.Quantity time) =
            Core.getTime coreState

        height =
            50

        maxOffset =
            height + 10

        innerWidth =
            width - padding * 2

        padding =
            4

        body =
            Element.el
                [ Element.height Element.fill
                , Element.width <| Element.px innerWidth
                ]
                bar

        bar =
            Element.el
                [ Element.height Element.fill
                , Element.width <| Element.px <| round (toFloat innerWidth * Quantity.toFloat hackerMeter.currentHackRatio)
                , Background.color <| Element.rgb 0 0 0
                ]
                Element.none
    in
    Element.el
        [ Element.padding padding
        , Element.centerX
        , Border.color <| Element.rgb 0 0 0
        , Border.width 5
        , Element.moveUp (hackerMeter.offset |> Quantity.toFloat |> (-) 1 |> (*) maxOffset |> (+) -10)
        , Element.height <| Element.px height
        , Element.inFront hackText
        , Background.color <| Element.rgba 0.5 0.5 0.5 0.3
        ]
        body
