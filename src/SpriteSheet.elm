module SpriteSheet exposing (Sprite, SpriteSheet, create, get, uvCoordinates, uvMatrix)

import Dict exposing (Dict)
import Helper exposing (ifElse)
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector3 as Vec3
import Parser exposing ((|.), (|=))
import Point2d exposing (Point2d)
import Vector2d exposing (Vector2d)
import WebGL.Texture as Texture exposing (Texture)


type alias Sprite =
    { position : ( Int, Int )
    , size : ( Int, Int )
    }


type alias SpriteUv =
    { position : Point2d
    , size : Vector2d
    }


type alias SpriteSheet =
    { sprites : Dict String Sprite
    , texture : Texture
    }


get : String -> SpriteSheet -> Sprite
get spriteName spriteSheet =
    Dict.get spriteName spriteSheet.sprites |> Maybe.withDefault { position = ( 0, 0 ), size = ( 0, 0 ) }


uvCoordinates : String -> SpriteSheet -> SpriteUv
uvCoordinates spriteName spriteSheet =
    let
        { position, size } =
            get spriteName spriteSheet

        ( px, py ) =
            position

        ( sx, sy ) =
            size

        ( tx, ty ) =
            Texture.size spriteSheet.texture
    in
    if tx == 0 || ty == 0 then
        { position = Point2d.fromCoordinates ( 0, 0 )
        , size = Vector2d.fromComponents ( 0, 0 )
        }

    else
        { position = Point2d.fromCoordinates ( toFloat px / toFloat tx, toFloat py / toFloat ty )
        , size = Vector2d.fromComponents ( toFloat sx / toFloat tx, toFloat sy / toFloat ty )
        }


uvMatrix : String -> SpriteSheet -> Mat4
uvMatrix spriteName spriteSheet =
    let
        { position, size } =
            uvCoordinates spriteName spriteSheet
    in
    Mat4.makeTranslate (Vec3.vec3 (Point2d.xCoordinate position) (Point2d.yCoordinate position) 0)
        |> Mat4.scale (Vec3.vec3 (Vector2d.xComponent size) (Vector2d.yComponent size) 1)


create : Texture -> String -> SpriteSheet
create texture stylFile =
    { sprites = parse stylFile
    , texture = texture
    }


parse : String -> Dict String Sprite
parse stylFile =
    let
        removeStartComments =
            case String.indexes "*\\" stylFile of
                first :: _ ->
                    String.dropLeft first stylFile

                _ ->
                    stylFile

        parseSpriteInfo : Parser.Parser ( String, Sprite )
        parseSpriteInfo =
            Parser.succeed (\x y width height name -> ( name, { position = ( x, y ), size = ( width, height ) } ))
                |= parsePx
                |= parsePx
                |. parsePx
                |. parsePx
                |= parsePx
                |= parsePx
                |. parsePx
                |. parsePx
                |. Parser.spaces
                |. singleQuoteText
                |. Parser.spaces
                |= singleQuoteText

        parsePx =
            Parser.succeed (\negated value -> ifElse negated -value value)
                |. Parser.spaces
                |= Parser.oneOf
                    [ Parser.chompIf ((==) '-') |> Parser.mapChompedString (\_ _ -> True)
                    , Parser.succeed False
                    ]
                |= Parser.int
                |. Parser.symbol "px"

        singleQuoteText =
            Parser.succeed identity
                |. Parser.symbol "'"
                |= (Parser.chompWhile ((/=) '\'') |> Parser.getChompedString)
                |. Parser.symbol "'"
    in
    removeStartComments
        |> String.filter ((/=) '\u{000D}')
        |> String.split "\n"
        |> List.filter (String.startsWith "$")
        |> List.filterMap
            (\line ->
                case String.split "=" line |> List.map String.trim of
                    _ :: second :: [] ->
                        case Parser.run parseSpriteInfo second of
                            Ok sprite ->
                                Just sprite

                            Err _ ->
                                Nothing

                    _ ->
                        Nothing
            )
        |> Dict.fromList
