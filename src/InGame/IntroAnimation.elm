module InGame.IntroAnimation exposing
    ( baseIntroStartTime
    , cameraWonkiness
    , inSeconds
    , introEndTime
    , missionIntroTimeAnim
    , missionNumberYAnim
    , missionTextYAnim
    , showErrorScreen
    , soundUpdate
    , splitterHeight
    , view
    )

import Element exposing (Element)
import Element.Background as Background
import Element.Font as Font
import Helper
import Hex
import Keyframe exposing (Animation, Extrapolate(..))
import Quantity exposing (Quantity, Unitless)
import RealTime exposing (RealTime)
import Sound
import Typing
import Vector2i exposing (Vector2i)


view : Bool -> Int -> Vector2i -> Quantity Float RealTime -> Quantity Float RealTime -> Element msg
view isGlitched missionNumber windowSize time startTime =
    if time |> Quantity.greaterThan (introEndTime isGlitched startTime) then
        Element.none

    else if showErrorScreen isGlitched time startTime then
        errorScreenView time startTime

    else
        Element.column
            [ Element.height Element.fill
            , Element.width Element.fill
            , Element.inFront <| splitter windowSize time startTime
            , Element.clip
            ]
            [ missionText isGlitched windowSize time startTime
            , missionNumberView isGlitched missionNumber windowSize time startTime
            ]


introEndTime : Bool -> Quantity Float RealTime -> Quantity Float RealTime
introEndTime isGlitched startTime =
    if isGlitched then
        errorScreenLines startTime |> Typing.endTime

    else
        startTime |> Quantity.plus (RealTime.seconds 5.3)



{- The time at which the basic intro starts (the one that just shows the player spawning). -}


baseIntroStartTime : Bool -> Quantity Float RealTime -> Quantity Float RealTime
baseIntroStartTime isGlitched startTime =
    if isGlitched then
        errorScreenLines startTime |> Typing.endTime

    else
        startTime |> Quantity.plus (RealTime.seconds 3)


splitterHeight : number
splitterHeight =
    3


splitter : Vector2i -> Quantity Float RealTime -> Quantity Float RealTime -> Element msg
splitter windowSize time startTime =
    let
        width =
            toFloat windowSize.width

        offset =
            Keyframe.startAt_ startTime width
                |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 0.5) 0
                |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 1.3) 0
                |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 0.5) -width
                |> Keyframe.valueAt_ time
    in
    Element.el
        [ Element.height <| Element.px splitterHeight
        , Element.width Element.fill
        , Background.color <| Element.rgb 1 1 1
        , Element.centerY
        , Element.moveLeft offset
        ]
        Element.none


type AnimationTime
    = AnimationTime Never


inSeconds : Quantity number AnimationTime -> number
inSeconds (Quantity.Quantity value) =
    value


missionTextYAnim : Animation AnimationTime Quantity.Unitless
missionTextYAnim =
    Keyframe.startAt_ Quantity.zero 415
        |> Keyframe.addConstant (seconds 0.3)
        |> Keyframe.add_ Keyframe.InOutExpo (seconds 1) 207
        |> Keyframe.insert_ Keyframe.Linear (seconds 3) 207
        |> Keyframe.add_ Keyframe.InExpo (seconds 1) -1080


missionIntroTimeAnim : Bool -> Quantity Float RealTime -> Animation RealTime AnimationTime
missionIntroTimeAnim glitched startTime =
    let
        oneSecondPerSecond =
            Quantity.per (RealTime.seconds 1) (seconds 1)

        glitchStart : Quantity Float RealTime
        glitchStart =
            Quantity.plus playGoSound goGlitchedSecondGoSound

        baseAnimation =
            Keyframe.startAt startTime Quantity.zero
                |> Keyframe.extrapolateBackward (ExtrapolateLinear oneSecondPerSecond)
                |> Keyframe.extrapolateForward (ExtrapolateLinear oneSecondPerSecond)

        baseValue =
            glitchStart |> Quantity.minus goGlitchedSecondGoSound |> rawQuantity

        addSawtooth baseValue_ delay steepness =
            let
                peakValue =
                    baseValue_ |> seconds |> Quantity.plus (seconds (delay * steepness))
            in
            Keyframe.add Keyframe.Linear Quantity.zero (seconds baseValue_)
                >> Keyframe.add Keyframe.Linear (RealTime.seconds delay) peakValue
    in
    if glitched then
        baseAnimation
            |> Keyframe.add Keyframe.Linear glitchStart (rawQuantity glitchStart |> seconds)
            |> addSawtooth baseValue 0.182 1
            |> addSawtooth baseValue 0.182 1
            |> addSawtooth baseValue 0.182 1
            |> addSawtooth baseValue 0.182 1
            |> addSawtooth baseValue 0.072 2
            |> addSawtooth baseValue 0.072 2
            |> addSawtooth baseValue 0.072 2
            |> addSawtooth baseValue 0.072 2
            |> addSawtooth baseValue 0.072 2
            |> addSawtooth baseValue 0.02695 2
            |> addSawtooth baseValue 0.02695 2
            |> addSawtooth baseValue 0.02695 2.4
            |> addSawtooth baseValue 0.02695 2.5
            |> addSawtooth baseValue 0.02695 2.7
            |> addSawtooth baseValue 0.02695 2.9
            |> addSawtooth baseValue 0.02695 3
            |> addSawtooth baseValue 0.02695 4
            |> addSawtooth baseValue 0.02695 4
            |> addSawtooth baseValue 0.02695 5
            |> addSawtooth baseValue 0.02695 6
            |> addSawtooth baseValue 0.02695 7
            |> addSawtooth baseValue 0.02695 8
            |> addSawtooth baseValue 0.02695 9
            |> addSawtooth baseValue 0.02695 10
            |> addSawtooth baseValue 0.02695 11
            |> addSawtooth baseValue 0.02695 12
            |> addSawtooth baseValue 0.02695 13
            |> addSawtooth baseValue 0.02695 14
            |> addSawtooth baseValue 0.02695 15
            |> Keyframe.add Keyframe.Linear Quantity.zero (seconds baseValue |> Quantity.plus (seconds 0.47))
            |> Keyframe.extrapolateForward (ExtrapolateLinear Quantity.zero)

    else
        baseAnimation


rawQuantity : Quantity number unit -> number
rawQuantity (Quantity.Quantity value) =
    value


missionText : Bool -> Vector2i -> Quantity Float RealTime -> Quantity Float RealTime -> Element msg
missionText glitched windowSize time startTime =
    let
        scale_ =
            scale windowSize

        animationTime =
            missionIntroTimeAnim glitched startTime |> Keyframe.valueAt time

        offset =
            missionTextYAnim
                |> Keyframe.valueAt_ animationTime
                |> (*) scale_

        text =
            Element.el
                [ Element.moveDown offset
                , Font.size <| round (scale_ * 150)
                , Font.bold
                , Font.color <| Element.rgb 1 1 1
                , Element.centerX
                ]
                (Element.text "Mission")
    in
    Element.el
        [ Element.height Element.fill
        , Element.width Element.fill
        , Element.clip
        , Element.inFront text
        ]
        Element.none


scale : Vector2i -> Float
scale windowSize =
    toFloat windowSize.height / 830


seconds : number -> Quantity number AnimationTime
seconds value =
    Quantity.Quantity value


missionNumberYAnim : Animation AnimationTime Quantity.Unitless
missionNumberYAnim =
    Keyframe.startAt_ Quantity.zero -395
        |> Keyframe.addConstant (seconds 1.5)
        |> Keyframe.addBezier_
            (seconds 0.15)
            550
            (seconds 0.4)
            0
            (seconds 1)
            -30
        |> Keyframe.addBezier_
            (seconds 0.4)
            0
            (seconds 0)
            0
            (seconds 0.6)
            -25
        |> Keyframe.addBezier_
            (seconds 0.8)
            30
            (seconds 0.1)
            400
            (seconds 1)
            1100


playMissionSound : Quantity Float RealTime
playMissionSound =
    RealTime.seconds 0.6


playMissionNumberSound : Quantity Float RealTime
playMissionNumberSound =
    RealTime.seconds 1.6


playGoSound : Quantity Float RealTime
playGoSound =
    RealTime.seconds 3.3


goGlitchedSecondGoSound : Quantity Float RealTime
goGlitchedSecondGoSound =
    RealTime.seconds 0.581


goGlitchedLastBeep : Quantity Float RealTime
goGlitchedLastBeep =
    RealTime.seconds 3.482


showErrorScreen : Bool -> Quantity Float RealTime -> Quantity Float RealTime -> Bool
showErrorScreen isGlitched time startTime =
    let
        greaterThan =
            time |> Quantity.greaterThan (errorScreenStartTime startTime)

        lessThan =
            time |> Quantity.lessThanOrEqualTo (Typing.endTime (errorScreenLines startTime))
    in
    greaterThan && lessThan && isGlitched


errorScreenStartTime startTime =
    startTime |> Quantity.plus playGoSound |> Quantity.plus goGlitchedLastBeep


errorScreenLines startTime =
    Typing.startAt (errorScreenStartTime startTime)
        |> Typing.pause (RealTime.seconds 2)
        |> Typing.instantTyping "A fatal error has occurred.\n"
        |> Typing.pause (RealTime.seconds 2.5)
        |> Typing.instantTyping "You don't know how to play this game.\n"
        |> Typing.pause (RealTime.seconds 2)
        |> Typing.instantTyping ".\n"
        |> Typing.pause (RealTime.seconds 0.5)
        |> Typing.instantTyping ".\n"
        |> Typing.pause (RealTime.seconds 0.5)
        |> Typing.instantTyping ".\n"
        |> Typing.pause (RealTime.seconds 0.5)
        |> Typing.instantTyping "This stage is being replaced with a tutorial.\n"
        |> Typing.pause (RealTime.seconds 2.5)
        |> Typing.instantTyping "1%\n"
        |> Typing.pause (RealTime.seconds 1)
        |> Typing.instantTyping "2%\n"
        |> Typing.pause (RealTime.seconds 1)
        |> Typing.instantTyping "2.1%\n"
        |> Typing.pause (RealTime.seconds 3)
        |> Typing.instantTyping "3%\n"
        |> Typing.pause (RealTime.seconds 0.1)
        |> Typing.instantTyping "100%\n"
        |> Typing.pause (RealTime.seconds 0.2)
        |> Typing.instantTyping "Restarting mission.\n"
        |> Typing.pause (RealTime.seconds 1)


errorScreenView : Quantity Float RealTime -> Quantity Float RealTime -> Element msg
errorScreenView time startTime =
    Element.el
        [ Element.width Element.fill
        , Element.height Element.fill
        , Background.color <| Element.rgb 0.04 0.57 0.87
        , Element.paddingXY 30 60
        , Element.behindContent <|
            Element.image
                [ Element.height <| Element.px 200
                , Element.centerX
                , Element.centerY
                , Element.alpha 0.5
                ]
                { src = "images/info_i_blue.svg", description = "" }
        ]
        (Element.column
            [ Font.size 26
            , Font.family [ Font.typeface "Courier New" ]
            , Font.color <| Element.rgb 1 1 1
            ]
            (errorScreenLines startTime |> Typing.currentString time |> Helper.stringToParagraphs)
        )


cameraWonkiness : Quantity Float RealTime -> Animation RealTime Unitless
cameraWonkiness startTime =
    Keyframe.startAt_ startTime 0
        |> Keyframe.addConstant (playGoSound |> Quantity.plus goGlitchedSecondGoSound |> Quantity.plus (RealTime.seconds 0.93))
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.07) 1
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.07) 2
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.07) 3
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.03) 4
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.03) 5
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.03) 6
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.03) 7
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.03) 8
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.03) 9
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.03) 10
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.03) 11
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 5.03) 12
        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 5) 0


soundUpdate : Int -> Bool -> Bool -> Quantity Float RealTime -> Quantity Float RealTime -> Sound.Model -> Sound.Model
soundUpdate missionNumber isGlitched showIntro time introStartTime sounds =
    (if showIntro then
        let
            playGoSound_ =
                if isGlitched then
                    Sound.play Sound.AnnouncerGoGlitched (introStartTime |> Quantity.plus playGoSound)

                else
                    Sound.play Sound.AnnouncerGo (introStartTime |> Quantity.plus playGoSound)
        in
        sounds
            |> Sound.play Sound.AnnouncerMission (introStartTime |> Quantity.plus playMissionSound)
            |> Sound.playSpeech (intToHexText missionNumber) (introStartTime |> Quantity.plus playMissionNumberSound)
            |> playGoSound_

     else
        sounds
    )
        |> playInGameMusic isGlitched time introStartTime


playInGameMusic : Bool -> Quantity Float RealTime -> Quantity Float RealTime -> Sound.Model -> Sound.Model
playInGameMusic isGlitched time startTime =
    if isGlitched then
        (if time |> Quantity.lessThan (errorScreenStartTime startTime) then
            Sound.play Sound.InGameMusicGlitched startTime

         else
            Sound.stop Sound.InGameMusicGlitched
        )
            >> Sound.play Sound.InGameMusic (introEndTime isGlitched startTime)

    else
        Sound.play Sound.InGameMusic startTime


missionNumberView : Bool -> Int -> Vector2i -> Quantity Float RealTime -> Quantity Float RealTime -> Element msg
missionNumberView glitched missionNumber windowSize time startTime =
    let
        scale_ =
            scale windowSize

        animationTime =
            missionIntroTimeAnim glitched startTime |> Keyframe.valueAt time

        offset =
            missionNumberYAnim
                |> Keyframe.valueAt_ animationTime
                |> (*) scale_

        text =
            Element.el
                [ Element.moveDown offset
                , Font.size <| round (scale_ * 400)
                , Font.bold
                , Font.color <| Element.rgb 1 1 1
                , Element.centerX
                ]
                (Hex.toString missionNumber |> String.toUpper |> Element.text)
    in
    Element.el
        [ Element.height Element.fill
        , Element.width Element.fill
        , if startTime |> Quantity.plus (RealTime.seconds 2) |> Quantity.greaterThanOrEqualTo time then
            Element.clip

          else
            Helper.noneAttribute
        , Element.inFront text
        ]
        Element.none


intToHexText : Int -> String
intToHexText value =
    case Hex.toString value |> String.filter ((/=) '-') |> String.toList |> List.reverse of
        onesPlace :: sixteensPlace :: _ ->
            let
                onesPlaceText =
                    onesPlaceToString onesPlace
            in
            case sixteensPlace of
                '1' ->
                    case onesPlace of
                        '0' ->
                            "ten"

                        '1' ->
                            "eleven"

                        '2' ->
                            "twelve"

                        '3' ->
                            "thirteen"

                        '4' ->
                            "fourteen"

                        '5' ->
                            "fifteen"

                        '6' ->
                            "sixteen"

                        '7' ->
                            "seventeen"

                        '8' ->
                            "eighteen"

                        '9' ->
                            "nineteen"

                        'a' ->
                            "abteen"

                        'b' ->
                            "bibteen"

                        'c' ->
                            "cibteen"

                        'd' ->
                            "dibbleteen"

                        'e' ->
                            "ebbleteen"

                        'f' ->
                            "fleventeen"

                        _ ->
                            ""

                '2' ->
                    "twenty " ++ onesPlaceText

                '3' ->
                    "thirty " ++ onesPlaceText

                '4' ->
                    "fourty " ++ onesPlaceText

                '5' ->
                    "fifty " ++ onesPlaceText

                '6' ->
                    "sixty " ++ onesPlaceText

                '7' ->
                    "seventy " ++ onesPlaceText

                '8' ->
                    "eighty " ++ onesPlaceText

                '9' ->
                    "ninety " ++ onesPlaceText

                'a' ->
                    "atta " ++ onesPlaceText

                'b' ->
                    "bitta " ++ onesPlaceText

                'c' ->
                    "citta " ++ onesPlaceText

                'd' ->
                    "dickety " ++ onesPlaceText

                'e' ->
                    "eckity " ++ onesPlaceText

                'f' ->
                    "fleventy " ++ onesPlaceText

                _ ->
                    onesPlaceText

        onesPlace :: _ ->
            onesPlaceToString onesPlace

        [] ->
            "zero"


onesPlaceToString : Char -> String
onesPlaceToString char =
    case char of
        '0' ->
            "zero"

        '1' ->
            "one"

        '2' ->
            "two"

        '3' ->
            "three"

        '4' ->
            "four"

        '5' ->
            "five"

        '6' ->
            "six"

        '7' ->
            "seven"

        '8' ->
            "eight"

        '9' ->
            "nine"

        'a' ->
            "A"

        'b' ->
            "B"

        'c' ->
            "C"

        'd' ->
            "D"

        'e' ->
            "E"

        'f' ->
            "F"

        _ ->
            ""
