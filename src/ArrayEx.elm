module ArrayEx exposing (findIndex)

import Array exposing (Array)


{-| If roundDown is true then find the index one smaller than the index of the given value. The array must be sorted first!
-}
findIndex : Bool -> comparable -> Array ( comparable, a ) -> Int
findIndex roundDown value array =
    let
        helper low high =
            let
                midpointIndex =
                    (low + high) // 2
            in
            case Array.get midpointIndex array of
                Just midpointValue ->
                    let
                        midpointComparable =
                            Tuple.first midpointValue
                    in
                    if midpointComparable == value then
                        if roundDown then
                            expandLeft midpointComparable midpointIndex - 1

                        else
                            expandRight midpointComparable midpointIndex + 1

                    else if high - low <= 1 then
                        if roundDown then
                            low

                        else
                            high

                    else if midpointComparable < value then
                        helper midpointIndex high

                    else
                        helper low midpointIndex

                Nothing ->
                    0

        expandLeft : comparable -> Int -> Int
        expandLeft value_ index =
            expand ((+) -1) value_ index

        expandRight : comparable -> Int -> Int
        expandRight value_ index =
            expand ((+) 1) value_ index

        expand : (Int -> Int) -> comparable -> Int -> Int
        expand func value_ index =
            case Array.get (func index) array of
                Just arrayValue ->
                    if Tuple.first arrayValue == value_ then
                        expand func value_ (func index)

                    else
                        index

                Nothing ->
                    index
    in
    helper -1 (Array.length array)
