module Keyframe exposing
    ( Animation
    , Extrapolate(..)
    , Keyframe
    , Transition(..)
    , add
    , addBezier
    , addBezier_
    , addConstant
    , add_
    , bezierCurve
    , bezierCurveSolveX
    , extrapolateBackward
    , extrapolateForward
    , getExtrapolateBackward
    , getExtrapolateForward
    , getKeyframes
    , getStartTime
    , getStartValue
    , insert
    , insert_
    , startAt
    , startAt_
    , valueAt
    , valueAt_
    )

import Array exposing (Array)
import Ease
import Point2d exposing (Point2d)
import Quantity exposing (Quantity, Rate, Unitless)
import Vector2d exposing (Vector2d)


type Animation time a
    = Animation (Animation_ time a)


type Transition time a
    = Constant
    | Linear
    | InExpo
    | OutExpo
    | InOutExpo
    | InBack
    | OutBack
    | InOutBack
    | OutElastic
    | InElastic
    | InOutElastic
    | OutCirc
    | InCirc
    | InOutCirc
    | OutQuart
    | InQuart
    | InOutQuart
    | OutBounce
    | InBounce
    | InOutBounce
    | Bezier { t0 : Quantity Float time, v0 : Quantity Float a, t1 : Quantity Float time, v1 : Quantity Float a }
    | SineWave { amplitude : Quantity Float a, count : Float }


type Extrapolate time a
    = ExtrapolateLinear (Quantity Float (Rate a time))


type alias Animation_ time a =
    { startValue : Quantity Float a
    , startTime : Quantity Float time
    , keyframes : Array (Keyframe time a)
    , extrapolateBackward : Extrapolate time a
    , extrapolateForward : Extrapolate time a
    }


type alias Keyframe time a =
    { value : Quantity Float a
    , time : Quantity Float time
    , transition : Transition time a
    }


startAt : Quantity Float time -> Quantity Float a -> Animation time a
startAt startingTime startingValue =
    { startValue = startingValue
    , startTime = startingTime
    , keyframes = Array.empty
    , extrapolateBackward = ExtrapolateLinear (Quantity.per (Quantity.Quantity 1) Quantity.zero)
    , extrapolateForward = ExtrapolateLinear (Quantity.per (Quantity.Quantity 1) Quantity.zero)
    }
        |> Animation


startAt_ : Quantity Float time -> Float -> Animation time Unitless
startAt_ startingTime startingValue =
    { startValue = Quantity.float startingValue
    , startTime = startingTime
    , keyframes = Array.empty
    , extrapolateBackward = ExtrapolateLinear (Quantity.per (Quantity.Quantity 1) Quantity.zero)
    , extrapolateForward = ExtrapolateLinear (Quantity.per (Quantity.Quantity 1) Quantity.zero)
    }
        |> Animation


extrapolateForward : Extrapolate time a -> Animation time a -> Animation time a
extrapolateForward extrapolation (Animation animation) =
    Animation { animation | extrapolateForward = extrapolation }


extrapolateBackward : Extrapolate time a -> Animation time a -> Animation time a
extrapolateBackward extrapolation (Animation animation) =
    Animation { animation | extrapolateBackward = extrapolation }


getExtrapolateForward : Animation time a -> Extrapolate time a
getExtrapolateForward (Animation animation) =
    animation.extrapolateForward


getExtrapolateBackward : Animation time a -> Extrapolate time a
getExtrapolateBackward (Animation animation) =
    animation.extrapolateBackward


add : Transition time a -> Quantity Float time -> Quantity Float a -> Animation time a -> Animation time a
add transition delay value (Animation animation) =
    let
        previousTime =
            Animation animation |> lastKeyframe |> .time
    in
    if Quantity.greaterThanOrEqualTo Quantity.zero delay then
        { animation
            | keyframes =
                Array.push
                    { value = value, time = Quantity.plus previousTime delay, transition = transition }
                    animation.keyframes
        }
            |> Animation

    else
        Animation animation


insert : Transition time a -> Quantity Float time -> Quantity Float a -> Animation time a -> Animation time a
insert transition time value (Animation animation) =
    let
        absoluteTime =
            Quantity.plus animation.startTime time

        { index } =
            keyframeBetween absoluteTime (Animation animation)
    in
    if index <= 0 then
        Animation animation

    else
        { animation
            | keyframes =
                insertAt
                    index
                    { value = value, time = absoluteTime, transition = transition }
                    animation.keyframes
        }
            |> Animation


insert_ :
    Transition time Unitless
    -> Quantity Float time
    -> Float
    -> Animation time Unitless
    -> Animation time Unitless
insert_ transition time value animation =
    insert transition time (Quantity.float value) animation


add_ :
    Transition time Unitless
    -> Quantity Float time
    -> Float
    -> Animation time Unitless
    -> Animation time Unitless
add_ transition delay value animation =
    add transition delay (Quantity.float value) animation


addBezier :
    Quantity Float time
    -> Quantity Float a
    -> Quantity Float time
    -> Quantity Float a
    -> Quantity Float time
    -> Quantity Float a
    -> Animation time a
    -> Animation time a
addBezier t0 v0 t1 v1 t2 v2 animation =
    add
        (Bezier
            { t0 = t0
            , v0 = v0
            , t1 = Quantity.negate t1 |> Quantity.plus t2
            , v1 = Quantity.negate v1 |> Quantity.plus v2 |> Quantity.minus (lastKeyframe animation).value
            }
        )
        t2
        v2
        animation


addBezier_ :
    Quantity Float time
    -> Float
    -> Quantity Float time
    -> Float
    -> Quantity Float time
    -> Float
    -> Animation time Unitless
    -> Animation time Unitless
addBezier_ t0 v0 t1 v1 t2 v2 animation =
    addBezier
        t0
        (Quantity.float v0)
        t1
        (Quantity.float v1)
        t2
        (Quantity.float v2)
        animation


addConstant : Quantity Float time -> Animation time a -> Animation time a
addConstant duration animation =
    add Linear duration (lastKeyframe animation).value animation


getKeyframesField (Animation animation) =
    animation.keyframes


lastKeyframe : Animation time a -> Keyframe time a
lastKeyframe animation =
    case last (getKeyframesField animation) of
        Just keyframe ->
            keyframe

        Nothing ->
            firstKeyframe animation


firstKeyframe (Animation animation) =
    { value = animation.startValue, time = animation.startTime, transition = Constant }


getStartValue : Animation time a -> Quantity Float a
getStartValue (Animation animation) =
    animation.startValue


getStartTime : Animation time a -> Quantity Float time
getStartTime (Animation animation) =
    animation.startTime


rawQuantity =
    \(Quantity.Quantity a) -> a


valueAt : Quantity Float time -> Animation time a -> Quantity Float a
valueAt time animation =
    let
        { previous, next } =
            keyframeBetween time animation
    in
    case ( previous, next ) of
        ( Just previous_, Just next_ ) ->
            let
                timeDelta : Float
                timeDelta =
                    next_.time |> Quantity.minus previous_.time |> rawQuantity

                t =
                    time |> Quantity.minus previous_.time |> rawQuantity |> (*) (1 / timeDelta)

                tFixed : Float
                tFixed =
                    if isInfinite t || isNaN t then
                        1

                    else
                        clamp 0 1 t

                valueDelta : Float
                valueDelta =
                    next_.value |> Quantity.minus previous_.value |> rawQuantity

                ease easeFunc =
                    easeFunc tFixed |> (*) valueDelta |> (+) (rawQuantity previous_.value) |> Quantity.Quantity
            in
            case next_.transition of
                Constant ->
                    next_.value

                Linear ->
                    ease Ease.linear

                InExpo ->
                    ease Ease.inExpo

                OutExpo ->
                    ease Ease.outExpo

                InOutExpo ->
                    ease Ease.inOutExpo

                InBack ->
                    ease Ease.inBack

                OutBack ->
                    ease Ease.outBack

                InOutBack ->
                    ease Ease.inOutBack

                OutElastic ->
                    ease Ease.outElastic

                InElastic ->
                    ease Ease.inElastic

                InOutElastic ->
                    ease Ease.inOutElastic

                OutCirc ->
                    ease Ease.outCirc

                InCirc ->
                    ease Ease.inCirc

                InOutCirc ->
                    ease Ease.inOutCirc

                Bezier { t0, v0, t1, v1 } ->
                    let
                        startTangent =
                            Vector2d.fromComponents ( rawQuantity t0, rawQuantity v0 )

                        endTangent =
                            Vector2d.fromComponents ( rawQuantity t1, rawQuantity v1 )
                    in
                    bezierCurveSolveX (keyframeToPoint previous_) startTangent endTangent (keyframeToPoint next_) (rawQuantity time) |> Quantity.Quantity

                SineWave { amplitude, count } ->
                    let
                        t1 =
                            tFixed * 2 * pi / count
                    in
                    if isInfinite t1 || isNaN t1 then
                        ease Ease.linear

                    else
                        amplitude |> Quantity.multiplyBy (sin t1) |> Quantity.plus (ease Ease.linear)

                OutQuart ->
                    ease Ease.outQuart

                InQuart ->
                    ease Ease.inQuart

                InOutQuart ->
                    ease Ease.inOutQuart

                OutBounce ->
                    ease Ease.outBounce

                InBounce ->
                    ease Ease.inBounce

                InOutBounce ->
                    ease Ease.inOutBounce

        ( Nothing, Just next_ ) ->
            case getExtrapolateBackward animation of
                ExtrapolateLinear rate ->
                    let
                        timeDelta =
                            time |> Quantity.minus next_.time
                    in
                    timeDelta |> Quantity.at rate |> Quantity.plus next_.value

        ( Just previous_, Nothing ) ->
            case getExtrapolateForward animation of
                ExtrapolateLinear rate ->
                    let
                        timeDelta =
                            time |> Quantity.minus previous_.time
                    in
                    timeDelta |> Quantity.at rate |> Quantity.plus previous_.value

        ( Nothing, Nothing ) ->
            getStartValue animation


keyframeToPoint keyframe =
    Point2d.fromCoordinates
        ( rawQuantity keyframe.time
        , rawQuantity keyframe.value
        )


bezierCurve : Point2d -> Vector2d -> Vector2d -> Point2d -> Float -> Point2d
bezierCurve start v1 v2 end t =
    let
        ( x1, y1 ) =
            Vector2d.components v1

        ( x2, y2 ) =
            Vector2d.components v2

        ( x3, y3 ) =
            Vector2d.components (Vector2d.from start end)

        x =
            3 * t * x1 * (1 - t) ^ 2 + 3 * (1 - t) * x2 * t ^ 2 + x3 * t ^ 3

        y =
            3 * t * y1 * (1 - t) ^ 2 + 3 * (1 - t) * y2 * t ^ 2 + y3 * t ^ 3
    in
    Point2d.translateBy (Vector2d.fromComponents ( x, y )) start


bezierCurveSolveX : Point2d -> Vector2d -> Vector2d -> Point2d -> Float -> Float
bezierCurveSolveX start v1 v2 end x =
    let
        iterate low high =
            let
                mid =
                    (low + high) * 0.5

                xGuess =
                    bezierCurve start v1 v2 end mid |> Point2d.xCoordinate
            in
            if xGuess > x then
                ( low, mid )

            else
                ( mid, high )

        ( low_, high_ ) =
            List.repeat 25 () |> List.foldr (\_ ( low, high ) -> iterate low high) ( 0, 1 )
    in
    bezierCurve start v1 v2 end ((low_ + high_) * 0.5) |> Point2d.yCoordinate


valueAt_ : Quantity Float time -> Animation time Unitless -> Float
valueAt_ time animation =
    valueAt time animation |> Quantity.toFloat


keyframeBetween :
    Quantity Float time
    -> Animation time a
    -> { index : Int, previous : Maybe (Keyframe time a), next : Maybe (Keyframe time a) }
keyframeBetween time animation =
    let
        keyframes =
            getKeyframes animation
    in
    keyframes
        |> Array.foldr
            (\keyframe (( continue, { index, previous, next } ) as state) ->
                if continue then
                    ( Quantity.greaterThan time keyframe.time, { index = index - 1, previous = Just keyframe, next = previous } )

                else
                    state
            )
            ( True, { index = Array.length keyframes, previous = Nothing, next = Nothing } )
        |> Tuple.second


getKeyframes : Animation time a -> Array (Keyframe time a)
getKeyframes animation =
    Array.append
        (Array.fromList [ firstKeyframe animation ])
        (getKeyframesField animation)


{-| Insert an element at the given index.
insertAt 1 'b' (fromList [ 'a', 'c' ]) == fromList [ 'a', 'b', 'c' ]
insertAt -1 'b' (fromList [ 'a', 'c' ]) == fromList [ 'b', 'a', 'c' ]
insertAt 10 'b' (fromList [ 'a', 'c' ]) == fromList [ 'a', 'c', 'b' ]
-}
insertAt : Int -> a -> Array a -> Array a
insertAt index val values =
    let
        length =
            Array.length values

        index_ =
            clamp 0 length index

        before =
            Array.slice 0 index_ values

        after =
            Array.slice index_ length values
    in
    Array.append (Array.push val before) after


last : Array a -> Maybe a
last array =
    Array.get (Array.length array - 1) array
