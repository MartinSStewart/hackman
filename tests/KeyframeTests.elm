module KeyframeTests exposing (all)

import Array exposing (Array)
import Duration
import Expect
import Fuzz
import Keyframe exposing (Extrapolate(..))
import Length
import ListHelper as List
import Quantity exposing (per)
import Test exposing (..)


all : Test
all =
    describe "A Test Suite" <|
        [ fuzz Fuzz.float "Linear interpolation" <|
            \value ->
                let
                    actual =
                        Keyframe.startAt Quantity.zero (Quantity.float -5)
                            |> Keyframe.add Keyframe.Linear (Duration.seconds 1) (Quantity.float -4)
                            |> Keyframe.valueAt (Duration.seconds value)

                    expected =
                        clamp 0 1 value |> (+) -5 |> Quantity.float
                in
                Expect.within (Expect.AbsoluteOrRelative 0.0001 0.0001) (Quantity.toFloat expected) (Quantity.toFloat actual)
        , test "Handle discontinuities test 1" <|
            \_ ->
                let
                    actual =
                        Keyframe.startAt Quantity.zero (Quantity.float -5)
                            |> Keyframe.add Keyframe.Linear (Duration.seconds 1) (Quantity.float -4)
                            |> Keyframe.add Keyframe.Linear Quantity.zero Quantity.zero
                            |> Keyframe.valueAt (Duration.seconds 1)
                in
                Expect.within (Expect.AbsoluteOrRelative 0.0001 0.0001) 0 (Quantity.toFloat actual)
        , test "Handle discontinuities test 2" <|
            \_ ->
                let
                    actual =
                        Keyframe.startAt Quantity.zero (Quantity.float -5)
                            |> Keyframe.add Keyframe.Linear Quantity.zero Quantity.zero
                            |> Keyframe.valueAt Quantity.zero
                in
                Expect.within (Expect.AbsoluteOrRelative 0.0001 0.0001) 0 (Quantity.toFloat actual)
        , fuzz Fuzz.float "Constant has same behavior as overlapping keyframes" <|
            \value ->
                let
                    actual =
                        Keyframe.startAt Quantity.zero Quantity.zero
                            |> Keyframe.add Keyframe.Linear Quantity.zero (Quantity.float 1)
                            |> Keyframe.add Keyframe.Linear (Duration.seconds 1) (Quantity.float 1)
                            |> Keyframe.valueAt (Duration.seconds value)

                    expected =
                        Keyframe.startAt Quantity.zero Quantity.zero
                            |> Keyframe.add Keyframe.Constant (Duration.seconds 1) (Quantity.float 1)
                            |> Keyframe.valueAt (Duration.seconds value)
                in
                Expect.within (Expect.AbsoluteOrRelative 0.0001 0.0001) (Quantity.toFloat expected) (Quantity.toFloat actual)
        , fuzz Fuzz.float "Ignore add with negative delay" <|
            \delay ->
                let
                    actual =
                        Keyframe.startAt Quantity.zero (Quantity.float -5)
                            |> Keyframe.add Keyframe.Linear (Duration.seconds delay) Quantity.zero
                            |> Keyframe.getKeyframes
                            |> Array.length

                    expected =
                        if delay >= 0 then
                            2

                        else
                            1
                in
                Expect.equal expected actual
        , fuzz fuzzKeyframe "Keyframe time increases monotonically" <|
            monotonicallyIncreases
                >> Expect.true "Expected keyframes to be increasing order by time."
        , fuzz Fuzz.float "Fuzz test linear extrapolation" <|
            \time ->
                Keyframe.startAt Quantity.zero (Length.meters 3)
                    |> Keyframe.extrapolateForward (ExtrapolateLinear (per (Duration.seconds 1) (Length.meters 1)))
                    |> Keyframe.extrapolateBackward (ExtrapolateLinear (per (Duration.seconds 1) (Length.meters 1)))
                    |> Keyframe.valueAt (Duration.seconds time)
                    |> rawQuantity
                    |> Expect.within (Expect.AbsoluteOrRelative 0.001 0.001) (time + 3)
        ]


rawQuantity (Quantity.Quantity value) =
    value


monotonicallyIncreases : Keyframe.Animation independentUnit a -> Bool
monotonicallyIncreases =
    Keyframe.getKeyframes
        >> Array.toList
        >> List.pairwise
        >> List.all (\( previous, next ) -> Quantity.greaterThanOrEqualTo previous.time next.time)


fuzzKeyframe =
    Fuzz.oneOf
        [ Fuzz.floatRange 0 10 |> Fuzz.map (\delay -> Keyframe.add_ Keyframe.Linear (Duration.seconds delay) 5)
        , Fuzz.floatRange 0 100 |> Fuzz.map (\time -> Keyframe.insert_ Keyframe.Linear (Duration.seconds time) 10)
        ]
        |> Fuzz.list
        |> Fuzz.map (List.foldr (\value keyframe -> value keyframe) (Keyframe.startAt_ (Duration.seconds 5) 0))
