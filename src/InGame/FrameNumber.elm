module InGame.FrameNumber exposing (FrameNumber, init, previous, step, time, value)

import InGameTime exposing (InGameTime)
import Quantity exposing (Quantity)


type FrameNumber
    = FrameNumber Never


init : Quantity Int FrameNumber
init =
    Quantity.Quantity 0


step : Quantity Int FrameNumber -> Quantity Int FrameNumber
step (Quantity.Quantity frameNumber) =
    frameNumber + 1 |> Quantity.Quantity


time : Int -> Quantity Int FrameNumber -> Quantity Float InGameTime
time framesPerSecond (Quantity.Quantity frameNumber) =
    toFloat frameNumber / toFloat framesPerSecond |> Quantity.Quantity


previous : Quantity Int FrameNumber -> Quantity Int FrameNumber
previous (Quantity.Quantity frameNumber) =
    frameNumber - 1 |> Quantity.Quantity


value : Quantity Int FrameNumber -> Int
value (Quantity.Quantity frameNumber) =
    frameNumber
