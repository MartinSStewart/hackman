module Editor.Helper exposing
    ( circuitWorldPosition
    , drawRectangle
    , snapDistance
    , snapToNearest
    , viewScalarToWorld
    )

import Agent
import Circuit exposing (CircuitPosition(..), WiresAndChips)
import Editor.Circuit exposing (Circuit)
import Geometry.Svg as Svg
import List.Extra as List
import Point2d exposing (Point2d)
import Polygon2d exposing (Polygon2d)
import Svg exposing (Svg)
import Vector2d exposing (Vector2d)


snapToNearest : Float -> Point2d -> List { b | position : Point2d } -> Maybe { b | position : Point2d }
snapToNearest viewZoom worldPoint snapWorldPoints_ =
    snapWorldPoints_
        |> List.map (\snapPoint -> ( snapPoint, Point2d.distanceFrom snapPoint.position worldPoint ))
        |> List.minimumBy Tuple.second
        |> Maybe.andThen
            (\( snapPoint, distance ) ->
                if distance < snapDistance viewZoom then
                    Just snapPoint

                else
                    Nothing
            )


snapDistance : Float -> Float
snapDistance viewZoom =
    10 / viewZoom


drawRectangle : List (Svg.Attribute msg) -> Point2d -> Vector2d -> Svg msg
drawRectangle attributes position size =
    let
        ( x, y ) =
            Point2d.coordinates position

        ( width, height ) =
            Vector2d.components size
    in
    Svg.polygon2d
        attributes
        ([ ( x + width, y + height )
         , ( x + width, y )
         , ( x, y )
         , ( x, y + height )
         ]
            |> List.map (\( x_, y_ ) -> Point2d.fromCoordinates ( x_, y_ ))
            |> Polygon2d.convexHull
        )


circuitWorldPosition : Circuit -> CircuitPosition b c -> Maybe Point2d
circuitWorldPosition circuit position =
    case position of
        WirePosition { wireId, t } ->
            case Circuit.getWire circuit wireId of
                Just wire ->
                    Agent.wirePosition circuit wire t

                Nothing ->
                    Nothing

        ChipPosition chipPosition ->
            Circuit.getChip circuit chipPosition.chipId
                |> Maybe.map (\chip -> Agent.worldPositionOnChip chip chipPosition.position)


viewScalarToWorld : { a | viewZoom : Float } -> Float -> Float
viewScalarToWorld canvasView scalar =
    scalar / canvasView.viewZoom
