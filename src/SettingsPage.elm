module SettingsPage exposing (view)

import Color
import Element exposing (Element)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import Helper exposing (filler)
import Html.Attributes
import MenuState exposing (MainMenu_)
import Model exposing (Model, Msg(..))
import Sound
import Style


view : Model -> MainMenu_ -> Element Msg
view model mainMenu =
    let
        slider msg value label =
            Input.slider
                [ Element.width Element.fill
                , Element.behindContent
                    (Element.el
                        [ Element.width Element.fill
                        , Element.height (Element.px 2)
                        , Element.centerY
                        , Background.color <| Element.rgba 0.9 0.9 0.9 1
                        ]
                        Element.none
                    )
                ]
                { onChange = msg
                , label = Input.labelHidden label
                , min = 0
                , max = 1
                , value = value
                , thumb = Input.defaultThumb
                , step = Nothing
                }

        antialiasCheckbox label =
            Input.checkbox
                []
                { onChange = SetAntialias
                , icon = Style.checkboxIcon Color.white
                , checked = model.antialias
                , label = Input.labelHidden label
                }

        backButton =
            Input.button
                (Font.size 40 :: Style.menuFormButton)
                { onPress = Just PressedSettingsPageBack
                , label = Element.text "Back"
                }

        resetButton =
            Input.button
                (Font.size 40 :: Element.alignRight :: Style.menuFormButton)
                { onPress = Just ResetSettings
                , label = Element.text "Reset"
                }

        watchTrailer =
            Input.button
                Style.menuFormButton
                { onPress = Just PressedShowTrailer
                , label = Element.text "Watch official trailer"
                }

        loadReplay =
            Element.row
                [ Element.spacing 30 ]
                [ Input.button
                    Style.menuFormButton
                    { onPress = Just PressedLoadReplayInSettings
                    , label = Element.text "Load replay"
                    }
                , if mainMenu.showReplayParseError then
                    Element.el [ Font.color <| Element.rgb 1 0.1 0.1 ] (Element.text "Failed to load replay.")

                  else
                    Element.none
                ]

        downloadOriginal =
            Element.row [ Element.paddingXY 20 8 ]
                [ Element.download
                    (Style.menuLink ++ [ Font.color (Element.rgb 1 1 1) ])
                    { url = "extras/HackMan.exe", label = Element.text "Play the game jam version" }
                , Element.text " (Windows only)"
                ]

        viewSource =
            Element.newTabLink
                (Style.menuLink ++ [ Font.color (Element.rgb 1 1 1), Element.paddingXY 20 8 ])
                { url = "https://gitlab.com/MartinSStewart/hackman"
                , label =
                    Element.row
                        [ Element.spacing 8 ]
                        [ Element.text "View source code"
                        , Element.image
                            [ Element.height <| Element.px 40 ]
                            { src = "https://about.gitlab.com/images/press/logo/svg/gitlab-icon-rgb.svg", description = "" }
                        ]
                }

        changeColors =
            Input.button
                Style.menuFormButton
                { onPress = Just PressedChangeColorsInSettings
                , label = Element.text "Change color palette"
                }

        table =
            Element.table
                [ Element.width Element.fill
                , Style.tableNameColumnFontSize
                , Element.htmlAttribute <| Html.Attributes.style "line-height" "30px"
                ]
                { data =
                    [ { text = "Master Volume"
                      , slider = slider SetMasterVolume (Sound.getMasterVolume model.sounds)
                      }
                    , { text = "Music"
                      , slider = slider SetMusicVolume (Sound.getMusicVolume model.sounds)
                      }
                    , { text = "Sound Effects"
                      , slider = slider SetSoundEffectVolume (Sound.getSoundEffectVolume model.sounds)
                      }
                    , { text = "Anti-aliasing", slider = antialiasCheckbox }
                    ]
                , columns =
                    [ { header = Element.none
                      , width = Element.shrink
                      , view =
                            .text
                                >> Element.text
                                >> Element.el
                                    [ Font.color <| Helper.colorToElement Style.tableNameColumnFontColor
                                    , Element.paddingEach { left = 0, top = 8, bottom = 8, right = 8 }
                                    ]
                      }
                    , { header = Element.none
                      , width = Element.fill
                      , view = \record -> record.slider record.text |> Element.el [ Element.centerY ]
                      }
                    ]
                }

        extras =
            Element.column
                [ Style.tableNameColumnFontSize ]
                [ watchTrailer, loadReplay, downloadOriginal, viewSource, changeColors ]
    in
    Element.row
        [ Element.width Element.fill
        , Font.color <| Element.rgb 1 1 1
        ]
        [ filler
        , Element.column
            [ Style.menuBackground
            , Element.centerX
            , Element.width (Element.fillPortion 3)
            , Element.spacing 10
            , Style.menuFormPadding
            ]
            [ table
            , extras
            , hSplit
            , Element.row
                [ Element.width Element.fill ]
                [ backButton, resetButton ]
            ]
        , filler
        ]


hSplit =
    Style.hSplit (Element.rgba 1 1 1 0.5)
