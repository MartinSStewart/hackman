module Point2i exposing
    ( Point2i
    , distanceFrom
    , fromTuple
    , length
    , map
    , map2
    , origin
    , roundFrom
    , toPoint2d
    , translateBy
    , tuple
    , xy
    )

import Point2d exposing (Point2d)
import Vector2i exposing (Vector2i)


type alias Point2i =
    { x : Int, y : Int }


xy : ( Int, Int ) -> Point2i
xy ( x, y ) =
    { x = x, y = y }


origin : Point2i
origin =
    { x = 0, y = 0 }


map : (Int -> Int) -> Point2i -> Point2i
map mapFunc point =
    { x = mapFunc point.x, y = mapFunc point.y }


map2 : (Int -> Int -> Int) -> Point2i -> Point2i -> Point2i
map2 mapFunc point0 point1 =
    { x = mapFunc point0.x point1.x, y = mapFunc point0.y point1.y }


tuple : Point2i -> ( Int, Int )
tuple point =
    ( point.x, point.y )


fromTuple : ( Int, Int ) -> Point2i
fromTuple ( x, y ) =
    { x = x, y = y }


length : Point2i -> Float
length { x, y } =
    x ^ 2 + y ^ 2 |> toFloat |> sqrt


distanceFrom : Point2i -> Point2i -> Float
distanceFrom point0 point1 =
    Vector2i.from point0 point1 |> Vector2i.length


toPoint2d : Point2i -> Point2d
toPoint2d point =
    Point2d.fromCoordinates ( toFloat point.x, toFloat point.y )


roundFrom : Point2d -> Point2i
roundFrom point =
    let
        ( x, y ) =
            Point2d.coordinates point
    in
    { x = round x, y = round y }


translateBy : Vector2i -> Point2i -> Point2i
translateBy v p =
    { x = p.x + v.width, y = p.y + v.height }
