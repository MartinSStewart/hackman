module T exposing (T, inT, t)

{-| Represents interpolation between values.
Between a pair of points this will be 0 to 1.
For a position along a wire this will be between 0 and the number of wire nodes.
-}

import Quantity exposing (Quantity)


type T
    = T Never


t : number -> Quantity number T
t value =
    Quantity.Quantity value


inT : Quantity number T -> number
inT (Quantity.Quantity t_) =
    t_
