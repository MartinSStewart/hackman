module EditorTests exposing (all)

import AssocList
import Basics.Extra exposing (flip)
import BoundingBox2d
import Cardinal
import Chip
import Circuit exposing (AgentStartType(..), ChipEdgePoint, ChipId(..), CircuitPosition(..), Wire, WireDirection(..), WireId(..))
import Dict
import Editor
import Editor.Circuit as Circuit exposing (Circuit)
import Editor.Helper as Helper
import Editor.Selection as Selection
import Editor.SnapSettings as SnapSettings
import Editor.UndoListEx as UndoList
import Editor.UndoModel as UndoModel exposing (Tool(..), UndoModel)
import Editor.WirePlacer as WirePlacer
import Expect
import Fuzz exposing (Fuzzer)
import Helper exposing (ifElse)
import Point2d
import Set
import T exposing (T(..))
import Test exposing (..)
import Vector2d
import Vector2i
import Wire


all : Test
all =
    describe "A Test Suite"
        [ test "Clicking last wire node stops it" <|
            \_ ->
                let
                    wirePlacer : WirePlacer.Model
                    wirePlacer =
                        WirePlacer.startDisconnected (Point2d.fromCoordinates ( 0, 0 ))
                            |> WirePlacer.addNode (Point2d.fromCoordinates ( 2, 0 ))

                    editorModel : Editor.Model
                    editorModel =
                        Editor.init (Vector2d.fromComponents ( 1000, 1000 ))
                            |> UndoList.updateUndoModel (UndoModel.setTool (WireTool <| Just wirePlacer) >> Just)

                    result =
                        Editor.toolClick (Point2d.fromCoordinates ( 3, 0 )) editorModel
                            |> Editor.toolClick (Point2d.fromCoordinates ( 3, 0 ))
                in
                case (UndoList.present result).tool of
                    WireTool wireTool ->
                        Expect.equal Nothing wireTool

                    _ ->
                        Expect.fail "We should have wire tool selected."
        , test "Snap along side a wire" <|
            \_ ->
                let
                    circuit : Maybe Circuit
                    circuit =
                        WirePlacer.startDisconnected Point2d.origin
                            |> WirePlacer.addNode (Point2d.fromCoordinates ( 1, 1 ))
                            |> flip WirePlacer.endOnNothing Circuit.empty
                in
                case circuit of
                    Just circuit_ ->
                        let
                            snapPoint =
                                WirePlacer.getSnapPoint
                                    UndoModel.defaultZoom
                                    SnapSettings.init
                                    circuit_
                                    (WirePlacer.startDisconnected (Point2d.fromCoordinates ( 0, 3 )) |> Just)
                                    (Point2d.fromCoordinates ( 2, 3 ))

                            isAlongWire =
                                case snapPoint of
                                    WirePlacer.AlongWire _ ->
                                        True

                                    _ ->
                                        False
                        in
                        Expect.true ("Expect snap point to be along wire but got \"" ++ Debug.toString snapPoint ++ "\"") isAlongWire

                    Nothing ->
                        Expect.fail "We failed to place the first wire."
        , test "Start on existing wire's endpoint" <|
            \_ ->
                let
                    ( circuit, wireId ) =
                        Circuit.empty |> Circuit.addWire otherWire

                    expected =
                        Wire.init
                            Nothing
                            [ Point2d.origin
                            , Point2d.fromCoordinates ( 1, 0 )
                            , Point2d.fromCoordinates ( 1, 1 )
                            , Point2d.fromCoordinates ( 0, 1 )
                            ]
                            Nothing

                    circuit2 =
                        WirePlacer.startOnWire wireId True
                            |> WirePlacer.addNode (Point2d.fromCoordinates ( 1, 1 ))
                            |> WirePlacer.addNode (Point2d.fromCoordinates ( 0, 1 ))
                            |> flip WirePlacer.endOnNothing circuit
                in
                case circuit2 of
                    Just circuit2_ ->
                        Circuit.getWires circuit2_
                            |> List.head
                            |> Maybe.map Tuple.second
                            |> Expect.equal (Just expected)

                    Nothing ->
                        Expect.fail "Failed to add wire."
        , test "Start on existing wire's start point" <|
            \_ ->
                let
                    ( circuit, wireId ) =
                        Circuit.empty |> Circuit.addWire otherWire

                    expected =
                        Wire.init
                            Nothing
                            [ Point2d.fromCoordinates ( 1, 0 )
                            , Point2d.origin
                            , Point2d.fromCoordinates ( 1, 1 )
                            , Point2d.fromCoordinates ( 0, 1 )
                            ]
                            Nothing

                    circuit2 =
                        WirePlacer.startOnWire wireId False
                            |> WirePlacer.addNode (Point2d.fromCoordinates ( 1, 1 ))
                            |> WirePlacer.addNode (Point2d.fromCoordinates ( 0, 1 ))
                            |> flip WirePlacer.endOnNothing circuit
                in
                case circuit2 of
                    Just circuit2_ ->
                        Circuit.getWires circuit2_
                            |> List.head
                            |> Maybe.map Tuple.second
                            |> Expect.equal (Just expected)

                    Nothing ->
                        Expect.fail "Failed to add wire."
        , test "End on existing wire's endpoint" <|
            \_ ->
                let
                    ( circuit, wireId ) =
                        Circuit.empty |> Circuit.addWire otherWire

                    expected =
                        Wire.init
                            Nothing
                            [ Point2d.fromCoordinates ( 1, 1 )
                            , Point2d.fromCoordinates ( 0, 1 )
                            , Point2d.fromCoordinates ( 1, 0 )
                            , Point2d.origin
                            ]
                            Nothing

                    circuit2 =
                        WirePlacer.startDisconnected (Point2d.fromCoordinates ( 1, 1 ))
                            |> WirePlacer.addNode (Point2d.fromCoordinates ( 0, 1 ))
                            |> (\a -> WirePlacer.endOnWire wireId True a circuit)
                in
                case circuit2 of
                    Just circuit2_ ->
                        Circuit.getWires circuit2_
                            |> List.head
                            |> Maybe.map Tuple.second
                            |> Expect.equal (Just expected)

                    Nothing ->
                        Expect.fail "Failed to add wire."
        , test "End on existing wire's start point" <|
            \_ ->
                let
                    ( circuit, wireId ) =
                        Circuit.empty |> Circuit.addWire otherWire

                    expected : Wire
                    expected =
                        Wire.init
                            Nothing
                            [ Point2d.fromCoordinates ( 1, 1 )
                            , Point2d.fromCoordinates ( 0, 1 )
                            , Point2d.origin
                            , Point2d.fromCoordinates ( 1, 0 )
                            ]
                            Nothing

                    circuit2 =
                        WirePlacer.startDisconnected (Point2d.fromCoordinates ( 1, 1 ))
                            |> WirePlacer.addNode (Point2d.fromCoordinates ( 0, 1 ))
                            |> (\a -> WirePlacer.endOnWire wireId False a circuit)
                in
                case circuit2 of
                    Just circuit2_ ->
                        Circuit.getWires circuit2_
                            |> List.head
                            |> Maybe.map Tuple.second
                            |> Expect.equal (Just expected)

                    Nothing ->
                        Expect.fail "Failed to add wire."
        , test "Merge wires" <|
            \_ ->
                let
                    ( circuit, firstWireId ) =
                        Circuit.empty
                            |> Circuit.addWire
                                (Wire.init
                                    Nothing
                                    [ Point2d.origin, Point2d.fromCoordinates ( 1, 0 ) ]
                                    Nothing
                                )

                    ( circuit2, secondWireId ) =
                        circuit
                            |> Circuit.addWire
                                (Wire.init
                                    Nothing
                                    [ Point2d.fromCoordinates ( 2, 0 ), Point2d.fromCoordinates ( 3, 0 ) ]
                                    Nothing
                                )

                    expected =
                        Wire.init
                            Nothing
                            [ Point2d.origin, Point2d.fromCoordinates ( 1, 0 ), Point2d.fromCoordinates ( 2, 0 ), Point2d.fromCoordinates ( 3, 0 ) ]
                            Nothing
                in
                case circuit2 |> Circuit.mergeWires firstWireId secondWireId of
                    Just ( circuit3, wireId ) ->
                        Circuit.getWire circuit3 wireId
                            |> Expect.equal (Just expected)

                    Nothing ->
                        Expect.fail "Failed to merge wires"
        , fuzz Fuzz.bool "Merging wires shouldn't delete or move agents" <|
            \mergeOrder ->
                let
                    ( circuit, firstWireId ) =
                        Circuit.empty
                            |> Circuit.addWire
                                (Wire.init
                                    Nothing
                                    [ Point2d.origin, Point2d.fromCoordinates ( 1, 0 ), Point2d.fromCoordinates ( 2, 0 ) ]
                                    Nothing
                                )

                    circuit2 =
                        circuit
                            |> Circuit.addAgent (PassiveAgentStart { position = Circuit.wirePosition firstWireId (T.t 0.3) Forward })

                    ( circuit3, secondWireId ) =
                        circuit2
                            |> Circuit.addWire
                                (Wire.init
                                    Nothing
                                    [ Point2d.fromCoordinates ( 2, 1 )
                                    , Point2d.fromCoordinates ( 3, 1 )
                                    , Point2d.fromCoordinates ( 4, 1 )
                                    ]
                                    Nothing
                                )

                    maybeCircuit4 =
                        circuit3
                            |> Circuit.mergeWires
                                (ifElse mergeOrder firstWireId secondWireId)
                                (ifElse mergeOrder secondWireId firstWireId)
                            |> Maybe.map Tuple.first
                in
                case maybeCircuit4 of
                    Just circuit4 ->
                        let
                            oldAgents =
                                Circuit.getAgents circuit2

                            newAgents =
                                Circuit.getAgents circuit4

                            equalLength =
                                AssocList.size oldAgents == AssocList.size newAgents
                        in
                        if equalLength then
                            List.map2
                                (\oldAgent newAgent ->
                                    case
                                        ( Helper.circuitWorldPosition circuit2 (Circuit.getAgentStartPosition oldAgent)
                                        , Helper.circuitWorldPosition circuit4 (Circuit.getAgentStartPosition newAgent)
                                        )
                                    of
                                        ( Just agentPosition, Just newAgentPosition ) ->
                                            Point2d.distanceFrom agentPosition newAgentPosition |> Expect.lessThan 0.0001

                                        _ ->
                                            Expect.fail "Failed to get agent world position"
                                )
                                (AssocList.values oldAgents)
                                (AssocList.values newAgents)
                                |> List.map (\a -> \() -> a)
                                |> flip Expect.all ()

                        else
                            Expect.fail "Wrong number of agents after merge"

                    Nothing ->
                        Expect.fail "Failed to merge wires"
        , test "Remove chip and part of wire" <|
            \_ ->
                let
                    chipId =
                        ChipId 0

                    wireId =
                        WireId 0

                    wire =
                        Wire.init
                            (Just <| ChipEdgePoint chipId Cardinal.Left 0)
                            [ Point2d.fromCoordinates ( 4, 1 ), Point2d.fromCoordinates ( 8, 1 ) ]
                            Nothing

                    chip =
                        Chip.init Point2d.origin (Vector2i.xy ( 2, 2 )) Set.empty

                    circuit : Circuit
                    circuit =
                        Circuit.from [ ( wireId, wire ) ] [ ( chipId, chip ) ] Nothing [] Dict.empty

                    selection =
                        Selection.selectRegion circuit (BoundingBox2d.fromExtrema { minX = -1, minY = -1, maxX = 5, maxY = 5 })
                in
                UndoModel.removeSelected selection circuit
                    |> Expect.all
                        [ \actual -> Circuit.getChips actual |> List.isEmpty |> Expect.true "Expected there to be no chips."
                        , \actual ->
                            case Circuit.getWires actual of
                                [ ( _, wireActual ) ] ->
                                    wireActual
                                        |> Expect.equal
                                            (Wire.init
                                                Nothing
                                                [ Point2d.fromCoordinates ( 4, 1 ), Point2d.fromCoordinates ( 8, 1 ) ]
                                                Nothing
                                            )

                                _ ->
                                    Expect.fail "Expected there to be exactly one wire."
                        ]

        --        , fuzz Fuzz.bool "Implicitly remove agent that is on removed wire segment" <|
        --            \reverseWire ->
        --                let
        --                    wireId =
        --                        WireId 0
        --
        --                    wireNodes =
        --                        [ Point2d.origin, Point2d.fromCoordinates ( 3, 0 ), Point2d.fromCoordinates ( 3, 3 ) ]
        --
        --                    wire =
        --                        if reverseWire then
        --                            Wire.init
        --                                Nothing
        --                                (List.reverse wireNodes)
        --                                Nothing
        --
        --                        else
        --                            Wire.init
        --                                Nothing
        --                                wireNodes
        --                                Nothing
        --
        --                    agent =
        --                        PassiveAgentStart
        --                            { position =
        --                                Circuit.wirePosition wireId (ifElse reverseWire (T.t 1.9) (T.t 0.1)) Forward
        --                            }
        --
        --                    circuit =
        --                        Circuit.from [ ( wireId, wire ) ] [] Nothing [] Dict.empty
        --
        --                    selection =
        --                        Selection.selectRegion circuit (BoundingBox2d.fromExtrema { minX = -1, minY = -1, maxX = 5, maxY = 1 })
        --
        --                    circuitWithAgent =
        --                        Circuit.addAgent agent circuit
        --                in
        --                UndoModel.removeSelected selection circuitWithAgent
        --                    |> Expect.all
        --                        [ \actual -> Circuit.getWires actual |> List.length |> Expect.equal 1
        --                        , \actual -> Circuit.getAgents actual |> AssocList.isEmpty |> Expect.true "Expected the agent to be removed."
        --                        ]
        , test "Remove selected agents" <|
            \_ ->
                let
                    wire =
                        Wire.init Nothing [ Point2d.origin, Point2d.fromCoordinates ( 10, 0 ) ] Nothing

                    wireId =
                        WireId 0

                    circuit =
                        Circuit.from
                            [ ( wireId, wire ) ]
                            []
                            Nothing
                            [ PassiveAgentStart { position = Circuit.wirePosition wireId (T.t 0.1) Forward }
                            , PassiveAgentStart { position = Circuit.wirePosition wireId (T.t 0.7) Forward }
                            ]
                            Dict.empty

                    selection =
                        Selection.selectRegion circuit (BoundingBox2d.fromExtrema { minX = -1, minY = -1, maxX = 5, maxY = 2 })

                    circuitWithSelectionRemoved =
                        UndoModel.removeSelected selection circuit
                in
                case Circuit.getAgents circuitWithSelectionRemoved |> AssocList.values of
                    agent :: [] ->
                        case Circuit.getAgentStartPosition agent |> Helper.circuitWorldPosition circuitWithSelectionRemoved of
                            Just position ->
                                Point2d.distanceFrom position (Point2d.fromCoordinates ( 7, 0 )) |> Expect.lessThan 0.0001

                            Nothing ->
                                Expect.fail "Failed to get agents position."

                    _ :: _ :: _ ->
                        Expect.fail "There's more than one agent left."

                    [] ->
                        Expect.fail "There should be one agent left."
        , test "Remove selected agents when there's two wires" <|
            \_ ->
                let
                    wire =
                        Wire.init Nothing [ Point2d.origin, Point2d.fromCoordinates ( 10, 0 ) ] Nothing

                    wireId =
                        WireId 0

                    wire2 =
                        Wire.init Nothing [ Point2d.fromCoordinates ( 0, 5 ), Point2d.fromCoordinates ( 10, 5 ) ] Nothing

                    wireId2 =
                        WireId 1

                    circuit =
                        Circuit.from
                            [ ( wireId, wire ), ( wireId2, wire2 ) ]
                            []
                            Nothing
                            [ PassiveAgentStart { position = Circuit.wirePosition wireId2 (T.t 0.7) Forward }
                            , PassiveAgentStart { position = Circuit.wirePosition wireId (T.t 0.1) Forward }
                            ]
                            Dict.empty

                    selection =
                        Selection.selectRegion circuit (BoundingBox2d.fromExtrema { minX = -1, minY = -1, maxX = 5, maxY = 2 })

                    circuitWithSelectionRemoved =
                        UndoModel.removeSelected selection circuit
                in
                case Circuit.getAgents circuitWithSelectionRemoved |> AssocList.values of
                    agent :: [] ->
                        case Circuit.getAgentStartPosition agent |> Helper.circuitWorldPosition circuitWithSelectionRemoved of
                            Just position ->
                                Point2d.distanceFrom position (Point2d.fromCoordinates ( 7, 5 )) |> Expect.lessThan 0.0001

                            Nothing ->
                                Expect.fail "Failed to get agents position."

                    _ :: _ :: _ ->
                        Expect.fail "There's more than one agent left."

                    [] ->
                        Expect.fail "There should be one agent left."
        ]


otherWire =
    Wire.init
        Nothing
        [ Point2d.origin, Point2d.fromCoordinates ( 1, 0 ) ]
        Nothing
