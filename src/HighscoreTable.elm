module HighscoreTable exposing
    ( Config
    , LatestScore(..)
    , Model
    , MsgConfig
    , PendingHighscore
    , ScoreId(..)
    , StageStatus(..)
    , acceptHighscore
    , getAllHighscores
    , getHighscoreName
    , getHighscores
    , getLatestHighscore
    , highscoreTableRowCount
    , init
    , isNewHighscore
    , newScoreNameOverlay
    , scoreIdCodec
    , setHighscoreName
    , setLatestScore
    , setPendingHighscore
    , setSortBy
    , stageStatus
    , step
    , toStageScore
    , view
    )

import AssocList
import Codec.Serialize as Codec exposing (Codec)
import Color exposing (Color)
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import EverySet
import Helper exposing (ifElse)
import Hex
import Html.Attributes
import InGame.IntroAnimation as IntroAnimation
import InGameTime exposing (InGameTime)
import Keyframe
import List.Extra as List
import ListHelper as List
import Quantity exposing (Quantity)
import RealTime exposing (RealTime)
import Replay exposing (CompleteReplay)
import StageData exposing (StageId)
import StageScore exposing (StageScore)
import Style
import Vector2i exposing (Vector2i)


type alias MsgConfig a msg =
    { a
        | typedHighscoreName : String -> msg
        , pressedAcceptedHighscoreName : PendingHighscore -> msg
        , pressedHighscoreSortBy : StageScore.SortBy -> msg
    }


type alias Config a =
    { a
        | windowSize : Vector2i
        , highscoreState : Model
        , time : Quantity Float RealTime
    }


type alias Model_ =
    { pendingHighscore : Maybe PendingHighscore
    , highscoreSortBy : StageScore.SortBy
    , stageScores : AssocList.Dict StageId (List ( ScoreId, StageScore ))
    , highscoreName : String
    , latestScore : LatestScore
    }


type Model
    = Model Model_


type StageStatus
    = StageLocked
    | StageUnlocked
    | StageCompleted


stageStatus : StageId -> Model -> StageStatus
stageStatus stageId highscoreState =
    if getHighscores StageScore.SortByHealth stageId highscoreState /= [] then
        StageCompleted

    else if stageId == StageData.tutorialId then
        StageUnlocked

    else if
        StageData.getAdjacentStages stageId
            |> EverySet.toList
            |> List.any (\adjacentStageId -> getHighscores StageScore.SortByHealth adjacentStageId highscoreState /= [])
    then
        StageUnlocked

    else
        StageLocked


setHighscoreName : String -> Model -> Model
setHighscoreName name (Model model) =
    Model { model | highscoreName = name }


setSortBy : StageScore.SortBy -> Model -> Model
setSortBy sortBy (Model model) =
    Model { model | highscoreSortBy = sortBy }


setPendingHighscore : Maybe PendingHighscore -> Model -> Model
setPendingHighscore pendingHighscore (Model model) =
    Model { model | pendingHighscore = pendingHighscore }


setLatestScore : LatestScore -> Model -> Model
setLatestScore latestScore (Model model) =
    Model { model | latestScore = latestScore }


getHighscoreName : Model -> String
getHighscoreName (Model model) =
    model.highscoreName


getLatestHighscore : Model -> LatestScore
getLatestHighscore (Model model) =
    model.latestScore


getPendingHighscore : Model -> Maybe PendingHighscore
getPendingHighscore (Model model) =
    model.pendingHighscore


getSortBy : Model -> StageScore.SortBy
getSortBy (Model model) =
    model.highscoreSortBy


getHighscores : StageScore.SortBy -> StageId -> Model -> List ( ScoreId, StageScore )
getHighscores sortBy stageId (Model model) =
    AssocList.get stageId model.stageScores
        |> Maybe.withDefault []
        |> List.sortWith (\( _, a ) ( _, b ) -> StageScore.compare sortBy a b)
        |> List.take highscoreTableRowCount


getAllHighscores : Model -> AssocList.Dict StageId (List ( ScoreId, StageScore ))
getAllHighscores (Model model) =
    model.stageScores


isNewHighscore : StageId -> StageScore -> Model -> Bool
isNewHighscore stageId stageScore model =
    let
        sortBy =
            StageScore.SortByTimePlusPenalty

        currentHighscores =
            getHighscores sortBy stageId model

        slotsLeft =
            List.length currentHighscores < highscoreTableRowCount
    in
    slotsLeft || List.any (\( _, score_ ) -> StageScore.compare sortBy stageScore score_ |> (==) LT) currentHighscores


acceptHighscore : Quantity Float RealTime -> PendingHighscore -> Model -> Model
acceptHighscore time pendingHighscore (Model model) =
    let
        stageScore =
            toStageScore model.highscoreName pendingHighscore

        newScoreId : ScoreId
        newScoreId =
            model.stageScores
                |> AssocList.values
                |> List.concat
                |> List.map (Tuple.first >> (\(ScoreId a) -> a))
                |> List.maximum
                |> Maybe.map ((+) 1)
                |> Maybe.withDefault 0
                |> ScoreId

        newModel =
            { model
                | stageScores =
                    AssocList.update
                        pendingHighscore.stageId
                        (\maybeScores ->
                            case maybeScores of
                                Just scores ->
                                    ( newScoreId, stageScore ) :: scores |> Just

                                Nothing ->
                                    Just [ ( newScoreId, stageScore ) ]
                        )
                        model.stageScores
                , latestScore = LatestHighscore { scoreId = newScoreId, acceptedTime = time }
                , pendingHighscore = Nothing
            }
    in
    Model newModel


init : AssocList.Dict StageId (List ( ScoreId, StageScore )) -> Model
init stageScores =
    { pendingHighscore = Nothing
    , highscoreSortBy = StageScore.SortByTimePlusPenalty
    , stageScores = stageScores
    , highscoreName = ""
    , latestScore = NoLatestScore
    }
        |> Model


step : MsgConfig a msg -> Config b -> Bool -> Model -> ( Model, List msg )
step msgConfig config submitPressed model =
    case ( getPendingHighscore model, submitPressed ) of
        ( Just pendingHighscore, True ) ->
            ( model, [ msgConfig.pressedAcceptedHighscoreName pendingHighscore ] )

        _ ->
            ( model, [] )


type LatestScore
    = NoLatestScore
    | LatestHighscore LatestHighscore_
    | LatestScore LatestScore_


type alias LatestHighscore_ =
    { scoreId : ScoreId
    , acceptedTime : Quantity Float RealTime
    }


type alias LatestScore_ =
    { score : PendingHighscore
    }


type ScoreId
    = ScoreId Int


scoreIdCodec : Codec ScoreId
scoreIdCodec =
    Codec.int |> Codec.map ScoreId (\(ScoreId scoreId) -> scoreId)


type alias PendingHighscore =
    { replay : CompleteReplay
    , stageId : StageId
    , health : Quantity Int StageScore.HealthPoints
    , goodChipMoves : Quantity Int StageScore.ChipMoves
    , badChipMoves : Quantity Int StageScore.ChipMoves
    , creationTime : Quantity Float RealTime
    }


toStageScore : String -> PendingHighscore -> StageScore
toStageScore name pendingHighscore =
    { time = Replay.replayLength pendingHighscore.replay
    , health = pendingHighscore.health
    , goodChipMoves = pendingHighscore.goodChipMoves
    , badChipMoves = pendingHighscore.badChipMoves
    , name = name
    }


newScoreNameOverlay : MsgConfig a msg -> Config b -> Model -> Element msg
newScoreNameOverlay msgConfig config (Model model) =
    let
        { offset, rowHeight, backgroundAlpha, blockMouseInput } =
            case ( model.pendingHighscore, model.latestScore ) of
                ( Just pendingHighscore, _ ) ->
                    let
                        startTime =
                            pendingHighscore.creationTime |> Quantity.plus (RealTime.seconds 1.5)
                    in
                    { offset =
                        Keyframe.startAt_ startTime (toFloat config.windowSize.width)
                            |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 0.5) 0
                            |> Keyframe.valueAt_ config.time
                    , rowHeight =
                        Keyframe.startAt_ startTime IntroAnimation.splitterHeight
                            |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 0.6) IntroAnimation.splitterHeight
                            |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 0.6) 200
                            |> Keyframe.valueAt_ config.time
                    , backgroundAlpha =
                        Keyframe.startAt_ startTime 0
                            |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 0.5) 0
                            |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 0.5) 0.5
                            |> Keyframe.valueAt_ config.time
                    , blockMouseInput = True
                    }

                ( Nothing, LatestHighscore latestHighscore ) ->
                    { offset =
                        Keyframe.startAt_ latestHighscore.acceptedTime 0
                            |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 0.5) 0
                            |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 0.5) (toFloat -config.windowSize.width)
                            |> Keyframe.valueAt_ config.time
                    , rowHeight =
                        Keyframe.startAt_ latestHighscore.acceptedTime 200
                            |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 0.5) IntroAnimation.splitterHeight
                            |> Keyframe.valueAt_ config.time
                    , backgroundAlpha =
                        Keyframe.startAt_ latestHighscore.acceptedTime 0.5
                            |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 0.5) 0
                            |> Keyframe.valueAt_ config.time
                    , blockMouseInput = False
                    }

                ( Nothing, _ ) ->
                    { offset = 0
                    , rowHeight = 0
                    , backgroundAlpha = 0
                    , blockMouseInput = False
                    }

        nameInputRow =
            Element.row
                [ Element.spacing 16, Font.size 36, Element.centerX ]
                [ Input.text
                    [ Element.width <| Element.px 400
                    , Element.paddingXY 8 8
                    , Font.regular
                    , Element.htmlAttribute <| Html.Attributes.id "new-highscore-name-input"
                    , Font.center
                    ]
                    { onChange = msgConfig.typedHighscoreName
                    , text = getHighscoreName config.highscoreState
                    , placeholder = Element.text "Enter your name" |> Input.placeholder [] |> Just
                    , label = Input.labelHidden "Enter your name"
                    }
                , Input.button
                    [ Element.alignRight
                    , Element.paddingXY 16 0
                    , Element.height Element.fill
                    , Background.color <| Element.rgba 0 0 0 0.1
                    , Border.rounded 3
                    , Element.mouseOver [ Background.color <| Element.rgba 0 0 0 0.2 ]
                    ]
                    { onPress =
                        case model.pendingHighscore of
                            Just pendingHighscore ->
                                Just (msgConfig.pressedAcceptedHighscoreName pendingHighscore)

                            Nothing ->
                                Nothing
                    , label = Element.el [ Element.moveLeft 2, Element.moveDown 1 ] <| Element.text "Done"
                    }
                ]

        body =
            Element.el
                [ Element.width Element.fill
                , Element.height <| Element.px <| round rowHeight
                , Element.centerY
                , Background.color <| Element.rgb 1 1 1
                , Element.moveLeft offset
                , Element.clip
                ]
            <|
                Element.column
                    [ ifElse (rowHeight <= IntroAnimation.splitterHeight) 0 1 |> Element.alpha, Element.centerY, Element.centerX, Element.spacing 30 ]
                    [ Element.el [ Font.color <| Element.rgb 0 0 0, Font.size 48, Element.centerX ] (Element.text "New Highscore")
                    , nameInputRow
                    ]
    in
    Element.el
        [ Background.color <| Element.rgba 1 1 1 backgroundAlpha
        , Element.width Element.fill
        , Element.height Element.fill
        , Element.inFront body
        , Element.htmlAttribute <| Html.Attributes.style "pointer-events" "none"
        ]
        Element.none


rowColor : Color.Color
rowColor =
    Color.rgba 1 1 1 0.5


rowAnim config firstShownTime =
    Keyframe.startAt_ firstShownTime (toFloat config.windowSize.width)
        |> Keyframe.add_ Keyframe.InOutBack (InGameTime.seconds 0.6) 0


rowOffset config firstShownTime currentTime index =
    Keyframe.valueAt_
        (InGameTime.seconds -0.05 |> Quantity.multiplyBy (toFloat index) |> Quantity.plus currentTime)
        (rowAnim config firstShownTime)


title =
    Element.el
        [ Element.clip
        , Font.size 40
        , Font.color (Helper.colorToElement rowColor)
        , Font.bold
        , Element.width Element.fill
        , Element.height Element.fill
        ]
    <|
        Element.el [] (Element.text "Highscores")


emptyScoreRow : Float -> Int -> Color -> Element msg
emptyScoreRow horizontalOffset index placeTextColor =
    let
        placeEl =
            indexToPlaceText index |> Element.el [ Helper.colorToElement placeTextColor |> Font.color ]
    in
    Element.el
        [ Element.width Element.fill
        , Font.color <| Element.rgba 0 0 0 0.5
        , Element.moveLeft horizontalOffset
        , Background.color (Helper.colorToElement rowColor)
        ]
        (tableRow placeEl "" "-:--.---" "-:--.---" "-")


tableRow : Element msg -> String -> String -> String -> String -> Element msg
tableRow placeEl nameText_ timeAndPenaltyText timeText healthText_ =
    Element.row
        [ Element.width Element.fill
        , Element.height <| Element.px tableRowHeight
        ]
        [ Element.el [ Element.width placeTextWidth ] placeEl
        , Element.el
            [ Element.width <| Element.px nameTextWidth
            , Element.paddingXY 4 0
            , Element.clip
            ]
            (Element.text nameText_)
        , Element.el [ Helper.monospace, Element.width timestampPenaltyWidth, Font.alignRight ] (Element.text timeAndPenaltyText)
        , Element.el [ Helper.monospace, Element.width timestampWidth, Font.alignRight ] (Element.text timeText)
        , Element.el [ Helper.monospace, Element.width healthWidth, Font.center ] (Element.text healthText_)
        ]


highscoreTableRowCount : number
highscoreTableRowCount =
    16


fallingOffEmptyScoreRow : Quantity Float RealTime -> Quantity Float RealTime -> Element msg
fallingOffEmptyScoreRow startTime time =
    let
        alpha =
            Keyframe.startAt_ startTime 1
                |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 1) 0
                |> Keyframe.valueAt_ time

        offset =
            Keyframe.startAt_ startTime 0
                |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 1) 20
                |> Keyframe.valueAt_ time
    in
    Element.el [ Element.alpha alpha, Element.moveDown offset ] (emptyScoreRow 0 15 Color.black)


tableRowHeight =
    30


timestampWidth =
    Element.px 140


healthWidth =
    Element.px 100


healthText score =
    let
        (Quantity.Quantity health) =
            score.health
    in
    health |> String.fromInt


nameTextWidth =
    250


timestampPenaltyWidth =
    Element.px 140


scoreRow :
    Config b
    -> Quantity Float InGameTime
    -> Quantity Float InGameTime
    -> ( ScoreId, StageScore )
    -> ( Int, Color )
    -> Element msg
scoreRow config firstShownTime currentTime ( scoreId, score ) ( index, indexColor ) =
    let
        default =
            { offset = rowOffset config firstShownTime currentTime index
            , height = tableRowHeight
            , rowStyle = [ Background.color (Helper.colorToElement rowColor) ]
            }

        { offset, height, rowStyle } =
            case ( getLatestHighscore config.highscoreState, getPendingHighscore config.highscoreState ) of
                ( LatestHighscore latestHighscore, Nothing ) ->
                    if scoreId == latestHighscore.scoreId then
                        { offset =
                            Keyframe.startAt_ latestHighscore.acceptedTime (toFloat config.windowSize.width)
                                |> Keyframe.add_ Keyframe.Linear scoreRowSlideInDelay (toFloat config.windowSize.width)
                                |> Keyframe.add_ Keyframe.InOutBack scoreRowSlideInLength 0
                                |> Keyframe.valueAt_ config.time
                        , height =
                            Keyframe.startAt_ latestHighscore.acceptedTime 0
                                |> Keyframe.add_ Keyframe.Linear scoreRowHeightGrowAnimationDelay 0
                                |> Keyframe.add_ Keyframe.Linear scoreRowHeightGrowAnimationLength tableRowHeight
                                |> Keyframe.valueAt_ config.time
                                |> round
                        , rowStyle =
                            [ Style.attentionBackground rowColor (InGameTime.inSeconds currentTime)
                            , Font.bold
                            ]
                        }

                    else
                        default

                ( _, _ ) ->
                    default

        placeEl =
            Element.el [ Font.color <| Helper.colorToElement indexColor ] (indexToPlaceText index)

        timePlusPenalty =
            Helper.timestamp (InGameTime.inMinutes (StageScore.timePlusPenalty score)) |> String.padLeft 10 ' '

        time =
            Helper.timestamp (InGameTime.inMinutes score.time)
    in
    tableRow
        placeEl
        score.name
        timePlusPenalty
        time
        (healthText score)
        |> Element.el
            ([ Element.moveLeft offset
             , Element.width Element.fill
             , Element.height <| Element.px height
             ]
                ++ rowStyle
            )


scoreRowSlideInDelay =
    scoreRowHeightGrowAnimationDelay |> Quantity.plus (RealTime.seconds 0.3)


scoreRowSlideInLength =
    RealTime.seconds 0.6


scoreRowHeightGrowAnimationDelay =
    RealTime.seconds 1


scoreRowHeightGrowAnimationLength : Quantity Float RealTime
scoreRowHeightGrowAnimationLength =
    RealTime.seconds 0.5


placeTextWidth =
    Element.px 30


indexToPlaceText index =
    (String.toUpper (Hex.toString index) ++ ".")
        |> Element.text
        |> Element.el [ Element.width Element.fill, Element.padding 4 ]


offsetScoreIndexAndColor : Config a -> Int -> ( Int, Color )
offsetScoreIndexAndColor config placeIndex =
    case getLatestHighscore config.highscoreState of
        LatestHighscore latestHighscore ->
            let
                startTime =
                    Quantity.sum
                        [ latestHighscore.acceptedTime
                        , scoreRowSlideInDelay
                        , scoreRowSlideInLength
                        , toFloat placeIndex * 0.07 |> RealTime.seconds
                        ]

                timeToUpdate =
                    startTime
                        |> Quantity.greaterThan config.time

                colorAnim =
                    Keyframe.startAt_ startTime 0
                        |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 0.01) 1
                        |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 0.4) 0
                        |> Keyframe.valueAt_ config.time

                isAfterNewScore =
                    case getHighscorePlace (getSortBy config.highscoreState) latestHighscore.scoreId config.highscoreState of
                        Just ( _, highscorePlace ) ->
                            highscorePlace < placeIndex

                        Nothing ->
                            False
            in
            if timeToUpdate && isAfterNewScore then
                ( placeIndex - 1, Color.black )

            else
                ( placeIndex, Color.rgb 0 colorAnim 0 )

        _ ->
            ( placeIndex, Color.black )


getHighscoreStage : ScoreId -> Model -> Maybe StageId
getHighscoreStage scoreId model =
    getAllHighscores model
        |> AssocList.filter (\_ value -> List.any (\( scoreId_, _ ) -> scoreId == scoreId_) value)
        |> AssocList.toList
        |> List.head
        |> Maybe.map Tuple.first


getHighscorePlace : StageScore.SortBy -> ScoreId -> Model -> Maybe ( StageId, Int )
getHighscorePlace sortBy scoreId model =
    case getHighscoreStage scoreId model of
        Just stageId ->
            getHighscores sortBy stageId model
                |> List.findIndex (\( scoreId_, _ ) -> scoreId == scoreId_)
                |> Maybe.map (Tuple.pair stageId)

        Nothing ->
            Nothing


view :
    MsgConfig a msg
    -> Config b
    -> StageId
    -> Quantity Float InGameTime
    -> Quantity Float InGameTime
    -> Element msg
view msgConfig config stageId firstShownTime currentTime =
    let
        rowOffset_ : Int -> Float
        rowOffset_ index =
            rowOffset config firstShownTime currentTime index

        extraRow =
            case getLatestHighscore config.highscoreState of
                LatestHighscore latestHighscore ->
                    let
                        startTime =
                            Quantity.sum
                                [ latestHighscore.acceptedTime
                                , scoreRowHeightGrowAnimationDelay
                                , scoreRowHeightGrowAnimationLength
                                , 0.1 * highscoreTableRowCount |> RealTime.seconds
                                ]
                    in
                    fallingOffEmptyScoreRow startTime config.time

                LatestScore latestScore ->
                    if latestScore.score.stageId == stageId then
                        let
                            stageScore =
                                latestScore.score |> toStageScore ""

                            timePlusPenalty =
                                Helper.timestamp (InGameTime.inMinutes (StageScore.timePlusPenalty stageScore)) |> String.padLeft 10 ' '

                            time =
                                Helper.timestamp (InGameTime.inMinutes stageScore.time)
                        in
                        Element.el
                            [ Element.moveDown tableRowHeight
                            , Style.attentionBackground rowColor (InGameTime.inSeconds currentTime)
                            , rowOffset_ (highscoreTableRowCount + 1) |> Element.moveLeft
                            ]
                            (tableRow
                                Element.none
                                "Your score"
                                timePlusPenalty
                                time
                                (healthText stageScore)
                            )

                    else
                        Element.el [ Element.height <| Element.px tableRowHeight ] Element.none

                NoLatestScore ->
                    Element.el [ Element.height <| Element.px tableRowHeight ] Element.none

        columnRows =
            getHighscores (getSortBy config.highscoreState) stageId config.highscoreState
                |> List.map (\score ( index, color ) -> scoreRow config firstShownTime currentTime score ( index, color ))
                |> List.padRight 16 (\( index, placeTextColor ) -> emptyScoreRow (rowOffset_ index) index placeTextColor)
                |> List.indexedMap (\index func -> func (offsetScoreIndexAndColor config index))
                |> (::) headerView
    in
    Element.column
        []
        [ title
        , Element.column
            [ Element.height <| Element.px <| (highscoreTableRowCount + 3) * tableRowHeight
            ]
            (columnRows ++ [ extraRow ])
        ]


headerView =
    Element.row
        [ Background.color (Helper.colorToElement rowColor), Element.width Element.fill, Element.height <| Element.px tableRowHeight ]
        [ Element.el [ Element.width placeTextWidth ] Element.none
        , Element.el [ Element.width <| Element.px nameTextWidth ] (Element.text "Name")
        , Element.el [ Element.width timestampPenaltyWidth, Font.alignRight ] (Element.text "Time + Penalty")
        , Element.el [ Element.width timestampWidth, Font.alignRight ] (Element.text "Time")
        , Element.el [ Element.width healthWidth, Font.center ] (Element.text "Health")
        ]
