module WireTests exposing (all)

import Chip
import Circuit exposing (WireId(..))
import Dict
import Editor.Circuit as Circuit
import Expect
import Fuzz exposing (Fuzzer)
import List.Extra as List
import ListHelper as List
import Point2d exposing (Point2d)
import Quantity exposing (Quantity)
import Set
import T exposing (T(..))
import Test exposing (..)
import Wire


all : Test
all =
    describe "A Test Suite"
        [ fuzz3 (Fuzz.list fuzzPoint2d) fuzzT Fuzz.float "Test Wire.moveT invariants" <|
            \nodes t worldSpeed ->
                let
                    wire =
                        Wire.init Nothing nodes Nothing

                    t1 =
                        Wire.clampT defaultCircuit wire t |> T.inT

                    result =
                        Wire.moveT defaultCircuit worldSpeed wire t |> T.inT
                in
                Expect.true "Invariant didn't hold."
                    (((worldSpeed < 0) && (result < t1 || (result == 0 && result == t1)))
                        || ((worldSpeed > 0) && (result > t1 || (result == toFloat (List.length nodes - 1) && result == t1)))
                        || (worldSpeed == 0 && result == t1)
                        || (List.isEmpty nodes && result == t1)
                    )
        , fuzz3 (Fuzz.list (Fuzz.intRange 0 2147483647)) fuzzT Fuzz.int "Wire.moveT worldSpeed matches distance travelled on straight line" <|
            \lengths t worldSpeed ->
                let
                    t1 =
                        Wire.clampT defaultCircuit wire t

                    nodes =
                        List.foldl
                            (\value ( n, count ) -> ( Point2d.fromCoordinates ( count, 0 ) :: n, count + toFloat value ))
                            ( [], 0 )
                            lengths
                            |> Tuple.first

                    wire =
                        Wire.init Nothing nodes Nothing

                    t2 =
                        Wire.moveT defaultCircuit (toFloat worldSpeed) wire t1

                    getDistance : Quantity Float T -> Quantity Float T -> Float
                    getDistance tFirst tSecond =
                        [ tFirst, tSecond ]
                            |> List.map (Wire.tPoint defaultCircuit wire >> Maybe.withDefault Point2d.origin)
                            |> List.pairwise
                            |> List.map (\( a, b ) -> Point2d.distanceFrom a b)
                            |> List.head
                            |> Maybe.withDefault 0

                    minT =
                        Quantity.zero

                    maxT =
                        List.length nodes - 1 |> toFloat |> T.t
                in
                if t2 == minT then
                    getDistance minT t1 |> Expect.atMost (abs <| toFloat worldSpeed)

                else if t2 == maxT then
                    getDistance t1 maxT |> Expect.atMost (toFloat worldSpeed)

                else
                    getDistance t1 t2
                        |> Expect.within (Expect.AbsoluteOrRelative 0.0001 0.0001) (abs <| toFloat worldSpeed)
        , test "Wire.moveT moves correct distance" <|
            \_ ->
                let
                    wire =
                        Wire.init Nothing [ Point2d.origin, Point2d.fromCoordinates ( 5, 0 ) ] Nothing
                in
                Wire.moveT defaultCircuit 2 wire (T.t 0.2)
                    |> T.inT
                    |> Expect.within (Expect.Absolute 0.000001) 0.6
        , test "Wire.moveT moves correct distance across node" <|
            \_ ->
                let
                    wire =
                        Wire.init
                            Nothing
                            [ Point2d.origin, Point2d.fromCoordinates ( 5, 0 ), Point2d.fromCoordinates ( 8, 0 ) ]
                            Nothing
                in
                Wire.moveT defaultCircuit 2 wire (T.t 0.8)
                    |> T.inT
                    |> Expect.within (Expect.Absolute 0.000001) 1.3333333333
        , test "Wire.moveT moves correct on second edge" <|
            \_ ->
                let
                    wire =
                        Wire.init
                            Nothing
                            [ Point2d.origin, Point2d.fromCoordinates ( 5, 0 ), Point2d.fromCoordinates ( 8, 0 ) ]
                            Nothing
                in
                Wire.moveT defaultCircuit 1 wire (T.t 1.333333333)
                    |> T.inT
                    |> Expect.within (Expect.Absolute 0.000001) 1.666666666
        , fuzz2 (Fuzz.list fuzzPoint2d) fuzzT "Wire.splitNodes invariants" <|
            \nodes t ->
                Expect.all
                    [ \( startNodes, endNodes ) ->
                        nodes
                            |> List.all
                                (\node ->
                                    List.any ((==) node) startNodes
                                        || List.any ((==) node) endNodes
                                )
                            |> Expect.true "Not all nodes accounted for"
                    , \( startNodes, endNodes ) ->
                        let
                            newNodesCount =
                                List.length startNodes + List.length endNodes

                            oldNodesCount =
                                List.length nodes
                        in
                        Expect.true
                            "Wrong number of nodes"
                            (newNodesCount >= oldNodesCount || newNodesCount - 2 <= oldNodesCount)
                    , \( startNodes, endNodes ) ->
                        let
                            a =
                                List.last startNodes

                            b =
                                List.head endNodes
                        in
                        Expect.true
                            "Last start node should equal last end node"
                            (a == b || a == Nothing || b == Nothing)
                    ]
                    (Wire.splitNodes nodes t)
        , test "Wire intersection test" <|
            \_ ->
                let
                    wire =
                        Wire.init
                            Nothing
                            [ Point2d.fromCoordinates ( 2, 1 ), Point2d.fromCoordinates ( 2, -1 ) ]
                            Nothing
                in
                Wire.intersections
                    defaultCircuit
                    wire
                    (Point2d.fromCoordinates ( 1, 0 ))
                    (Point2d.fromCoordinates ( 7, 0 ))
                    |> Expect.equal [ T.t (1 / 2) ]
        , fuzz2 Fuzz.float Fuzz.float "Signed distance returns correct value" <|
            \t0 t1 ->
                let
                    wire =
                        Wire.init
                            Nothing
                            [ Point2d.origin, Point2d.fromCoordinates ( 10, 0 ) ]
                            Nothing

                    circuit =
                        Circuit.from [ ( WireId 0, wire ) ] [] Nothing [] Dict.empty
                in
                wire
                    |> Wire.tSignedDistance circuit (T.t t0) (T.t t1)
                    |> Expect.within (Expect.AbsoluteOrRelative 0.001 0.001) ((clamp 0 1 t1 - clamp 0 1 t0) * 10)
        ]


defaultCircuit =
    Circuit.empty
        |> Circuit.addChip (Chip.init (Point2d.fromCoordinates ( 1, 0 )) { width = 3, height = 4 } Set.empty)
        |> Tuple.first
        |> Circuit.addWire
            (Wire.init
                Nothing
                [ Point2d.fromCoordinates ( 5, 2 ), Point2d.fromCoordinates ( 5, 4 ) ]
                Nothing
            )
        |> Tuple.first


fuzzPoint2d : Fuzzer Point2d
fuzzPoint2d =
    Fuzz.map2 (\x y -> Point2d.fromCoordinates ( x, y )) Fuzz.float Fuzz.float


fuzzT : Fuzzer (Quantity Float T)
fuzzT =
    Fuzz.map T.t Fuzz.float
