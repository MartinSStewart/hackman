# ToDo
- Show where electrons are going to be next cycle on a chip so that the player has more time to react
- Make it clear that reaching the ends of wires isn't fatal

- No sound effect plays when key pressing title buttons
- Down on mouse wheel should drag the view in the editor
- Fix scroll zoom being way too sensitive sometimes (happened on Chrome once)
- Add settings for changing controls (will help with letting the user know how to view the map)
- Be able to move wire nodes
- Be able to move agents
- Maybe "download" should be "save"?

- If the user is changing the color palette then the main menu replay should always show 
- Fix performance issue when the player has made many chip moves (it seems to be caused by iterating through the whole allChipMoves list on every frame)
- Agents should use additive blending instead of alpha blending
    - ❌ Doesn't look good on it's own, isn't worth implementing
- Using gamma encoding
    - ❌ Can't be done until there is support for rendering to textures
- Chip movement is difficult
    - Show cycle clock even when on wire?
    - Not obvious how to time chip movement
    - Make it obvious not to hit the center of the cycle bar
    - Add delay zone to clock cycle
- Dont lose speed on wire jumps

- Moving sideways off a chip causes the side movement to also move the player sideway on the wire
- When a chip is hacked, add a glow effect and temporarily make the chips left number larger
- Highlight the movement arrow that the player pressed
- Damage sound effects sometimes don't play when fast forwarding replay
- Triggers currently have to have unique names and it's kind of broken. Replace that with ids and so they can have non-unique names. This will be useful for defining non-contiguous trigger regions. 
- Having a wire endpoint that ends with two nodes directly on top of eachother can sometimes cause the player to spin around and go back along the same wire.
- Single wire nodes can be left behind when deleting a bunch of wires connected to chips.
- Work around svg sizing issue on Safari. The editor svg canvas has the wrong height. Adding the following lines to Editor.drawCanvas seems to fix the issue but it's laggy.
`
, style "height" (String.fromInt (Vector2d.yComponent model.canvasSize |> floor) ++ "px")
, style "position" "absolute"
`
Look more into this some other time
- Add visual indicator for where things are about to move while on a chip (with arrows probably)