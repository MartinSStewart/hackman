module JumpTests exposing (all)

import Circuit exposing (Chip, ChipEdgePoint, ChipId(..), Circuit, CircuitPosition(..), Wire, WireId(..), WiresAndChips)
import Dict exposing (Dict)
import Expect
import Fuzz exposing (Fuzzer)
import Helper
import Jump
import Point2d
import Set
import T exposing (T(..))
import Test exposing (..)
import Wire


all : Test
all =
    describe "Jump tests" <|
        [ test "Perpendicular wire has correct jump point directions" <|
            \_ ->
                let
                    circuit =
                        [ Wire.init Nothing [ Point2d.origin, Point2d.fromCoordinates ( 5, 0 ) ] Nothing
                        , Wire.init Nothing [ Point2d.fromCoordinates ( 6, -5 ), Point2d.fromCoordinates ( 6, 5 ) ] Nothing
                        ]
                            |> List.indexedMap Tuple.pair
                            |> Dict.fromList
                            |> defaultCircuit
                in
                Jump.agentJumpPoints circuit (Jump.init circuit) { wireId = WireId 0, t = T.t 1, data = Circuit.Forward }
                    |> getJumpDirections
                    |> Expect.equal
                        { left = Just Circuit.Backward, right = Just Circuit.Forward }
        , test "Parallel wire has correct jump point t" <|
            \_ ->
                Jump.agentJumpPoints
                    parallelWiresCircuit
                    (Jump.init parallelWiresCircuit)
                    { wireId = WireId 0, t = T.t 0, data = Circuit.Forward }
                    |> .right
                    |> (\a ->
                            case a of
                                Just ( _, { t } ) ->
                                    T.inT t |> Expect.within (Expect.Absolute 0.01) 0.2

                                Nothing ->
                                    Expect.fail "Expected to find a jump point on the right."
                       )
        , test "Other parallel wire has correct jump point t" <|
            \_ ->
                Jump.agentJumpPoints
                    parallelWiresCircuit
                    (Jump.init parallelWiresCircuit)
                    { wireId = WireId 1, t = T.t 0, data = Circuit.Forward }
                    |> .left
                    |> (\a ->
                            case a of
                                Just ( _, { t } ) ->
                                    T.inT t |> Expect.within (Expect.Absolute 0.01) 0.2

                                Nothing ->
                                    Expect.fail "Expected to find a jump point on the left."
                       )
        ]


parallelWiresCircuit =
    [ Wire.init Nothing [ Point2d.origin, Point2d.fromCoordinates ( 5, 0 ) ] Nothing
    , Wire.init Nothing [ Point2d.fromCoordinates ( 0, 1 ), Point2d.fromCoordinates ( 5, 1 ) ] Nothing
    ]
        |> List.indexedMap Tuple.pair
        |> Dict.fromList
        |> defaultCircuit


getJumpDirections jumpPoints =
    { left = jumpPoints.left |> Maybe.map (Tuple.second >> .direction)
    , right = jumpPoints.right |> Maybe.map (Tuple.second >> .direction)
    }


defaultCircuit wires =
    { wires = wires
    , chips =
        [ ( 0
          , { position = Point2d.fromCoordinates ( 1, 0 )
            , size = { width = 3, height = 4 }
            , hackedPoints = Set.empty
            }
          )
        ]
            |> Dict.fromList
    , triggers = Dict.empty
    }


fuzzWireDirection =
    Fuzz.bool |> Fuzz.map (\a -> Helper.ifElse a Circuit.Forward Circuit.Backward)
