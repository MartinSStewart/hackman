module InGameTests exposing (all)

import Chip
import Circuit exposing (Circuit, CircuitPosition(..), WireDirection(..), WireId(..))
import Codec.Serialize as Codec
import ColorPalette
import Dict
import Expect exposing (Expectation)
import Fuzz
import InGame
import InGame.Core as Core
import InGame.FrameNumber as FrameNumber
import Input
import Point2d
import Point3d
import Polygon2d
import Replay exposing (CompleteReplay)
import Set
import SketchPlane3d
import T
import Test exposing (Test, describe, only, test)
import Vector2i
import Vector3d
import Wire


all : Test
all =
    describe "InGame tests"
        [ test "line plane intersection test" <|
            \_ ->
                let
                    rayOrigin =
                        Point3d.fromCoordinates ( 0, 0, 3 )

                    rayVector =
                        Vector3d.fromComponents ( 0, 0, -1 )

                    plane =
                        SketchPlane3d.throughPoints
                            Point3d.origin
                            (Point3d.fromCoordinates ( 1, 0, 0 ))
                            (Point3d.fromCoordinates ( 0, 1, 0 ))
                in
                case plane of
                    Just plane_ ->
                        InGame.linePlaneIntersection rayOrigin rayVector plane_
                            |> Expect.equal (Just Point2d.origin)

                    Nothing ->
                        Expect.fail "Invalid plane"
        , test "line plane intersection test 2" <|
            \_ ->
                let
                    rayOrigin =
                        Point3d.fromCoordinates ( 2, 0, 1 )

                    rayVector =
                        Vector3d.fromComponents ( 0, 1, -1 )

                    plane =
                        SketchPlane3d.throughPoints
                            Point3d.origin
                            (Point3d.fromCoordinates ( 1, 0, 0 ))
                            (Point3d.fromCoordinates ( 0, 1, 0 ))
                in
                case plane of
                    Just plane_ ->
                        InGame.linePlaneIntersection rayOrigin rayVector plane_
                            |> Expect.equal (Just (Point2d.fromCoordinates ( 2, 1 )))

                    Nothing ->
                        Expect.fail "Invalid plane"
        , test "line plane intersection test 3" <|
            \_ ->
                let
                    rayOrigin =
                        Point3d.fromCoordinates ( 0, 0, 1 )

                    rayVector =
                        Vector3d.fromComponents ( 0, 1, -1 )

                    plane =
                        SketchPlane3d.throughPoints
                            (Point3d.fromCoordinates ( 1, 0, 0 ))
                            Point3d.origin
                            (Point3d.fromCoordinates ( 0, 1, 0 ))
                in
                case plane of
                    Just plane_ ->
                        InGame.linePlaneIntersection rayOrigin rayVector plane_
                            |> Expect.equal (Just (Point2d.fromCoordinates ( 1, 1 )))

                    Nothing ->
                        Expect.fail "Invalid plane"
        , Test.fuzz2 (Fuzz.floatRange -0.9999 0.9999) (Fuzz.floatRange -0.9999 0.9999) "inside convex polygon" <|
            \x y ->
                let
                    point =
                        Point2d.fromCoordinates ( x, y )

                    min =
                        Point2d.fromCoordinates ( -1, -1 )

                    max =
                        Point2d.fromCoordinates ( 1, 1 )

                    polygon =
                        Polygon2d.singleLoop
                            [ min
                            , Point2d.fromCoordinates ( 1, -1 )
                            , max
                            , Point2d.fromCoordinates ( -1, 1 )
                            ]
                in
                Expect.equal
                    True
                    (InGame.pointInConvexPolygon polygon point)
        , Test.fuzz2 (Fuzz.floatRange -1000 1000) (Fuzz.floatRange -1000 1000) "outside convex polygon" <|
            \x y ->
                let
                    point =
                        Point2d.fromCoordinates ( x, y )

                    min =
                        Point2d.fromCoordinates ( -1, -1 )

                    max =
                        Point2d.fromCoordinates ( 1, 1 )

                    polygon =
                        Polygon2d.singleLoop
                            [ min
                            , Point2d.fromCoordinates ( 1, -1 )
                            , max
                            , Point2d.fromCoordinates ( -1, 1 )
                            ]
                in
                if -2 < x && x < 2 && -2 < y && y < 2 then
                    -- The fuzzer generated an invalid value so ignore it.
                    Expect.pass

                else
                    Expect.equal
                        False
                        (InGame.pointInConvexPolygon polygon point)
        , test "Simple replay doesn't desync" <|
            \_ -> testReplay simpleReplay
        ]


testReplay : CompleteReplay -> Expectation
testReplay completeReplay =
    let
        ({ circuit } as initialState) =
            Replay.getInitialState completeReplay

        coreState =
            Core.init { circuit = circuit, lookupTable = Nothing }

        ( finishedCoreState, newFinishedReplay ) =
            List.repeat (Replay.replayLength_ completeReplay |> FrameNumber.value) ()
                |> List.foldl
                    (\() ( state, newReplay ) ->
                        let
                            input =
                                Replay.getInput completeReplay state
                        in
                        ( Core.step input state, Replay.addInput input newReplay )
                    )
                    ( coreState, Replay.init initialState )
    in
    newFinishedReplay
        |> Replay.endReplay ColorPalette.defaultColors finishedCoreState
        |> Replay.hashesAreEqual completeReplay
        |> Expect.true "Expected that replay hashes are equal"


simpleCircuit : Circuit
simpleCircuit =
    { wires =
        [ ( 0, Wire.init Nothing [ Point2d.origin, Point2d.fromCoordinates ( 10, 0 ) ] Nothing )
        , ( 1, Wire.init Nothing [ Point2d.fromCoordinates ( 0, 1 ), Point2d.fromCoordinates ( 10, 1 ) ] Nothing )
        ]
            |> Dict.fromList
    , chips = [ ( 0, Chip.init (Point2d.fromCoordinates ( 5, 5 )) (Vector2i.xy ( 2, 2 )) Set.empty ) ] |> Dict.fromList
    , name = ""
    , description = ""
    , playerStart = WirePosition { wireId = WireId 0, t = T.t 0.5, data = Forward }
    , agentsStart = []
    , triggers = Dict.empty
    }


simpleReplay : CompleteReplay
simpleReplay =
    let
        input0 =
            Input.setInput False False True False False False

        input1 =
            Input.setInput False True False False False False

        initialState =
            { circuit = simpleCircuit, lookupTable = Nothing }

        state =
            Core.init initialState
                |> Core.step input0
                |> Core.step input1
    in
    Replay.init initialState
        |> Replay.addInput input0
        |> Replay.addInput input1
        |> Replay.endReplay ColorPalette.defaultColors state
