module Sound exposing
    ( Model
    , Sound(..)
    , SoundError
    , SoundId
    , allSounds
    , defaultMasterVolume
    , defaultMusicVolume
    , defaultSoundEffectVolume
    , getMasterVolume
    , getMusicVolume
    , getSoundEffectVolume
    , init
    , initEmpty
    , load
    , play
    , playSpeech
    , setMasterVolume
    , setMusicVolume
    , setPlaybackRate
    , setSoundEffectVolume
    , setVolume
    , soundData
    , stop
    , stopSpeech
    , subscribe
    , updateCmd
    )

import Dict exposing (Dict)
import Helper exposing (ifElse)
import Json.Decode as JD
import Json.Encode as JE
import List.Nonempty exposing (Nonempty(..))
import Ports
import Quantity exposing (Quantity)
import RealTime exposing (RealTime)


type SoundType
    = IsMusic
    | IsSoundEffect


allSounds =
    [ Grind
    , Damage
    , ButtonClick
    , BackButtonClick
    , InGameMusic
    , InGameMusicGlitched
    , IntroMusic
    , StageComplete
    , ChipHacked
    , ChipMovementEarly
    , AnnouncerGo
    , AnnouncerGoGlitched
    , AnnouncerMission
    , StageUnlock0
    , StageUnlock1
    ]


type Sound
    = Grind
    | Damage
    | ButtonClick
    | BackButtonClick
    | InGameMusic
    | InGameMusicGlitched
    | IntroMusic
    | StageComplete
    | ChipHacked
    | ChipMovementEarly
    | AnnouncerGo
    | AnnouncerGoGlitched
    | AnnouncerMission
    | StageUnlock0
    | StageUnlock1


type alias SoundData =
    { defaultVolume : Float, path : String, soundType : SoundType, loops : Bool }


soundData : Sound -> SoundData
soundData sound =
    case sound of
        Grind ->
            SoundData 1.3 "sounds/grind.wav" IsSoundEffect True

        Damage ->
            SoundData 0.4 "sounds/damage.mp3" IsSoundEffect False

        ButtonClick ->
            SoundData 0.3 "sounds/button.mp3" IsSoundEffect False

        BackButtonClick ->
            SoundData 0.5 "sounds/back_button.mp3" IsSoundEffect False

        InGameMusic ->
            SoundData 0.3 "sounds/music.mp3" IsMusic True

        InGameMusicGlitched ->
            SoundData 0.5 "sounds/music_glitched.mp3" IsMusic False

        IntroMusic ->
            SoundData 0.3 "sounds/intro_music.mp3" IsMusic False

        StageComplete ->
            SoundData 1 "sounds/stage_complete.mp3" IsSoundEffect False

        ChipHacked ->
            SoundData 0.5 "sounds/chip_hacked.mp3" IsSoundEffect False

        ChipMovementEarly ->
            SoundData 1 "sounds/chip_movement_early.mp3" IsSoundEffect False

        AnnouncerGo ->
            SoundData 1 "sounds/announcer_go.mp3" IsSoundEffect False

        AnnouncerGoGlitched ->
            SoundData 1 "sounds/announcer_go_glitched.mp3" IsSoundEffect False

        AnnouncerMission ->
            SoundData 1 "sounds/announcer_mission.mp3" IsSoundEffect False

        StageUnlock0 ->
            SoundData 0.3 "sounds/stage_unlock0.mp3" IsSoundEffect False

        StageUnlock1 ->
            SoundData 0.3 "sounds/stage_unlock1.mp3" IsSoundEffect False


type SoundId
    = SoundId Int


type Model
    = Model Model_


type Speech
    = Speaking SpeechState
    | NotSpeaking


type alias SpeechState =
    { volume : Float
    , startTime : Quantity Float RealTime
    , text : String
    }


type alias Model_ =
    { sounds : Dict String SoundState
    , speech : Speech
    , masterVolume : Float
    , musicVolume : Float
    , soundEffectVolume : Float
    }


setMasterVolume : Float -> Model -> Model
setMasterVolume volume (Model model) =
    Model { model | masterVolume = volume }


setMusicVolume : Float -> Model -> Model
setMusicVolume volume (Model model) =
    Model { model | musicVolume = volume }


setSoundEffectVolume : Float -> Model -> Model
setSoundEffectVolume volume (Model model) =
    Model { model | soundEffectVolume = volume }


getMasterVolume : Model -> Float
getMasterVolume (Model model) =
    model.masterVolume


getMusicVolume : Model -> Float
getMusicVolume (Model model) =
    model.musicVolume


getSoundEffectVolume : Model -> Float
getSoundEffectVolume (Model model) =
    model.soundEffectVolume


type alias SoundState =
    { soundId : SoundId, volume : Float, startTime : Maybe (Quantity Float RealTime), playbackRate : Float }


initSoundState : SoundId -> SoundState
initSoundState soundId =
    { soundId = soundId, volume = 1, startTime = Nothing, playbackRate = 1 }


init : Dict String SoundId -> Maybe Model
init sounds =
    if List.all (\sound -> Dict.get (soundData sound).path sounds /= Nothing) allSounds && Dict.size sounds == List.length allSounds then
        { sounds = Dict.map (\_ v -> initSoundState v) sounds
        , speech = NotSpeaking
        , masterVolume = defaultMasterVolume
        , musicVolume = defaultMusicVolume
        , soundEffectVolume = defaultSoundEffectVolume
        }
            |> Model
            |> Just

    else
        Nothing


defaultMasterVolume =
    0.5


defaultMusicVolume =
    1


defaultSoundEffectVolume =
    1


initEmpty : Model
initEmpty =
    Model
        { sounds = Dict.empty
        , speech = NotSpeaking
        , masterVolume = defaultMasterVolume
        , musicVolume = defaultMusicVolume
        , soundEffectVolume = defaultSoundEffectVolume
        }


getSound : Sound -> Model -> Maybe SoundState
getSound sound (Model model) =
    Dict.get (soundData sound).path model.sounds


getValue : SoundId -> Int
getValue (SoundId soundId) =
    soundId


type ToJsMsg
    = UpdateSounds (List JE.Value)
    | LoadSound String Bool


encodeToJsMsg : ToJsMsg -> JE.Value
encodeToJsMsg msg =
    case msg of
        UpdateSounds commands ->
            JE.object
                [ ( "Constructor", JE.string "UpdateSounds" )
                , ( "updates", JE.list identity commands )
                ]

        LoadSound soundPath_ loops ->
            JE.object
                [ ( "Constructor", JE.string "LoadSound" )
                , ( "soundPath", JE.string soundPath_ )
                , ( "loops", JE.bool loops )
                ]


startSoundJson : Bool -> Quantity Float RealTime -> Quantity Float RealTime -> SoundId -> JE.Value
startSoundJson loops currentTime startTime soundId =
    JE.object
        [ ( "type", JE.int 0 )
        , ( "soundId", soundId |> getValue |> JE.int )
        , ( "delay", startTime |> Quantity.minus currentTime |> RealTime.inSeconds |> max 0 |> JE.float )
        , ( "offset", ifElse loops (startTime |> Quantity.minus currentTime |> RealTime.inSeconds) 0 |> JE.float )
        ]


stopSoundJson : SoundId -> JE.Value
stopSoundJson soundId =
    JE.object
        [ ( "type", JE.int 1 )
        , ( "soundId", soundId |> getValue |> JE.int )
        ]


setVolumeJson : Float -> SoundId -> JE.Value
setVolumeJson volume soundId =
    JE.object
        [ ( "type", JE.int 2 )
        , ( "soundId", soundId |> getValue |> JE.int )
        , ( "volume", JE.float volume )
        ]


setPlaybackRateJson : Float -> SoundId -> JE.Value
setPlaybackRateJson playbackRate soundId =
    JE.object
        [ ( "type", JE.int 3 )
        , ( "soundId", soundId |> getValue |> JE.int )
        , ( "playbackRate", JE.float playbackRate )
        ]


startSpeechJson : Float -> String -> Quantity Float RealTime -> Quantity Float RealTime -> JE.Value
startSpeechJson volume text currentTime startTime =
    JE.object
        [ ( "type", JE.int 4 )
        , ( "text", JE.string text )
        , ( "delay", startTime |> Quantity.minus currentTime |> RealTime.inSeconds |> max 0 |> JE.float )
        , ( "volume", JE.float volume )
        ]


stopSpeechJson : JE.Value
stopSpeechJson =
    JE.object
        [ ( "type", JE.int 5 )
        ]


type alias MsgConfig msg =
    { soundLoaded : String -> Result SoundError SoundId -> msg
    , portError : JD.Error -> msg
    }


subscribe msgConfig =
    Ports.fromJsPort (decodePort msgConfig)


decodePort : MsgConfig msg -> JD.Value -> msg
decodePort msgConfig value =
    case JD.decodeValue (decodeMsg msgConfig) value of
        Ok msg ->
            msg

        Err error ->
            msgConfig.portError error


decodeMsg : MsgConfig msg -> JD.Decoder msg
decodeMsg msgConfig =
    JD.field "Constructor" JD.string
        |> JD.andThen
            (\constructor ->
                case constructor of
                    "SoundLoaded" ->
                        JD.map2 msgConfig.soundLoaded
                            (JD.field "soundPath" JD.string)
                            (JD.field "result" (decodeResult decodeSoundError (JD.map SoundId JD.int)))

                    _ ->
                        JD.fail "Invalid constructor"
            )


decodeResult : JD.Decoder err -> JD.Decoder ok -> JD.Decoder (Result err ok)
decodeResult errorDecoder okDecoder =
    JD.field "Constructor" JD.string
        |> JD.andThen
            (\constructor ->
                case constructor of
                    "Ok" ->
                        JD.field "value" okDecoder |> JD.map Ok

                    "Err" ->
                        JD.field "value" errorDecoder |> JD.map Err

                    _ ->
                        JD.fail "Invalid constructor"
            )


decodeSoundError : JD.Decoder SoundError
decodeSoundError =
    JD.map2 SoundError
        (JD.field "message" JD.string)
        (JD.field "soundPath" JD.string)


play : Sound -> Quantity Float RealTime -> Model -> Model
play sound startTime model =
    updateSound (\state -> { state | volume = (soundData sound).defaultVolume, startTime = Just startTime }) sound model


playSpeech : String -> Quantity Float RealTime -> Model -> Model
playSpeech text startTime (Model model) =
    Model { model | speech = Speaking { volume = 1, text = text, startTime = startTime } }


stopSpeech : Model -> Model
stopSpeech (Model model) =
    Model { model | speech = NotSpeaking }


updateSound : (SoundState -> SoundState) -> Sound -> Model -> Model
updateSound updateFunc sound (Model model) =
    let
        soundState =
            getSound sound (Model model) |> Maybe.map updateFunc
    in
    case soundState of
        Just soundState_ ->
            { model | sounds = Dict.insert (soundData sound).path soundState_ model.sounds } |> Model

        Nothing ->
            Model model


stop : Sound -> Model -> Model
stop sound model =
    updateSound (\state -> { state | startTime = Nothing }) sound model


load : Sound -> Cmd msg
load sound =
    let
        { path, loops } =
            soundData sound
    in
    LoadSound path loops
        |> encodeToJsMsg
        |> Ports.toJsPort


setVolume : Float -> Sound -> Model -> Model
setVolume volume sound model =
    updateSound (\state -> { state | volume = volume }) sound model


setPlaybackRate : Float -> Sound -> Model -> Model
setPlaybackRate playbackRate sound model =
    updateSound (\state -> { state | playbackRate = playbackRate }) sound model


getVolume : SoundState -> Model -> Sound -> Float
getVolume state model sound =
    case (soundData sound).soundType of
        IsMusic ->
            state.volume * getMusicVolume model * getMasterVolume model

        IsSoundEffect ->
            state.volume * getSoundEffectVolume model * getMasterVolume model


getSpeechVolume : Model -> Float
getSpeechVolume model =
    getSoundEffectVolume model * getMasterVolume model


soundUpdates : Quantity Float RealTime -> Model -> Model -> List JE.Value
soundUpdates currentTime previousModel model =
    allSounds
        |> List.concatMap
            (\sound ->
                case ( getSound sound previousModel, getSound sound model ) of
                    ( Just oldSound, Just newSound ) ->
                        let
                            volumeJson =
                                if getVolume oldSound previousModel sound /= getVolume newSound model sound && newSound.startTime /= Nothing then
                                    [ setVolumeJson (getVolume newSound model sound) newSound.soundId ]

                                else
                                    []

                            startStopJson =
                                if oldSound.startTime /= newSound.startTime then
                                    case newSound.startTime of
                                        Just startTime ->
                                            [ setVolumeJson (getVolume newSound model sound) newSound.soundId
                                            , startSoundJson (soundData sound).loops currentTime startTime newSound.soundId
                                            ]

                                        Nothing ->
                                            [ stopSoundJson newSound.soundId ]

                                else
                                    []

                            playbackRateJson =
                                case ( oldSound.startTime, newSound.startTime ) of
                                    ( Nothing, Just _ ) ->
                                        [ setPlaybackRateJson newSound.playbackRate newSound.soundId ]

                                    ( Just _, Just _ ) ->
                                        if oldSound.playbackRate /= newSound.playbackRate || oldSound.startTime /= newSound.startTime then
                                            [ setPlaybackRateJson newSound.playbackRate newSound.soundId ]

                                        else
                                            []

                                    ( _, Nothing ) ->
                                        []
                        in
                        volumeJson ++ startStopJson ++ playbackRateJson

                    ( Nothing, Just newSound ) ->
                        case newSound.startTime of
                            Just startTime ->
                                [ setVolumeJson (getVolume newSound model sound) newSound.soundId
                                , startSoundJson (soundData sound).loops currentTime startTime newSound.soundId
                                ]

                            Nothing ->
                                [ stopSoundJson newSound.soundId ]

                    ( _, Nothing ) ->
                        []
            )


getSpeech : Model -> Speech
getSpeech (Model model) =
    model.speech


speechUpdates : Quantity Float RealTime -> Model -> Model -> List JE.Value
speechUpdates currentTime previousModel model =
    case ( getSpeech previousModel, getSpeech model ) of
        ( NotSpeaking, Speaking speechState ) ->
            [ startSpeechJson (getSpeechVolume model) speechState.text currentTime speechState.startTime ]

        ( Speaking _, NotSpeaking ) ->
            [ stopSpeechJson ]

        ( Speaking old, Speaking new ) ->
            let
                startTimeChanged =
                    old.startTime |> Quantity.equalWithin (RealTime.seconds 0.0001) new.startTime |> not
            in
            if startTimeChanged || old.text /= new.text then
                [ stopSpeechJson, startSpeechJson (getSpeechVolume model) new.text currentTime new.startTime ]

            else
                []

        ( NotSpeaking, NotSpeaking ) ->
            []


updateCmd : Quantity Float RealTime -> Model -> Model -> Cmd msg
updateCmd currentTime previousModel model =
    soundUpdates currentTime previousModel model
        |> List.append (speechUpdates currentTime previousModel model)
        |> UpdateSounds
        |> encodeToJsMsg
        |> Ports.toJsPort


{-| Error message from JS sound library.
-}
type alias SoundError =
    { message : String
    , path : String
    }
