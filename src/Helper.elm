module Helper exposing
    ( addCmd
    , addCmdNone
    , between
    , ceilingBy
    , cmdLoopback
    , colorToElement
    , distanceBetweenLines
    , filler
    , fillerPortion
    , floorBy
    , floorByWithOffset
    , fontSize
    , frac
    , handleMouseEvent
    , ifElse
    , intervalElapsed
    , intervalOffsetElapsed
    , isBetween
    , jumpToBottom
    , linePointDistance
    , mergeCmdTuple
    , monospace
    , multVector
    , nearestLinePoint
    , nearestLineT
    , noneAttribute
    , randomFromTime
    , roundBy
    , roundByWithOffset
    , secondsPerStep
    , setAlpha
    , spacing
    , stepTowards
    , stringToParagraphs
    , timestamp
    , while
    )

import Basics.Extra as Basics
import Browser.Dom as Dom
import Color exposing (Color)
import Element exposing (Element)
import Element.Font as Font
import Html.Attributes
import LineSegment2d exposing (LineSegment2d)
import Point2d exposing (Point2d)
import Quantity exposing (Quantity)
import Random
import T exposing (T(..))
import Task
import Vector2d exposing (Vector2d)


rawQuantity : Quantity Float a -> Float
rawQuantity (Quantity.Quantity value) =
    value


intervalElapsed : Quantity Float a -> Quantity Float a -> Quantity Float a -> Bool
intervalElapsed interval previousTime currentTime =
    floorBy (rawQuantity interval) (rawQuantity previousTime)
        /= floorBy (rawQuantity interval) (rawQuantity currentTime)


intervalOffsetElapsed : Quantity Float a -> Quantity Float a -> Quantity Float a -> Quantity Float a -> Bool
intervalOffsetElapsed interval offset previousTime currentTime =
    floorByWithOffset (rawQuantity interval) (rawQuantity offset) (rawQuantity previousTime)
        /= floorByWithOffset (rawQuantity interval) (rawQuantity offset) (rawQuantity currentTime)


roundBy : Float -> Float -> Float
roundBy interval value =
    value / interval |> round |> toFloat |> (*) interval


ceilingBy : Float -> Float -> Float
ceilingBy interval value =
    value / interval |> ceiling |> toFloat |> (*) interval


floorBy : Float -> Float -> Float
floorBy interval value =
    value / interval |> floor |> toFloat |> (*) interval


floorByWithOffset : Float -> Float -> Float -> Float
floorByWithOffset interval offset value =
    floorBy interval (value - offset) |> (+) offset


roundByWithOffset : Float -> Float -> Float -> Float
roundByWithOffset interval offset value =
    roundBy interval (value - offset) |> (+) offset


secondsPerStep : Quantity Float a
secondsPerStep =
    1 / 60 |> Quantity.Quantity


addCmdNone : a -> ( a, Cmd msg )
addCmdNone a =
    ( a, Cmd.none )


addCmd : Cmd msg -> a -> ( a, Cmd msg )
addCmd cmd a =
    ( a, cmd )


mergeCmdTuple : ( a, Cmd msg ) -> ( a, Cmd msg ) -> ( a, Cmd msg )
mergeCmdTuple ( _, cmd ) ( newModel, newCmd ) =
    ( newModel, Cmd.batch [ newCmd, cmd ] )


fontSize : Int -> Element.Attr a msg
fontSize scale =
    Element.modular 20 1.4 scale |> round |> Font.size


spacing : Int -> Int
spacing scale =
    Element.modular 12 2 scale |> round


filler : Element msg
filler =
    Element.el
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        Element.none


fillerPortion : Int -> Element msg
fillerPortion portion =
    Element.el
        [ Element.width <| Element.fillPortion portion
        , Element.height <| Element.fillPortion portion
        ]
        Element.none


noneAttribute : Element.Attribute msg
noneAttribute =
    Element.htmlAttribute <| Html.Attributes.style "" ""


nearestLinePoint : LineSegment2d -> Point2d -> Point2d
nearestLinePoint line point =
    let
        ( lineStart, lineEnd ) =
            LineSegment2d.endpoints line

        ( x, y ) =
            Point2d.coordinates point

        ( x1, y1 ) =
            Point2d.coordinates lineStart

        ( x2, y2 ) =
            Point2d.coordinates lineEnd

        a =
            x - x1

        b =
            y - y1

        c =
            x2 - x1

        d =
            y2 - y1

        dot =
            a * c + b * d

        len_sq =
            c * c + d * d

        param =
            if len_sq /= 0 then
                dot / len_sq

            else
                -1
    in
    if param < 0 then
        Point2d.fromCoordinates ( x1, y1 )

    else if param > 1 then
        Point2d.fromCoordinates ( x2, y2 )

    else
        Point2d.fromCoordinates ( x1 + param * c, y1 + param * d )


nearestLineT : LineSegment2d -> Point2d -> Quantity Float T
nearestLineT line point =
    let
        nearestPoint =
            nearestLinePoint line point

        ( lineStart, lineEnd ) =
            LineSegment2d.endpoints line

        d0 =
            Point2d.distanceFrom lineStart nearestPoint

        d1 =
            Point2d.distanceFrom lineStart lineEnd
    in
    d0 / d1 |> T.t


linePointDistance : LineSegment2d -> Point2d -> Float
linePointDistance line point =
    nearestLinePoint line point |> Point2d.distanceFrom point


distanceBetweenLines : LineSegment2d -> LineSegment2d -> Float
distanceBetweenLines line0 line1 =
    linePointDistance line0 (LineSegment2d.startPoint line1)
        |> min (linePointDistance line0 (LineSegment2d.endPoint line1))
        |> min (linePointDistance line1 (LineSegment2d.startPoint line0))
        |> min (linePointDistance line1 (LineSegment2d.endPoint line0))


{-| Acts like clamp but the order of the minimum and maximum parameters doesn't
matter.
-}
between : number -> number -> number -> number
between bound0 bound1 value =
    clamp (min bound0 bound1) (max bound0 bound1) value


{-| Returns true if a value lies between two other numbers.
The order of the bounding numbers doesn't matter.
-}
isBetween : number -> number -> number -> Bool
isBetween bound0 bound1 value =
    between bound0 bound1 value == value


{-| Add two numbers together but ignore the sign on the second.

    addMagnitude -1 6 == 5

    addMagnitude 1 -6 == -7

    addMagnitude -1 -6 == -5

    addMagnitude -3 -1 == 0

-}
addMagnitude : number -> number -> number
addMagnitude addBy value =
    if value > 0 then
        value + addBy |> max 0

    else
        value - addBy |> min 0


ifElse : Bool -> a -> a -> a
ifElse condition ifTrue ifFalse =
    if condition then
        ifTrue

    else
        ifFalse


frac : Float -> Float
frac value =
    value |> floor |> toFloat |> (-) value


cmdLoopback : msg -> Cmd msg
cmdLoopback msg =
    Task.succeed msg
        |> Task.perform identity


while : (a -> Maybe a) -> a -> a
while loop initial =
    case loop initial of
        Just value ->
            while loop value

        Nothing ->
            initial


jumpToBottom : msg -> String -> Cmd msg
jumpToBottom msg id =
    Dom.getViewportOf id
        |> Task.andThen (\info -> Dom.setViewportOf id 0 info.scene.height)
        |> Task.attempt (\_ -> msg)


randomFromTime : Quantity Float unit -> Random.Seed
randomFromTime time =
    rawQuantity time * 10000 |> floor |> Random.initialSeed


multVector : Vector2d -> Vector2d -> Vector2d
multVector v0 v1 =
    let
        ( x0, y0 ) =
            Vector2d.components v0

        ( x1, y1 ) =
            Vector2d.components v1
    in
    Vector2d.fromComponents ( x0 * x1, y0 * y1 )


colorToElement : Color -> Element.Color
colorToElement color =
    let
        { red, green, blue, alpha } =
            Color.toRgba color
    in
    Element.rgba red green blue alpha



{- Converts a String to a list of paragraph elements broken up by line breaks -}


stringToParagraphs : String -> List (Element msg)
stringToParagraphs text =
    String.split "\n" text |> List.map (Element.text >> List.singleton >> Element.paragraph [])


stepTowards : Quantity number unit -> Quantity number unit -> Quantity number unit -> Quantity number unit
stepTowards stepSize target value =
    if value |> Quantity.minus target |> Quantity.abs |> Quantity.lessThanOrEqualTo stepSize then
        target

    else if value |> Quantity.lessThan target then
        value |> Quantity.plus stepSize

    else
        value |> Quantity.minus stepSize


monospace : Element.Attribute msg
monospace =
    Font.family [ Font.monospace ]


timestamp : Float -> String
timestamp timeInMinutes =
    let
        minutesText =
            timeInMinutes |> floor |> String.fromInt

        seconds =
            Basics.fractionalModBy 1 timeInMinutes |> (*) 60

        millisecondsText =
            Basics.fractionalModBy 1 seconds |> (*) 1000 |> floor |> String.fromInt |> String.padLeft 3 '0'

        secondsText =
            seconds |> floor |> String.fromInt |> String.padLeft 2 '0'
    in
    minutesText ++ ":" ++ secondsText ++ "." ++ millisecondsText


handleMouseEvent : (Point2d -> msg) -> { a | offsetPos : ( Float, Float ) } -> msg
handleMouseEvent msg =
    .offsetPos
        >> Point2d.fromCoordinates
        >> msg


setAlpha : Float -> Color -> Color
setAlpha alpha color =
    let
        { red, green, blue } =
            Color.toRgba color
    in
    Color.fromRgba { red = red, green = green, blue = blue, alpha = alpha }
