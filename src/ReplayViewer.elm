module ReplayViewer exposing (Model, Msg, getShowReplayParseError, init, setShowReplayParseError, step, update, view)

import Basics.Extra exposing (flip)
import Color
import Draw
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Helper exposing (ifElse)
import InGame
import InGame.Core as Core
import InGameTime exposing (InGameTime)
import Input exposing (Input)
import Mesh exposing (ChipCapVertex, CircuitVertex, MapOverlayVertex, Vertex)
import Mission exposing (Mission(..))
import Point2d
import Quantity exposing (Quantity)
import RealTime exposing (RealTime)
import Replay exposing (CompleteReplay)
import Sound
import Style
import Svg
import Svg.Attributes
import WebGL exposing (Mesh)


type Model
    = Model Model_


type alias Model_ =
    { replay : Replay.CompleteReplay
    , fastForward : Bool
    , fastForwardClickTime : Quantity Float RealTime
    , circuitMesh : Mesh CircuitVertex
    , circuitMapMesh : Mesh MapOverlayVertex
    , chipCapsMesh : Mesh ChipCapVertex
    , musicStartTime : Quantity Float RealTime
    , replayable : InGame.Replayable
    , showReplayParseError : Bool
    }


type Msg
    = RestartReplay
    | ToggleFastForward


type alias MsgConfig msg =
    { noOp : msg
    , back : msg
    , internalMsg : Msg -> msg
    , loadReplay : msg
    }


step : InGame.Config a -> Model -> ( Model, Sound.Model )
step config viewerModel =
    let
        steps =
            List.repeat (ifElse (getFastForward viewerModel) 3 1) ()
                |> List.foldr
                    (\_ { model, sounds } ->
                        InGame.replayableStep config True sounds (Replay.getInput (getReplay viewerModel) model.coreState) model
                    )
                    { model = getReplayable viewerModel, sounds = config.sounds }
    in
    ( setReplayable steps.model viewerModel, steps.sounds |> soundStep viewerModel )


getFastForward (Model model) =
    model.fastForward


getReplay (Model model) =
    model.replay


getReplayable (Model model) =
    model.replayable


setReplayable replayable (Model model) =
    Model { model | replayable = replayable }


getShowReplayParseError : Model -> Bool
getShowReplayParseError (Model model) =
    model.showReplayParseError


setShowReplayParseError : Bool -> Model -> Model
setShowReplayParseError show (Model model) =
    Model { model | showReplayParseError = show }


update : Msg -> Model -> Model
update msg (Model model) =
    case msg of
        RestartReplay ->
            init model.musicStartTime model.replay

        ToggleFastForward ->
            { model | fastForward = not model.fastForward } |> Model


soundStep : Model -> Sound.Model -> Sound.Model
soundStep (Model model) sounds =
    sounds |> Sound.play Sound.InGameMusic model.musicStartTime


view : MsgConfig msg -> InGame.Config a -> Input -> Model -> Element msg
view msgConfig config input (Model model) =
    let
        cameraTransform =
            InGame.cameraTransform InGame.cameraDefaultZ model.replayable
    in
    Element.el
        [ Element.inFront <| InGame.gameplayHud config model.replayable
        , Element.inFront <| topMenu msgConfig
        , Element.inFront <| replayToolbarView config.time msgConfig model
        , Element.inFront <| progressBar (Core.getTime model.replayable.coreState) model.replay
        ]
        (InGame.webGLView
            config
            input
            cameraTransform
            (Color.rgba 0 0 0 0)
            model
            model.replayable
            identity
            True
            InGame.PlayerControlledMap
        )


progressBar : Quantity Float InGameTime -> CompleteReplay -> Element msg
progressBar time replay =
    let
        t =
            Quantity.ratio time (Replay.replayLength replay) |> clamp 0 1
    in
    Element.row
        [ Background.color <| Element.rgba 0 0 0 0.3
        , Element.height <| Element.px 5
        , Element.width Element.fill
        , Element.alignBottom
        ]
        [ Element.el
            [ Element.width <| Element.fillPortion <| round (t * 10000)
            , Element.height Element.fill
            , Background.color <| Element.rgba 0 1 0 0.5
            ]
            Element.none
        , Element.el
            [ Element.width <| Element.fillPortion <| round ((1 - t) * 10000)
            , Element.height Element.fill
            ]
            Element.none
        ]


replayToolbarView : Quantity Float RealTime -> MsgConfig msg -> Model_ -> Element msg
replayToolbarView time msgConfig model =
    Element.row
        [ Element.alignBottom
        , Element.centerX
        , Element.paddingEach { left = 5, right = 5, bottom = 10, top = 5 }
        , Element.spacing 30
        , Element.onRight (ifElse model.showReplayParseError replayParseErrorView Element.none)
        ]
        [ InGame.restartReplayButton (msgConfig.internalMsg RestartReplay)
        , InGame.fastForwardButton (msgConfig.internalMsg ToggleFastForward) time model
        , loadReplayButton msgConfig
        ]


replayParseErrorView =
    Element.el
        [ Font.color <| Element.rgb 1 0 0
        , Element.paddingXY 20 0
        , Element.centerY
        , Font.size 30
        ]
        (Element.text "Failed to load replay.")


topMenu : MsgConfig msg -> Element msg
topMenu msgConfig =
    Element.row
        [ Style.menuBackground
        , Element.alignTop
        , Element.alignLeft
        , Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 0, bottomRight = 4 }
        ]
        [ Input.button
            (Border.roundEach { topLeft = 0, topRight = 0, bottomLeft = 0, bottomRight = 4 } :: Style.menuFormButton)
            { onPress = Just msgConfig.back
            , label = Element.el [ Element.padding 4 ] <| Element.text "Back"
            }
        ]


init : Quantity Float RealTime -> Replay.CompleteReplay -> Model
init currentTime replay =
    let
        { circuit } =
            Replay.getInitialState replay
    in
    { circuitMesh = circuit |> Mesh.circuit |> Mesh.toMesh
    , circuitMapMesh = circuit |> Mesh.circuitOverlayMap |> Mesh.toMesh
    , chipCapsMesh = circuit |> Mesh.chipCaps |> Mesh.toMesh
    , replay = replay
    , fastForward = False
    , fastForwardClickTime = Quantity.zero
    , musicStartTime = currentTime
    , replayable = InGame.startMission NormalMission { circuit = circuit, lookupTable = Nothing }
    , showReplayParseError = False
    }
        |> Model


loadReplayButton : MsgConfig msg -> Element msg
loadReplayButton msgConfig =
    let
        height =
            60

        centerX =
            width / 2

        width =
            height * 7 / 8

        bottomLineThickness =
            6

        bottomLine =
            Draw.lineStrip
                Color.black
                bottomLineThickness
                [ Point2d.fromCoordinates ( 0, height - bottomLineThickness / 2 )
                , Point2d.fromCoordinates ( width, height - bottomLineThickness / 2 )
                ]

        arrow =
            Draw.arrow
                Color.black
                10
                (centerX - 3)
                (Point2d.fromCoordinates ( centerX, height * 6.5 / 8 ))
                (Point2d.fromCoordinates ( centerX, 0 ))
    in
    Input.button
        []
        { onPress = Just msgConfig.loadReplay
        , label =
            Svg.svg
                [ width |> String.fromFloat |> flip (++) "px" |> Svg.Attributes.width
                , height |> String.fromFloat |> flip (++) "px" |> Svg.Attributes.height
                ]
                [ bottomLine, arrow ]
                |> Element.html
        }
