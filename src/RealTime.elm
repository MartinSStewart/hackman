module RealTime exposing (RealTime, inSeconds, milliseconds, seconds)

import Quantity exposing (Quantity)


type RealTime
    = RealTime Never


inSeconds : Quantity number RealTime -> number
inSeconds (Quantity.Quantity time) =
    time


seconds : number -> Quantity number RealTime
seconds time =
    Quantity.Quantity time


milliseconds : Float -> Quantity Float RealTime
milliseconds time =
    Quantity.Quantity (time / 1000)
