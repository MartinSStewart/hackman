module Changelog exposing (view)

import Element exposing (Element)
import Element.Background as Background
import Element.Font as Font
import Html.Attributes
import Vector2i exposing (Vector2i)


view : Vector2i -> Element msg
view windowSize =
    Element.column
        [ Element.width Element.fill, Font.color <| Element.rgb 0.3 0.3 0.3 ]
        [ header
        , Element.el
            [ Element.width Element.fill, Element.htmlAttribute <| Html.Attributes.class "select" ]
          <|
            Element.column
                [ Element.spacing 12
                , Element.padding 16
                , Element.height <| Element.maximum (windowSize.height // 3) Element.shrink
                , Element.centerX
                , Element.width <| Element.maximum 1000 Element.shrink
                ]
                [ version "1.1"
                    [ "Improved keyboard navigation"
                    , "Most text is no longer selectable (I was tired of accidentally highlighting the intro animation)"
                    , "Removed gamepad support, it wasn't well implemented and was preventing the escape key from being used for keyboard navigation"
                    , "Added a changelog"
                    ]
                , version "1.0" [ "The entire game" ]
                ]
        ]


titleLine alignment =
    Element.el
        [ alignment, Element.width Element.fill, Element.height <| Element.px 1, Background.color <| Element.rgb 0.3 0.3 0.3 ]
        Element.none


title =
    Element.el [ Font.size 30, Element.centerX, Element.paddingXY 12 0 ] (Element.text "Changelog")


header =
    Element.row [ Element.width Element.fill ] [ titleLine Element.alignLeft, title, titleLine Element.alignRight ]


version : String -> List String -> Element msg
version versionText changes =
    let
        body =
            changes
                |> List.map ((++) "• " >> Element.text >> List.singleton >> Element.paragraph [])
                |> Element.column [ Element.spacing 4, Element.paddingEach { left = 16, top = 0, bottom = 0, right = 0 } ]
    in
    Element.column
        [ Font.size 20, Element.spacing 4 ]
        [ Element.text ("Version " ++ versionText)
        , body
        ]
