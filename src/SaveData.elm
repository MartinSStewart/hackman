module SaveData exposing (SaveData, codec, defaultSaveData)

import AssocList
import Codec.Serialize as Codec exposing (Codec)
import CodecEx as Codec
import ColorPalette exposing (ColorPalette)
import HighscoreTable exposing (ScoreId)
import Sound
import StageData exposing (StageId)
import StageScore exposing (StageScore)


type alias SaveData =
    { masterVolume : Float
    , musicVolume : Float
    , soundEffectVolume : Float
    , antialias : Bool
    , highscores : AssocList.Dict StageId (List ( ScoreId, StageScore ))
    , hideFramerateWarning : Bool
    , colorPalette : ColorPalette
    }


defaultSaveData : SaveData
defaultSaveData =
    { masterVolume = Sound.defaultMasterVolume
    , musicVolume = Sound.defaultMusicVolume
    , soundEffectVolume = Sound.defaultSoundEffectVolume
    , antialias = False
    , highscores = AssocList.empty
    , hideFramerateWarning = False
    , colorPalette = ColorPalette.defaultColors
    }


type SaveDataVersion
    = SaveDataV0 SaveData


codec : Codec SaveData
codec =
    Codec.customType
        (\encodeV0 value ->
            case value of
                SaveDataV0 value_ ->
                    encodeV0 value_
        )
        |> Codec.variant1 SaveDataV0 codecV0
        |> Codec.finishCustomType
        |> Codec.map
            (\value ->
                case value of
                    SaveDataV0 value_ ->
                        value_
            )
            SaveDataV0


codecV0 : Codec SaveData
codecV0 =
    Codec.record SaveData
        |> Codec.field .masterVolume Codec.float
        |> Codec.field .musicVolume Codec.float
        |> Codec.field .soundEffectVolume Codec.float
        |> Codec.field .antialias Codec.bool
        |> Codec.field .highscores (Codec.assocList StageData.stageIdCodec (Codec.list (Codec.tuple HighscoreTable.scoreIdCodec stageScoreCodec)))
        |> Codec.field .hideFramerateWarning Codec.bool
        |> Codec.field .colorPalette ColorPalette.codec
        |> Codec.finishRecord


stageScoreCodec : Codec StageScore
stageScoreCodec =
    Codec.record StageScore
        |> Codec.field .time Codec.quantity
        |> Codec.field .health Codec.quantityInt
        |> Codec.field .badChipMoves Codec.quantityInt
        |> Codec.field .goodChipMoves Codec.quantityInt
        |> Codec.field .name Codec.string
        |> Codec.finishRecord
