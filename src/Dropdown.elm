module Dropdown exposing (view)

import Element exposing (Element)
import Html
import Html.Attributes
import Html.Events
import List.Extra as List
import List.Nonempty exposing (Nonempty)


view : (a -> msg) -> a -> Nonempty a -> (a -> String) -> Element msg
view onSelect selected values toText =
    let
        valueList =
            List.Nonempty.toList values

        selectedIndex =
            List.findIndex ((==) selected) valueList |> Maybe.withDefault 0
    in
    valueList
        |> List.indexedMap
            (\index value ->
                Html.option
                    [ Html.Attributes.value (String.fromInt index)
                    , Html.Attributes.selected (selectedIndex == index)
                    ]
                    [ toText value |> Html.text ]
            )
        |> Html.select
            [ Html.Events.onInput
                (\input ->
                    input
                        |> String.toInt
                        |> Maybe.map (\index -> List.Nonempty.get index values)
                        |> Maybe.withDefault (List.Nonempty.head values)
                        |> onSelect
                )
            , Html.Attributes.style "font-size" "16px"
            ]
        |> Element.html
        |> Element.el []
