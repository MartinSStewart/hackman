module Editor.SnapSettings exposing (SnapSettings, getAlongWire, getToAngle, getToGrid, init, toggleAlongWire, toggleToAngle, toggleToGrid)


type SnapSettings
    = SnapSettings
        { toGrid : Bool
        , toAngle : Bool
        , alongWire : Bool
        }


toggleToGrid : SnapSettings -> SnapSettings
toggleToGrid (SnapSettings settings) =
    SnapSettings { settings | toGrid = not settings.toGrid }


toggleToAngle : SnapSettings -> SnapSettings
toggleToAngle (SnapSettings settings) =
    SnapSettings { settings | toAngle = not settings.toAngle }


toggleAlongWire : SnapSettings -> SnapSettings
toggleAlongWire (SnapSettings settings) =
    SnapSettings { settings | alongWire = not settings.alongWire }


getToGrid : SnapSettings -> Bool
getToGrid (SnapSettings settings) =
    settings.toGrid


getToAngle : SnapSettings -> Bool
getToAngle (SnapSettings settings) =
    settings.toAngle


getAlongWire : SnapSettings -> Bool
getAlongWire (SnapSettings settings) =
    settings.alongWire


init : SnapSettings
init =
    SnapSettings
        { toGrid = True
        , toAngle = True
        , alongWire = True
        }
