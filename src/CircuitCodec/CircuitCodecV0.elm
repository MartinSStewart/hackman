module CircuitCodec.CircuitCodecV0 exposing (circuit, circuitPosition)

import Chip
import Circuit
    exposing
        ( AgentStartType(..)
        , Chip
        , ChipEdgePoint
        , ChipId(..)
        , ChipPosition_
        , Circuit
        , CircuitPosition(..)
        , PlayerStart
        , Trigger
        , Wire
        , WireDirection(..)
        , WireId(..)
        , WirePosition_
        )
import Codec.Serialize as Codec exposing (Codec)
import CodecEx as Codec


circuit : Codec Circuit
circuit =
    Codec.record
        (\wires chips name description _ playerStart agentsStart triggers ->
            Circuit wires chips name description playerStart agentsStart triggers
        )
        |> Codec.field .wires (Codec.dict Codec.int wire)
        |> Codec.field .chips (Codec.dict Codec.int chip)
        |> Codec.field .name Codec.string
        |> Codec.field .description Codec.string
        -- This was an id counter but it isn't needed anymore.
        |> Codec.field (always 0) Codec.int
        |> Codec.field .playerStart (circuitPosition wireDirection Codec.cardinal)
        |> Codec.field .agentsStart (Codec.list agentStartType)
        |> Codec.field .triggers (Codec.dict Codec.string trigger)
        |> Codec.finishRecord


agentStartType : Codec AgentStartType
agentStartType =
    Codec.customType
        (\passive chaser value ->
            case value of
                PassiveAgentStart { position } ->
                    passive position

                ChaserAgentStart { position } ->
                    chaser position
        )
        |> Codec.variant1 (\a -> PassiveAgentStart { position = a }) (circuitPosition wireDirection Codec.cardinal)
        |> Codec.variant1 (\a -> ChaserAgentStart { position = a }) (circuitPosition wireDirection Codec.cardinal)
        |> Codec.finishCustomType


chip : Codec Chip
chip =
    Codec.record Chip.init
        |> Codec.field .position Codec.point2d
        |> Codec.field .size Codec.vector2i
        |> Codec.field .hackedPoints (Codec.set (Codec.tuple Codec.int Codec.int))
        |> Codec.finishRecord


wire : Codec Wire
wire =
    Codec.record Wire
        |> Codec.field .start (Codec.maybe chipEdgePoint)
        |> Codec.field .end (Codec.maybe chipEdgePoint)
        |> Codec.field .nodes (Codec.list Codec.point2d)
        |> Codec.finishRecord


chipEdgePoint : Codec ChipEdgePoint
chipEdgePoint =
    Codec.record ChipEdgePoint
        |> Codec.field .chipId chipId
        |> Codec.field .side Codec.cardinal
        |> Codec.field .offset Codec.int
        |> Codec.finishRecord


chipId : Codec ChipId
chipId =
    Codec.int |> Codec.map ChipId (\(ChipId a) -> a)


wireDirection : Codec WireDirection
wireDirection =
    Codec.customType
        (\forward backward value ->
            case value of
                Forward ->
                    forward

                Backward ->
                    backward
        )
        |> Codec.variant0 Forward
        |> Codec.variant0 Backward
        |> Codec.finishCustomType


circuitPosition : Codec a -> Codec b -> Codec (CircuitPosition a b)
circuitPosition wireDataCodec chipDataCodec =
    Codec.customType
        (\chipPosition wirePosition value ->
            case value of
                ChipPosition chipPosition__ ->
                    chipPosition chipPosition__

                WirePosition wirePosition__ ->
                    wirePosition wirePosition__
        )
        |> Codec.variant1 ChipPosition (chipPosition_ chipDataCodec)
        |> Codec.variant1 WirePosition (wirePosition_ wireDataCodec)
        |> Codec.finishCustomType


wirePosition_ : Codec wireData -> Codec (WirePosition_ wireData)
wirePosition_ dataCodec =
    Codec.record WirePosition_
        |> Codec.field .wireId wireId
        |> Codec.field .t Codec.quantity
        |> Codec.field .data dataCodec
        |> Codec.finishRecord


chipPosition_ : Codec chipData -> Codec (ChipPosition_ chipData)
chipPosition_ dataCodec =
    Codec.record ChipPosition_
        |> Codec.field .chipId chipId
        |> Codec.field .position Codec.point2i
        |> Codec.field .chipData dataCodec
        |> Codec.finishRecord


wireId : Codec WireId
wireId =
    Codec.int |> Codec.map WireId (\(WireId a) -> a)


trigger : Codec Trigger
trigger =
    Codec.boundingBox2d |> Codec.map Trigger .region
