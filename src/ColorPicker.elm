module ColorPicker exposing (Model, Msg, colorInCircle, hsvToRgb, init, rgbToHsv, update, view)

import Angle exposing (Angle)
import Basics.Extra as Basics exposing (flip)
import Circle2d
import Color exposing (Color)
import Direction2d
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Geometry.Svg as Svg
import Helper
import Hex
import Html.Events.Extra.Mouse as Mouse
import Image exposing (Image)
import Image.Color
import List.Extra as List
import Point2d exposing (Point2d)
import Svg
import Svg.Attributes
import Vector2d


init : Model
init =
    { isDragging = False, userHexInput = Nothing, tempHue = Angle.radians 0, tempSaturation = 0 }


colorInCircle : Float -> Point2d -> Color
colorInCircle value pos =
    hsvInCircle value pos |> hsvToRgb


hsvInCircle : Float -> Point2d -> Hsv
hsvInCircle value point =
    case Vector2d.lengthAndDirection (Vector2d.from colorCenter point) of
        Just ( length, direction ) ->
            { hue = Direction2d.toAngle direction |> Angle.radians
            , saturation = min 1 (length * 2)
            , value = value
            }

        Nothing ->
            { hue = Angle.radians 0, saturation = 0, value = value }


width =
    200


colorCenter =
    Point2d.fromCoordinates ( 0.5, 0.5 )


colorCircle : Int -> Image
colorCircle diameter =
    let
        d =
            toFloat diameter
    in
    List.range 0 (diameter ^ 2 - 1)
        |> List.map
            (\index ->
                let
                    point =
                        Point2d.fromCoordinates
                            ( (toFloat (remainderBy diameter index) + 0.5) / d
                            , index // diameter |> toFloat |> (+) 0.5 |> (*) (1 / d)
                            )
                in
                colorInCircle 1 point
            )
        |> Image.Color.fromList diameter


colorCircle200 =
    Element.image [] { src = "images/color_circle.png", description = "" }


type Msg
    = MouseLeave
    | MouseDown Point2d
    | MouseUp Point2d
    | MouseMove Point2d
    | ValueSliderChanged Float
    | HexCodeChanged String


type alias Model =
    { isDragging : Bool
    , userHexInput : Maybe String

    -- If the value is set to zero we want to remember where the color wheel selector was.
    , tempHue : Angle
    , tempSaturation : Float
    }


mousePosToColor : Float -> Point2d -> Color
mousePosToColor value =
    mousePosToLocal >> colorInCircle value


mousePosToLocal : Point2d -> Point2d
mousePosToLocal =
    Point2d.scaleAbout Point2d.origin (1 / toFloat width)


mouseInCircle : Point2d -> Bool
mouseInCircle =
    mousePosToLocal >> Point2d.distanceFrom colorCenter >> (>=) 0.5


update : Msg -> Color -> Model -> ( Model, Color )
update msg color model =
    let
        { value } =
            rgbToHsv color

        hsvInCircle_ =
            mousePosToLocal >> hsvInCircle value
    in
    case msg of
        MouseLeave ->
            ( { model | isDragging = False }, color )

        MouseDown mousePos ->
            if mouseInCircle mousePos then
                ( { model
                    | isDragging = True
                    , tempHue = hsvInCircle_ mousePos |> .hue
                    , tempSaturation = hsvInCircle_ mousePos |> .saturation
                  }
                    |> resetUserHexInput
                , mousePosToColor value mousePos
                )

            else
                ( model, color )

        MouseUp mousePos ->
            if model.isDragging then
                ( { model
                    | isDragging = False
                    , tempHue = hsvInCircle_ mousePos |> .hue
                    , tempSaturation = hsvInCircle_ mousePos |> .saturation
                  }
                    |> resetUserHexInput
                , mousePosToColor value mousePos
                )

            else
                ( model, color )

        MouseMove mousePos ->
            if model.isDragging then
                ( { model
                    | tempHue = hsvInCircle_ mousePos |> .hue
                    , tempSaturation = hsvInCircle_ mousePos |> .saturation
                  }
                    |> resetUserHexInput
                , mousePosToColor value mousePos
                )

            else
                ( model, color )

        ValueSliderChanged newValue ->
            ( model |> resetUserHexInput
            , rgbToHsv color
                |> (\a ->
                        { a
                            | value = newValue
                            , hue =
                                if a.value <= minValue then
                                    model.tempHue

                                else
                                    a.hue
                            , saturation =
                                if a.value <= minValue then
                                    model.tempSaturation

                                else
                                    a.saturation
                        }
                   )
                |> hsvToRgb
            )

        HexCodeChanged hexText ->
            case hexToColor hexText of
                Just newColor ->
                    let
                        hsv =
                            rgbToHsv newColor
                    in
                    ( { model | userHexInput = Just hexText, tempHue = hsv.hue, tempSaturation = hsv.saturation }
                    , newColor
                    )

                Nothing ->
                    ( { model | userHexInput = Just hexText }, color )


resetUserHexInput : Model -> Model
resetUserHexInput model =
    { model | userHexInput = Nothing }


view : Color -> Model -> Element Msg
view color model =
    Element.column
        [ Element.spacing 8 ]
        [ Element.row
            [ Element.width Element.fill, Element.height Element.fill, Element.spacing 16 ]
            [ colorWheel color model, Element.el [ Element.paddingXY 0 8, Element.height Element.fill ] (valueSlider model color) ]
        , Element.el [ Element.centerX ] (textInput color model)
        ]


minValue =
    0.00001


colorWheel : Color -> Model -> Element Msg
colorWheel color model =
    let
        hsv =
            rgbToHsv color

        selectorPos =
            (if hsv.value > minValue then
                Vector2d.fromPolarComponents ( hsv.saturation / 2, Angle.inRadians hsv.hue )

             else
                Vector2d.fromPolarComponents ( model.tempSaturation / 2, Angle.inRadians model.tempHue )
            )
                |> flip Point2d.translateBy colorCenter
                |> Point2d.scaleAbout Point2d.origin width

        selector =
            Svg.svg
                [ Svg.Attributes.viewBox <| "0 0 " ++ String.fromInt width ++ " " ++ String.fromInt width
                , Svg.Attributes.pointerEvents "none"
                , Svg.Attributes.overflow "visible"
                ]
                [ Svg.circle2d
                    [ Svg.Attributes.stroke "white"
                    , Svg.Attributes.strokeWidth "2px"
                    , Svg.Attributes.fill "none"
                    , Svg.Attributes.pointerEvents "none"
                    ]
                    (Circle2d.withRadius 4 selectorPos)
                , Svg.circle2d
                    [ Svg.Attributes.stroke "black"
                    , Svg.Attributes.strokeWidth "1px"
                    , Svg.Attributes.fill "none"
                    , Svg.Attributes.pointerEvents "none"
                    ]
                    (Circle2d.withRadius 5 selectorPos)
                ]
    in
    colorCircle200
        |> Element.el [ Border.rounded width, Element.clip, Element.alpha hsv.value ]
        |> Element.el
            [ Border.rounded width
            , Background.color <| Element.rgb 0 0 0
            ]
        |> Element.el
            [ Element.inFront <| Element.html selector
            , Events.onMouseLeave MouseLeave
            , Mouse.onDown (Helper.handleMouseEvent MouseDown) |> Element.htmlAttribute
            , Mouse.onUp (Helper.handleMouseEvent MouseUp) |> Element.htmlAttribute
            , Mouse.onMove (Helper.handleMouseEvent MouseMove) |> Element.htmlAttribute
            ]


valueSlider : Model -> Color -> Element Msg
valueSlider model color =
    let
        hsv =
            rgbToHsv color

        maxValueColor =
            if hsv.value > minValue then
                { hsv | value = 1 } |> hsvToRgb

            else
                { value = 1, hue = model.tempHue, saturation = model.tempSaturation } |> hsvToRgb

        minValueColor =
            Color.black
    in
    Input.slider
        [ Element.width <| Element.px 16
        , Element.height Element.fill
        , Background.gradient
            { angle = 0, steps = [ minValueColor, maxValueColor ] |> List.map Helper.colorToElement }
        ]
        { onChange = ValueSliderChanged
        , label = Input.labelHidden "Color value slider"
        , min = 0
        , max = 1
        , value = hsv.value
        , thumb =
            Input.thumb
                [ Border.width 4
                , Element.width <| Element.px 24
                , Element.height <| Element.px 14
                , Border.rounded 8
                , Border.color <| Element.rgb 1 1 1
                ]
        , step = Nothing
        }


textInput : Color -> Model -> Element Msg
textInput color model =
    let
        backgroundColor =
            case model.userHexInput of
                Just userHexInput ->
                    case hexToColor userHexInput of
                        Just _ ->
                            Background.color <| Element.rgb 1 1 1

                        Nothing ->
                            Background.color <| Element.rgb 0.9 0.1 0.1

                Nothing ->
                    Background.color <| Element.rgb 1 1 1
    in
    Input.text
        [ Font.size 20, backgroundColor, Element.paddingXY 8 4, Element.width <| Element.px 120 ]
        { onChange = HexCodeChanged
        , text = model.userHexInput |> Maybe.withDefault (colorToHex color)
        , placeholder = Nothing
        , label = Input.labelLeft [ Font.color <| Element.rgb 1 1 1, Element.centerY ] <| Element.text "Hex code"
        }


type alias Hsv =
    { hue : Angle, saturation : Float, value : Float }


colorToHex : Color -> String
colorToHex color =
    let
        { red, green, blue } =
            Color.toRgba color

        red255 =
            round (red * 255) |> Hex.toString |> String.padLeft 2 '0'

        green255 =
            round (green * 255) |> Hex.toString |> String.padLeft 2 '0'

        blue255 =
            round (blue * 255) |> Hex.toString |> String.padLeft 2 '0'
    in
    red255 ++ green255 ++ blue255 |> String.toUpper


hexToColor : String -> Maybe Color
hexToColor hex =
    case hex |> String.toLower |> String.toList |> List.dropWhile (\c -> c == '$' || c == '#') of
        a :: b :: c :: d :: e :: f :: [] ->
            let
                red =
                    [ a, b ] |> String.fromList |> Hex.fromString

                green =
                    [ c, d ] |> String.fromList |> Hex.fromString

                blue =
                    [ e, f ] |> String.fromList |> Hex.fromString
            in
            Result.map3 Color.rgb255 red green blue |> Result.toMaybe

        _ ->
            Nothing


rgbToHsv : Color -> Hsv
rgbToHsv color =
    let
        { red, green, blue } =
            Color.toRgba color

        cMax =
            max red green |> max blue

        cMin =
            min red green |> min blue

        delta =
            cMax - cMin
    in
    { hue =
        (if red == cMax then
            (green - blue) / delta |> Basics.fractionalModBy 6

         else if green == cMax then
            (blue - red) / delta |> (+) 2

         else
            (red - green) / delta |> (+) 4
        )
            |> (*) 60
            |> nanOrInfDefaultsTo 0
            |> Angle.degrees
    , saturation = nanOrInfDefaultsTo 0 (delta / cMax)
    , value = cMax
    }


nanOrInfDefaultsTo : Float -> Float -> Float
nanOrInfDefaultsTo default value =
    if isNaN value || isInfinite value then
        default

    else
        value


hsvToRgb : Hsv -> Color
hsvToRgb { hue, saturation, value } =
    let
        hueDegrees =
            Angle.inDegrees hue |> Basics.fractionalModBy 360

        c =
            value * saturation

        x =
            c * (hueDegrees / 60 |> Basics.fractionalModBy 2 |> (+) -1 |> abs |> (-) 1)

        m =
            value - c
    in
    (if hueDegrees < 60 then
        ( c, x, 0 )

     else if hueDegrees < 120 then
        ( x, c, 0 )

     else if hueDegrees < 180 then
        ( 0, c, x )

     else if hueDegrees < 240 then
        ( 0, x, c )

     else if hueDegrees < 300 then
        ( x, 0, c )

     else
        ( c, 0, x )
    )
        |> (\( r, g, b ) ->
                Color.rgb (r + m) (g + m) (b + m)
           )
