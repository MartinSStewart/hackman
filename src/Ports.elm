port module Ports exposing (fromJsPort, toJsPort)

import Json.Decode as JD
import Json.Encode as JE


port toJsPort : JE.Value -> Cmd msg


port fromJsPort : (JD.Value -> msg) -> Sub msg
