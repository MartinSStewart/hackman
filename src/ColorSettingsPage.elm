module ColorSettingsPage exposing (..)

import Color exposing (Color)
import ColorPalette exposing (ColorPalette, ColorSelection(..))
import ColorPicker
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Lazy
import Helper
import Html.Attributes
import Html.Events
import Style
import Url exposing (Url)
import UrlData
import Vector2i exposing (Vector2i)


init : Model
init =
    { colorSelection = Nothing, colorPicker = ColorPicker.init }


type alias Config a =
    { a | windowSize : Vector2i, colorPalette : ColorPalette, url : Url }


type alias Model =
    { colorSelection : Maybe ColorSelection, colorPicker : ColorPicker.Model }


type alias MsgConfig msg =
    { noOp : msg
    , userSelectedLinkText : msg
    , userPressedColorSelection : ColorSelection -> msg
    , userPressedBackButton : msg
    , userPressedRestButton : msg
    , colorPickerMsg : ColorPicker.Msg -> msg
    }


view : MsgConfig msg -> Config a -> Model -> Element.Element msg
view msgConfig config colorSettings =
    Element.column
        [ Element.alignLeft
        , Element.height Element.fill
        , Style.menuBackground
        ]
        [ scrollable msgConfig config colorSettings, footer msgConfig config ]


colorSchemeLinkId : String
colorSchemeLinkId =
    "colorSchemeLinkId"


footer : MsgConfig msg -> Config a -> Element msg
footer msgConfig config =
    Element.column
        [ Element.padding 16, Element.alignBottom ]
        [ Element.el [ Element.paddingXY 8 16, Element.width Element.fill ] <|
            Input.text
                [ Element.paddingXY 8 4
                , Font.size 20
                , Element.htmlAttribute <| Html.Attributes.readonly True
                , Element.htmlAttribute <| Html.Events.onFocus msgConfig.userSelectedLinkText
                , Element.htmlAttribute <| Html.Attributes.id colorSchemeLinkId
                ]
                { onChange = \_ -> msgConfig.noOp
                , text = UrlData.toUrl config.url { colorPalette = config.colorPalette }
                , placeholder = Nothing
                , label = Input.labelAbove [ Font.color <| Element.rgb 1 1 1 ] <| Element.text "Color palette link"
                }
        , Element.row
            [ Element.spacing 16 ]
            [ backButton msgConfig, Element.el [ Element.alignRight ] (resetButton msgConfig) ]
        ]


footerHeight =
    180


scrollable : MsgConfig msg -> Config a -> Model -> Element msg
scrollable msgConfig config colorSettings =
    Element.column
        [ Element.height <| Element.px <| config.windowSize.height - footerHeight
        , Element.spacing 16
        , Element.scrollbarY
        , Element.padding 16
        , Element.width Element.fill
        ]
        [ Element.Lazy.lazy3 colorPickers msgConfig config.colorPalette colorSettings
        ]


colorList =
    [ PaletteBackground
    , PaletteWireMiddle
    , PaletteWireEdge
    , PaletteChip
    , PaletteChipLegs
    , PaletteChipHacked
    , PalettePassiveAgent
    , PaletteChaserAgent
    , PalettePlayer
    , PalettePlayerDamage
    , PaletteMovementArrows
    , PaletteMapWire
    , PaletteMapChip
    , PaletteMapChipHacked
    ]
        |> List.sortBy colorName


colorPickers : MsgConfig msg -> ColorPalette -> Model -> Element msg
colorPickers msgConfig colorPalette model =
    colorList
        |> List.map
            (\selection ->
                colorPicker
                    msgConfig
                    (msgConfig.userPressedColorSelection selection)
                    (colorName selection)
                    (model.colorSelection == Just selection)
                    (ColorPalette.getColor selection colorPalette)
                    model.colorPicker
            )
        |> Element.column [ Element.width Element.fill, Element.height Element.fill ]


colorName : ColorSelection -> String
colorName selection =
    case selection of
        PaletteBackground ->
            "Background"

        PaletteWireMiddle ->
            "Wire middle"

        PaletteWireEdge ->
            "Wire edge"

        PaletteChip ->
            "Chip"

        PaletteChipLegs ->
            "Chip legs"

        PaletteChipHacked ->
            "Chip hacked"

        PaletteChaserAgent ->
            "Chaser agent"

        PalettePassiveAgent ->
            "Passive agent"

        PalettePlayer ->
            "Player"

        PalettePlayerDamage ->
            "Player damaged"

        PaletteMovementArrows ->
            "Movement arrows"

        PaletteMapWire ->
            "Map wire"

        PaletteMapChip ->
            "Map chip"

        PaletteMapChipHacked ->
            "Map chip hacked"


backButton : MsgConfig msg -> Element msg
backButton msgConfig =
    Input.button
        (Font.size 40 :: Element.alignBottom :: Style.menuFormButton)
        { onPress = Just msgConfig.userPressedBackButton
        , label = Element.text "Back"
        }


resetButton : MsgConfig msg -> Element msg
resetButton msgConfig =
    Input.button
        (Font.size 40 :: Element.alignRight :: Style.menuFormButton)
        { onPress = Just msgConfig.userPressedRestButton
        , label = Element.text "Reset"
        }


title =
    Element.el
        [ Font.size 40, Font.color <| Element.rgb 1 1 1, Element.centerX ]
        (Element.text "Color picker")


colorPicker : MsgConfig msg -> msg -> String -> Bool -> Color -> ColorPicker.Model -> Element.Element msg
colorPicker msgConfig msg name selected color colorPickerModel =
    let
        colorElement =
            Element.el
                [ Background.color <| Helper.colorToElement color
                , Element.width <| Element.px 26
                , Element.height <| Element.px 26
                , Element.alignRight
                , Border.width 2
                , Border.color <| Element.rgba 0 0 0 0.5
                , Border.rounded 10
                ]
                Element.none
    in
    Element.column
        [ Element.width Element.fill ]
        [ Input.button
            [ Element.width Element.fill
            , Element.paddingXY 10 10
            , Element.mouseOver <| [ Background.color <| Element.rgba 1 1 1 0.5 ]
            , Font.color <| Element.rgb 1 1 1
            ]
            { onPress = Just msg
            , label =
                Element.row
                    [ Font.size 12
                    , Font.color <| Element.rgb 1 1 1
                    , Element.width Element.fill
                    ]
                    [ if selected then
                        Element.text "▼"

                      else
                        Element.text "▲"
                    , Element.el [ Font.size 24, Element.paddingXY 14 0 ] (Element.text name)
                    , colorElement
                    ]
            }
        , if selected then
            ColorPicker.view color colorPickerModel
                |> Element.map msgConfig.colorPickerMsg
                |> Element.el [ Element.centerX, Element.paddingXY 0 8 ]

          else
            Element.none
        ]
