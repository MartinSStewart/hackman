module InGame.Core exposing
    ( ChipMovementInput
    , InitialState
    , MissionStatus(..)
    , Model
    , agentChipPosition
    , agentWorldPosition
    , chipDirectionPressed
    , chipMoveStart
    , chipMoveTiming
    , chipsRemaining
    , codec
    , framesPerSecond
    , getAgents
    , getAllChipMoves
    , getChipMoveStart
    , getCircuit
    , getCurrentFrame
    , getJumpLookup
    , getMoveOnChip
    , getPlayer
    , getPreviousInput
    , getTime
    , hackedChipCount
    , init
    , introAnimationLength
    , isLeavingChip
    , missionStatus
    , moveOnChip_
    , secondsPerFrame
    , step
    )

{-| Core handles any gameplay state and logic that needs to support replaying and isn't purely visual.
-}

import Agent exposing (AgentType(..), ChaserAgent_, Damaged_, Health(..), PassiveAgent_, WireTransition)
import Array
import Basics.Extra exposing (flip)
import BoundingBox2i
import Cardinal exposing (Cardinal)
import Chip
import Circuit exposing (ChipId, Circuit, CircuitPosition(..), WireDirection(..), WireId, WiresAndChips)
import CircuitCodec
import CircuitCodec.CircuitCodecV0 as CircuitCodecV0
import Codec.Serialize as Codec exposing (Codec)
import CodecEx as Codec
import Direction2d
import Helper exposing (ifElse)
import InGame.ChipMoveTime as ChipMoveTime exposing (ChipMoveTime, ChipMoveTiming)
import InGame.CycleTime as CycleTime exposing (CycleTime)
import InGame.FrameNumber as FrameNumber exposing (FrameNumber)
import InGameTime exposing (InGameTime)
import Input exposing (Input)
import Jump exposing (JumpPoint)
import Keyframe
import List.Extra as List
import Maybe.Extra as Maybe
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Quantity exposing (Quantity)
import Random
import RandomEx as Random
import Set
import T exposing (T(..))
import Vector2d
import Vector2i
import Wire


type Model
    = Model Model_


type alias Model_ =
    { circuit : Circuit
    , agents : List AgentType
    , player : Agent.Player
    , moveOnChip : ChipMovementInput
    , previousInput : Input
    , currentFrame : Quantity Int FrameNumber
    , allChipMoves : List ChipMoveTime
    , jumpLookup : Jump.LookupTable
    }


codec : Codec Model
codec =
    Codec.record Model_
        |> Codec.field .circuit CircuitCodec.circuitVersion
        |> Codec.field .agents (Codec.list agentTypeCodec)
        |> Codec.field .player playerCodec
        |> Codec.field .moveOnChip chipMovementInputCodec
        |> Codec.field .previousInput Input.codec
        |> Codec.field .currentFrame Codec.quantityInt
        |> Codec.field .allChipMoves (Codec.list chipMoveTimeCodec)
        |> Codec.field .jumpLookup Jump.codec
        |> Codec.finishRecord
        |> Codec.map Model (\(Model m) -> m)


chipMovementInputCodec : Codec ChipMovementInput
chipMovementInputCodec =
    Codec.record ChipMovementInput
        |> Codec.field .frameNumber Codec.quantityInt
        |> Codec.field .direction (Codec.maybe Codec.cardinal)
        |> Codec.finishRecord


playerCodec : Codec Agent.Player
playerCodec =
    Codec.record Agent.Player
        |> Codec.field .position (CircuitCodecV0.circuitPosition wireDataCodec chipDataCodec)
        |> Codec.field .health healthCodec
        |> Codec.finishRecord


healthCodec : Codec Agent.Health
healthCodec =
    Codec.customType
        (\maxHealthEncoder damagedEncoder value ->
            case value of
                MaxHealth ->
                    maxHealthEncoder

                Damaged health ->
                    damagedEncoder health
        )
        |> Codec.variant0 MaxHealth
        |> Codec.variant1 Damaged damagedCodec
        |> Codec.finishCustomType


damagedCodec : Codec Damaged_
damagedCodec =
    Codec.record Damaged_
        |> Codec.field .health Codec.int
        |> Codec.field .lastDamaged Codec.quantity
        |> Codec.finishRecord


agentTypeCodec : Codec AgentType
agentTypeCodec =
    Codec.customType
        (\passiveAgentEncoder chaserAgentEncoder value ->
            case value of
                PassiveAgent passiveAgent ->
                    passiveAgentEncoder passiveAgent

                ChaserAgent chaserAgent ->
                    chaserAgentEncoder chaserAgent
        )
        |> Codec.variant1 PassiveAgent passiveAgentCodec
        |> Codec.variant1 ChaserAgent chaserAgentCodec
        |> Codec.finishCustomType


passiveAgentCodec : Codec PassiveAgent_
passiveAgentCodec =
    Codec.record PassiveAgent_
        |> Codec.field .position (CircuitCodecV0.circuitPosition wireDataCodec chipDataCodec)
        |> Codec.finishRecord


chaserAgentCodec : Codec ChaserAgent_
chaserAgentCodec =
    Codec.record ChaserAgent_
        |> Codec.field .position (CircuitCodecV0.circuitPosition wireDataCodec chipDataCodec)
        |> Codec.finishRecord


wireDataCodec : Codec Agent.WireData
wireDataCodec =
    Codec.record Agent.WireData
        |> Codec.field .direction wireDirectionCodec
        |> Codec.field .transitioning (Codec.maybe wireTransitionCodec)
        |> Codec.field .speedFactor Codec.float
        |> Codec.finishRecord


wireTransitionCodec : Codec WireTransition
wireTransitionCodec =
    Codec.record WireTransition
        |> Codec.field .start Codec.point2d
        |> Codec.field .t Codec.quantity
        |> Codec.finishRecord


wireDirectionCodec : Codec WireDirection
wireDirectionCodec =
    Codec.enum Forward [ Backward ]


chipDataCodec : Codec Agent.ChipData
chipDataCodec =
    Codec.record Agent.ChipData
        |> Codec.field .direction Codec.cardinal
        |> Codec.field .previousPoint Codec.point2i
        |> Codec.finishRecord


chipMoveTimeCodec : Codec ChipMoveTime
chipMoveTimeCodec =
    Codec.record ChipMoveTime
        |> Codec.field .moveTime Codec.quantity
        |> Codec.field .relativeToCycle Codec.quantity
        |> Codec.finishRecord


getCircuit : Model -> Circuit
getCircuit (Model model) =
    model.circuit


getPlayer : Model -> Agent.Player
getPlayer (Model model) =
    model.player


getAgents : Model -> List AgentType
getAgents (Model model) =
    model.agents


getTime : Model -> Quantity Float InGameTime
getTime (Model model) =
    FrameNumber.time framesPerSecond model.currentFrame


getPreviousTime : Model -> Quantity Float InGameTime
getPreviousTime (Model model) =
    FrameNumber.previous model.currentFrame |> FrameNumber.time framesPerSecond


getPreviousInput : Model -> Input
getPreviousInput (Model model) =
    model.previousInput


getMoveOnChip : Model -> ChipMovementInput
getMoveOnChip (Model model) =
    model.moveOnChip


getCurrentFrame : Model -> Quantity Int FrameNumber
getCurrentFrame (Model model) =
    model.currentFrame


getChipMoveStart : Model -> Quantity Float InGameTime
getChipMoveStart (Model model) =
    model.moveOnChip.frameNumber |> FrameNumber.time framesPerSecond


getAllChipMoves : Model -> List ChipMoveTime
getAllChipMoves (Model model) =
    model.allChipMoves


getJumpLookup : Model -> Jump.LookupTable
getJumpLookup (Model model) =
    model.jumpLookup


type alias InitialState =
    { lookupTable : Maybe Jump.LookupTable
    , circuit : Circuit
    }


init : InitialState -> Model
init initialState =
    { circuit = initialState.circuit
    , agents = initialState.circuit.agentsStart |> List.map Agent.spawn
    , player =
        { position = Agent.fromStartPosition initialState.circuit.playerStart
        , health = Agent.MaxHealth
        }
    , moveOnChip = { frameNumber = FrameNumber.init, direction = Nothing }
    , previousInput = Input.noInput
    , currentFrame = FrameNumber.init
    , allChipMoves = []
    , jumpLookup =
        case initialState.lookupTable of
            Just lookupTable ->
                lookupTable

            Nothing ->
                Jump.init initialState.circuit
    }
        |> Model


type alias ChipMovementInput =
    { frameNumber : Quantity Int FrameNumber, direction : Maybe Cardinal }


secondsPerFrame : Quantity Float InGameTime
secondsPerFrame =
    1 / framesPerSecond |> InGameTime.seconds


{-| The number of animation frames per second. Increase this number to make the game run in slow motion.
-}
framesPerSecond : number
framesPerSecond =
    60 * 1


cycleDefaultLength : Quantity Float InGameTime
cycleDefaultLength =
    InGameTime.seconds 0.45


agentHitRadius : Float
agentHitRadius =
    0.5


playerHitRadius : Float
playerHitRadius =
    0.5


hackedPointUpdateDuration : Quantity Float InGameTime
hackedPointUpdateDuration =
    InGameTime.seconds 0.2


introAnimationLength : Quantity Float InGameTime
introAnimationLength =
    InGameTime.seconds 1


introNoPlayerMovementEnd : Quantity Float InGameTime
introNoPlayerMovementEnd =
    InGameTime.seconds 0.5 |> Quantity.plus introAnimationLength


type MissionStatus
    = MissionStarting
    | MissionSuccess
    | MissionFailure
    | MissionInProgress


missionStatus : Model -> MissionStatus
missionStatus model =
    if Quantity.greaterThan (getTime model) introNoPlayerMovementEnd then
        MissionStarting

    else if chipsRemaining model == 0 then
        MissionSuccess

    else if Agent.isAlive (getPlayer model) |> not then
        MissionFailure

    else
        MissionInProgress


{-| The direction the user wants to move while on a chip. This is in world coordinates, not view coordinates.
-}
chipDirectionPressed : Cardinal -> Input -> Input -> Maybe Cardinal
chipDirectionPressed currentDirection previousInput input =
    case ( Input.cardinal previousInput, Input.cardinal input ) of
        ( _, Nothing ) ->
            Nothing

        ( Just _, _ ) ->
            Nothing

        ( Nothing, Just cardinal ) ->
            viewCardinalToWorld currentDirection cardinal |> Just


chipMoveTiming : Quantity Float InGameTime -> Model -> ChipMoveTiming
chipMoveTiming moveTime model =
    { relativeToCycle = CycleTime.from (getChipMoveStart model) moveTime, moveTime = moveTime }
        |> ChipMoveTime.chipMoveTiming


step : Input -> Model -> Model
step input model =
    model
        |> stepChips
        |> stepHackedChips
        |> stepCurrentFrame
        |> stepMoveInput input
        |> stepMovePlayerAndAgents input
        |> setPreviousInput input


stepMovePlayerAndAgents : Input -> Model -> Model
stepMovePlayerAndAgents input model =
    let
        time =
            getTime model

        player =
            getPlayer model

        newPlayer =
            { player | position = playerStep input model }

        isDamaged =
            (missionStatus model == MissionInProgress)
                && (Agent.damagedRecently (getTime model) player |> not)
                && playerHit model

        newPlayer2 =
            ifElse isDamaged (Agent.decrementHealth time newPlayer) newPlayer

        agents =
            agentsWithId model
                |> List.map
                    (\( id, a ) ->
                        case a of
                            PassiveAgent passiveAgent ->
                                PassiveAgent (passiveAgentStep model id passiveAgent)

                            ChaserAgent chaserAgent ->
                                ChaserAgent (chaserAgentStep model chaserAgent id)
                    )
                |> agentsSeparate model
    in
    model |> setPlayer newPlayer2 |> setAgents agents


agentsSeparate : Model -> List AgentType -> List AgentType
agentsSeparate model agents =
    agents
        |> List.gatherEqualsBy
            (\agent ->
                case Agent.getPosition agent of
                    WirePosition wirePosition ->
                        if wirePosition.data.transitioning == Nothing then
                            Just
                                ( wirePosition.wireId
                                , wirePosition.data.direction
                                , case agent of
                                    PassiveAgent _ ->
                                        0

                                    ChaserAgent _ ->
                                        1
                                )

                        else
                            Nothing

                    ChipPosition _ ->
                        Nothing
            )
        |> List.map (agentsSeperateOnWire model)
        |> List.concat


agentsSeperateOnWire : Model -> ( AgentType, List AgentType ) -> List AgentType
agentsSeperateOnWire model ( agent, agents ) =
    let
        circuit =
            getCircuit model

        getAgentT agent_ =
            case Agent.getPosition agent_ of
                WirePosition wirePosition_ ->
                    wirePosition_.t

                ChipPosition _ ->
                    T.t 0

        setAgentT t =
            Agent.updatePosition
                (\position ->
                    case position of
                        WirePosition wirePosition ->
                            WirePosition { wirePosition | t = t }

                        ChipPosition _ ->
                            position
                )
    in
    case Agent.getPosition agent of
        WirePosition { wireId } ->
            case Circuit.getWire circuit wireId of
                Just wire ->
                    let
                        sortedAgents =
                            agent
                                :: agents
                                |> List.sortBy (getAgentT >> (\(Quantity.Quantity t) -> t))
                                |> Array.fromList
                    in
                    sortedAgents
                        |> Array.indexedMap
                            (\index agent_ ->
                                let
                                    previousAgentDistance : Float
                                    previousAgentDistance =
                                        Array.get (index - 1) sortedAgents |> Maybe.map (\a -> Wire.tSignedDistance circuit agentT (getAgentT a) wire) |> Maybe.withDefault (passiveAgentMinSeperation + 1)

                                    nextAgentDistance : Float
                                    nextAgentDistance =
                                        Array.get (index + 1) sortedAgents |> Maybe.map (\a -> Wire.tSignedDistance circuit agentT (getAgentT a) wire) |> Maybe.withDefault (passiveAgentMinSeperation + 1)

                                    agentT : Quantity Float T
                                    agentT =
                                        getAgentT agent_
                                in
                                case ( abs previousAgentDistance < passiveAgentMinSeperation, abs nextAgentDistance < passiveAgentMinSeperation ) of
                                    ( True, True ) ->
                                        agentT |> Wire.moveT circuit ((previousAgentDistance + nextAgentDistance) / 2) wire |> flip setAgentT agent_

                                    ( True, False ) ->
                                        agentT |> Wire.moveT circuit 0.1 wire |> flip setAgentT agent_

                                    ( False, True ) ->
                                        agentT |> Wire.moveT circuit -0.1 wire |> flip setAgentT agent_

                                    ( False, False ) ->
                                        agent_
                            )
                        |> Array.toList

                Nothing ->
                    agent :: agents

        ChipPosition _ ->
            agent :: agents



--                        positionOffset =
--                            if wirePosition.data.transitioning == Nothing then
--                                passiveAgentsOnWire model wirePosition.wireId wirePosition.data.direction
--                                    |> List.filter (Tuple.first >> (/=) id)
--                                    |> List.foldr
--                                        (\( agentId, t ) state ->
--                                            let
--                                                distance =
--                                                    Wire.tSignedDistance circuit t wirePosition.t wire
--                                                        |> (+) (getTime model |> InGameTime.inSeconds |> (+) (toFloat agentId) |> randomOffset)
--                                            in
--                                            if abs distance < passiveAgentMinSeperation then
--                                                if distance > 0 then
--                                                    passiveAgentMinSeperation - distance + state
--
--                                                else
--                                                    -passiveAgentMinSeperation - distance + state
--
--                                            else
--                                                state
--                                        )
--                                        0
--                                    |> (*) 1
--                                    |> (\value ->
--                                            case wirePosition.data.direction of
--                                                Forward ->
--                                                    value
--
--                                                Backward ->
--                                                    -value
--                                       )
--
--                            else
--                                0


agentsWithId : Model -> List ( Int, AgentType )
agentsWithId model =
    getAgents model |> List.indexedMap Tuple.pair


setPlayer : Agent.Player -> Model -> Model
setPlayer player (Model model) =
    Model { model | player = player }


setAgents : List AgentType -> Model -> Model
setAgents agents (Model model) =
    Model { model | agents = agents }


stepMoveInput : Input -> Model -> Model
stepMoveInput input model =
    let
        currentFrame =
            getCurrentFrame model

        time : Quantity Float InGameTime
        time =
            getTime model

        chipMoveInput : ChipMovementInput
        chipMoveInput =
            case ( chipMoveTiming time model, (getPlayer model).position ) of
                ( _, WirePosition _ ) ->
                    if getTime model |> Quantity.minus (getChipMoveStart model) |> Quantity.greaterThanOrEqualTo cycleDefaultLength then
                        { frameNumber = currentFrame, direction = Nothing }

                    else
                        getMoveOnChip model

                ( ChipMoveTime.Late, ChipPosition _ ) ->
                    { frameNumber = currentFrame, direction = Nothing }

                ( ChipMoveTime.Early, ChipPosition _ ) ->
                    getMoveOnChip model

                ( ChipMoveTime.OnTime, ChipPosition chipPosition ) ->
                    case
                        chipDirectionPressed chipPosition.chipData.direction (getPreviousInput model) input
                    of
                        Just cardinal ->
                            { frameNumber = currentFrame, direction = Just cardinal }

                        Nothing ->
                            getMoveOnChip model

        model2 =
            case (getPlayer model).position of
                WirePosition _ ->
                    model

                ChipPosition chipPosition ->
                    case chipDirectionPressed chipPosition.chipData.direction (getPreviousInput model) input of
                        Just _ ->
                            model |> addMoveToAllMoves { moveTime = time, relativeToCycle = CycleTime.from (getChipMoveStart model) time }

                        Nothing ->
                            model
    in
    setMoveOnChip chipMoveInput model2


setMoveOnChip : ChipMovementInput -> Model -> Model
setMoveOnChip chipMove (Model model) =
    Model { model | moveOnChip = chipMove }


addMoveToAllMoves : ChipMoveTime -> Model -> Model
addMoveToAllMoves chipMoveTime (Model model) =
    Model { model | allChipMoves = chipMoveTime :: model.allChipMoves }


setPreviousInput : Input -> Model -> Model
setPreviousInput input (Model model) =
    Model { model | previousInput = input }


chipMoveStart : Model -> Bool
chipMoveStart model =
    getCurrentFrame model == (getMoveOnChip model).frameNumber


setCircuit : Circuit -> Model -> Model
setCircuit circuit (Model model) =
    Model { model | circuit = circuit }


stepCurrentFrame : Model -> Model
stepCurrentFrame (Model model) =
    Model { model | currentFrame = FrameNumber.step model.currentFrame }


stepChips : Model -> Model
stepChips model =
    case (getPlayer model).position of
        WirePosition _ ->
            model

        ChipPosition { chipId, position } ->
            Circuit.updateChip (getCircuit model) chipId (Chip.hackPosition position) |> flip setCircuit model


stepHackedChips : Model -> Model
stepHackedChips model =
    let
        random =
            Helper.randomFromTime (getTime model)

        randomPoint pos =
            Random.uniform
                []
                [ [ Point2i.translateBy (Vector2i.xy ( 1, 0 )) pos ]
                , [ Point2i.translateBy (Vector2i.xy ( 0, 1 )) pos ]
                , [ Point2i.translateBy (Vector2i.xy ( 0, -1 )) pos ]
                , [ Point2i.translateBy (Vector2i.xy ( -1, 0 )) pos ]
                ]
    in
    if Helper.intervalElapsed hackedPointUpdateDuration (getTime model) (getPreviousTime model) then
        Circuit.updateChips
            (\_ chip ->
                if Chip.isHacked chip && Set.size chip.hackedPoints < Chip.tileCount chip then
                    { position = chip.position
                    , size = chip.size
                    , hackedPoints =
                        chip.hackedPoints
                            |> Set.toList
                            |> List.foldr
                                (\p ( points, seed ) ->
                                    Random.step (Point2i.fromTuple p |> randomPoint) seed
                                        |> Tuple.mapFirst ((++) points)
                                )
                                ( [], random )
                            |> Tuple.first
                            |> List.filter (\point -> Chip.tileBounds chip |> BoundingBox2i.contains point)
                            |> List.map (\point -> ( point.x, point.y ))
                            |> Set.fromList
                            |> Set.union chip.hackedPoints
                    }

                else
                    chip
            )
            (getCircuit model)
            |> flip setCircuit model

    else
        model


hackedChipCount : Model -> Int
hackedChipCount (Model model) =
    Circuit.getChips model.circuit |> List.count (Tuple.second >> Chip.isHacked)


chipsRemaining : Model -> Int
chipsRemaining (Model model) =
    Circuit.getChips model.circuit |> List.filter (\( _, chip ) -> Chip.isHacked chip |> not) |> List.length


playerStep : Input -> Model -> CircuitPosition Agent.WireData Agent.ChipData
playerStep input model =
    let
        canMove =
            missionStatus model == MissionInProgress

        player =
            getPlayer model
    in
    case player.position of
        WirePosition ({ data } as wirePosition) ->
            let
                speedFactor =
                    (if Input.accelerate input then
                        data.speedFactor + (1 * InGameTime.inSeconds secondsPerFrame)

                     else
                        data.speedFactor - (2 * InGameTime.inSeconds secondsPerFrame)
                    )
                        |> clamp Agent.playerMinWireSpeedFactor Agent.playerMaxWireSpeedFactor

                wireMove =
                    case ( Input.moveLeft input, Input.moveRight input, canMove ) of
                        ( True, False, True ) ->
                            WireStepMoveLeft

                        ( False, True, True ) ->
                            WireStepMoveRight

                        _ ->
                            WireStepMoveNone

                newWirePosition =
                    { wirePosition | data = { data | speedFactor = speedFactor } }
            in
            wireStep
                (ifElse canMove Agent.playerWireBaseSpeed 0)
                model
                wireMove
                newWirePosition

        ChipPosition chipPosition ->
            let
                movement =
                    if canMove then
                        (getMoveOnChip model).direction

                    else
                        Nothing
            in
            if chipMoveStart model then
                moveOnChip model chipPosition movement

            else
                player.position


viewCardinalToWorld : Cardinal -> Cardinal -> Cardinal
viewCardinalToWorld currentDirection cardinal =
    Cardinal.toValue cardinal
        |> (\a -> Cardinal.toValue currentDirection + a - 1)
        |> Cardinal.fromValue


passiveAgentMinSeperation =
    2


passiveAgentStep : Model -> Int -> Agent.PassiveAgent_ -> Agent.PassiveAgent_
passiveAgentStep model _ passiveAgent =
    case passiveAgent.position of
        WirePosition wirePosition ->
            let
                circuit =
                    getCircuit model
            in
            case Circuit.getWire circuit wirePosition.wireId of
                Just _ ->
                    { position = wireStep Agent.passiveAgentSpeed model WireStepMoveNone wirePosition }

                Nothing ->
                    passiveAgent

        ChipPosition chipPosition ->
            if chipMoveStart model then
                { position = moveOnChip model chipPosition (Just chipPosition.chipData.direction) }

            else
                passiveAgent


chaserAgentWireStep :
    Model
    -> Int
    -> Circuit.WirePosition_ Agent.WireData
    -> CircuitPosition Agent.WireData Agent.ChipData
chaserAgentWireStep model index ({ wireId, t, data } as wirePosition) =
    let
        circuit =
            getCircuit model

        seed =
            Random.initialSeed2 (toFloat index) (getTime model |> InGameTime.inSeconds)

        ( moveDirection, speedFactor ) =
            case ( agentWorldPosition model (getPlayer model).position, Circuit.getWire circuit wireId ) of
                ( Just playerPos, Just wire ) ->
                    case ( Wire.tPoint circuit wire t, Wire.direction circuit wire t data.direction ) of
                        ( Just agentPos, Just worldDirection ) ->
                            let
                                randomlyMove =
                                    Random.weighted
                                        ( 0.01, True )
                                        [ ( 0.99, False ) ]
                                        |> flip Random.step seed
                                        |> Tuple.first

                                directionVector =
                                    Direction2d.toVector worldDirection

                                directionToPlayer =
                                    Vector2d.from agentPos playerPos
                                        |> Vector2d.normalize

                                wireStepMove =
                                    if randomlyMove then
                                        directionToPlayer
                                            |> Vector2d.dotProduct (Vector2d.perpendicularTo directionVector)
                                            |> (\a ->
                                                    if a < -0.01 then
                                                        WireStepMoveLeft

                                                    else if a > 0.01 then
                                                        WireStepMoveRight

                                                    else
                                                        WireStepMoveNone
                                               )

                                    else
                                        WireStepMoveNone

                                speedFactor_ =
                                    directionToPlayer
                                        |> Vector2d.dotProduct directionVector
                                        |> (\a ->
                                                if a < -0.01 then
                                                    data.speedFactor - (0.5 * InGameTime.inSeconds secondsPerFrame)

                                                else if a > 0.01 then
                                                    data.speedFactor + (0.5 * InGameTime.inSeconds secondsPerFrame)

                                                else
                                                    data.speedFactor
                                           )
                                        |> clamp Agent.chaserAgentMinWireSpeedFactor Agent.chaserAgentMaxWireSpeedFactor
                            in
                            ( wireStepMove, speedFactor_ )

                        ( _, _ ) ->
                            ( WireStepMoveNone, data.speedFactor )

                ( _, _ ) ->
                    ( WireStepMoveNone, data.speedFactor )
    in
    wireStep
        Agent.chaserAgentBaseSpeed
        model
        moveDirection
        { wirePosition | data = { data | speedFactor = speedFactor } }


chaserAgentChipStep : Model -> Int -> Circuit.ChipPosition_ Agent.ChipData -> Maybe Cardinal
chaserAgentChipStep model index chipPosition =
    let
        circuit =
            getCircuit model

        seed =
            Random.initialSeed2 (toFloat index) (getTime model |> InGameTime.inSeconds)

        moveTowardsTarget : Bool -> Point2i -> Maybe Cardinal
        moveTowardsTarget includeWires targetLocalPos =
            let
                diff =
                    Vector2i.from chipPosition.position targetLocalPos
            in
            if diff == Vector2i.zero then
                Nothing

            else
                Cardinal.all
                    |> List.filter
                        (\cardinal ->
                            case moveOnChip_ circuit chipPosition (Just cardinal) of
                                Just (ChipPosition _) ->
                                    True

                                Just (WirePosition _) ->
                                    includeWires

                                Nothing ->
                                    False
                        )
                    |> List.map
                        (\cardinal ->
                            ( Cardinal.toVector2i cardinal
                                |> Vector2i.dotProduct diff
                                |> toFloat
                                -- We give the other directions a slight chance of occurring so that the chasers don't bunch up quite as much.
                                |> (+) 0.04
                                |> max 0
                            , Just cardinal
                            )
                        )
                    |> Random.weighted ( 0, Nothing )
                    |> flip Random.step seed
                    |> Tuple.first

        moveTorwardsPlayerOutsideChip () =
            case
                ( Circuit.getChip circuit chipPosition.chipId
                , agentWorldPosition model (getPlayer model).position
                )
            of
                ( Just chip, Just playerPos ) ->
                    Chip.connectedWires circuit chipPosition.chipId
                        |> List.minimumBy
                            (\{ edgePoint } ->
                                Chip.edgePointWorldPosition chip edgePoint
                                    |> Point2d.distanceFrom playerPos
                            )
                        |> Maybe.map
                            (.edgePoint >> Chip.edgePointLocalPosition chip >> moveTowardsTarget True)
                        |> Maybe.withDefault Nothing

                ( _, _ ) ->
                    Nothing
    in
    if missionStatus model == MissionInProgress then
        case (getPlayer model).position of
            ChipPosition playerChipPosition ->
                if playerChipPosition.chipId == chipPosition.chipId then
                    moveTowardsTarget False playerChipPosition.position

                else
                    moveTorwardsPlayerOutsideChip ()

            WirePosition _ ->
                moveTorwardsPlayerOutsideChip ()

    else
        Nothing


chaserAgentStep : Model -> Agent.ChaserAgent_ -> Int -> Agent.ChaserAgent_
chaserAgentStep model chaserAgent index =
    case chaserAgent.position of
        WirePosition wirePosition ->
            { position = chaserAgentWireStep model index wirePosition }

        ChipPosition chipPosition ->
            if chipMoveStart model then
                { position = chaserAgentChipStep model index chipPosition |> moveOnChip model chipPosition }

            else
                chaserAgent


type WireStepMove
    = WireStepMoveLeft
    | WireStepMoveRight
    | WireStepMoveNone


passiveAgentsOnWire : Model -> WireId -> WireDirection -> List ( Int, Quantity Float T )
passiveAgentsOnWire model selectedWireId wireDirection =
    agentsWithId model
        |> List.filterMap
            (\( index, agent ) ->
                case agent of
                    PassiveAgent { position } ->
                        case position of
                            WirePosition { wireId, t, data } ->
                                if data.transitioning == Nothing && selectedWireId == wireId && data.direction == wireDirection then
                                    Just ( index, t )

                                else
                                    Nothing

                            ChipPosition _ ->
                                Nothing

                    ChaserAgent _ ->
                        Nothing
            )


wireStep :
    Float
    -> Model
    -> WireStepMove
    -> Circuit.WirePosition_ Agent.WireData
    -> CircuitPosition Agent.WireData Agent.ChipData
wireStep speed model wireMove ({ wireId, t, data } as wirePosition) =
    let
        circuit =
            getCircuit model
    in
    case Circuit.getWire circuit wireId of
        Just wire ->
            let
                speedPerFrame =
                    speed * data.speedFactor * InGameTime.inSeconds secondsPerFrame

                directionMult =
                    case data.direction of
                        Circuit.Forward ->
                            1

                        Circuit.Backward ->
                            -1

                nextT =
                    Wire.moveT circuit (directionMult * speedPerFrame) wire t

                nextPos =
                    Circuit.wirePosition
                        wireId
                        nextT
                        { direction = data.direction
                        , transitioning = Nothing
                        , speedFactor = data.speedFactor
                        }

                leftAndRight () =
                    Jump.agentJumpPoints circuit (getJumpLookup model) { wireId = wireId, t = nextT, data = data.direction }

                jump : JumpPoint -> Point2d -> Circuit.CircuitPosition Agent.WireData Agent.ChipData
                jump jumpPoint start =
                    Circuit.wirePosition
                        jumpPoint.wireId
                        jumpPoint.t
                        { direction = jumpPoint.direction
                        , transitioning = Agent.transitionStart start |> Just
                        , speedFactor = data.speedFactor
                        }
            in
            case data.transitioning of
                Just transition_ ->
                    let
                        tSpeed =
                            case Agent.wirePosition circuit wire t of
                                Just wirePosition_ ->
                                    Point2d.distanceFrom wirePosition_ transition_.start
                                        |> max 0.1
                                        |> (/) (speedPerFrame * 1.3)
                                        |> T.t

                                Nothing ->
                                    T.t 0.1
                    in
                    Circuit.wirePosition
                        wireId
                        t
                        { direction = data.direction
                        , transitioning = Agent.transitionStep tSpeed transition_
                        , speedFactor = data.speedFactor
                        }

                Nothing ->
                    let
                        forceJump () =
                            let
                                { left, right } =
                                    leftAndRight ()
                            in
                            Maybe.or right left
                                |> Maybe.map (\( start, jumpPoint ) -> jump jumpPoint start)
                                |> Maybe.withDefault nextPos

                        gotoChip : Circuit.ChipEdgePoint -> Circuit.CircuitPosition Agent.WireData Agent.ChipData
                        gotoChip ({ chipId, side } as chipEdgePoint) =
                            case Chip.wireChipEndLocalPosition circuit chipEdgePoint of
                                Just localPos ->
                                    Circuit.chipPosition
                                        chipId
                                        localPos
                                        { direction = Cardinal.reverse side, previousPoint = localPos }

                                Nothing ->
                                    nextPos

                        moveForward () =
                            case ( Wire.partAtT circuit wire nextT, data.direction ) of
                                ( Wire.WireStart, Circuit.Backward ) ->
                                    case wire.start of
                                        Just chipEdgePoint ->
                                            gotoChip chipEdgePoint

                                        Nothing ->
                                            forceJump ()

                                ( Wire.WireEnd, Circuit.Forward ) ->
                                    case wire.end of
                                        Just chipEdgePoint ->
                                            gotoChip chipEdgePoint

                                        Nothing ->
                                            forceJump ()

                                ( _, _ ) ->
                                    nextPos
                    in
                    case wireMove of
                        WireStepMoveLeft ->
                            case leftAndRight () |> .left of
                                Just ( start, jumpPoint ) ->
                                    jump jumpPoint start

                                Nothing ->
                                    moveForward ()

                        WireStepMoveRight ->
                            case leftAndRight () |> .right of
                                Just ( start, jumpPoint ) ->
                                    jump jumpPoint start

                                Nothing ->
                                    moveForward ()

                        WireStepMoveNone ->
                            moveForward ()

        Nothing ->
            WirePosition wirePosition


hasMovedOnChip chipPosition =
    chipPosition.chipData.previousPoint /= chipPosition.position


isLeavingChip : Circuit -> Circuit.ChipPosition_ Agent.ChipData -> Bool
isLeavingChip circuit chipPosition =
    case Circuit.getChip circuit chipPosition.chipId of
        Just chip ->
            let
                exitPoints =
                    Chip.connectedWires circuit chipPosition.chipId

                maybeWireConnection pos =
                    List.find (.edgePoint >> Chip.edgePointLocalPosition chip >> (==) pos) exitPoints
            in
            case maybeWireConnection chipPosition.position of
                Just wireConnection ->
                    case ( hasMovedOnChip chipPosition, Circuit.getWire circuit wireConnection.wireId ) of
                        ( True, Just _ ) ->
                            True

                        _ ->
                            False

                Nothing ->
                    False

        Nothing ->
            False


moveOnChip_ : Circuit -> Circuit.ChipPosition_ Agent.ChipData -> Maybe Cardinal -> Maybe (CircuitPosition Agent.WireData Agent.ChipData)
moveOnChip_ circuit chipPosition movement =
    case Circuit.getChip circuit chipPosition.chipId of
        Just chip ->
            let
                newLocal =
                    movement
                        |> Maybe.map Cardinal.toVector2i
                        |> Maybe.withDefault Vector2i.zero
                        |> flip Point2i.translateBy chipPosition.position

                isWireConnectionPos pos =
                    maybeWireConnection pos /= Nothing

                validPos pos =
                    (isWireConnectionPos pos && not (isWireConnectionPos chipPosition.position))
                        || BoundingBox2i.contains pos (Chip.tileBounds chip)

                exitPoints =
                    Chip.connectedWires circuit chipPosition.chipId

                newChipPosition =
                    if validPos newLocal then
                        Circuit.chipPosition
                            chipPosition.chipId
                            newLocal
                            { direction = chipPosition.chipData.direction, previousPoint = chipPosition.position }
                            |> Just

                    else
                        Nothing

                maybeWireConnection pos =
                    List.find (.edgePoint >> Chip.edgePointLocalPosition chip >> (==) pos) exitPoints
            in
            case maybeWireConnection chipPosition.position of
                Just wireConnection ->
                    case
                        ( hasMovedOnChip chipPosition || movement == Just wireConnection.edgePoint.side
                        , Circuit.getWire circuit wireConnection.wireId
                        )
                    of
                        ( True, Just wire ) ->
                            if wireConnection.isWireStart then
                                Circuit.wirePosition
                                    wireConnection.wireId
                                    Wire.tMin
                                    { direction = Circuit.Forward, transitioning = Nothing, speedFactor = 1 }
                                    |> Just

                            else
                                Circuit.wirePosition
                                    wireConnection.wireId
                                    (Wire.tMax circuit wire)
                                    { direction = Circuit.Backward, transitioning = Nothing, speedFactor = 1 }
                                    |> Just

                        _ ->
                            newChipPosition

                Nothing ->
                    newChipPosition

        Nothing ->
            Nothing


moveOnChip :
    Model
    -> Circuit.ChipPosition_ Agent.ChipData
    -> Maybe Cardinal
    -> CircuitPosition Agent.WireData Agent.ChipData
moveOnChip model ({ position, chipData } as chipPosition) movement =
    moveOnChip_ (getCircuit model) chipPosition movement
        |> Maybe.withDefault (ChipPosition { chipPosition | chipData = { chipData | previousPoint = position } })


playerHit : Model -> Bool
playerHit model =
    let
        player =
            getPlayer model

        circuit =
            getCircuit model
    in
    case player.position of
        WirePosition wirePosition ->
            case agentWirePosition circuit wirePosition of
                Just position ->
                    getAgents model
                        |> List.filterMap
                            (\agent ->
                                case Agent.getPosition agent of
                                    WirePosition agentWirePosition_ ->
                                        agentWirePosition circuit agentWirePosition_

                                    ChipPosition _ ->
                                        Nothing
                            )
                        |> List.any (Point2d.distanceFrom position >> (>) ((agentHitRadius + playerHitRadius) / 2))

                Nothing ->
                    False

        ChipPosition { chipId, position, chipData } ->
            getAgents model
                |> List.filter
                    (\agent ->
                        case Agent.getPosition agent of
                            WirePosition _ ->
                                False

                            ChipPosition agentChip ->
                                let
                                    swappedPlaces =
                                        chipData.previousPoint == agentChip.position && position == agentChip.chipData.previousPoint

                                    samePlace =
                                        position == agentChip.position
                                in
                                (chipId == agentChip.chipId) && (samePlace || swappedPlaces)
                    )
                |> List.any (always True)


agentWorldPosition : Model -> CircuitPosition Agent.WireData Agent.ChipData -> Maybe Point2d
agentWorldPosition model position =
    case position of
        WirePosition wirePosition ->
            agentWirePosition (getCircuit model) wirePosition

        ChipPosition chipPosition ->
            agentChipPosition model chipPosition


agentWirePosition : Circuit -> Circuit.WirePosition_ Agent.WireData -> Maybe Point2d
agentWirePosition circuit { wireId, t, data } =
    case ( Circuit.getWire circuit wireId, data.transitioning ) of
        ( Just wire, Just transition ) ->
            case Agent.wirePosition circuit wire t of
                Just endPoint ->
                    agentTransitioningWirePosition transition endPoint |> Just

                Nothing ->
                    Nothing

        ( Just wire, Nothing ) ->
            Agent.wirePosition circuit wire t

        ( Nothing, _ ) ->
            Nothing


agentChipPosition : Model -> Circuit.ChipPosition_ Agent.ChipData -> Maybe Point2d
agentChipPosition model chipPosition =
    case Circuit.getChip (getCircuit model) chipPosition.chipId of
        Just chip ->
            agentTransitioningChipPosition
                model
                (Agent.worldPositionOnChip chip chipPosition.chipData.previousPoint)
                (Agent.worldPositionOnChip chip chipPosition.position)
                |> Just

        Nothing ->
            Nothing


agentTransitioningWirePosition : Agent.WireTransition -> Point2d -> Point2d
agentTransitioningWirePosition { start, t } endPoint =
    Point2d.interpolateFrom start endPoint (T.inT t)


agentTransitioningChipPosition : Model -> Point2d -> Point2d -> Point2d
agentTransitioningChipPosition model startPoint endPoint =
    let
        duration =
            ChipMoveTime.chipTransitionDuration |> Quantity.multiplyBy 0.8
    in
    Keyframe.startAt_ (getChipMoveStart model) 0
        |> Keyframe.add_ Keyframe.Linear duration 1
        |> Keyframe.valueAt_ (getTime model)
        |> Point2d.interpolateFrom startPoint endPoint
