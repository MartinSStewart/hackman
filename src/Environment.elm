module Environment exposing (buildId, buildTime, isDebug)

{-| Contains functions that will change depending on whether we are running in debug or production.
-}

import Time


isDebug : Bool
isDebug =
    True


buildId : Int
buildId =
    0


buildTime : Time.Posix
buildTime =
    Time.millisToPosix 0
