module ColorPickerTests exposing (..)

import Color
import ColorPicker
import Expect exposing (FloatingPointTolerance(..))
import Fuzz
import Point2d
import Test exposing (Test, describe, fuzz, test)


all : Test
all =
    describe "Color picker"
        [ test "Color in circle center" <|
            \_ ->
                ColorPicker.colorInCircle 0.5 (Point2d.fromCoordinates ( 0.5, 0.5 )) |> Expect.equal (Color.hsl 0 0 0.5)
        , fuzz fuzzColor "Round trip rgb to hsv" <|
            \color ->
                ColorPicker.rgbToHsv color
                    |> ColorPicker.hsvToRgb
                    |> Color.toRgba
                    |> Expect.all
                        [ .red >> Expect.within (Absolute 0.01) (Color.toRgba color |> .red)
                        , .green >> Expect.within (Absolute 0.01) (Color.toRgba color |> .green)
                        , .blue >> Expect.within (Absolute 0.01) (Color.toRgba color |> .blue)
                        ]
        ]


fuzzColor =
    Fuzz.map3 Color.rgb
        (Fuzz.floatRange 0 1)
        (Fuzz.floatRange 0 1)
        (Fuzz.floatRange 0 1)
