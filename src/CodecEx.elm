module CodecEx exposing (assocList, boundingBox2d, cardinal, direction2d, lineSegment2d, point2d, point2i, quantity, quantityInt, vector2i)

import AssocList
import BoundingBox2d exposing (BoundingBox2d)
import Cardinal exposing (Cardinal(..))
import Codec.Serialize as Codec exposing (Codec)
import Direction2d exposing (Direction2d)
import LineSegment2d exposing (LineSegment2d)
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Quantity exposing (Quantity)
import Vector2i exposing (Vector2i)


point2i : Codec Point2i
point2i =
    Codec.record Point2i
        |> Codec.field .x Codec.int
        |> Codec.field .y Codec.int
        |> Codec.finishRecord


vector2i : Codec Vector2i
vector2i =
    Codec.record Vector2i
        |> Codec.field .width Codec.int
        |> Codec.field .height Codec.int
        |> Codec.finishRecord


boundingBox2d : Codec BoundingBox2d
boundingBox2d =
    Codec.record (\minX minY maxX maxY -> BoundingBox2d.fromExtrema { minX = minX, minY = minY, maxX = maxX, maxY = maxY })
        |> Codec.field (BoundingBox2d.extrema >> .minX) Codec.float
        |> Codec.field (BoundingBox2d.extrema >> .minY) Codec.float
        |> Codec.field (BoundingBox2d.extrema >> .maxX) Codec.float
        |> Codec.field (BoundingBox2d.extrema >> .maxY) Codec.float
        |> Codec.finishRecord


point2d : Codec Point2d
point2d =
    Codec.tuple Codec.float Codec.float |> Codec.map Point2d.fromCoordinates Point2d.coordinates


lineSegment2d : Codec LineSegment2d
lineSegment2d =
    Codec.record LineSegment2d.from
        |> Codec.field LineSegment2d.startPoint point2d
        |> Codec.field LineSegment2d.endPoint point2d
        |> Codec.finishRecord


direction2d : Codec Direction2d
direction2d =
    Codec.float |> Codec.map Direction2d.fromAngle (Direction2d.angleFrom (Direction2d.fromAngle 0))


cardinal : Codec Cardinal
cardinal =
    Codec.customType
        (\right top left bottom value ->
            case value of
                Left ->
                    left

                Right ->
                    right

                Top ->
                    top

                Bottom ->
                    bottom
        )
        |> Codec.variant0 Right
        |> Codec.variant0 Top
        |> Codec.variant0 Left
        |> Codec.variant0 Bottom
        |> Codec.finishCustomType


quantity : Codec (Quantity Float unit)
quantity =
    Codec.float |> Codec.map Quantity.Quantity (\(Quantity.Quantity value) -> value)


quantityInt : Codec (Quantity Int unit)
quantityInt =
    Codec.int |> Codec.map Quantity.Quantity (\(Quantity.Quantity value) -> value)


assocList : Codec key -> Codec value -> Codec (AssocList.Dict key value)
assocList keyCodec valueCodec =
    Codec.list (Codec.tuple keyCodec valueCodec) |> Codec.map AssocList.fromList AssocList.toList
