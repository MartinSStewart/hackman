module HighscoreTests exposing (all)

import AssocList
import Expect
import Fuzz exposing (Fuzzer)
import HighscoreTable exposing (ScoreId(..))
import InGameTime
import Quantity
import StageData
import StageScore exposing (StageScore)
import Test exposing (Test, describe, only, test)


all : Test
all =
    describe "Highscore tests"
        [ test "Add highscore to stage" <|
            \_ ->
                HighscoreTable.isNewHighscore stageId goodStageScore noHighscores |> Expect.true "Expected a new highscore"
        , test "Add better highscore to stage" <|
            \_ ->
                addHighscores HighscoreTable.highscoreTableRowCount badStageScore
                    |> HighscoreTable.isNewHighscore stageId goodStageScore
                    |> Expect.true "Expected a new highscore"
        , test "Reject worse highscore" <|
            \_ ->
                addHighscores HighscoreTable.highscoreTableRowCount goodStageScore
                    |> HighscoreTable.isNewHighscore stageId badStageScore
                    |> Expect.false "Expected no new highscore"
        , test "Add worse highscore if there's space" <|
            \_ ->
                addHighscores (HighscoreTable.highscoreTableRowCount - 1) goodStageScore
                    |> HighscoreTable.isNewHighscore stageId badStageScore
                    |> Expect.true "Expected a new highscore"
        , test "Get highscores" <|
            \_ ->
                addHighscores HighscoreTable.highscoreTableRowCount goodStageScore
                    |> HighscoreTable.getHighscores StageScore.SortByTimePlusPenalty stageId
                    |> List.length
                    |> Expect.equal HighscoreTable.highscoreTableRowCount
        ]


stageId =
    StageData.tutorialId


goodStageScore : StageScore
goodStageScore =
    { time = Quantity.zero
    , health = StageScore.healthPoints 3
    , badChipMoves = StageScore.chipMoves 1
    , goodChipMoves = StageScore.chipMoves 1
    , name = "AT"
    }


badStageScore : StageScore
badStageScore =
    { time = InGameTime.seconds 100
    , health = StageScore.healthPoints 0
    , badChipMoves = StageScore.chipMoves 9999
    , goodChipMoves = StageScore.chipMoves 0
    , name = "SA"
    }


noHighscores =
    HighscoreTable.init AssocList.empty


addHighscores n score =
    HighscoreTable.init
        (List.range 1 n
            |> List.map (\value -> ( ScoreId value, score ))
            |> Tuple.pair stageId
            |> List.singleton
            |> AssocList.fromList
        )
