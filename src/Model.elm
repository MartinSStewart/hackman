module Model exposing
    ( AssetState(..)
    , ColorPaletteSource(..)
    , Flags
    , FramerateWarningStatus(..)
    , LoadingModel
    , LoadingMsg(..)
    , MissionCompleted_
    , Model
    , ModelBase(..)
    , Msg(..)
    , MsgBase(..)
    , SaveDataError(..)
    , StageUnlockAnimation(..)
    , debounceConfig
    , decodeFlags
    , getSaveData
    , getSounds
    , getTime
    , setGameState
    )

import Bytes exposing (Bytes)
import Codec.Serialize as Codec exposing (Codec)
import ColorPalette exposing (ColorPalette, ColorSelection)
import ColorPicker
import Debounce exposing (Debounce)
import Dict exposing (Dict)
import Editor
import File exposing (File)
import HighscoreTable exposing (ScoreId)
import Http
import InGame
import InGame.Core as Core
import Json.Decode as JD
import Keyboard exposing (Key)
import MenuState exposing (Game)
import Point2d exposing (Point2d)
import Quantity exposing (Quantity)
import Random exposing (Seed)
import RealTime exposing (RealTime)
import Replay exposing (CompleteReplay, Replay)
import ReplayViewer
import SaveData exposing (SaveData)
import Sound exposing (Model, Sound, SoundError, SoundId)
import SpriteSheet exposing (SpriteSheet)
import StageData exposing (StageId)
import StageScore exposing (StageScore)
import Url exposing (Url)
import UrlData exposing (UrlData)
import Vector2i exposing (Vector2i)
import WebGL.Texture exposing (Texture)


type Msg
    = NoOp
    | KeyMsg Keyboard.Msg
    | Step (Quantity Float RealTime)
    | SingleStep
    | WindowResize_ Vector2i
    | StartStage
    | PressedStageTile { stageId : StageId }
    | MouseEnterStageSelectButton MenuState.StageSelectButton
    | KeyPressedStageTile { stageId : StageId }
    | SelectedStageSelecTileWithArrowKeys { stageId : StageId }
    | PressedTitlePageStartButton
    | PlaySound Sound
    | PressedTitlePageEditorButton
    | PressedEditorBackButton
    | EditorMsg Editor.Msg
    | PortError_ JD.Error
    | SoundLoaded_ String (Result SoundError SoundId)
    | Batch (List Msg)
    | MouseMove Point2d
    | MouseDown Point2d
    | MouseUp Point2d
    | PressInGameBackToStageSelect
    | SaveReplay CompleteReplay
    | InGameMsg InGame.InGameMsg
    | SetMasterVolume Float
    | SetMusicVolume Float
    | SetSoundEffectVolume Float
    | ResetSettings
    | DebounceMsg Debounce.Msg
    | SaveSaveData
    | SetAntialias Bool
    | PressedShowTrailer
    | PressedCloseTrailer
    | PressedLoadReplayInSettings
    | PressedChangeColorsInSettings
    | PressedColorSettingsPageBack
    | PressedResetColorSettings
    | PressedLoadReplayInReplayViewer
    | SelectedReplayFileInSettings File
    | SelectedReplayFileInReplayViewer File
    | ReplayFileLoadedInSettings Bytes
    | ReplayFileLoadedInReplayViewer Bytes
    | PressedReplayBackButton
    | ReplayViewerMsg ReplayViewer.Msg
    | TypedHighscoreName String
    | PressedAcceptHighscoreName HighscoreTable.PendingHighscore
    | PressedHighscoreSortBy StageScore.SortBy
    | PressedTitlePageSettings
    | PressedSettingsPageBack
    | PressedStageSelectBack
    | PressedStageSelectStartHacking
    | PressedFramerateWarningPopup
    | MissionCompleted MissionCompleted_
    | ColorPickerMsg ColorPicker.Msg
    | PressedColorSelection ColorSelection
    | SelectedColorSchemeLinkText
    | MouseOverTitlePageButton MenuState.TitlePageButton


type alias MissionCompleted_ =
    { stageId : StageId, coreState : Core.Model, replay : CompleteReplay, completionTime : Quantity Float RealTime }


type ModelBase
    = Loading LoadingModel
    | Loaded Model
    | InvalidFlags String


type MsgBase
    = LoadingMsg LoadingMsg
    | LoadedMsg Msg


type LoadingMsg
    = SoundLoaded String (Result SoundError SoundId)
    | PortError JD.Error
    | WindowResize Vector2i
    | TextureLoaded (Result WebGL.Texture.Error Texture)
    | TextureDataLoaded (Result Http.Error String)
    | StartGame Sound.Model SpriteSheet
    | UpdateCurrentTime (Quantity Float RealTime)
    | UserPressedOverwriteColorPaletteOption ColorPaletteSource
    | KeyMsgLoading Keyboard.RawKey


type AssetState
    = NoneLoaded
    | SpriteSheetLoaded { spriteSheet : Texture }
    | SpriteSheetDataLoaded { spriteSheet : SpriteSheet }


type alias LoadingModel =
    { assetState : AssetState
    , spriteSheetError : Maybe WebGL.Texture.Error
    , spriteSheetDataError : Maybe Http.Error
    , sounds : Dict String SoundId
    , randomSeed : Seed
    , soundErrors : List SoundError
    , windowSize : Vector2i
    , time : Quantity Float RealTime
    , saveData : Result SaveDataError SaveData
    , url : Url
    , urlData : Result UrlData.UrlDataError UrlData
    , colorPaletteSource : ColorPaletteSource
    }


type ColorPaletteSource
    = ColorPaletteFromSaveData
    | ColorPaletteFromUrlData


type alias Model =
    { keys : List Key
    , previousKeys : List Key
    , randomSeed : Random.Seed
    , gameState : Game
    , time : Quantity Float RealTime
    , windowSize : Vector2i
    , stepPaused : Bool
    , stageSelectId : StageId

    -- Time between the last two steps in milliseconds.
    , stepDelta : Quantity Float RealTime
    , averageStepDelta : Quantity Float RealTime
    , framerateWarningStatus : FramerateWarningStatus
    , editorState : Editor.Model
    , portErrors : List JD.Error
    , url : Url
    , sounds : Sound.Model
    , soundsPrevious : Sound.Model
    , mouseDown : Bool
    , previousMouseDown : Bool
    , mousePosition : Point2d
    , previousMousePosition : Point2d
    , spriteSheet : SpriteSheet

    {- We don't need to store save data in the debouncer since it's already in the model and we just don't want to
       overwhelm our app by writing to local storage constantly.
    -}
    , debounceSaveData : Debounce ()
    , antialias : Bool
    , highscoreState : HighscoreTable.Model
    , tutorialIntroShown : Bool
    , stageUnlockAnimation : StageUnlockAnimation
    , colorPalette : ColorPalette
    , debounceSetColorPalette : Debounce ColorPalette
    , timeDrift : Quantity Float RealTime
    }


type StageUnlockAnimation
    = NoStageUnlocked
    | PendingStageUnlock { unlockedStages : List StageId }
    | StageUnlocking { unlockedStages : List StageId, startTime : Quantity Float RealTime }


type FramerateWarningStatus
    = GoodFramerate
    | BadFramerate
    | FramerateWarningHidden


debounceConfig : Debounce.Config Msg
debounceConfig =
    { strategy = Debounce.soon 1000
    , transform = DebounceMsg
    }


setGameState : Game -> Model -> Model
setGameState gameState model =
    { model | gameState = gameState }


getSounds : ModelBase -> Sound.Model
getSounds modelBase =
    case modelBase of
        Loading _ ->
            Sound.initEmpty

        Loaded loaded ->
            loaded.sounds

        InvalidFlags _ ->
            Sound.initEmpty


getTime : ModelBase -> Quantity Float RealTime
getTime modelBase =
    case modelBase of
        Loading loading ->
            loading.time

        Loaded loaded ->
            loaded.time

        InvalidFlags _ ->
            Quantity.zero


getSaveData : Model -> SaveData
getSaveData model =
    { masterVolume = Sound.getMasterVolume model.sounds
    , musicVolume = Sound.getMusicVolume model.sounds
    , soundEffectVolume = Sound.getSoundEffectVolume model.sounds
    , antialias = model.antialias
    , highscores = HighscoreTable.getAllHighscores model.highscoreState
    , hideFramerateWarning = model.framerateWarningStatus == FramerateWarningHidden
    , colorPalette = model.colorPalette
    }


type alias Flags =
    { windowWidth : Int
    , windowHeight : Int
    , seed : Int
    , saveData : Result SaveDataError SaveData
    , url : Url
    }


type SaveDataError
    = NoSaveData
    | InvalidSaveData


decodeFlags : JD.Decoder Flags
decodeFlags =
    JD.map5 Flags
        (JD.field "windowWidth" JD.int)
        (JD.field "windowHeight" JD.int)
        (JD.field "seed" JD.int)
        (JD.field "saveData"
            (JD.nullable JD.string
                |> JD.andThen
                    (\maybeBase64 ->
                        case maybeBase64 of
                            Just base64 ->
                                case Codec.fromString SaveData.codec base64 of
                                    Ok saveData ->
                                        JD.succeed (Ok saveData)

                                    Err _ ->
                                        JD.succeed (Err InvalidSaveData)

                            Nothing ->
                                JD.succeed (Err NoSaveData)
                    )
            )
        )
        (JD.field "url"
            (JD.string
                |> JD.andThen
                    (\urlText ->
                        case Url.fromString urlText of
                            Just url ->
                                JD.succeed url

                            Nothing ->
                                JD.fail "Invalid URL"
                    )
            )
        )
