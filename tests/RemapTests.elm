module RemapTests exposing (..)

import AssocList
import Dict
import Editor.Remap as Remap
import Expect
import Fuzz
import Test exposing (..)


all : Test
all =
    describe "DictRemap"
        [ dictRemap
        , assocListRemap
        ]


dictRemap =
    describe "Remap dict tests"
        [ fuzz2 fuzzDict fuzzDict "There are no key collisions between the mapped source and the destination" <|
            \source destination ->
                let
                    remap =
                        Remap.remapDict source destination
                in
                source
                    |> Dict.keys
                    |> List.all
                        (\sourceKey ->
                            case Dict.get sourceKey remap of
                                Just destinationKey ->
                                    Dict.member destinationKey destination |> not

                                Nothing ->
                                    False
                        )
                    |> Expect.true "Expected no key collisions"
        , fuzz2 fuzzDict fuzzDict "Remap dict has same size at source dict" <|
            \source destination ->
                let
                    remap =
                        Remap.remapDict source destination
                in
                source |> Dict.size |> Expect.equal (Dict.size remap)
        ]


assocListRemap =
    describe "Remap assoc list tests"
        [ fuzz2 fuzzAssocList fuzzAssocList "There are no key collisions between the mapped source and the destination" <|
            \source destination ->
                let
                    remap =
                        Remap.remapAssocList Key source destination
                in
                source
                    |> AssocList.keys
                    |> List.all
                        (\sourceKey ->
                            case AssocList.get sourceKey remap of
                                Just destinationKey ->
                                    AssocList.member destinationKey destination |> not

                                Nothing ->
                                    False
                        )
                    |> Expect.true "Expected no key collisions"
        , fuzz2 fuzzAssocList fuzzAssocList "Remap dict has same size at source dict" <|
            \source destination ->
                let
                    remap =
                        Remap.remapAssocList Key source destination
                in
                source |> AssocList.size |> Expect.equal (AssocList.size remap)
        ]


fuzzDict =
    Fuzz.intRange 0 10 |> Fuzz.map (\key -> ( key, Char.fromCode (key + 65) )) |> Fuzz.list |> Fuzz.map Dict.fromList


type Key
    = Key Int


fuzzAssocList : Fuzz.Fuzzer (AssocList.Dict Key Char)
fuzzAssocList =
    Fuzz.intRange 0 10 |> Fuzz.map (\key -> ( Key key, Char.fromCode (key + 65) )) |> Fuzz.list |> Fuzz.map AssocList.fromList
