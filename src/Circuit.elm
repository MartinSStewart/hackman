module Circuit exposing
    ( AgentStartType(..)
    , Chip
    , ChipEdgePoint
    , ChipId(..)
    , ChipPosition_
    , Circuit
    , CircuitPosition(..)
    , EdgePoint
    , PlayerStart
    , Trigger
    , Wire
    , WireDirection(..)
    , WireId(..)
    , WirePosition_
    , WiresAndChips
    , chipPosition
    , getAgentStartChipId
    , getAgentStartPosition
    , getAgentStartWireId
    , getChip
    , getChips
    , getTriggers
    , getWire
    , getWires
    , isChipPosition
    , isWirePosition
    , updateChip
    , updateChips
    , wirePosition
    )

import BoundingBox2d exposing (BoundingBox2d)
import Cardinal exposing (Cardinal(..))
import Dict exposing (Dict)
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Quantity exposing (Quantity)
import Set exposing (Set)
import T exposing (T(..))
import Vector2i exposing (Vector2i)


type AgentStartType
    = PassiveAgentStart { position : CircuitPosition WireDirection Cardinal }
    | ChaserAgentStart { position : CircuitPosition WireDirection Cardinal }


getAgentStartPosition : AgentStartType -> CircuitPosition WireDirection Cardinal
getAgentStartPosition agentStartType =
    case agentStartType of
        PassiveAgentStart { position } ->
            position

        ChaserAgentStart { position } ->
            position


getAgentStartWireId : AgentStartType -> Maybe WireId
getAgentStartWireId agentStartType =
    case getAgentStartPosition agentStartType of
        WirePosition { wireId } ->
            Just wireId

        ChipPosition _ ->
            Nothing


getAgentStartChipId : AgentStartType -> Maybe ChipId
getAgentStartChipId agentStartType =
    case getAgentStartPosition agentStartType of
        WirePosition _ ->
            Nothing

        ChipPosition { chipId } ->
            Just chipId


type alias PlayerStart =
    CircuitPosition WireDirection Cardinal


type alias Circuit =
    { wires : Dict Int Wire
    , chips : Dict Int Chip
    , name : String
    , description : String
    , playerStart : PlayerStart
    , agentsStart : List AgentStartType
    , triggers : Dict String Trigger
    }


type alias Trigger =
    { region : BoundingBox2d
    }


type WireDirection
    = Forward
    | Backward


type alias WiresAndChips a =
    { a
        | wires : Dict Int Wire
        , chips : Dict Int Chip
        , triggers : Dict String Trigger
    }


type alias Wire =
    { start : Maybe ChipEdgePoint
    , end : Maybe ChipEdgePoint
    , nodes : List Point2d
    }


type alias EdgePoint =
    { side : Cardinal
    , offset : Int
    }


type alias ChipEdgePoint =
    { chipId : ChipId
    , side : Cardinal
    , offset : Int
    }


type CircuitPosition wireData chipData
    = WirePosition (WirePosition_ wireData)
    | ChipPosition (ChipPosition_ chipData)


type alias WirePosition_ wireData =
    { wireId : WireId
    , t : Quantity Float T
    , data : wireData
    }


type alias ChipPosition_ chipData =
    { chipId : ChipId
    , position : Point2i
    , chipData : chipData
    }


wirePosition : WireId -> Quantity Float T -> wireData -> CircuitPosition wireData b
wirePosition wireId t data =
    WirePosition { wireId = wireId, t = t, data = data }


chipPosition : ChipId -> Point2i -> chipData -> CircuitPosition a chipData
chipPosition chipId position chipData =
    ChipPosition { chipId = chipId, position = position, chipData = chipData }


isWirePosition : CircuitPosition a b -> Bool
isWirePosition circuitPosition =
    case circuitPosition of
        WirePosition _ ->
            True

        _ ->
            False


isChipPosition : CircuitPosition a b -> Bool
isChipPosition circuitPosition =
    case circuitPosition of
        ChipPosition _ ->
            True

        _ ->
            False


type alias Chip =
    { -- World position of chip
      position : Point2d
    , size : Vector2i
    , hackedPoints : Set ( Int, Int )
    }


type ChipId
    = ChipId Int


type WireId
    = WireId Int


getChip : WiresAndChips a -> ChipId -> Maybe Chip
getChip circuit (ChipId chipId) =
    Dict.get chipId circuit.chips


updateChip : WiresAndChips a -> ChipId -> (Chip -> Chip) -> WiresAndChips a
updateChip circuit (ChipId chipId) setFunc =
    { circuit
        | chips =
            Dict.update
                chipId
                (\maybeChip ->
                    case maybeChip of
                        Just chip ->
                            setFunc chip |> Just

                        Nothing ->
                            maybeChip
                )
                circuit.chips
    }


getWire : WiresAndChips a -> WireId -> Maybe Wire
getWire circuit (WireId wireId) =
    Dict.get wireId circuit.wires


getChips : WiresAndChips a -> List ( ChipId, Chip )
getChips circuit =
    circuit.chips |> Dict.toList |> List.map (Tuple.mapFirst ChipId)


updateChips : (ChipId -> Chip -> Chip) -> WiresAndChips a -> WiresAndChips a
updateChips updateFunc circuit =
    { circuit | chips = Dict.map (\k v -> updateFunc (ChipId k) v) circuit.chips }


getWires : WiresAndChips a -> List ( WireId, Wire )
getWires circuit =
    circuit.wires |> Dict.toList |> List.map (Tuple.mapFirst WireId)


getTriggers : WiresAndChips a -> List ( String, Trigger )
getTriggers circuit =
    Dict.toList circuit.triggers
