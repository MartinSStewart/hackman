module HelperTests exposing (all)

import Expect
import Fuzz
import Helper
import Test exposing (Test, describe, fuzz, test)


all : Test
all =
    describe "A Test Suite"
        [ fuzz Fuzz.float "roundBy with interval 1 behaves like round" <|
            \value -> Expect.equal (round value |> toFloat) (Helper.roundBy 1 value)
        , fuzz Fuzz.float "floorBy with interval 1 behaves like floor" <|
            \value -> Expect.equal (floor value |> toFloat) (Helper.floorBy 1 value)
        , fuzz Fuzz.float "ceilingBy with interval 1 behaves like ceiling" <|
            \value -> Expect.equal (ceiling value |> toFloat) (Helper.ceilingBy 1 value)
        ]
