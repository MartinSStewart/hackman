module Editor exposing
    ( Model
    , Msg
    , init
    , step
    , subscriptions
    , toolClick
    , update
    , view
    , windowResize
    )

import Agent
import AssocList
import Basics.Extra as Basics exposing (flip)
import BoundingBox2d
import BoundingBox2i
import Browser.Dom
import Cardinal exposing (Cardinal)
import Chip
import Circuit
    exposing
        ( AgentStartType(..)
        , Chip
        , ChipEdgePoint
        , ChipId(..)
        , CircuitPosition(..)
        , EdgePoint
        , Trigger
        , Wire
        , WireDirection(..)
        , WireId
        , WiresAndChips
        )
import CircuitCodec
import Codec.Serialize as Codec
import Color exposing (Color)
import ColorPalette exposing (ColorPalette)
import Dict exposing (Dict)
import Direction2d exposing (Direction2d)
import Draw
import Dropdown
import Editor.Circuit exposing (AgentStartId, Circuit, ToCircuitError(..))
import Editor.DragState as DragState
import Editor.Hand as Hand
import Editor.Helper as Helper
import Editor.Selection as Selection exposing (Selection)
import Editor.SnapSettings as SnapSettings exposing (SnapSettings)
import Editor.UndoListEx as UndoList
import Editor.UndoModel as UndoModel exposing (AgentToolItem(..), Tool(..), UndoModel)
import Editor.WirePlacer as WirePlacer exposing (ChipSnapPoint)
import Element exposing (Element, px)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Environment
import File
import File.Download
import File.Select
import Geometry.Svg as Svg
import Helper exposing (ifElse)
import Hex
import HighscoreTable
import Html.Attributes exposing (style)
import Html.Events
import Html.Events.Extra.Mouse as Mouse
import Html.Events.Extra.Wheel as Wheel
import InGame exposing (InGameModel)
import Input exposing (Input)
import Keyboard exposing (Key(..))
import KeyboardHelper as Keyboard exposing (KeyboardState)
import Keyframe
import LineSegment2d
import List.Extra as List
import List.Nonempty
import ListHelper as List
import Maybe.Extra as Maybe
import Mission exposing (Mission(..))
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Quantity exposing (Quantity)
import RealTime exposing (RealTime)
import Replay exposing (CompleteReplay, Replay)
import Round
import Set exposing (Set)
import Sound exposing (Sound)
import SpriteSheet exposing (SpriteSheet)
import StageData exposing (StageData, StageId)
import Style
import Svg exposing (Svg)
import Svg.Attributes
import T exposing (T(..))
import Task
import UndoList exposing (UndoList)
import Vector2d exposing (Vector2d)
import Vector2i exposing (Vector2i)
import Wire


type alias Model =
    { mouseCanvasPosition : Maybe Point2d
    , canvasSize : Vector2d
    , snapSettings : SnapSettings
    , showGuidelines : Bool
    , inGame : Maybe InGameModel
    , placeDirection : Cardinal
    , dragState : DragState.DragState
    , undoList : UndoList UndoModel
    , showModal : Maybe ModalType
    , mission : Mission
    , clipboard : Maybe Circuit
    , fileName : String
    , stageError : Maybe StageError
    , nameTextboxHasFocus : Bool
    }


type ModalType
    = OpenUnlockedStageModal
    | OpenUnlockedStageConfirmationModal Circuit.Circuit
    | NewStageConfirmationModal
    | OpenFileConfirmationModal Circuit.Circuit
    | OpenFileFailedModal


type alias StageError =
    { error : Editor.Circuit.ToCircuitError, startTime : Quantity Float RealTime }


type alias Config a =
    { a
        | windowSize : Vector2i
        , keys : List Key
        , previousKeys : List Key
        , time : Quantity Float RealTime
        , stepDelta : Quantity Float RealTime
        , mouseDown : Bool
        , previousMouseDown : Bool
        , mousePosition : Point2d
        , previousMousePosition : Point2d
        , spriteSheet : SpriteSheet
        , sounds : Sound.Model
        , antialias : Bool
        , highscoreState : HighscoreTable.Model
        , colorPalette : ColorPalette
    }


type Msg
    = NoOp
    | SelectTool Tool
    | MouseLeave
      {- Occurs when mouse moves in editor canvas.
         Mouse position is in world coordinates.
      -}
    | MouseMove Point2d
    | MouseClick Point2d
    | MouseUp Point2d
    | MouseDown Point2d
    | Zoom Float
    | CanvasResize Browser.Dom.Element
    | ToggleSnapToGrid
    | ToggleSnapToAngle
    | ToggleSnapAlongWire
    | ToggleShowGuidelines
    | ExportStage
    | ImportStage
    | ImportInternalStage
    | ImportedStage File.File
    | UserPressedStageInOpenUnlockedStageList Circuit.Circuit
    | ImportStageConverted String Circuit.Circuit
    | ImportStageFailed
    | GotoMainMenu
    | Play
    | StopPlaying
    | SaveReplay CompleteReplay
    | SetTriggerName String
    | SetShowTriggers Bool
    | Undo
    | Redo
    | InGameMsg InGame.InGameMsg
    | SetMission Mission
    | CopyToClipboard
    | PasteFromClipboard
    | UserTypedFileName String
    | UserPressedNewButton
    | UserPressedCancelInModal
    | UserPressedOkayInNewModal
    | UserPressedOkayInUnlockedStageConfirmationModal Circuit.Circuit
    | UserPressedOkayInErrorModal
    | NameTextboxFocusChanged Bool


tools : List Tool
tools =
    [ HandTool
    , MoveTool
    , ChipTool
    , WireTool Nothing
    , PlayerStartTool
    , AgentTool ItemPassiveAgent
    , AgentTool ItemChaserAgent
    ]
        ++ ifElse Environment.isDebug [ TriggerTool ] []


underlineText : String -> Element msg
underlineText text =
    -- We use a border instead of Font.underline because the underline gets place too close to the text otherwise.
    Element.el [ Border.widthEach { bottom = 1, top = 0, left = 0, right = 0 }, Element.moveDown 0.5 ] (Element.text text)


toolIcon : Tool -> Element msg
toolIcon toolType =
    case toolType of
        HandTool ->
            Element.row [ Font.size 18 ] [ underlineText "S", Element.text "elect" ]

        MoveTool ->
            Element.row [ Font.size 18 ] [ underlineText "D", Element.text "rag" ]

        ChipTool ->
            Element.row [ Font.size 18 ] [ underlineText "C", Element.text "hip" ]

        WireTool _ ->
            Element.row [ Font.size 18 ] [ underlineText "W", Element.text "ire" ]

        PlayerStartTool ->
            Element.row [ Font.size 18 ] [ Element.text "Play", underlineText "e", Element.text "r" ]

        AgentTool item ->
            case item of
                ItemPassiveAgent ->
                    Element.row [ Font.size 18 ] [ underlineText "A", Element.text "gent" ]

                ItemChaserAgent ->
                    Element.row [ Font.size 18 ] [ Element.text "C", underlineText "h", Element.text "aser" ]

        TriggerTool ->
            Element.el [ Font.size 18 ] (Element.text "Trigger")


init : Vector2d -> Model
init canvasSize =
    { mouseCanvasPosition = Nothing
    , canvasSize = canvasSize
    , snapSettings = SnapSettings.init
    , showGuidelines = True
    , inGame = Nothing
    , placeDirection = Cardinal.Top
    , dragState = DragState.DragNone
    , undoList =
        case StageData.exampleStage.circuit of
            StageData.Data circuit _ ->
                circuit |> UndoModel.initWithCircuit |> UndoList.fresh

            StageData.FailedToParse ->
                UndoList.fresh UndoModel.init
    , showModal = Nothing
    , mission = CustomMission
    , clipboard = Nothing
    , fileName = StageData.exampleStage.name
    , stageError = Nothing
    , nameTextboxHasFocus = False
    }


{-| Pixels per second
-}
viewSpeed : UndoModel -> Float
viewSpeed canvasView =
    260 / canvasView.viewZoom


inGameMsgConfig : InGame.MsgConfig Msg
inGameMsgConfig =
    { noOp = NoOp
    , internalMsg = InGameMsg
    , backToStageSelect = StopPlaying
    , saveReplay = SaveReplay
    , typedHighscoreName = always NoOp
    , pressedAcceptedHighscoreName = \_ -> NoOp
    , pressedHighscoreSortBy = always NoOp
    , missionCompleted = \_ _ -> NoOp
    }


step : MsgConfig msg -> Config a -> Model -> { model : Model, cmdMsg : Cmd Msg, msgs : List msg, sounds : Sound.Model }
step msgConfig config model =
    case model.inGame of
        Just inGameModel ->
            let
                stepResult =
                    InGame.step inGameMsgConfig config True (Input.fromKeyboard config) inGameModel

                model2 =
                    List.foldl
                        (\msg state -> update msgConfig config msg state |> .model)
                        { model | inGame = Just stepResult.model }
                        stepResult.msgs
            in
            { model = model2
            , cmdMsg = Cmd.none
            , msgs = []
            , sounds = stepResult.sounds
            }

        Nothing ->
            { model =
                model
                    |> updatePlaceDirection config
                    |> UndoList.skippable (moveViewWithArrowKeys config model)
                    |> checkKeyboardAction config
                    |> checkHotkeys config
                    |> checkKeyDeleteSelected config
            , cmdMsg = Cmd.none
            , msgs = []
            , sounds = config.sounds
            }


toolHotkeys : List ( String, Tool )
toolHotkeys =
    [ ( "A", AgentTool ItemPassiveAgent )
    , ( "S", HandTool )
    , ( "C", ChipTool )
    , ( "D", MoveTool )
    , ( "E", PlayerStartTool )
    , ( "H", AgentTool ItemChaserAgent )
    , ( "W", WireTool Nothing )
    ]


checkHotkeys : Config a -> Model -> Model
checkHotkeys config model =
    if
        Keyboard.isDown config Keyboard.Control
            || Keyboard.isDown config Keyboard.Shift
            || Keyboard.isDown config Keyboard.Meta
            || Keyboard.isDown config Keyboard.Alt
            || model.nameTextboxHasFocus
    then
        model

    else if Keyboard.isDown config (Keyboard.Character "P") then
        playStage config model

    else
        case List.find (Tuple.first >> Keyboard.Character >> Keyboard.isPressed config) toolHotkeys of
            Just ( _, tool ) ->
                pickTool tool model

            Nothing ->
                model


checkKeyDeleteSelected : Config a -> Model -> Model
checkKeyDeleteSelected config model =
    let
        undoModel : UndoModel
        undoModel =
            UndoList.present model
    in
    case ( undoModel.tool, Keyboard.isPressed config Keyboard.Delete ) of
        ( HandTool, True ) ->
            { model | undoList = UndoList.new (Hand.removeSelected undoModel) model.undoList }

        ( TriggerTool, True ) ->
            case ( undoModel.selectedTrigger, undoModel.showTriggers ) of
                ( Just { name }, True ) ->
                    UndoList.updateCircuit (Editor.Circuit.removeTrigger name >> Just) model

                _ ->
                    model

        _ ->
            model


moveViewWithArrowKeys : Config a -> Model -> UndoModel -> UndoModel
moveViewWithArrowKeys config model undoModel =
    if model.nameTextboxHasFocus then
        undoModel

    else
        { undoModel
            | viewWorldPosition =
                Point2d.translateBy
                    (Keyboard.arrowDown config
                        |> Vector2i.mirrorY
                        |> Vector2i.toVector2d
                        |> Vector2d.scaleBy (viewSpeed undoModel * RealTime.inSeconds Helper.secondsPerStep)
                    )
                    undoModel.viewWorldPosition
        }


updatePlaceDirection : Config a -> Model -> Model
updatePlaceDirection config model =
    { model
        | placeDirection =
            if Keyboard.isPressed config Keyboard.Spacebar then
                Cardinal.rotateLeft model.placeDirection

            else
                model.placeDirection
    }


checkKeyboardAction : Config a -> Model -> Model
checkKeyboardAction config model =
    case Keyboard.actionPressed config of
        Just Keyboard.UndoAction ->
            undo model

        Just Keyboard.RedoAction ->
            redo model

        Just Keyboard.CopyAction ->
            copyToClipboard model

        Just Keyboard.PasteAction ->
            pasteFromClipboard model

        Nothing ->
            model


copyToClipboard : Model -> Model
copyToClipboard model =
    if Selection.isEmpty (UndoList.present model).selection then
        model

    else
        { model | clipboard = UndoModel.selectionToCircuit (UndoList.present model) |> Just }


pasteFromClipboard : Model -> Model
pasteFromClipboard model =
    case model.clipboard of
        Just clipboard_ ->
            UndoList.updateUndoModel (UndoModel.pasteClipboard clipboard_ >> Just) model

        Nothing ->
            model


windowResize : Cmd Msg
windowResize =
    Browser.Dom.getElement editorCanvasName
        |> Task.attempt
            (\a ->
                case a of
                    Ok element ->
                        CanvasResize element

                    Err _ ->
                        NoOp
            )


type alias MsgConfig msg =
    { noOp : msg
    , gotoMainMenu : msg
    , saveReplay : CompleteReplay -> msg
    }


update :
    MsgConfig msg
    -> Config a
    -> Msg
    -> Model
    -> { model : Model, cmdMsg : Cmd Msg, msgs : List msg, sounds : Sound.Model }
update msgConfig config msg model =
    let
        addCmdNone : Model -> { model : Model, cmdMsg : Cmd Msg, msgs : List msg, sounds : Sound.Model }
        addCmdNone model_ =
            { model = model_, cmdMsg = Cmd.none, msgs = [], sounds = config.sounds }
    in
    case msg of
        NoOp ->
            model |> addCmdNone

        SelectTool tool ->
            pickTool tool model |> addCmdNone

        MouseLeave ->
            setMousePosition Nothing model |> addCmdNone

        MouseMove mouseCanvasPosition ->
            let
                mouseWorldPosition =
                    viewToWorld (UndoList.present model) mouseCanvasPosition
            in
            { model | dragState = DragState.mouseMove (UndoList.present model).viewZoom mouseWorldPosition model.dragState }
                |> Hand.mouseMove mouseWorldPosition
                |> setMousePosition (Just mouseCanvasPosition)
                |> addCmdNone

        MouseDown mouseCanvasPosition ->
            let
                mouseWorldPosition =
                    viewToWorld (UndoList.present model) mouseCanvasPosition
            in
            { model =
                if model.nameTextboxHasFocus then
                    model

                else
                    setMousePosition (Just mouseCanvasPosition) model
                        |> (\model_ -> { model_ | dragState = DragState.mouseDown mouseWorldPosition })
            , cmdMsg = Browser.Dom.blur nameTextboxId |> Task.attempt (always NoOp)
            , msgs = []
            , sounds = config.sounds
            }

        MouseClick mouseCanvasPosition ->
            let
                clickWorldPosition =
                    viewToWorld (UndoList.present model) mouseCanvasPosition
            in
            setMousePosition (Just mouseCanvasPosition) model
                |> toolClick clickWorldPosition
                |> addCmdNone

        MouseUp mouseCanvasPosition ->
            setMousePosition (Just mouseCanvasPosition) model
                |> toolMouseUp config mouseCanvasPosition
                |> (\model_ -> { model_ | dragState = DragState.mouseUp })
                |> addCmdNone

        Zoom zoomDelta ->
            updateZoom zoomDelta model |> addCmdNone

        CanvasResize canvas ->
            { model
                | canvasSize = Vector2d.fromComponents ( canvas.element.width, canvas.element.height )
            }
                |> addCmdNone

        ToggleSnapToGrid ->
            { model | snapSettings = SnapSettings.toggleToGrid model.snapSettings } |> addCmdNone

        ToggleSnapToAngle ->
            { model | snapSettings = SnapSettings.toggleToAngle model.snapSettings } |> addCmdNone

        ToggleSnapAlongWire ->
            { model | snapSettings = SnapSettings.toggleAlongWire model.snapSettings } |> addCmdNone

        ExportStage ->
            case getCircuit model of
                Ok circuit ->
                    { model = model, cmdMsg = exportStage (model.fileName ++ fileExtension) circuit, msgs = [], sounds = config.sounds }

                Err error ->
                    { model | stageError = Just { error = error, startTime = config.time } } |> addCmdNone

        ImportStage ->
            { model = model
            , cmdMsg = File.Select.file [ fileExtension ] ImportedStage
            , msgs = []
            , sounds = config.sounds
            }

        ImportedStage file ->
            let
                fileName =
                    if File.name file |> String.endsWith fileExtension then
                        File.name file |> String.dropRight (String.length fileExtension)

                    else
                        File.name file
            in
            { model = model
            , cmdMsg =
                File.toBytes file
                    |> Task.perform
                        (\bytes ->
                            case Codec.fromBytes CircuitCodec.circuitVersion bytes of
                                Ok circuit ->
                                    ImportStageConverted fileName circuit

                                Err _ ->
                                    ImportStageFailed
                        )
            , msgs = []
            , sounds = config.sounds
            }

        ImportStageConverted filename circuit ->
            { model | fileName = filename }
                |> showConfirmationModalIfUnsavedChanges
                    (OpenUnlockedStageConfirmationModal circuit)
                    (setOpenedStage circuit)
                |> addCmdNone

        GotoMainMenu ->
            { model = model, cmdMsg = Cmd.none, msgs = [ msgConfig.gotoMainMenu ], sounds = config.sounds }

        Play ->
            playStage config model |> addCmdNone

        StopPlaying ->
            { model = { model | inGame = Nothing }
            , cmdMsg = Cmd.none
            , msgs = []
            , sounds = config.sounds
            }

        SaveReplay inGameModel ->
            { model = model, cmdMsg = Cmd.none, msgs = [ msgConfig.saveReplay inGameModel ], sounds = config.sounds }

        SetTriggerName triggerName ->
            { model | undoList = UndoList.new (editTriggerName triggerName (UndoList.present model)) model.undoList }
                |> addCmdNone

        SetShowTriggers value ->
            { model | undoList = UndoList.new (setShowTrigger value (UndoList.present model)) model.undoList } |> addCmdNone

        Undo ->
            undo model |> addCmdNone

        Redo ->
            redo model |> addCmdNone

        ToggleShowGuidelines ->
            { model | showGuidelines = not model.showGuidelines } |> addCmdNone

        InGameMsg inGameMsg ->
            case model.inGame of
                Just inGame_ ->
                    { model | inGame = InGame.update config inGameMsg inGame_ |> Just } |> addCmdNone

                Nothing ->
                    model |> addCmdNone

        ImportInternalStage ->
            { model | showModal = Just OpenUnlockedStageModal } |> addCmdNone

        UserPressedStageInOpenUnlockedStageList circuit ->
            showConfirmationModalIfUnsavedChanges
                (OpenUnlockedStageConfirmationModal circuit)
                (setOpenedStage circuit)
                model
                |> addCmdNone

        SetMission mission ->
            { model | mission = mission } |> addCmdNone

        CopyToClipboard ->
            copyToClipboard model |> addCmdNone

        PasteFromClipboard ->
            pasteFromClipboard model |> addCmdNone

        UserTypedFileName fileName ->
            { model | fileName = fileName } |> addCmdNone

        UserPressedNewButton ->
            showConfirmationModalIfUnsavedChanges NewStageConfirmationModal setNewStage model |> addCmdNone

        UserPressedCancelInModal ->
            closeModal model |> addCmdNone

        UserPressedOkayInNewModal ->
            setNewStage model |> closeModal |> addCmdNone

        UserPressedOkayInUnlockedStageConfirmationModal circuit ->
            { model
                | undoList = UndoModel.initWithCircuit circuit |> UndoList.fresh
                , showModal = Nothing
            }
                |> addCmdNone

        ImportStageFailed ->
            { model | showModal = Just OpenFileFailedModal } |> addCmdNone

        UserPressedOkayInErrorModal ->
            closeModal model |> addCmdNone

        NameTextboxFocusChanged focus ->
            { model | nameTextboxHasFocus = focus } |> addCmdNone


playStage config model =
    case getCircuit model of
        Ok circuit ->
            { model
                | inGame =
                    InGame.init False 31 config.time model.mission { circuit = circuit, lookupTable = Nothing } |> Just
            }

        Err error ->
            { model | stageError = Just { error = error, startTime = config.time } }


showConfirmationModalIfUnsavedChanges : ModalType -> (Model -> Model) -> Model -> Model
showConfirmationModalIfUnsavedChanges modalType action model =
    if UndoList.lengthFuture model.undoList == 0 && UndoList.lengthPast model.undoList == 0 then
        action model |> closeModal

    else
        { model | showModal = Just modalType }


setOpenedStage : Circuit.Circuit -> Model -> Model
setOpenedStage circuit model =
    { model
        | undoList = UndoModel.initWithCircuit circuit |> UndoList.fresh
        , showModal = Nothing
    }


setNewStage : Model -> Model
setNewStage model =
    { model | fileName = "New Stage", undoList = UndoModel.init |> UndoList.fresh }


closeModal model =
    { model | showModal = Nothing }


updateZoom : Float -> Model -> Model
updateZoom scrollAmount model =
    let
        undoModel =
            UndoList.present model

        adjustedZoom =
            {- The scroll speed in Chrome is crazy high compared to
               Firefox, so we account for that here.
            -}
            if abs scrollAmount > 100 then
                scrollAmount / 15

            else
                scrollAmount

        newViewZoom =
            undoModel.viewZoom * 1.05 ^ -adjustedZoom

        zoomRelativeTo =
            model.mouseCanvasPosition
                |> Maybe.withDefault (Point2d.translateBy (Vector2d.scaleBy 0.5 model.canvasSize) Point2d.origin)
                |> viewToWorld undoModel
    in
    UndoList.skippable (UndoModel.setZoom zoomRelativeTo newViewZoom) model


undo : Model -> Model
undo model =
    { model | undoList = UndoList.undo model.undoList }


redo : Model -> Model
redo model =
    { model | undoList = UndoList.redo model.undoList }


setShowTrigger : Bool -> UndoModel -> UndoModel
setShowTrigger showTriggers undoModel =
    { undoModel | showTriggers = showTriggers }


editTriggerName : String -> UndoModel -> UndoModel
editTriggerName newName undoModel =
    { undoModel
        | selectedTrigger =
            undoModel.selectedTrigger |> Maybe.map (\a -> { a | editedName = newName })
    }


fileExtension =
    ".at"


exportStage : String -> Circuit.Circuit -> Cmd Msg
exportStage fileName circuit =
    circuit
        |> Codec.toBytes CircuitCodec.circuitVersion
        |> File.Download.bytes fileName "application/octet-stream"


getCircuit : Model -> Result Editor.Circuit.ToCircuitError Circuit.Circuit
getCircuit model =
    let
        model2 : UndoModel
        model2 =
            applyTriggerName (UndoList.present model)
    in
    Editor.Circuit.toCircuit model2.circuit


pickTool : Tool -> Model -> Model
pickTool selectedTool model =
    case (UndoList.present model).tool of
        HandTool ->
            UndoList.skippable (UndoModel.setTool selectedTool) model

        MoveTool ->
            UndoList.skippable (UndoModel.setTool selectedTool) model

        ChipTool ->
            UndoList.skippable (UndoModel.setTool selectedTool) model

        WireTool (Just wirePlacerModel) ->
            UndoList.updateUndoModel
                (\undoModel ->
                    WirePlacer.endOnNothing wirePlacerModel undoModel.circuit
                        |> Maybe.withDefault undoModel.circuit
                        |> flip UndoModel.setCircuit undoModel
                        |> UndoModel.setTool selectedTool
                        |> Just
                )
                model

        WireTool Nothing ->
            UndoList.skippable (UndoModel.setTool selectedTool) model

        PlayerStartTool ->
            UndoList.skippable (UndoModel.setTool selectedTool) model

        AgentTool _ ->
            UndoList.skippable (UndoModel.setTool selectedTool) model

        TriggerTool ->
            UndoList.skippable (applyTriggerName >> UndoModel.setTool selectedTool) model


setMousePosition : Maybe Point2d -> Model -> Model
setMousePosition mouseCanvasPosition model =
    { model | mouseCanvasPosition = mouseCanvasPosition }


setPlayerStart : Cardinal -> Point2d -> UndoModel -> Maybe UndoModel
setPlayerStart placeDirection clickWorldPosition undoModel =
    case agentStartPosition placeDirection undoModel.circuit undoModel clickWorldPosition of
        Just startPos ->
            undoModel.circuit
                |> Editor.Circuit.setPlayer (Just startPos)
                |> flip UndoModel.setCircuit undoModel
                |> Just

        Nothing ->
            Nothing


toolClick : Point2d -> Model -> Model
toolClick clickWorldPosition model =
    case (UndoList.present model).tool of
        WireTool wirePlacer ->
            toolClickWirePlacer clickWorldPosition wirePlacer model

        PlayerStartTool ->
            UndoList.updateUndoModel (setPlayerStart model.placeDirection clickWorldPosition) model

        AgentTool agentType ->
            UndoList.updateUndoModel
                (newAgentStartPosition model.placeDirection agentType clickWorldPosition)
                model

        _ ->
            model


agentStartPosition : Cardinal -> Circuit -> UndoModel -> Point2d -> Maybe (CircuitPosition WireDirection Cardinal)
agentStartPosition placeDirection circuit undoModel mouseWorldPosition =
    case nearestCircuitPosition placeDirection undoModel mouseWorldPosition of
        Just position ->
            case Helper.circuitWorldPosition circuit position of
                Just worldPos ->
                    if
                        Point2d.distanceFrom worldPos mouseWorldPosition
                            <= Helper.viewScalarToWorld undoModel 40
                    then
                        Just position

                    else
                        Nothing

                Nothing ->
                    Nothing

        Nothing ->
            Nothing


newAgentStartPosition : Cardinal -> AgentToolItem -> Point2d -> UndoModel -> Maybe UndoModel
newAgentStartPosition placeDirection agentSelection mouseWorldPosition undoModel =
    let
        getNewAgent : CircuitPosition WireDirection Cardinal -> AgentStartType
        getNewAgent position =
            case agentSelection of
                ItemPassiveAgent ->
                    PassiveAgentStart { position = position }

                ItemChaserAgent ->
                    ChaserAgentStart { position = position }
    in
    case nearestCircuitPosition placeDirection undoModel mouseWorldPosition of
        Just position ->
            case Helper.circuitWorldPosition undoModel.circuit position of
                Just worldPos ->
                    if
                        Point2d.distanceFrom worldPos mouseWorldPosition
                            <= Helper.viewScalarToWorld undoModel 40
                    then
                        Editor.Circuit.addAgent (getNewAgent position) undoModel.circuit
                            |> flip UndoModel.setCircuit undoModel
                            |> Just

                    else
                        Nothing

                Nothing ->
                    Nothing

        Nothing ->
            Nothing


toolClickWirePlacer : Point2d -> Maybe WirePlacer.Model -> Model -> Model
toolClickWirePlacer clickWorldPosition maybeWirePlacer model =
    let
        snapPoint =
            WirePlacer.getSnapPoint
                (UndoList.present model).viewZoom
                model.snapSettings
                (UndoList.present model).circuit
                maybeWirePlacer
                clickWorldPosition
    in
    case ( maybeWirePlacer, snapPoint ) of
        ( Just wirePlacer, WirePlacer.ChipEdge chipEdgePoint ) ->
            UndoList.updateUndoModel
                (\undoModel ->
                    let
                        undoModel2 =
                            { undoModel | tool = WireTool Nothing }
                    in
                    case WirePlacer.endOnChipEdge chipEdgePoint wirePlacer undoModel2.circuit of
                        Just circuit ->
                            UndoModel.setCircuit circuit undoModel2 |> Just

                        Nothing ->
                            Nothing
                )
                model

        ( Just wirePlacer, WirePlacer.WireEndpoint { wireId, isEnd } ) ->
            UndoList.updateUndoModel
                (\undoModel ->
                    let
                        undoModel2 =
                            { undoModel | tool = WireTool Nothing }
                    in
                    case WirePlacer.endOnWire wireId isEnd wirePlacer undoModel2.circuit of
                        Just circuit ->
                            UndoModel.setCircuit circuit undoModel2 |> Just

                        Nothing ->
                            Nothing
                )
                model

        ( Just wirePlacer, WirePlacer.GridPosition position ) ->
            UndoList.updateUndoModel
                (UndoModel.setTool (WirePlacer.addNode position wirePlacer |> Just |> WireTool) >> Just)
                model

        ( Just wirePlacer, WirePlacer.LastPlacedPosition ) ->
            UndoList.updateUndoModel
                (\undoModel ->
                    case WirePlacer.endOnNothing wirePlacer undoModel.circuit of
                        Just circuit ->
                            UndoModel.setCircuit circuit undoModel |> (\a -> { a | tool = WireTool Nothing }) |> Just

                        Nothing ->
                            Nothing
                )
                model

        ( Just wirePlacer, WirePlacer.AlongWire alongWire ) ->
            UndoList.updateUndoModel
                (UndoModel.setTool (WirePlacer.addNode (WirePlacer.alongWirePosition alongWire) wirePlacer |> Just |> WireTool) >> Just)
                model

        ( Nothing, WirePlacer.GridPosition position ) ->
            UndoList.updateUndoModel
                (UndoModel.setTool (WirePlacer.startDisconnected position |> Just |> WireTool) >> Just)
                model

        ( Nothing, WirePlacer.ChipEdge chipEdgePoint ) ->
            UndoList.updateUndoModel
                (UndoModel.setTool (WirePlacer.startOnChipEdge chipEdgePoint |> Just |> WireTool) >> Just)
                model

        ( Nothing, WirePlacer.WireEndpoint { wireId, isEnd } ) ->
            UndoList.updateUndoModel
                (UndoModel.setTool (WirePlacer.startOnWire wireId isEnd |> Just |> WireTool) >> Just)
                model

        ( Nothing, WirePlacer.LastPlacedPosition ) ->
            model

        ( Nothing, WirePlacer.AlongWire alongWire ) ->
            UndoList.updateUndoModel
                (UndoModel.setTool (WirePlacer.alongWirePosition alongWire |> WirePlacer.startDisconnected |> Just |> WireTool) >> Just)
                model


type alias WireDetachedEndpoint =
    { wireId : WireId, isEnd : Bool, position : Point2d }


placeChip : Point2d -> Point2d -> Model -> Model
placeChip dragStart dragEnd model =
    UndoList.updateCircuit
        (\circuit ->
            let
                ( circuitWithChip, chipId ) =
                    Editor.Circuit.addChip chip circuit

                start =
                    placeChipStart (SnapSettings.getToGrid model.snapSettings) dragStart

                chip =
                    Chip.init start (Vector2d.from start dragEnd |> Vector2i.roundFrom |> chipSize) Set.empty
            in
            snapDetachedWiresToChip circuitWithChip ( chipId, chip ) |> Just
        )
        model


snapDetachedWiresToChip : Circuit -> ( ChipId, Chip ) -> Circuit
snapDetachedWiresToChip circuit ( chipId, chip ) =
    Editor.Circuit.updateAllWires
        (\( _, wire ) ->
            UndoModel.detachedWireSnapsToChip wire chip
                |> Maybe.map (UndoModel.updateWireFromDetachedWireSnapsToChip chipId wire)
                |> Maybe.withDefault wire
        )
        circuit


placeChipStart : Bool -> Point2d -> Point2d
placeChipStart snapToGrid dragStart =
    if snapToGrid then
        Point2i.roundFrom dragStart |> Point2i.toPoint2d

    else
        dragStart


toolMouseUp : KeyboardState a -> Point2d -> Model -> Model
toolMouseUp keyboard mouseCanvasPosition model =
    let
        mouseWorldPosition =
            viewToWorld (UndoList.present model) mouseCanvasPosition

        undoModel =
            UndoList.present model
    in
    case ( undoModel.tool, model.dragState ) of
        ( ChipTool, DragState.DragReady dragStart ) ->
            placeChip dragStart mouseWorldPosition model

        ( ChipTool, DragState.Dragging dragStart ) ->
            placeChip dragStart mouseWorldPosition model

        ( TriggerTool, DragState.Dragging dragStart ) ->
            if Point2d.distanceFrom dragStart mouseWorldPosition > Helper.viewScalarToWorld undoModel 10 then
                let
                    region =
                        BoundingBox2d.from dragStart mouseWorldPosition

                    ( newCircuit, triggerId ) =
                        Editor.Circuit.addTrigger { region = region } undoModel.circuit

                    newUndoList =
                        UndoModel.setCircuit newCircuit undoModel
                in
                { model
                    | undoList = UndoList.new (selectTrigger triggerId newUndoList) model.undoList
                }

            else
                let
                    updateTriggerSelection undoModel_ =
                        Circuit.getTriggers undoModel_.circuit
                            |> List.find (\( _, box ) -> BoundingBox2d.contains mouseWorldPosition box.region)
                            |> Maybe.map Tuple.first
                            |> (\a ->
                                    case a of
                                        Just b ->
                                            selectTrigger b undoModel_

                                        Nothing ->
                                            deselectTrigger undoModel_
                               )
                in
                UndoList.updateUndoModel (updateTriggerSelection >> Just) model

        ( HandTool, _ ) ->
            Hand.mouseUp keyboard mouseWorldPosition model

        ( MoveTool, (DragState.Dragging _) as dragState ) ->
            let
                snapToGrid =
                    SnapSettings.getToGrid model.snapSettings

                moveBy =
                    DragState.getOffset snapToGrid mouseWorldPosition dragState
            in
            UndoList.updateUndoModel
                (\undoModel_ ->
                    let
                        newCircuit =
                            UndoModel.moveSelection moveBy undoModel_.selection undoModel_.circuit
                    in
                    Just { undoModel_ | circuit = newCircuit }
                )
                model

        _ ->
            model


canSetTriggerName : String -> String -> Circuit -> Bool
canSetTriggerName triggerId newTriggerId circuit =
    newTriggerId /= "" && (triggerId == newTriggerId || (Dict.member newTriggerId circuit.triggers |> not))


setTriggerName : { name : String, editedName : String } -> Circuit -> Circuit
setTriggerName { name, editedName } circuit =
    case ( Dict.get name circuit.triggers, canSetTriggerName name editedName circuit ) of
        ( Just trigger, True ) ->
            { circuit | triggers = Dict.remove name circuit.triggers |> Dict.insert editedName trigger }

        _ ->
            circuit


applyTriggerName : UndoModel -> UndoModel
applyTriggerName undoModel =
    case undoModel.selectedTrigger of
        Just selected ->
            setTriggerName selected undoModel.circuit |> flip UndoModel.setCircuit undoModel

        Nothing ->
            undoModel


selectTrigger : String -> UndoModel -> UndoModel
selectTrigger triggerId undoModel =
    let
        newUndoList =
            applyTriggerName undoModel
    in
    { newUndoList | selectedTrigger = Just { name = triggerId, editedName = triggerId } }


deselectTrigger : UndoModel -> UndoModel
deselectTrigger undoModel =
    let
        newUndoModel =
            applyTriggerName undoModel
    in
    { newUndoModel | selectedTrigger = Nothing }


placeWireDirection : Cardinal -> WireDirection
placeWireDirection placeDirection =
    case placeDirection of
        Cardinal.Left ->
            Forward

        Cardinal.Right ->
            Forward

        Cardinal.Top ->
            Backward

        Cardinal.Bottom ->
            Backward


nearestCircuitPosition : Cardinal -> UndoModel -> Point2d -> Maybe (CircuitPosition WireDirection Cardinal)
nearestCircuitPosition placeDirection undoModel worldPoint =
    case ( nearestWireT undoModel worldPoint, nearestChipLocal undoModel worldPoint ) of
        ( Just wire, Just { chipId, localPoint, distance } ) ->
            Just <|
                if (wire.distance / 2) < distance then
                    placeWireDirection placeDirection |> Circuit.wirePosition wire.wireId wire.nearestT

                else
                    Circuit.chipPosition chipId localPoint placeDirection

        ( Just wire, Nothing ) ->
            placeWireDirection placeDirection |> Circuit.wirePosition wire.wireId wire.nearestT |> Just

        ( Nothing, Just { chipId, localPoint } ) ->
            Circuit.chipPosition chipId localPoint placeDirection |> Just

        ( Nothing, Nothing ) ->
            Nothing


nearestWireT : UndoModel -> Point2d -> Maybe { wireId : WireId, nearestT : Quantity Float T, distance : Float }
nearestWireT undoModel worldPoint =
    Circuit.getWires undoModel.circuit
        |> List.concatMap
            (\( wireId, wire ) ->
                let
                    nearestT =
                        wireNearestT undoModel.circuit wire worldPoint
                in
                Agent.wirePosition undoModel.circuit wire nearestT
                    |> Maybe.map (\nearestPoint -> ( wireId, nearestT, nearestPoint ))
                    |> Maybe.toList
            )
        |> List.minimumBy (\( _, _, nearestPoint ) -> Point2d.distanceFrom worldPoint nearestPoint)
        |> Maybe.andThen
            (\( wireId, nearestT, nearestPoint ) ->
                let
                    distance =
                        Point2d.distanceFrom nearestPoint worldPoint
                in
                if distance < Helper.viewScalarToWorld undoModel 40 then
                    Just { wireId = wireId, nearestT = nearestT, distance = distance }

                else
                    Nothing
            )


nearestChipLocal :
    UndoModel
    -> Point2d
    -> Maybe { chipId : ChipId, localPoint : Point2i, distance : Float }
nearestChipLocal undoModel worldPoint =
    let
        chipDistance chip localPoint =
            Chip.localToWorld chip localPoint |> Point2d.distanceFrom worldPoint
    in
    Circuit.getChips undoModel.circuit
        |> List.map (\( chipId, chip ) -> ( chipId, chip, chipNearestLocalPoint chip worldPoint ))
        |> List.minimumBy
            (\( _, chip, localPoint ) -> chipDistance chip localPoint)
        |> Maybe.andThen
            (\( chipId, chip, localPoint ) ->
                let
                    distance =
                        chipDistance chip localPoint
                in
                if distance < Helper.viewScalarToWorld undoModel 40 then
                    Just { chipId = chipId, localPoint = localPoint, distance = distance }

                else
                    Nothing
            )


chipNearestLocalPoint : Chip -> Point2d -> Point2i
chipNearestLocalPoint chip worldPoint =
    let
        ( x, y ) =
            Chip.worldToLocal chip worldPoint |> Point2i.roundFrom |> Point2i.tuple

        { minX, minY, maxX, maxY } =
            Chip.tileBounds chip |> BoundingBox2i.extrema
    in
    { x = clamp minX maxX x, y = clamp minY maxY y }


wireNearestT : WiresAndChips a -> Wire -> Point2d -> Quantity Float T
wireNearestT circuit wire point =
    Wire.nodePoints circuit wire
        |> List.pairwise
        |> List.indexedMap
            (\index ( end, start ) ->
                let
                    line =
                        LineSegment2d.from end start

                    nearestPoint =
                        Helper.nearestLinePoint line point

                    distance =
                        Point2d.distanceFrom point nearestPoint
                in
                ( distance, Helper.nearestLineT line point |> Quantity.plus (toFloat index |> T.t) )
            )
        |> List.minimumBy Tuple.first
        |> Maybe.map Tuple.second
        |> Maybe.withDefault Quantity.zero


toolButtonStyle : Bool -> List (Element.Attribute msg)
toolButtonStyle selected =
    [ Element.width <| px 64
    , Element.height <| px 64
    , if selected then
        Background.color <| Element.rgba 0 0 0 0.1

      else
        Background.color <| Element.rgba 0 0 0 0.2
    , Border.rounded <| round 5
    , Element.mouseOver [ Background.color <| Element.rgba 0 0 0 0.1 ]
    ]


wireSelectedNodes : Model -> WireId -> Set Int
wireSelectedNodes model wireId =
    let
        set =
            (UndoList.present model).selection.wireSegments
                |> List.filter (\( wireId_, _ ) -> wireId_ == wireId)
                |> List.map Tuple.second
                |> Selection.selectedWireSegments
    in
    case (UndoList.present model).tool of
        HandTool ->
            set

        MoveTool ->
            set

        _ ->
            Set.empty


drawWire : Float -> (Int -> Bool) -> Circuit -> Wire -> Svg msg
drawWire viewZoom segmentSelected circuit wire =
    let
        nodes =
            Wire.nodePoints circuit wire

        color =
            Color.blue

        lineThickness =
            2 / viewZoom

        selectedSegmentsView =
            nodes
                |> List.pairwise
                |> List.indexedMap
                    (\index ( first, second ) ->
                        if segmentSelected index then
                            Draw.lineStrip selectionStrokeColor lineThickness [ first, second ] |> Just

                        else
                            Nothing
                    )
                |> List.filterMap identity
                |> Svg.g []

        endPointsView =
            case ( List.reverse nodes |> List.head, List.head nodes ) of
                ( Just end, Just start ) ->
                    [ ( start, segmentSelected 0 ), ( end, segmentSelected (List.length nodes - 2) ) ]
                        |> List.map (\( pos, selected ) -> Draw.circle (ifElse selected selectionStrokeColor color) (4 / viewZoom) pos)
                        |> Svg.g []

                _ ->
                    Svg.g [] []
    in
    Svg.g
        []
        [ Draw.lineStrip color lineThickness nodes
        , endPointsView
        , selectedSegmentsView
        ]


chipSize : Vector2i -> Vector2i
chipSize dragSize =
    dragSize
        |> Vector2i.map
            (\a ->
                if a < -1 || a > 1 then
                    a

                else if a < 0 then
                    -2

                else
                    2
            )


drawChipTool : Maybe Point2d -> UndoModel -> Model -> Svg msg
drawChipTool maybeMouseCanvasPosition undoModel model =
    case maybeMouseCanvasPosition of
        Just mouseCanvasPosition ->
            let
                mouseGridPosition =
                    viewToWorld undoModel mouseCanvasPosition
            in
            case chipFromDrag model mouseGridPosition of
                Just chip ->
                    Helper.drawRectangle
                        [ Svg.Attributes.stroke "blue"
                        , Svg.Attributes.fill "none"
                        , Svg.Attributes.strokeWidth <| String.fromFloat <| 2 / undoModel.viewZoom
                        ]
                        chip.position
                        (chip.size |> Vector2i.toVector2d)

                Nothing ->
                    Svg.g [] []

        _ ->
            Svg.g [] []


chipFromDrag : Model -> Point2d -> Maybe Chip
chipFromDrag model mouseGridPosition =
    case model.dragState of
        DragState.Dragging dragWorldStart ->
            let
                start =
                    placeChipStart (SnapSettings.getToGrid model.snapSettings) dragWorldStart
            in
            Chip.init start (Vector2d.from start mouseGridPosition |> Vector2i.roundFrom |> chipSize) Set.empty |> Just

        _ ->
            Nothing


drawTriggerTool : Maybe Point2d -> Model -> Svg msg
drawTriggerTool maybeMouseCanvasPosition model =
    case ( maybeMouseCanvasPosition, model.dragState ) of
        ( Just mouseCanvasPosition, DragState.Dragging dragWorldStart ) ->
            let
                mouseWorldPosition =
                    viewToWorld (UndoList.present model) mouseCanvasPosition
            in
            Helper.drawRectangle
                [ Svg.Attributes.stroke "purple"
                , Svg.Attributes.fill "none"
                , Svg.Attributes.strokeWidth <| String.fromFloat <| 2 / (UndoList.present model).viewZoom
                ]
                dragWorldStart
                (Vector2d.from dragWorldStart mouseWorldPosition)

        _ ->
            Svg.g [] []


selectionFillColor =
    Color.lightGreen


selectionStrokeColor =
    Color.green


drawChip : Model -> ChipId -> Chip -> Bool -> Svg msg
drawChip model chipId chip selected =
    let
        strokeColor =
            ifElse selected selectionStrokeColor Color.blue

        fillColor =
            ifElse selected selectionFillColor Color.lightBlue

        undoModel =
            UndoList.present model

        snapPoints =
            case undoModel.tool of
                WireTool wirePlacer ->
                    WirePlacer.chipSnapPoints ( chipId, chip ) wirePlacer
                        |> List.map
                            (.position
                                >> Draw.circleWithOutline
                                    fillColor
                                    strokeColor
                                    (4 / undoModel.viewZoom)
                                    (2 / undoModel.viewZoom)
                            )

                _ ->
                    []
    in
    Svg.g []
        (Draw.rectangleWithOutline fillColor
            strokeColor
            (2 / (UndoList.present model).viewZoom)
            (Chip.worldBounds chip)
            :: snapPoints
        )


triggerVisibleName : Model -> String -> String
triggerVisibleName model triggerId =
    case (UndoList.present model).tool of
        TriggerTool ->
            case (UndoList.present model).selectedTrigger of
                Just { name, editedName } ->
                    if triggerId == name then
                        editedName

                    else
                        triggerId

                Nothing ->
                    triggerId

        _ ->
            triggerId


drawTrigger : Model -> ( String, Trigger ) -> Svg msg
drawTrigger model ( triggerId, trigger ) =
    let
        selected =
            case (UndoList.present model).tool of
                TriggerTool ->
                    case (UndoList.present model).selectedTrigger of
                        Just { name } ->
                            triggerId == name

                        Nothing ->
                            False

                _ ->
                    False

        viewZoom =
            (UndoList.present model).viewZoom

        { minX, minY } =
            BoundingBox2d.extrema trigger.region

        strokeColor =
            if selected then
                Color.rgba 0.7 0.2 0.7 1

            else
                Color.rgba 0.4 0 0.4 1

        fillColor =
            if selected then
                Color.rgba 0.7 0.2 0.7 0.4

            else
                Color.rgba 0.4 0 0.4 0.4
    in
    Svg.g []
        [ Svg.text_
            [ Svg.Attributes.x <| String.fromFloat minX
            , Svg.Attributes.y <| String.fromFloat (minY + 20 / viewZoom)
            , Svg.Attributes.fontSize <| String.fromFloat <| 20 / viewZoom
            ]
            [ Svg.text <| triggerVisibleName model triggerId ]
        , Draw.rectangleWithOutline fillColor
            strokeColor
            (2 / viewZoom)
            trigger.region
        ]


playerStartArrow : Color -> Circuit -> Model -> Point2d -> CircuitPosition WireDirection Cardinal -> Maybe (Svg msg)
playerStartArrow color circuit model position circuitPosition =
    let
        undoModel =
            UndoList.present model
    in
    case playerStartDirection circuit circuitPosition of
        Just startDirection ->
            startDirection
                |> Direction2d.toVector
                |> Vector2d.scaleBy (Helper.viewScalarToWorld undoModel 20)
                |> flip Point2d.translateBy position
                |> Draw.arrow
                    color
                    (Helper.viewScalarToWorld undoModel 4)
                    (Helper.viewScalarToWorld undoModel 10)
                    position
                |> Just

        Nothing ->
            Nothing


drawPlayerStartTool : Color -> Circuit -> Model -> Svg msg
drawPlayerStartTool color circuit model =
    let
        undoModel =
            UndoList.present model
    in
    case model.mouseCanvasPosition of
        Just mouseCanvasPosition ->
            case viewToWorld undoModel mouseCanvasPosition |> agentStartPosition model.placeDirection circuit undoModel of
                Just circuitPos ->
                    Helper.circuitWorldPosition circuit circuitPos
                        |> Maybe.map (Tuple.pair circuitPos)
                        |> Maybe.map
                            (\( circuitPos_, pos ) ->
                                Draw.circle color (Helper.viewScalarToWorld undoModel 10) pos
                                    :: Maybe.toList (playerStartArrow color circuit model pos circuitPos_)
                                    |> Svg.g [ Svg.Attributes.opacity "0.5" ]
                            )
                        |> Maybe.withDefault (Svg.g [] [])

                Nothing ->
                    Svg.g [] []

        Nothing ->
            Svg.g [] []


drawAgentStart : Color -> Circuit -> Model -> CircuitPosition WireDirection Cardinal -> Svg msg
drawAgentStart color circuit model startPosition =
    Helper.circuitWorldPosition circuit startPosition
        |> Maybe.map
            (\pos ->
                Draw.circle color (Helper.viewScalarToWorld (UndoList.present model) 10) pos
                    :: (playerStartArrow color circuit model pos startPosition |> Maybe.toList)
                    |> Svg.g []
            )
        |> Maybe.withDefault (Svg.g [] [])


playerStartDirection : Circuit -> CircuitPosition WireDirection Cardinal -> Maybe Direction2d
playerStartDirection circuit circuitPosition =
    case circuitPosition of
        WirePosition { wireId, t, data } ->
            Circuit.getWire circuit wireId
                |> Maybe.andThen (\wire -> Wire.direction circuit wire t data)

        ChipPosition { chipData } ->
            Cardinal.direction chipData |> Just


drawBackground : UndoModel -> List (Svg msg)
drawBackground undoModel =
    let
        backgroundOffset : Float -> String
        backgroundOffset =
            Basics.fractionalModBy undoModel.viewZoom >> String.fromFloat

        ( x, y ) =
            Point2d.coordinates undoModel.viewWorldPosition
    in
    [ Svg.defs
        []
        [ Svg.pattern
            [ Svg.Attributes.patternUnits "userSpaceOnUse"
            , Svg.Attributes.id "grid"
            , Svg.Attributes.width <| String.fromFloat undoModel.viewZoom
            , Svg.Attributes.height <| String.fromFloat undoModel.viewZoom
            ]
            [ Svg.rect
                [ Svg.Attributes.width "1"
                , Svg.Attributes.height "1"
                , Svg.Attributes.fill "black"
                ]
                []
            ]
        ]
    , Svg.rect
        [ Svg.Attributes.width "100%"
        , Svg.Attributes.height "100%"
        , Svg.Attributes.transform <|
            "translate("
                ++ backgroundOffset (-x * undoModel.viewZoom)
                ++ " "
                ++ backgroundOffset (-y * undoModel.viewZoom)
                ++ ")"
        , Svg.Attributes.fill "url(#grid)"
        ]
        []
    ]


showChipSelected : Model -> ChipId -> Bool
showChipSelected model chipId =
    let
        undoModel =
            UndoList.present model
    in
    case undoModel.tool of
        HandTool ->
            Selection.chipSelected undoModel.selection chipId

        MoveTool ->
            Selection.chipSelected undoModel.selection chipId

        _ ->
            False


viewCircuit : Model -> Circuit
viewCircuit model =
    let
        circuit =
            (UndoList.present model).circuit

        undoModel =
            UndoList.present model
    in
    case undoModel.tool of
        MoveTool ->
            case model.mouseCanvasPosition of
                Just mouseCanvasPosition_ ->
                    UndoModel.moveSelection
                        (DragState.getOffset
                            (SnapSettings.getToGrid model.snapSettings)
                            (viewToWorld undoModel mouseCanvasPosition_)
                            model.dragState
                        )
                        undoModel.selection
                        circuit

                Nothing ->
                    circuit

        _ ->
            circuit


drawCanvas : Model -> Element msg
drawCanvas model =
    let
        circuit =
            viewCircuit model

        undoModel =
            UndoList.present model

        mouseWorldPosition =
            model.mouseCanvasPosition |> Maybe.map (viewToWorld undoModel)

        toolSvg =
            case undoModel.tool of
                HandTool ->
                    Hand.draw undoModel.viewZoom mouseWorldPosition model

                MoveTool ->
                    Svg.g [] []

                ChipTool ->
                    drawChipTool model.mouseCanvasPosition undoModel model

                WireTool wirePlacer ->
                    WirePlacer.view
                        undoModel.viewZoom
                        model.snapSettings
                        circuit
                        mouseWorldPosition
                        wirePlacer

                PlayerStartTool ->
                    drawPlayerStartTool Color.black circuit model

                AgentTool agentType ->
                    case agentType of
                        ItemPassiveAgent ->
                            drawPlayerStartTool (Color.rgb 0.6 0.6 0.6) circuit model

                        ItemChaserAgent ->
                            drawPlayerStartTool (Color.rgb 1 0.3 0.3) circuit model

                TriggerTool ->
                    drawTriggerTool model.mouseCanvasPosition model

        agentStartColor selected agentStartType =
            case ( selected, agentStartType ) of
                ( False, PassiveAgentStart _ ) ->
                    Color.rgb 0.3 0.3 0.3

                ( False, ChaserAgentStart _ ) ->
                    Color.rgb 0.7 0 0

                ( True, PassiveAgentStart _ ) ->
                    Color.rgb 0.6 0.6 0.6

                ( True, ChaserAgentStart _ ) ->
                    Color.rgb 1 0.3 0.3

        playerStartColor selected =
            if selected then
                Color.rgb 0.8 0.8 0.3

            else
                Color.rgb 0.6 0.5 0

        agentSelected : AgentStartId -> Bool
        agentSelected agentStartId =
            case undoModel.tool of
                HandTool ->
                    Selection.agentSelected undoModel.selection agentStartId

                MoveTool ->
                    False

                _ ->
                    False

        playerSelected =
            case undoModel.tool of
                HandTool ->
                    undoModel.selection.player

                MoveTool ->
                    undoModel.selection.player

                _ ->
                    False

        triggers =
            if undoModel.showTriggers then
                Circuit.getTriggers circuit |> List.map (drawTrigger model)

            else
                []

        guidelines =
            if model.showGuidelines then
                case mouseWorldPosition of
                    Just mouseWorldPos ->
                        let
                            ( x, y ) =
                                if SnapSettings.getToGrid model.snapSettings then
                                    Point2i.roundFrom mouseWorldPos |> Point2i.toPoint2d |> Point2d.coordinates

                                else
                                    mouseWorldPos |> Point2d.coordinates

                            width =
                                1 / undoModel.viewZoom

                            color =
                                Color.rgba 0 0 0 0.5
                        in
                        [ Draw.lineStrip color width [ Point2d.fromCoordinates ( x, -10000 ), Point2d.fromCoordinates ( x, 10000 ) ]
                        , Draw.lineStrip color width [ Point2d.fromCoordinates ( -10000, y ), Point2d.fromCoordinates ( 10000, y ) ]
                        ]

                    Nothing ->
                        []

            else
                []

        snapPoint =
            case ( undoModel.tool, mouseWorldPosition ) of
                ( WireTool wirePlacer, Just mousePos ) ->
                    WirePlacer.getSnapPoint
                        undoModel.viewZoom
                        model.snapSettings
                        circuit
                        wirePlacer
                        mousePos
                        |> Just

                _ ->
                    Nothing

        snapChipId chipId =
            case snapPoint of
                Just (WirePlacer.ChipEdge chipEdge) ->
                    chipId == chipEdge.chipId

                _ ->
                    False

        wireIsHighlighted wireId =
            case snapPoint of
                Just (WirePlacer.WireEndpoint wireEndpoint) ->
                    wireId == wireEndpoint.wireId

                _ ->
                    case ( undoModel.tool, mouseWorldPosition, Circuit.getWire circuit wireId ) of
                        ( ChipTool, Just mousePosition, Just wire ) ->
                            chipFromDrag model mousePosition |> Maybe.andThen (UndoModel.detachedWireSnapsToChip wire) |> (/=) Nothing

                        ( MoveTool, _, Just wire ) ->
                            List.any
                                (Circuit.getChip circuit >> Maybe.andThen (UndoModel.detachedWireSnapsToChip wire) >> (/=) Nothing)
                                undoModel.selection.chips

                        _ ->
                            False

        chips =
            Circuit.getChips circuit
                |> List.map
                    (\( chipId, chip ) ->
                        drawChip model chipId chip (showChipSelected model chipId || snapChipId chipId)
                    )

        wires =
            Circuit.getWires circuit
                |> List.map
                    (\( wireId, wire ) ->
                        drawWire
                            undoModel.viewZoom
                            (if wireIsHighlighted wireId then
                                always True

                             else
                                \index -> Set.member index (wireSelectedNodes model wireId)
                            )
                            circuit
                            wire
                    )

        agents : List (Svg msg)
        agents =
            Editor.Circuit.getAgents circuit
                |> AssocList.toList
                |> List.map
                    (\( id, agentStart ) ->
                        let
                            color =
                                agentStartColor (agentSelected id) agentStart
                        in
                        Circuit.getAgentStartPosition agentStart
                            |> drawAgentStart color circuit model
                    )

        player =
            circuit.playerStart
                |> Maybe.map (drawAgentStart (playerStartColor playerSelected) circuit model)
                |> Maybe.toList

        drawings =
            Svg.g
                [ Svg.Attributes.overflow "visible" ]
                (chips ++ wires ++ agents ++ player ++ triggers ++ guidelines ++ [ toolSvg ])
                |> Svg.translateBy (Vector2d.from undoModel.viewWorldPosition Point2d.origin)
                |> Svg.scaleAbout (Point2d.fromCoordinates ( 0, 0 )) undoModel.viewZoom

        mouseCoordinate =
            case model.mouseCanvasPosition of
                Just mousePos ->
                    let
                        ( x, y ) =
                            mousePos |> viewToWorld undoModel |> Point2d.coordinates
                    in
                    Element.el
                        [ Element.alignTop
                        , Font.color (Element.rgb 0.5 0.5 0.5)
                        , Element.padding 5
                        , Element.alignRight
                        , Element.htmlAttribute <| Html.Attributes.style "pointer-events" "none"
                        ]
                        (Element.text (Round.round 1 x ++ ", " ++ Round.round 1 y))

                Nothing ->
                    Element.none
    in
    Svg.svg
        [ style "width" "100%"
        , style "height" "100%"
        , style "pointer-events" "none"
        ]
        (drawings :: drawBackground (UndoList.present model))
        |> Element.html
        |> Element.el
            [ Element.inFront mouseCoordinate
            , Element.width Element.fill
            , Element.height Element.fill
            ]


view : Config a -> Model -> Element Msg
view config model =
    case model.inGame of
        Just inGameModel ->
            Element.el
                [ Element.width Element.fill
                , Element.height Element.fill
                ]
                (InGame.view
                    inGameMsgConfig
                    config
                    False
                    Nothing
                    (Input.fromKeyboard config)
                    True
                    True
                    InGame.PlayerControlledMap
                    inGameModel
                )

        Nothing ->
            Element.column
                [ Element.height Element.fill
                , Element.width Element.fill
                , Element.inFront <|
                    case model.showModal of
                        Just modal ->
                            Element.el
                                [ Element.width Element.fill
                                , Element.height Element.fill
                                , Element.htmlAttribute <| Html.Attributes.style "z-index" "3"
                                , Background.color <| Helper.colorToElement Style.menuBackgroundColor
                                ]
                                (case modal of
                                    OpenUnlockedStageModal ->
                                        importUnlockedStageModal config.highscoreState

                                    OpenUnlockedStageConfirmationModal circuit ->
                                        confirmationModal
                                            UserPressedCancelInModal
                                            (UserPressedOkayInUnlockedStageConfirmationModal circuit)
                                            "Are you sure you want to open this stage? You'll lose any unsaved work on the current stage."

                                    NewStageConfirmationModal ->
                                        confirmationModal
                                            UserPressedCancelInModal
                                            UserPressedOkayInNewModal
                                            "Are you sure you want to create a new stage? You'll lose any unsaved work on the current stage."

                                    OpenFileConfirmationModal circuit ->
                                        confirmationModal
                                            UserPressedCancelInModal
                                            (UserPressedOkayInUnlockedStageConfirmationModal circuit)
                                            "Are you sure you want to open this stage? You'll lose any unsaved work on the current stage."

                                    OpenFileFailedModal ->
                                        errorModal "Failed to open this stage. The file is corrupted."
                                )

                        Nothing ->
                            Element.none
                ]
                [ Element.el
                    [ Element.htmlAttribute <| Html.Attributes.style "z-index" "2" --Place this element in front of the side panel
                    , Element.width Element.fill
                    , Element.height Element.fill
                    , Border.shadow { offset = ( 0, 0 ), size = 0, blur = 6, color = Element.rgba 0 0 0 0.2 }
                    ]
                    (topView config model)
                , Element.row
                    [ Element.height Element.fill
                    , Element.width Element.fill
                    ]
                    [ Element.el
                        [ Element.htmlAttribute <| Html.Attributes.style "z-index" "1" --Place this element in front of the canvas panel
                        , Border.shadow { offset = ( 0, 0 ), size = 0, blur = 6, color = Element.rgba 0 0 0 0.2 }
                        ]
                        (sidePanelView (config.windowSize.height - topViewHeight) model)
                    , Element.el
                        [ Html.Attributes.id editorCanvasName |> Element.htmlAttribute
                        , Events.onMouseLeave MouseLeave
                        , Mouse.onDown (Helper.handleMouseEvent MouseDown)
                            |> Element.htmlAttribute
                        , Mouse.onUp (Helper.handleMouseEvent MouseUp)
                            |> Element.htmlAttribute
                        , Mouse.onClick (Helper.handleMouseEvent MouseClick)
                            |> Element.htmlAttribute
                        , Mouse.onMove (Helper.handleMouseEvent MouseMove)
                            |> Element.htmlAttribute
                        , Wheel.onWheel (.deltaY >> Zoom) |> Element.htmlAttribute
                        , Element.width Element.fill
                        , Element.height Element.fill
                        ]
                        (drawCanvas model)
                    ]
                ]


fileButtonStyle : Bool -> List (Element.Attribute msg)
fileButtonStyle enabled =
    [ Background.color <| Element.rgba 0 0 0 0.2
    , Element.mouseOver [ Background.color <| Element.rgba 0 0 0 0.1 ]
    , Element.paddingXY 10 6
    ]
        ++ (if enabled then
                [ Font.color <| Element.rgb 0 0 0
                ]

            else
                [ Font.color <| Element.rgba 0 0 0 0.35
                , Element.htmlAttribute (Html.Attributes.style "pointer-events" "none")
                ]
           )


listRowButtonStyle : Bool -> List (Element.Attribute msg)
listRowButtonStyle enabled =
    [ Background.color <| Element.rgba 0 0 0 0.2
    , Element.mouseOver [ Background.color <| Element.rgba 0 0 0 0.1 ]
    , Element.padding 5
    , Element.width Element.fill
    ]
        ++ (if enabled then
                [ Font.color <| Element.rgb 0 0 0
                ]

            else
                [ Font.color <| Element.rgba 0 0 0 0.35
                , Element.htmlAttribute (Html.Attributes.style "pointer-events" "none")
                ]
           )


canCopyToClipboard : Model -> Bool
canCopyToClipboard model =
    Selection.isEmpty (UndoList.present model).selection |> not


canPasteFromClipboard : Model -> Bool
canPasteFromClipboard model =
    model.clipboard /= Nothing


infoView : Circuit -> Element msg
infoView circuit =
    let
        agentList =
            Editor.Circuit.getAgents circuit |> AssocList.toList
    in
    Element.table
        [ Element.width Element.fill, Element.paddingXY 4 0 ]
        { data =
            [ { name = "Wires", value = circuit.wires |> Dict.size |> String.fromInt }
            , { name = "Chips", value = circuit.chips |> Dict.size |> String.fromInt }
            , { name = "Passive Agents"
              , value =
                    agentList
                        |> List.count
                            (\( _, agent ) ->
                                case agent of
                                    PassiveAgentStart _ ->
                                        True

                                    ChaserAgentStart _ ->
                                        False
                            )
                        |> String.fromInt
              }
            , { name = "Chaser Agents"
              , value =
                    agentList
                        |> List.count
                            (\( _, agent ) ->
                                case agent of
                                    PassiveAgentStart _ ->
                                        False

                                    ChaserAgentStart _ ->
                                        True
                            )
                        |> String.fromInt
              }
            ]
        , columns =
            [ { header = Element.none
              , width = Element.shrink
              , view = \row -> Element.text row.name
              }
            , { header = Element.none
              , width = Element.fill
              , view = \row -> Element.el [ Font.alignRight, Element.paddingXY 0 2 ] (Element.text row.value)
              }
            ]
        }


topViewHeight =
    40


topView : Config a -> Model -> Element Msg
topView config model =
    Element.row
        [ Element.width Element.fill
        , Background.color panelBackgroundColor
        , Element.height <| Element.px topViewHeight
        , Element.spacing 4
        , Element.padding 4
        ]
        [ Input.button
            (fileButtonStyle True)
            { onPress = Just GotoMainMenu
            , label = Element.text "Back to menu"
            }
        , Element.el [ Element.paddingXY 4 0, Element.height Element.fill ] vSplit
        , Input.button
            (fileButtonStyle True)
            { onPress = Just ExportStage
            , label = Element.text "Download"
            }
        , Input.button
            (fileButtonStyle True)
            { onPress = Just ImportStage
            , label = Element.text "Open from file"
            }
        , Input.button
            (fileButtonStyle True)
            { onPress = Just ImportInternalStage
            , label = Element.text "Open completed stages"
            }
        , Input.button
            (fileButtonStyle True)
            { onPress = Just UserPressedNewButton
            , label = Element.text "New"
            }
        , Element.el [ Element.paddingXY 4 0, Element.height Element.fill ] vSplit
        , Input.button
            (fileButtonStyle True)
            { onPress = Just Play
            , label = Element.row [] [ underlineText "P", Element.text "lay stage" ]
            }
        , Element.el [ Element.alignRight, Element.padding 4 ] <| errorMessageView config.time model.stageError
        ]


errorMessageView : Quantity Float RealTime -> Maybe StageError -> Element msg
errorMessageView time maybeError =
    case maybeError of
        Just { error, startTime } ->
            let
                alpha =
                    Keyframe.startAt_ startTime 0.3
                        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.15) 1
                        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.15) 0.3
                        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.15) 1
                        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 0.15) 0.3
                        |> Keyframe.add_ Keyframe.Constant (RealTime.seconds 8) 1
                        |> Keyframe.add_ Keyframe.Linear (RealTime.seconds 1) 0
                        |> Keyframe.valueAt_ time
            in
            Element.el [ Font.color <| Element.rgb 1 0 0, Element.alpha alpha ] <|
                Element.text <|
                    case error of
                        AtLeastOneChip ->
                            "You need to place at least one chip."

                        MissingPlayerStart ->
                            "You need a starting position for the player."

        Nothing ->
            Element.none


nameTextboxId =
    "editorNameTextbox"


sidePanelView : Int -> Model -> Element Msg
sidePanelView height model =
    let
        spacing =
            Helper.spacing 1

        columnCount =
            Helper.spacing 4 * 2 + spacing

        fileNameView =
            Input.text
                [ Element.padding 4
                , Element.htmlAttribute <| Html.Events.onFocus (NameTextboxFocusChanged True)
                , Element.htmlAttribute <| Html.Events.onBlur (NameTextboxFocusChanged False)
                , Element.htmlAttribute <| Html.Attributes.id nameTextboxId
                ]
                { onChange = UserTypedFileName
                , text = model.fileName
                , placeholder = Nothing
                , label = Input.labelLeft [ Element.centerY ] (Element.text "Name")
                }

        undoRedo =
            Element.row
                [ Element.width Element.fill
                , Element.spacing 5
                , Font.center
                ]
                [ Input.button
                    (Element.width Element.fill :: fileButtonStyle (UndoList.hasPast model.undoList))
                    { onPress = Just Undo
                    , label = Element.text "Undo"
                    }
                , Input.button
                    (Element.width Element.fill :: Element.alignRight :: fileButtonStyle (UndoList.hasFuture model.undoList))
                    { onPress = Just Redo
                    , label = Element.text "Redo"
                    }
                ]

        copyPaste =
            Element.row
                [ Element.width Element.fill
                , Element.spacing 5
                , Font.center
                ]
                [ Input.button
                    (Element.width Element.fill :: fileButtonStyle (canCopyToClipboard model))
                    { onPress = Just CopyToClipboard
                    , label = Element.text "Copy"
                    }
                , Input.button
                    (Element.width Element.fill
                        :: Element.alignRight
                        :: fileButtonStyle (canPasteFromClipboard model)
                    )
                    { onPress = Just PasteFromClipboard
                    , label = Element.text "Paste"
                    }
                ]

        toolButtons =
            List.map
                (\tool ->
                    Input.button
                        (toolButtonStyle (UndoModel.toolSelected tool (UndoList.present model)))
                        { onPress = Just <| SelectTool tool
                        , label = Element.el [ Element.centerX, Element.centerY ] (toolIcon tool)
                        }
                )
                tools

        missions =
            List.Nonempty.Nonempty
                Mission.tutorialMissionInit
                [ NormalMission
                , CustomMission
                ]

        missionSelect =
            Element.el
                [ Element.width Element.fill ]
                (Dropdown.view
                    SetMission
                    model.mission
                    missions
                    missionName
                )
    in
    Element.el
        [ Element.padding <| Helper.spacing 1
        , Background.color panelBackgroundColor
        , Element.width <| Element.px 250
        , Element.height <| Element.px height
        , Element.scrollbarY
        ]
        (Element.el [] <|
            Element.column
                [ Element.width Element.fill
                , Element.spacing <| Helper.spacing 1
                ]
                [ fileNameView
                , if Environment.isDebug then
                    missionSelect

                  else
                    Element.none
                , hSplit
                , undoRedo
                , copyPaste
                , hSplit
                , Element.wrappedRow
                    [ Element.spacing 6
                    , Element.width <| px columnCount
                    , Element.centerX
                    ]
                    toolButtons
                , toolView model
                , hSplit
                , globalOptionsView model.snapSettings (UndoList.present model).showTriggers model.showGuidelines
                , hSplit
                , infoView (UndoList.present model).circuit
                ]
        )


missionName mission =
    case mission of
        TutorialMission _ ->
            "Tutorial"

        NormalMission ->
            "Normal"

        CustomMission ->
            "Custom"

        MenuMission _ ->
            "Menu mission"


panelBackgroundColor =
    Element.rgb 0.75 0.95 0.75


hSplit =
    Style.hSplit (Element.rgba 0 0 0 0.2)


vSplit =
    Element.el
        [ Border.color <| Element.rgba 0 0 0 0.2
        , Border.width 1
        , Element.height Element.fill
        ]
        Element.none


infoFontSize =
    Font.size 16


toolView : Model -> Element Msg
toolView model =
    case (UndoList.present model).tool of
        HandTool ->
            Element.column
                [ Element.spacing 8, infoFontSize ]
                [ Element.text "Click and drag to select"
                , Element.text "Hold shift to multiselect"
                , Element.text "Delete with delete key"
                ]

        MoveTool ->
            Element.none

        ChipTool ->
            Element.el [ infoFontSize ] <| Element.text "Click and drag to place"

        WireTool _ ->
            Element.column
                [ Element.spacing 12 ]
                [ Input.checkbox
                    [ Element.width Element.fill ]
                    { onChange = always ToggleSnapToAngle
                    , icon = checkboxIcon
                    , checked = SnapSettings.getToAngle model.snapSettings
                    , label =
                        Input.labelRight [ Element.width Element.fill ] <|
                            Element.paragraph
                                []
                                [ Element.text "Snap to angle" ]
                    }
                , Input.checkbox
                    [ Element.width Element.fill ]
                    { onChange = always ToggleSnapAlongWire
                    , icon = checkboxIcon
                    , checked = SnapSettings.getAlongWire model.snapSettings
                    , label =
                        Input.labelRight [ Element.width Element.fill ] <|
                            Element.paragraph
                                []
                                [ Element.text "Snap along nearby wires" ]
                    }
                ]

        PlayerStartTool ->
            Element.el [ infoFontSize ] <| Element.text "Rotate with spacebar"

        AgentTool _ ->
            Element.el [ infoFontSize ] <| Element.text "Rotate with spacebar"

        TriggerTool ->
            case (UndoList.present model).selectedTrigger of
                Just { editedName } ->
                    Input.text
                        []
                        { onChange = SetTriggerName
                        , text = editedName
                        , placeholder = Nothing
                        , label = Input.labelLeft [ Element.centerY ] <| Element.text "Name"
                        }

                Nothing ->
                    Element.none


importInternalStage : Maybe Int -> StageData -> Element Msg
importInternalStage stageNumber stageData =
    let
        name =
            case stageNumber of
                Just stageNumber_ ->
                    (Hex.toString stageNumber_ |> String.toUpper) ++ ". " ++ stageData.name

                Nothing ->
                    stageData.name
    in
    case stageData.circuit of
        StageData.Data circuit _ ->
            Input.button
                (listRowButtonStyle True)
                { onPress = UserPressedStageInOpenUnlockedStageList circuit |> Just
                , label = Element.text name
                }

        StageData.FailedToParse ->
            Input.button
                (listRowButtonStyle False)
                { onPress = Nothing
                , label =
                    Element.row
                        [ Element.width Element.fill
                        , Element.spacing 5
                        ]
                        [ Element.el
                            [ Font.color (Element.rgb 0 0 0) ]
                            (Element.text name)
                        , Element.el
                            [ Font.color (Element.rgb 1 0 0) ]
                            (Element.text "(Failed to parse)")
                        ]
                }


isStageUnlocked highscores stageId =
    let
        allPlayableStagesUnlocked =
            StageData.playableStageOrder
                |> List.Nonempty.all (\stageId_ -> HighscoreTable.stageStatus stageId_ highscores == HighscoreTable.StageCompleted)
    in
    if Environment.isDebug then
        True

    else if stageId == StageData.menuStageId then
        allPlayableStagesUnlocked

    else if HighscoreTable.stageStatus stageId highscores == HighscoreTable.StageCompleted then
        True

    else if StageData.playableStageOrder |> List.Nonempty.any ((==) stageId) |> not then
        True

    else
        False


getUnlockedStages : HighscoreTable.Model -> List (Element Msg)
getUnlockedStages highscores =
    StageData.editorStages
        |> List.Nonempty.toList
        |> List.filterMap
            (\stageId ->
                case ( isStageUnlocked highscores stageId, AssocList.get stageId StageData.allStages ) of
                    ( True, Just stage ) ->
                        case StageData.playableStageOrder |> List.Nonempty.toList |> List.findIndex ((==) stageId) of
                            Just stageIndex ->
                                importInternalStage (Just stageIndex) stage |> Just

                            Nothing ->
                                importInternalStage Nothing stage |> Just

                    _ ->
                        Nothing
            )


importUnlockedStageModal highscores =
    let
        stages =
            getUnlockedStages highscores
                |> Element.column
                    [ Element.width Element.fill
                    , Element.height Element.fill
                    , Element.spacing 1
                    ]

        title =
            Element.el [ Element.centerX ] (Element.text "Open Completed Stages")

        cancelButton =
            Input.button
                (fileButtonStyle True)
                { onPress = Just UserPressedCancelInModal
                , label = Element.text "Cancel"
                }
    in
    Element.column
        [ Element.centerX
        , Element.centerY
        , Background.color panelBackgroundColor
        , Element.padding 10
        , Element.spacing 10
        , Element.width <| Element.maximum 300 Element.shrink
        ]
        [ title, stages, hSplit, cancelButton ]


confirmationModal : msg -> msg -> String -> Element msg
confirmationModal cancelMsg okMsg text =
    Element.column
        [ Element.centerX
        , Element.centerY
        , Background.color panelBackgroundColor
        , Element.padding 10
        , Element.spacing 10
        , Element.width <| Element.maximum 300 Element.shrink
        ]
        [ Element.paragraph [] [ Element.text text ]
        , Element.row
            [ Element.width Element.fill ]
            [ Input.button
                (fileButtonStyle True)
                { onPress = Just cancelMsg
                , label = Element.text "Cancel"
                }
            , Input.button
                (Element.alignRight :: fileButtonStyle True)
                { onPress = Just okMsg
                , label = Element.text "Okay"
                }
            ]
        ]


errorModal : String -> Element Msg
errorModal text =
    Element.column
        [ Element.centerX
        , Element.centerY
        , Background.color panelBackgroundColor
        , Element.padding 10
        , Element.spacing 10
        , Element.width <| Element.maximum 300 Element.shrink
        ]
        [ Element.paragraph [] [ Element.text <| "⚠️ " ++ text ]
        , Input.button
            (Element.centerX :: fileButtonStyle True)
            { onPress = Just UserPressedOkayInErrorModal
            , label = Element.text "Okay"
            }
        ]


checkboxIcon =
    Style.checkboxIcon Color.black


globalOptionsView snapSettings showTriggers showGuidelines =
    Element.column
        [ Element.spacing <| Helper.spacing 1 ]
        [ Input.checkbox
            []
            { onChange = always ToggleSnapToGrid
            , icon = checkboxIcon
            , checked = SnapSettings.getToGrid snapSettings
            , label = Input.labelRight [] <| Element.text "Snap to grid"
            }
        , Input.checkbox
            [ Element.width Element.fill ]
            { onChange = always ToggleShowGuidelines
            , icon = checkboxIcon
            , checked = showGuidelines
            , label =
                Input.labelRight [ Element.width Element.fill, Element.height Element.fill, Element.centerY ] <|
                    Element.paragraph
                        []
                        [ Element.text "Show guidelines" ]
            }
        , if Environment.isDebug then
            Input.checkbox
                [ Element.width Element.fill ]
                { onChange = SetShowTriggers
                , icon = checkboxIcon
                , checked = showTriggers
                , label =
                    Input.labelRight [ Element.width Element.fill ] <|
                        Element.paragraph
                            []
                            [ Element.text "Show triggers" ]
                }

          else
            Element.none
        ]


editorCanvasName : String
editorCanvasName =
    "editorCanvas"


viewToWorld : UndoModel -> Point2d -> Point2d
viewToWorld canvasView viewCoordinate =
    let
        ( x, y ) =
            Point2d.coordinates viewCoordinate

        ( cx, cy ) =
            Point2d.coordinates canvasView.viewWorldPosition
    in
    Point2d.fromCoordinates ( (x / canvasView.viewZoom) + cx, (y / canvasView.viewZoom) + cy )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
