module InGameTime exposing (InGameTime(..), inMinutes, inSeconds, seconds)

import Quantity exposing (Quantity)


type InGameTime
    = InGameTime Never


seconds : Float -> Quantity Float InGameTime
seconds value =
    Quantity.Quantity value


inSeconds : Quantity number InGameTime -> number
inSeconds (Quantity.Quantity time) =
    time


inMinutes : Quantity Float InGameTime -> Float
inMinutes value =
    inSeconds value |> (*) (1 / 60)
