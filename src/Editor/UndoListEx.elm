module Editor.UndoListEx exposing (..)

import Basics.Extra exposing (flip)
import Editor.Circuit exposing (Circuit)
import Editor.UndoModel as UndoModel exposing (UndoModel)
import UndoList exposing (UndoList)


type alias Model a =
    { a | undoList : UndoList UndoModel }


present : { a | undoList : UndoList b } -> b
present model =
    model.undoList.present


updateUndoModel : (UndoModel -> Maybe UndoModel) -> Model a -> Model a
updateUndoModel updateFunc model =
    case updateFunc (present model) of
        Just newUndo ->
            { model | undoList = UndoList.new newUndo model.undoList }

        Nothing ->
            model


updateCircuit : (Circuit -> Maybe Circuit) -> Model a -> Model a
updateCircuit updateFunc model =
    updateUndoModel
        (\undoModel -> updateFunc undoModel.circuit |> Maybe.map (flip UndoModel.setCircuit undoModel))
        model


skippable : (UndoModel -> UndoModel) -> Model a -> Model a
skippable updateFunc model =
    { model | undoList = UndoList.mapPresent updateFunc model.undoList }
