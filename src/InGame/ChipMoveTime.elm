module InGame.ChipMoveTime exposing (..)

import InGame.CycleTime as CycleTime exposing (CycleTime)
import InGameTime exposing (InGameTime)
import Quantity exposing (Quantity)


type alias ChipMoveTime =
    { moveTime : Quantity Float InGameTime, relativeToCycle : Quantity Float CycleTime }


chipMoveTiming : ChipMoveTime -> ChipMoveTiming
chipMoveTiming chipMoveTime =
    if chipMoveTime.relativeToCycle |> Quantity.lessThanOrEqualTo (InGameTime.inSeconds moveEarlyDuration |> CycleTime.seconds) then
        Early

    else if chipMoveTime.relativeToCycle |> Quantity.greaterThanOrEqualTo (InGameTime.inSeconds cycleMaxLength |> CycleTime.seconds) then
        Late

    else
        OnTime


type ChipMoveTiming
    = Early
    | OnTime
    | Late


moveEarlyDuration : Quantity Float InGameTime
moveEarlyDuration =
    chipTransitionDuration


moveLateDuration : Quantity Float InGameTime
moveLateDuration =
    cycleMaxLength


{-| How long it takes for the player and agents to move from their previous chip position to the next one.
Movement starts at the beginning of a cycle.
-}
chipTransitionDuration : Quantity Float InGameTime
chipTransitionDuration =
    InGameTime.seconds 0.3


cycleMaxLength : Quantity Float InGameTime
cycleMaxLength =
    InGameTime.seconds 0.6
