module ArrayExTests exposing (..)

import Array exposing (Array)
import ArrayEx as Array
import Expect
import Fuzz
import Test exposing (Test, describe, fuzz, fuzz2, fuzz3, only, test)


slowFindIndex : Bool -> comparable -> Array ( comparable, a ) -> Int
slowFindIndex roundDown value array =
    let
        getIndex func compareFunc index =
            case Array.get (func index) array of
                Just arrayValue ->
                    if compareFunc (Tuple.first arrayValue) value then
                        getIndex func compareFunc (func index)

                    else
                        index

                Nothing ->
                    index
    in
    if roundDown then
        getIndex ((+) 1) (<) -1

    else
        getIndex ((+) -1) (>) (Array.length array)


findIndexTest roundDown =
    \value head list ->
        let
            sortedArray =
                head :: list |> List.sort |> List.map (\a -> ( a, () )) |> Array.fromList
        in
        Array.findIndex roundDown value sortedArray
            |> Expect.equal (slowFindIndex roundDown value sortedArray)


all : Test
all =
    describe "ArrayEx"
        [ fuzz3 (Fuzz.intRange 0 7) (Fuzz.intRange 1 6) (Fuzz.list (Fuzz.intRange 1 6)) "findIndex round down" <|
            findIndexTest True
        , fuzz3 (Fuzz.intRange 0 7) (Fuzz.intRange 1 6) (Fuzz.list (Fuzz.intRange 1 6)) "findIndex round up" <|
            findIndexTest False
        , test "findIndex empty" <|
            \_ -> Array.findIndex True 0 Array.empty |> Expect.equal 0
        ]
