module Vector2i exposing
    ( Vector2i
    , aspectRatio
    , components
    , dotProduct
    , from
    , length
    , map
    , map2
    , mirrorX
    , mirrorY
    , mult
    , roundFrom
    , scaleBy
    , sub
    , sum
    , toPoint
    , toVector2d
    , xy
    , zero
    )

import Vector2d exposing (Vector2d)


type alias Vector2i =
    { width : Int, height : Int }


type alias Point2i =
    { x : Int, y : Int }


from : Point2i -> Point2i -> Vector2i
from start end =
    { width = end.x - start.x, height = end.y - start.y }


xy : ( Int, Int ) -> Vector2i
xy ( x, y ) =
    { width = x, height = y }


zero : Vector2i
zero =
    { width = 0, height = 0 }


sum : Vector2i -> Vector2i -> Vector2i
sum pointA pointB =
    { width = pointA.width + pointB.width, height = pointA.height + pointB.height }


sub : Vector2i -> Vector2i -> Vector2i
sub pointA pointB =
    { width = pointA.width - pointB.width, height = pointA.height - pointB.height }


mult : Vector2i -> Vector2i -> Vector2i
mult pointA pointB =
    { width = pointA.width * pointB.width, height = pointA.height * pointB.height }


map : (Int -> Int) -> Vector2i -> Vector2i
map mapFunc point =
    { width = mapFunc point.width, height = mapFunc point.height }


map2 : (Int -> Int -> Int) -> Vector2i -> Vector2i -> Vector2i
map2 mapFunc point0 point1 =
    { width = mapFunc point0.width point1.width, height = mapFunc point0.height point1.height }


mirrorY : Vector2i -> Vector2i
mirrorY point =
    { width = point.width, height = -point.height }


mirrorX : Vector2i -> Vector2i
mirrorX point =
    { width = -point.width, height = point.height }


toPoint : Vector2i -> { x : Int, y : Int }
toPoint { width, height } =
    { x = width, y = height }


toVector2d : Vector2i -> Vector2d
toVector2d size =
    Vector2d.fromComponents ( toFloat size.width, toFloat size.height )


roundFrom : Vector2d -> Vector2i
roundFrom v =
    let
        ( x, y ) =
            Vector2d.components v
    in
    { width = Basics.round x, height = Basics.round y }


{-| Width divided by height.
-}
aspectRatio : Vector2i -> Maybe Float
aspectRatio size =
    if size.height == 0 then
        Nothing

    else
        toFloat size.width / toFloat size.height |> Just


scaleBy : Int -> Vector2i -> Vector2i
scaleBy scalar v =
    { width = v.width * scalar, height = v.height * scalar }


length : Vector2i -> Float
length v =
    v.width ^ 2 + v.height ^ 2 |> toFloat |> sqrt


{-| Extract the components of a vector.

    Vector2d.components (Vector2d.fromComponents ( 2, 3 ))
    --> ( 2, 3 )

This combined with Elm's built-in tuple destructuring provides a convenient way
to extract both the X and Y components of a vector in one line of code:

    ( x, y ) =
        Vector2d.components vector

-}
components : Vector2i -> ( Int, Int )
components { width, height } =
    ( width, height )


{-| Find the dot product of two vectors.

    firstVector =
        Vector2d.fromComponents ( 1, 2 )

    secondVector =
        Vector2d.fromComponents ( 3, 4 )

    Vector2d.dotProduct firstVector secondVector
    --> 11

-}
dotProduct : Vector2i -> Vector2i -> Int
dotProduct firstVector secondVector =
    let
        ( x1, y1 ) =
            components firstVector

        ( x2, y2 ) =
            components secondVector
    in
    x1 * x2 + y1 * y2
