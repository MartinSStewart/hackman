module DebugHelper exposing (logFunc, logFunc2, logFunc3, logFunc4, logMap)

import Environment


logMap : (a -> b) -> a -> a
logMap map item =
    let
        _ =
            map item |> Environment.toString |> Environment.log ""
    in
    item


logFunc : String -> (a -> b) -> a -> b
logFunc functionName function param1 =
    let
        result =
            function param1

        param1Text =
            Environment.toString param1

        resultText =
            Environment.toString result

        _ =
            Environment.log
                functionName
                ("\nparam1: " ++ param1Text ++ "\nresult: " ++ resultText)
    in
    result


logFunc2 : String -> (a -> b -> c) -> a -> b -> c
logFunc2 functionName function param1 param2 =
    let
        result =
            function param1 param2

        paramsText =
            [ Environment.toString param1
            , Environment.toString param2
            ]
                |> List.indexedMap
                    (\index text ->
                        "Param"
                            ++ String.fromInt index
                            ++ ": "
                            ++ text
                            ++ "  "
                    )
                |> String.concat

        _ =
            Environment.log
                functionName
                (paramsText ++ "result: " ++ Environment.toString result)
    in
    result


logFunc3 : String -> (a -> b -> c -> d) -> a -> b -> c -> d
logFunc3 functionName function param1 param2 param3 =
    let
        result =
            function param1 param2 param3

        paramsText =
            [ Environment.toString param1
            , Environment.toString param2
            , Environment.toString param3
            ]
                |> List.indexedMap
                    (\index text ->
                        "Param"
                            ++ String.fromInt index
                            ++ ": "
                            ++ text
                            ++ "  "
                    )
                |> String.concat

        _ =
            Environment.log
                functionName
                (paramsText ++ "result: " ++ Environment.toString result)
    in
    result


logFunc4 : String -> (a -> b -> c -> d -> e) -> a -> b -> c -> d -> e
logFunc4 functionName function param1 param2 param3 param4 =
    let
        result =
            function param1 param2 param3 param4

        paramsText =
            [ Environment.toString param1
            , Environment.toString param2
            , Environment.toString param3
            , Environment.toString param4
            ]
                |> List.indexedMap
                    (\index text ->
                        "Param"
                            ++ String.fromInt index
                            ++ ": "
                            ++ text
                            ++ "  "
                    )
                |> String.concat

        _ =
            Environment.log
                functionName
                (paramsText ++ "result: " ++ Environment.toString result)
    in
    result
