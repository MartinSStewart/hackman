module Main exposing (main)

import Agent
import AssocList
import Basics.Extra exposing (flip)
import Browser
import Browser.Dom
import Browser.Events
import Changelog
import Codec.Serialize as Codec exposing (Codec)
import ColorPalette exposing (ColorPalette)
import ColorPicker
import ColorSettingsPage
import DateFormat
import Debounce
import Dict exposing (Dict)
import Editor
import Either
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Environment
import EverySet
import File
import File.Download
import File.Select
import Helper exposing (addCmd, addCmdNone, ifElse)
import HighscoreTable exposing (StageStatus(..))
import Html exposing (Html)
import Html.Attributes
import Html.Events.Extra.Mouse as Mouse
import Http
import InGame
import InGame.ChipMoveTime as ChipMoveTime
import InGame.Core as Core
import Input
import Json.Decode as JD
import Json.Encode as JE
import Jump
import Keyboard exposing (Key(..))
import KeyboardHelper as Keyboard
import List.Extra as List
import List.Nonempty
import MainMenu
import MenuState exposing (Game(..), MainMenuIntro(..), MainMenu_, Stage_)
import Mission
import Model exposing (AssetState(..), Flags, LoadingModel, LoadingMsg(..), Model, ModelBase(..), Msg(..), MsgBase(..))
import Point2d exposing (Point2d)
import Ports
import Quantity exposing (Quantity)
import Random
import RealTime exposing (RealTime)
import Replay exposing (CompleteReplay)
import ReplayViewer
import SaveData exposing (SaveData)
import Sound exposing (Sound, SoundId)
import SpriteSheet exposing (SpriteSheet)
import StageData exposing (ReplayData(..), StageData, StageId)
import StageSelect
import Task
import Time
import Url exposing (Url)
import UrlData exposing (UrlData)
import Vector2i exposing (Vector2i)
import WebGL.Texture as Texture exposing (Texture)


init : JE.Value -> ( ModelBase, Cmd MsgBase )
init flagsJson =
    case JD.decodeValue Model.decodeFlags flagsJson of
        Ok flags ->
            let
                url =
                    flags.url
            in
            ( Loading
                { assetState = NoneLoaded
                , spriteSheetError = Nothing
                , spriteSheetDataError = Nothing
                , sounds = Dict.empty
                , randomSeed = Random.initialSeed flags.seed
                , soundErrors = []
                , windowSize = { width = flags.windowWidth, height = flags.windowHeight }
                , time = Quantity.zero
                , saveData = flags.saveData
                , url = { url | query = Nothing, fragment = Nothing }
                , urlData = UrlData.fromUrl url
                , colorPaletteSource = Model.ColorPaletteFromSaveData
                }
            , Cmd.batch
                [ Sound.allSounds |> List.map Sound.load |> Cmd.batch
                , Texture.loadWith
                    { magnify = Texture.nearest
                    , minify = Texture.linear
                    , horizontalWrap = Texture.clampToEdge
                    , verticalWrap = Texture.clampToEdge
                    , flipY = False
                    }
                    "images/sprite_sheet.png"
                    |> Task.attempt (TextureLoaded >> LoadingMsg)
                ]
            )

        Err error ->
            ( InvalidFlags (JD.errorToString error), Cmd.none )


initModel : Sound.Model -> LoadingModel -> SpriteSheet -> Model
initModel soundData loadingModel spriteSheet =
    let
        saveData =
            case loadingModel.saveData of
                Ok saveData_ ->
                    saveData_

                Err _ ->
                    SaveData.defaultSaveData
    in
    { keys = []
    , previousKeys = []
    , randomSeed = loadingModel.randomSeed
    , gameState = MenuState.init loadingModel.time |> MainMenu
    , time = loadingModel.time
    , windowSize = loadingModel.windowSize
    , stepPaused = False
    , stageSelectId = List.Nonempty.head StageData.playableStageOrder
    , stepDelta = Quantity.zero
    , editorState =
        loadingModel.windowSize
            |> Vector2i.toVector2d
            |> Editor.init
    , portErrors = []
    , sounds =
        soundData
            |> Sound.setMasterVolume saveData.masterVolume
            |> Sound.setMusicVolume saveData.musicVolume
            |> Sound.setSoundEffectVolume saveData.soundEffectVolume
    , url = loadingModel.url
    , soundsPrevious = soundData
    , mouseDown = False
    , previousMouseDown = False
    , mousePosition = Point2d.origin
    , previousMousePosition = Point2d.origin
    , spriteSheet = spriteSheet
    , debounceSaveData = Debounce.init
    , antialias = saveData.antialias
    , highscoreState = HighscoreTable.init saveData.highscores
    , tutorialIntroShown = False
    , averageStepDelta = RealTime.seconds (1 / 60)
    , framerateWarningStatus = ifElse saveData.hideFramerateWarning Model.FramerateWarningHidden Model.GoodFramerate
    , stageUnlockAnimation = Model.NoStageUnlocked
    , colorPalette =
        case ( loadingModel.urlData, loadingModel.saveData ) of
            ( Ok urlData, Ok saveData_ ) ->
                if showOverwriteColorPalette saveData_ urlData && loadingModel.colorPaletteSource == Model.ColorPaletteFromSaveData then
                    saveData.colorPalette

                else
                    urlData.colorPalette

            ( Ok { colorPalette }, Err _ ) ->
                colorPalette

            ( _, _ ) ->
                saveData.colorPalette
    , debounceSetColorPalette = Debounce.init
    , timeDrift = Quantity.zero
    }


loadingUpdate : LoadingMsg -> LoadingModel -> ( ModelBase, Cmd MsgBase )
loadingUpdate msg model =
    case msg of
        SoundLoaded soundName result ->
            case result of
                Ok soundId ->
                    let
                        loading =
                            { model | sounds = Dict.insert soundName soundId model.sounds }
                    in
                    Loading loading |> addCmdNone

                Err error ->
                    Loading { model | soundErrors = error :: model.soundErrors } |> addCmdNone

        WindowResize windowSize ->
            { model | windowSize = windowSize } |> Loading |> addCmdNone

        TextureLoaded result ->
            case ( model.assetState, result ) of
                ( NoneLoaded, Ok texture ) ->
                    { model | assetState = SpriteSheetLoaded { spriteSheet = texture } }
                        |> Loading
                        |> addCmd
                            (Http.get
                                { url = "images/sprite_sheet.styl"
                                , expect = Http.expectString (TextureDataLoaded >> LoadingMsg)
                                }
                            )

                ( NoneLoaded, Err error ) ->
                    { model | spriteSheetError = Just error } |> Loading |> addCmdNone

                _ ->
                    model |> Loading |> addCmdNone

        TextureDataLoaded result ->
            case ( model.assetState, result ) of
                ( SpriteSheetLoaded { spriteSheet }, Ok textureData ) ->
                    let
                        loaded =
                            { spriteSheet = SpriteSheet.create spriteSheet textureData }

                        model_ =
                            { model | assetState = SpriteSheetDataLoaded loaded }
                    in
                    model_ |> Loading |> addCmdNone

                ( NoneLoaded, Err error ) ->
                    { model | spriteSheetDataError = Just error } |> Loading |> addCmdNone

                _ ->
                    model |> Loading |> addCmdNone

        PortError _ ->
            model |> Loading |> addCmdNone

        StartGame soundData spriteSheet ->
            loadingStartGame soundData spriteSheet model

        UpdateCurrentTime delta ->
            { model | time = Quantity.plus delta model.time } |> Loading |> addCmdNone

        UserPressedOverwriteColorPaletteOption colorPaletteSource ->
            { model | colorPaletteSource = colorPaletteSource } |> Loading |> addCmdNone

        KeyMsgLoading keyMsg ->
            case ( model.assetState, soundsLoaded model, Keyboard.anyKeyOriginal keyMsg ) of
                ( SpriteSheetDataLoaded { spriteSheet }, Just sounds, Just key ) ->
                    if key == Keyboard.Spacebar || key == Keyboard.Enter then
                        loadingStartGame sounds spriteSheet model

                    else
                        model |> Loading |> addCmdNone

                _ ->
                    model |> Loading |> addCmdNone


loadingStartGame soundData spriteSheet model =
    let
        model_ =
            initModel soundData model spriteSheet
    in
    model_ |> Loaded |> addCmd (saveSaveData model_)


updateBase : MsgBase -> ModelBase -> ( ModelBase, Cmd MsgBase )
updateBase msgBase modelBase =
    case ( msgBase, modelBase ) of
        ( LoadedMsg msg, Loaded model ) ->
            let
                ( newModel, cmd ) =
                    update msg model
            in
            ( Loaded newModel, Cmd.map LoadedMsg cmd )

        ( LoadingMsg msg, Loading model ) ->
            loadingUpdate msg model

        _ ->
            modelBase |> addCmdNone


editorMsgConfig =
    { noOp = NoOp, gotoMainMenu = PressedEditorBackButton, saveReplay = SaveReplay }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            model |> addCmdNone

        KeyMsg keyMsg ->
            { model | keys = Keyboard.update keyMsg model.keys } |> addCmdNone

        --|> (\m ->
        --        if
        --            List.any ((==) (Character "P")) m.keys
        --                && Environment.isDebug
        --                && (List.any ((==) (Character "P")) model.keys |> not)
        --        then
        --            { m | stepPaused = not m.stepPaused }
        --
        --        else
        --            m
        --   )
        --|> (\m ->
        --        if
        --            m.stepPaused
        --                && List.any ((==) (Character "N")) m.keys
        --                && Environment.isDebug
        --                && (List.any ((==) (Character "N")) model.keys |> not)
        --        then
        --            ( m, Helper.cmdLoopback SingleStep )
        --
        --        else
        --            m |> addCmdNone
        --   )
        Step delta ->
            let
                maximumStepDeltaBeforeWarning =
                    (60 / 55) * (1 / 60) |> RealTime.seconds

                secondsPerFrame =
                    1 / 60

                newDrift =
                    model.timeDrift |> Quantity.plus delta |> Quantity.min (RealTime.seconds 0.2)

                stepCount =
                    newDrift |> RealTime.inSeconds |> (\a -> a / secondsPerFrame) |> round

                ( newModel, cmd ) =
                    { model
                        | stepDelta = delta
                        , time = Quantity.plus delta model.time
                        , timeDrift = newDrift |> Quantity.minus (toFloat stepCount * secondsPerFrame |> RealTime.seconds)
                        , averageStepDelta =
                            if delta |> Quantity.lessThan (RealTime.seconds 0.5) then
                                model.averageStepDelta |> Quantity.multiplyBy 0.999 |> Quantity.plus (delta |> Quantity.multiplyBy 0.001)

                            else
                                model.averageStepDelta
                        , framerateWarningStatus =
                            case model.framerateWarningStatus of
                                Model.GoodFramerate ->
                                    if model.averageStepDelta |> Quantity.greaterThan maximumStepDeltaBeforeWarning then
                                        Model.BadFramerate

                                    else
                                        Model.GoodFramerate

                                _ ->
                                    model.framerateWarningStatus
                    }
                        |> soundStep
                        |> (\model_ ->
                                List.repeat stepCount ()
                                    |> List.foldl
                                        (\() ( state, cmd_ ) ->
                                            (if state.stepPaused then
                                                if Keyboard.isPressed state (Character "N") then
                                                    step state

                                                else
                                                    state |> addCmdNone

                                             else
                                                step state
                                            )
                                                |> Tuple.mapFirst inputStep
                                                |> Helper.mergeCmdTuple ( state, cmd_ )
                                        )
                                        ( model_, Cmd.none )
                           )

                soundCmd =
                    Sound.updateCmd newModel.time newModel.soundsPrevious newModel.sounds
            in
            { newModel | soundsPrevious = newModel.sounds } |> addCmd (Cmd.batch [ cmd, soundCmd ])

        WindowResize_ windowSize ->
            let
                cmd =
                    case model.gameState of
                        LevelEditor ->
                            Editor.windowResize |> Cmd.map EditorMsg

                        _ ->
                            Cmd.none
            in
            { model | windowSize = windowSize } |> flip Tuple.pair cmd

        StartStage ->
            startStage model |> addCmdNone

        PressedTitlePageEditorButton ->
            Model.setGameState LevelEditor model
                |> addCmd (Cmd.map EditorMsg Editor.windowResize)

        PressedTitlePageStartButton ->
            case model.gameState of
                MainMenu mainMenu ->
                    MenuState.transitionMenuPage
                        (MenuState.StageSelectPage
                            { stageTilePressTime = Nothing
                            , selectedButton =
                                StageData.playableStageIndex model.stageSelectId
                                    |> Maybe.map MenuState.StageButton
                                    |> Maybe.withDefault MenuState.StageSelectBackButton
                            }
                        )
                        model.time
                        mainMenu
                        |> MainMenu
                        |> flip Model.setGameState model
                        |> updateStageUnlocking
                        |> addCmdNone

                _ ->
                    model |> addCmdNone

        PressedEditorBackButton ->
            let
                ( replay, lookupTable ) =
                    case StageData.menuReplay of
                        ReplayData completeReplay lookupTable_ ->
                            ( completeReplay, lookupTable_ )

                        FailedToParseReplay _ ->
                            ( StageData.emptyReplay, Replay.getInitialState StageData.emptyReplay |> .circuit |> Jump.init )
            in
            Model.setGameState
                (MenuState.gotoMainMenu
                    (Either.Left replay)
                    lookupTable
                    (MenuState.TitlePage { selectedButton = MenuState.EditorButton })
                    False
                )
                model
                |> addCmdNone

        EditorMsg editorMsg ->
            case model.gameState of
                LevelEditor ->
                    let
                        updateState : { model : Editor.Model, cmdMsg : Cmd Editor.Msg, msgs : List Msg, sounds : Sound.Model }
                        updateState =
                            Editor.update editorMsgConfig model editorMsg model.editorState
                    in
                    editorUpdateToUpdate updateState model

                _ ->
                    model |> addCmdNone

        SingleStep ->
            model |> addCmdNone

        --update (Step Helper.secondsPerStep) model
        PortError_ error ->
            { model | portErrors = error :: model.portErrors } |> addCmdNone

        SoundLoaded_ _ _ ->
            model |> addCmdNone

        Batch list ->
            List.foldl
                (\msg_ ( model_, cmd ) ->
                    update msg_ model_
                        |> Tuple.mapSecond (\a -> a :: cmd)
                )
                ( model, [] )
                list
                |> Tuple.mapSecond Cmd.batch

        MouseMove mousePos ->
            { model | mousePosition = mousePos } |> addCmdNone

        MouseDown mousePos ->
            { model | mousePosition = mousePos, mouseDown = True } |> addCmdNone

        MouseUp mousePos ->
            { model | mousePosition = mousePos, mouseDown = False } |> addCmdNone

        PressInGameBackToStageSelect ->
            stopMission model

        SaveReplay replay ->
            model |> addCmd (saveReplay replay)

        InGameMsg inGameMsg ->
            case model.gameState of
                Stage stage ->
                    { stage | inGameModel = InGame.update model inGameMsg stage.inGameModel }
                        |> Stage
                        |> flip Model.setGameState model
                        |> addCmdNone

                _ ->
                    model |> addCmdNone

        SetMasterVolume volume ->
            setVolume (Sound.setMasterVolume volume) model

        SetMusicVolume volume ->
            setVolume (Sound.setMusicVolume volume) model

        SetSoundEffectVolume volume ->
            setVolume (Sound.setSoundEffectVolume volume) model

        ResetSettings ->
            resetSettings model |> addCmdNone

        PlaySound sound ->
            { model | sounds = Sound.play sound model.time model.sounds } |> addCmdNone

        DebounceMsg debounceMsg ->
            let
                ( debounce, cmd ) =
                    Debounce.update
                        Model.debounceConfig
                        (Debounce.takeLast (\_ -> saveSaveData model))
                        debounceMsg
                        model.debounceSaveData
            in
            { model | debounceSaveData = debounce } |> addCmd cmd

        SaveSaveData ->
            model |> addCmd (saveSaveData model)

        SetAntialias value ->
            { model | antialias = value } |> updateSaveData

        PressedShowTrailer ->
            updateMainMenu (MenuState.setShowTrailer True) model |> addCmdNone

        PressedCloseTrailer ->
            case model.gameState of
                MainMenu mainMenu ->
                    { model | gameState = MenuState.setShowTrailer False mainMenu |> MainMenu } |> addCmdNone

                _ ->
                    model |> addCmdNone

        PressedLoadReplayInSettings ->
            case model.gameState of
                MainMenu mainMenu ->
                    { model | gameState = MainMenu { mainMenu | showReplayParseError = False } }
                        |> addCmd (File.Select.file [ Replay.fileExtension ] SelectedReplayFileInSettings)

                _ ->
                    model |> addCmdNone

        PressedLoadReplayInReplayViewer ->
            case model.gameState of
                ReplayViewer replayViewer ->
                    { model | gameState = ReplayViewer (ReplayViewer.setShowReplayParseError False replayViewer) }
                        |> addCmd (File.Select.file [ Replay.fileExtension ] SelectedReplayFileInReplayViewer)

                _ ->
                    model |> addCmdNone

        SelectedReplayFileInSettings replayFile ->
            let
                cmd =
                    File.toBytes replayFile |> Task.perform ReplayFileLoadedInSettings
            in
            model |> addCmd cmd

        SelectedReplayFileInReplayViewer replayFile ->
            let
                cmd =
                    File.toBytes replayFile |> Task.perform ReplayFileLoadedInReplayViewer
            in
            model |> addCmd cmd

        ReplayFileLoadedInSettings replayBytes ->
            case Codec.fromBytes Replay.codec replayBytes of
                Ok replay ->
                    { model | gameState = ReplayViewer.init model.time replay |> ReplayViewer } |> addCmdNone

                Err _ ->
                    case model.gameState of
                        MainMenu mainMenu ->
                            { model | gameState = MainMenu { mainMenu | showReplayParseError = True } } |> addCmdNone

                        _ ->
                            model |> addCmdNone

        ReplayFileLoadedInReplayViewer replayBytes ->
            case Codec.fromBytes Replay.codec replayBytes of
                Ok replay ->
                    { model | gameState = ReplayViewer.init model.time replay |> ReplayViewer } |> addCmdNone

                Err _ ->
                    case model.gameState of
                        ReplayViewer replayViewer ->
                            { model | gameState = ReplayViewer (ReplayViewer.setShowReplayParseError True replayViewer) }
                                |> addCmdNone

                        _ ->
                            model |> addCmdNone

        PressedReplayBackButton ->
            case model.gameState of
                ReplayViewer _ ->
                    case StageData.menuReplay of
                        ReplayData replay lookupTable ->
                            { model | gameState = MenuState.gotoMainMenu (Either.Left replay) lookupTable MenuState.SettingsPage False } |> addCmdNone

                        FailedToParseReplay _ ->
                            { model
                                | gameState =
                                    MenuState.gotoMainMenu
                                        (Either.Right StageData.emptyStage)
                                        (Jump.init StageData.emptyStage)
                                        MenuState.SettingsPage
                                        False
                            }
                                |> addCmdNone

                _ ->
                    model |> addCmdNone

        ReplayViewerMsg replayViewerMsg ->
            case model.gameState of
                ReplayViewer replayViewer ->
                    { model | gameState = ReplayViewer.update replayViewerMsg replayViewer |> ReplayViewer } |> addCmdNone

                _ ->
                    model |> addCmdNone

        PressedStageTile { stageId } ->
            pressStageSelectTile True stageId model |> addCmdNone

        TypedHighscoreName highscoreName ->
            { model | highscoreState = HighscoreTable.setHighscoreName highscoreName model.highscoreState } |> addCmdNone

        PressedAcceptHighscoreName pendingHighscore ->
            let
                unlockedStages =
                    StageData.getAdjacentStages pendingHighscore.stageId
                        |> EverySet.toList
                        |> List.filter (\stageId -> HighscoreTable.stageStatus stageId model.highscoreState |> (==) StageLocked)

                newModel =
                    { model
                        | highscoreState = HighscoreTable.acceptHighscore model.time pendingHighscore model.highscoreState
                        , stageUnlockAnimation = Model.PendingStageUnlock { unlockedStages = unlockedStages }
                    }
            in
            newModel |> addCmd (saveSaveData newModel)

        PressedHighscoreSortBy sortBy ->
            { model | highscoreState = HighscoreTable.setSortBy sortBy model.highscoreState } |> addCmdNone

        PressedTitlePageSettings ->
            model |> updateMainMenu (MenuState.transitionMenuPage MenuState.SettingsPage model.time) |> addCmdNone

        PressedSettingsPageBack ->
            { model | sounds = Sound.play Sound.BackButtonClick model.time model.sounds }
                |> updateMainMenu
                    (MenuState.transitionMenuPage
                        (MenuState.TitlePage { selectedButton = MenuState.SettingsButton })
                        model.time
                    )
                |> addCmdNone

        PressedStageSelectBack ->
            { model | sounds = Sound.play Sound.BackButtonClick model.time model.sounds }
                |> updateMainMenu
                    (MenuState.transitionMenuPage
                        (MenuState.TitlePage { selectedButton = MenuState.StartButton })
                        model.time
                    )
                |> addCmdNone

        PressedStageSelectStartHacking ->
            startStage model |> addCmdNone

        PressedFramerateWarningPopup ->
            { model | framerateWarningStatus = Model.FramerateWarningHidden } |> addCmdNone

        MissionCompleted missionCompleted ->
            ( setHighscoreModel missionCompleted model, Browser.Dom.focus "new-highscore-name-input" |> Task.attempt (\_ -> NoOp) )

        PressedChangeColorsInSettings ->
            model
                |> updateMainMenu (MenuState.transitionMenuPage (MenuState.ColorSettingsPage ColorSettingsPage.init) model.time)
                |> addCmdNone

        PressedColorSettingsPageBack ->
            { model | sounds = Sound.play Sound.BackButtonClick model.time model.sounds }
                |> updateMainMenu (MenuState.transitionMenuPage MenuState.SettingsPage model.time)
                |> addCmdNone

        ColorPickerMsg colorPickerMsg ->
            case model.gameState of
                MainMenu mainMenu ->
                    case mainMenu.menuPage of
                        MenuState.ColorSettingsPage colorSettings ->
                            case colorSettings.colorSelection of
                                Just selection ->
                                    let
                                        ( colorPickerState, color ) =
                                            ColorPicker.update
                                                colorPickerMsg
                                                (ColorPalette.getColor selection model.colorPalette)
                                                colorSettings.colorPicker
                                    in
                                    setGameState model
                                        (MainMenu
                                            { mainMenu
                                                | menuPage =
                                                    MenuState.ColorSettingsPage { colorSettings | colorPicker = colorPickerState }
                                            }
                                        )
                                        |> setColorPalette (ColorPalette.setColor selection color model.colorPalette)

                                Nothing ->
                                    model |> addCmdNone

                        _ ->
                            model |> addCmdNone

                _ ->
                    model |> addCmdNone

        PressedColorSelection colorSelection ->
            case model.gameState of
                MainMenu mainMenu ->
                    setGameState model (MenuState.setColorSelection colorSelection mainMenu |> MainMenu) |> addCmdNone

                _ ->
                    model |> addCmdNone

        PressedResetColorSettings ->
            setColorPalette ColorPalette.defaultColors model

        SelectedColorSchemeLinkText ->
            model |> addCmd (selectTextInput ColorSettingsPage.colorSchemeLinkId)

        MouseOverTitlePageButton titlePageButton ->
            model
                |> updateMainMenu
                    (\mainMenu ->
                        if MenuState.menuIsTransitioning model.time mainMenu then
                            mainMenu

                        else
                            MenuState.setTitlePageButtonSelection titlePageButton mainMenu
                    )
                |> addCmdNone

        SelectedStageSelecTileWithArrowKeys { stageId } ->
            pressStageSelectTile False stageId model |> addCmdNone

        KeyPressedStageTile { stageId } ->
            startStage { model | stageSelectId = stageId } |> addCmdNone

        MouseEnterStageSelectButton _ ->
            model
                |> updateMainMenu
                    (\mainMenu ->
                        if MenuState.menuIsTransitioning model.time mainMenu then
                            mainMenu

                        else
                            MenuState.resetStageSelectSelectedButton model mainMenu
                    )
                |> addCmdNone


pressStageSelectTile : Bool -> StageId -> Model -> Model
pressStageSelectTile handleDoubleClick stageId model =
    case model.gameState of
        MainMenu mainMenu ->
            case mainMenu.menuPage of
                MenuState.StageSelectPage stageSelect ->
                    let
                        stageIndex =
                            StageData.playableStageIndex stageId |> Maybe.withDefault 0
                    in
                    if HighscoreTable.stageStatus stageId model.highscoreState == StageLocked then
                        model

                    else if model.stageSelectId == stageId && handleDoubleClick && doubleClick stageSelect.stageTilePressTime model.time then
                        startStage { model | stageSelectId = stageId }

                    else if model.stageSelectId /= stageId then
                        { model | stageSelectId = stageId }
                            |> setMainMenu
                                (case AssocList.get stageId StageData.allStages |> Maybe.map .circuit |> Maybe.withDefault StageData.FailedToParse of
                                    StageData.Data circuit lookupTable ->
                                        MenuState.pressStageTile stageIndex model.time mainMenu
                                            |> MenuState.setBackgroundGameplay (Either.Right circuit) lookupTable True

                                    StageData.FailedToParse ->
                                        mainMenu
                                )

                    else
                        model |> setMainMenu (MenuState.pressStageTile stageIndex model.time mainMenu)

                _ ->
                    model

        _ ->
            model


selectTextInput : String -> Cmd msg
selectTextInput textInputId =
    JE.object
        [ ( "Constructor", JE.string "SelectTextInput" )
        , ( "textInputId", JE.string textInputId )
        ]
        |> Ports.toJsPort


setColorPalette colorPalette model =
    let
        ( debounce, debounceCmd ) =
            Debounce.push Model.debounceConfig () model.debounceSaveData
    in
    { model | colorPalette = colorPalette, debounceSaveData = debounce }
        |> addCmd debounceCmd


updateStageUnlocking : Model -> Model
updateStageUnlocking model =
    case model.stageUnlockAnimation of
        Model.NoStageUnlocked ->
            model

        Model.PendingStageUnlock { unlockedStages } ->
            { model | stageUnlockAnimation = Model.StageUnlocking { unlockedStages = unlockedStages, startTime = model.time } }

        Model.StageUnlocking _ ->
            model


doubleClick : Maybe (Quantity Float RealTime) -> Quantity Float RealTime -> Bool
doubleClick firstClick secondClick =
    case firstClick of
        Just firstClick_ ->
            secondClick |> Quantity.minus firstClick_ |> Quantity.lessThan (RealTime.seconds 0.5)

        Nothing ->
            False


updateMainMenu : (MainMenu_ -> MainMenu_) -> Model -> Model
updateMainMenu mainMenuFunc model =
    case model.gameState of
        MainMenu mainMenu ->
            setMainMenu (mainMenuFunc mainMenu) model

        _ ->
            model


setMainMenu : MainMenu_ -> Model -> Model
setMainMenu mainMenu model =
    mainMenu |> MainMenu |> setGameState model


setVolume : (Sound.Model -> Sound.Model) -> Model -> ( Model, Cmd Msg )
setVolume updateFunc model =
    { model | sounds = updateFunc model.sounds } |> updateSaveData


updateSaveData : Model -> ( Model, Cmd Msg )
updateSaveData model =
    let
        ( debouncer, cmd ) =
            Debounce.push Model.debounceConfig () model.debounceSaveData
    in
    { model | debounceSaveData = debouncer } |> addCmd cmd


saveSaveData : Model -> Cmd msg
saveSaveData model =
    let
        saveData : String
        saveData =
            Model.getSaveData model |> Codec.toString SaveData.codec
    in
    JE.object
        [ ( "Constructor", JE.string "SaveSaveData" )
        , ( "saveData", JE.string saveData )
        ]
        |> Ports.toJsPort


soundStep : Model -> Model
soundStep model =
    { model
        | sounds =
            model.sounds
                |> (case model.gameState of
                        LevelEditor ->
                            case model.editorState.inGame of
                                Just _ ->
                                    Sound.stop Sound.IntroMusic

                                Nothing ->
                                    stopInGameMusic >> Sound.stop Sound.IntroMusic

                        Stage _ ->
                            Sound.stop Sound.IntroMusic

                        MainMenu mainMenu ->
                            case mainMenu.mainMenuIntro of
                                MainMenuIntro time ->
                                    Sound.play Sound.IntroMusic time >> stopInGameMusic >> StageSelect.sounds model

                                MainMenuIntroOver ->
                                    Sound.stop Sound.IntroMusic >> stopInGameMusic >> StageSelect.sounds model

                        ReplayViewer _ ->
                            Sound.stop Sound.IntroMusic
                   )
                |> Sound.stop Sound.Grind
    }


stopInGameMusic : Sound.Model -> Sound.Model
stopInGameMusic =
    Sound.stop Sound.InGameMusic >> Sound.stop Sound.InGameMusicGlitched


resetSettings model =
    { model
        | sounds =
            model.sounds
                |> Sound.setMasterVolume Sound.defaultMasterVolume
                |> Sound.setMusicVolume Sound.defaultMusicVolume
                |> Sound.setSoundEffectVolume Sound.defaultSoundEffectVolume
    }


stopMission model =
    case model.gameState of
        Stage { stageId } ->
            let
                stageIndex =
                    StageData.playableStageIndex stageId |> Maybe.withDefault 0

                { circuit, lookupTable } =
                    AssocList.get model.stageSelectId StageData.allStages
                        |> Maybe.map
                            (\stageData ->
                                case stageData.circuit of
                                    StageData.Data circuit_ lookupTable_ ->
                                        { circuit = circuit_, lookupTable = lookupTable_ }

                                    StageData.FailedToParse ->
                                        { circuit = StageData.emptyStage, lookupTable = Jump.init StageData.emptyStage }
                            )
                        |> Maybe.withDefault { circuit = StageData.emptyStage, lookupTable = Jump.init StageData.emptyStage }
            in
            { model
                | gameState =
                    MenuState.gotoStageSelect stageIndex (Either.Right circuit) lookupTable False
                , sounds = model.sounds |> stopInGameMusic
            }
                |> updateStageUnlocking
                |> addCmdNone

        _ ->
            model |> addCmdNone


saveReplay replay =
    Codec.toBytes Replay.codec replay
        |> File.Download.bytes ("Stage" ++ Replay.fileExtension) "application/octet-stream"


editorUpdateToUpdate : { model : Editor.Model, cmdMsg : Cmd Editor.Msg, msgs : List Msg, sounds : Sound.Model } -> Model -> ( Model, Cmd Msg )
editorUpdateToUpdate updateState model =
    let
        ( newModel, newCmd ) =
            updateState.msgs
                |> List.foldl (\msg (( m, _ ) as tuple) -> update msg m |> Helper.mergeCmdTuple tuple) ( model, Cmd.none )
    in
    ( { newModel
        | editorState = updateState.model
        , sounds = updateState.sounds
      }
    , Cmd.batch [ newCmd, updateState.cmdMsg |> Cmd.map EditorMsg ]
    )


startStage : Model -> Model
startStage model =
    case AssocList.get model.stageSelectId StageData.allStages of
        Just stageData ->
            case stageData.circuit of
                StageData.FailedToParse ->
                    model

                StageData.Data circuit lookupTable ->
                    if model.stageSelectId == StageData.tutorialId then
                        let
                            mission =
                                if model.tutorialIntroShown then
                                    Mission.NormalMission

                                else
                                    stageData.mission
                        in
                        { model
                            | gameState = MenuState.initStage model.time model.stageSelectId mission circuit lookupTable |> Stage
                            , tutorialIntroShown =
                                if model.stageSelectId == StageData.tutorialId then
                                    True

                                else
                                    model.tutorialIntroShown
                        }

                    else
                        { model | gameState = MenuState.initStage model.time model.stageSelectId stageData.mission circuit lookupTable |> Stage }

        _ ->
            model


setGameState : Model -> Game -> Model
setGameState model gameState =
    { model | gameState = gameState }


setSounds : Sound.Model -> Model -> Model
setSounds sounds model =
    { model | sounds = sounds }


{-| Core.Model isn't strictly needed as we can run the replay again to get the same final core state. The issue with doing that however is causes a noticeable pause in the game.
-}
initPendingHighscore : Quantity Float RealTime -> StageId -> Core.Model -> CompleteReplay -> HighscoreTable.PendingHighscore
initPendingHighscore completionTime stageId lastCoreState replay =
    { stageId = stageId
    , replay = replay
    , health = Core.getPlayer lastCoreState |> Agent.healthValue
    , goodChipMoves = Core.getAllChipMoves lastCoreState |> List.count (ChipMoveTime.chipMoveTiming >> (==) ChipMoveTime.OnTime) |> Quantity.Quantity
    , badChipMoves = Core.getAllChipMoves lastCoreState |> List.count (ChipMoveTime.chipMoveTiming >> (/=) ChipMoveTime.OnTime) |> Quantity.Quantity
    , creationTime = completionTime
    }


setHighscoreModel : Model.MissionCompleted_ -> Model -> Model
setHighscoreModel { stageId, coreState, replay, completionTime } model =
    let
        pendingHighscore =
            initPendingHighscore completionTime stageId coreState replay

        stageScore =
            HighscoreTable.toStageScore "" pendingHighscore
    in
    { model
        | highscoreState =
            if HighscoreTable.isNewHighscore stageId stageScore model.highscoreState then
                pendingHighscore
                    |> Just
                    |> flip HighscoreTable.setPendingHighscore model.highscoreState

            else
                HighscoreTable.setLatestScore (HighscoreTable.LatestScore { score = pendingHighscore }) model.highscoreState
    }


menuStateMsgConfig : MenuState.MsgConfig Msg
menuStateMsgConfig =
    { noOp = NoOp
    , pressedStartButton = PressedTitlePageStartButton
    , pressedEditorButton = PressedTitlePageEditorButton
    , pressedSettingsButton = PressedTitlePageSettings
    , keyPressedStageSelectBackButton = PressedStageSelectBack
    , keyPressedStageSelectStartButton = PressedStageSelectStartHacking
    , keyPressedStageSelectTile = KeyPressedStageTile
    , selectedStageSelecTileWithArrowKeys = SelectedStageSelecTileWithArrowKeys
    , pressedSettingsBackButton = PressedSettingsPageBack
    , pressedColorSettingsBackButton = PressedColorSettingsPageBack
    }


step : Model -> ( Model, Cmd Msg )
step model =
    let
        ( newModel, newMsgs ) =
            case model.gameState of
                MainMenu mainMenu ->
                    let
                        ( mainMenu2, mainMenuMsg ) =
                            MenuState.step menuStateMsgConfig model mainMenu
                    in
                    mainMenu2 |> MainMenu |> setGameState model |> update mainMenuMsg

                Stage stage ->
                    let
                        ( newModel_, newCmd ) =
                            stageStep model stage
                    in
                    ( newModel_, newCmd )

                LevelEditor ->
                    ( LevelEditor |> setGameState model, Cmd.none )

                ReplayViewer replayViewer ->
                    let
                        ( replayViewer_, sounds ) =
                            ReplayViewer.step model replayViewer
                    in
                    ( ReplayViewer replayViewer_ |> setGameState model |> setSounds sounds, Cmd.none )
    in
    if model.gameState == LevelEditor then
        Editor.step editorMsgConfig model model.editorState |> flip editorUpdateToUpdate model

    else
        ( newModel, newMsgs )


inputStep : Model -> Model
inputStep model =
    { model
        | previousKeys = model.keys
        , previousMousePosition = model.mousePosition
        , previousMouseDown = model.mouseDown
    }


stageStep : Model -> Stage_ -> ( Model, Cmd Msg )
stageStep model { stageId, inGameModel } =
    let
        input =
            Input.fromKeyboard model

        stepResult =
            InGame.step (MainMenu.inGameMsgConfig stageId model.time) model True input inGameModel

        ( newHighscoreState, highscoreStepMsgs ) =
            HighscoreTable.step
                (MainMenu.inGameMsgConfig stageId model.time)
                model
                (Keyboard.isPressed model Keyboard.Enter)
                model.highscoreState

        ( newModel, cmd ) =
            highscoreStepMsgs
                |> List.foldl
                    (\highscoreStepMsg ( model_, cmd_ ) ->
                        update highscoreStepMsg model_ |> Tuple.mapSecond (\a -> Cmd.batch [ a, cmd_ ])
                    )
                    ( { model | highscoreState = newHighscoreState }, Cmd.none )
    in
    { stageId = stageId
    , inGameModel = stepResult.model
    }
        |> Stage
        |> setGameState newModel
        |> setSounds stepResult.sounds
        |> (\model2 ->
                List.foldl
                    (\msg ( model3, cmd2 ) ->
                        update msg model3 |> Tuple.mapSecond (\newCmd -> Cmd.batch [ newCmd, cmd2 ])
                    )
                    ( model2, cmd )
                    stepResult.msgs
           )


viewFramerate : Model -> Element msg
viewFramerate model =
    let
        currentFps =
            String.fromInt (1 / max 0.00001 (RealTime.inSeconds model.stepDelta) |> round)
                ++ "/"
                ++ String.fromFloat (1 / RealTime.inSeconds Helper.secondsPerStep)
                |> Element.text

        averageFps =
            String.fromInt (1 / max 0.00001 (RealTime.inSeconds model.averageStepDelta) |> round)
                ++ "/"
                ++ String.fromFloat (1 / RealTime.inSeconds Helper.secondsPerStep)
                |> Element.text
    in
    Element.row
        [ Element.alignRight
        , Font.color <| Element.rgba 0 0 0 0.4
        , Element.alignBottom
        , Element.width <| Element.px 140
        , Element.padding 10
        ]
        [ currentFps
        , Element.el [ Element.alignRight ] averageFps
        ]


soundsLoaded : LoadingModel -> Maybe Sound.Model
soundsLoaded model =
    Sound.init model.sounds


amountLoaded : LoadingModel -> ( Int, Int )
amountLoaded model =
    ( Dict.size model.sounds, List.length Sound.allSounds )


mouseOverStyle =
    Element.mouseOver [ Background.color <| Element.rgba 0 0 0 0.2 ]


showOverwriteColorPalette : SaveData -> UrlData -> Bool
showOverwriteColorPalette saveData urlData =
    (ColorPalette.almostEqual saveData.colorPalette urlData.colorPalette |> not)
        && (ColorPalette.almostEqual saveData.colorPalette ColorPalette.defaultColors |> not)


loadingView : LoadingModel -> Element MsgBase
loadingView loadingModel =
    let
        soundErrors =
            loadingModel.soundErrors |> List.map .message

        overwriteColorPalette : Element MsgBase
        overwriteColorPalette =
            case ( loadingModel.saveData, loadingModel.urlData ) of
                ( Ok saveData, Ok urlData ) ->
                    if showOverwriteColorPalette saveData urlData then
                        let
                            buttonStyle selected =
                                Element.paddingXY 16 8
                                    :: Font.size 24
                                    :: (if selected then
                                            [ Background.color <| Element.rgb 0.8 0.8 1 ]

                                        else
                                            [ Background.color <| Element.rgb 1 1 1, mouseOverStyle ]
                                       )
                        in
                        Element.column
                            [ Element.centerX, Element.spacing 8, Element.padding 16 ]
                            [ Element.paragraph
                                [ Element.width <| Element.px 400, Element.centerX, Font.size 24 ]
                                [ Element.text "Overwrite your saved color palette with the color palette in the URL?" ]
                            , Element.row [ Element.centerX, Element.spacing 16 ]
                                [ Input.button
                                    (buttonStyle (loadingModel.colorPaletteSource == Model.ColorPaletteFromUrlData))
                                    { onPress = UserPressedOverwriteColorPaletteOption Model.ColorPaletteFromUrlData |> LoadingMsg |> Just
                                    , label = Element.text "Yes"
                                    }
                                , Input.button
                                    (buttonStyle (loadingModel.colorPaletteSource == Model.ColorPaletteFromSaveData))
                                    { onPress = UserPressedOverwriteColorPaletteOption Model.ColorPaletteFromSaveData |> LoadingMsg |> Just
                                    , label = Element.text "No"
                                    }
                                ]
                            ]

                    else
                        Element.none

                _ ->
                    Element.none

        saveDataError =
            case loadingModel.saveData of
                Ok _ ->
                    []

                Err Model.InvalidSaveData ->
                    [ "Failed to load local save data." ]

                Err Model.NoSaveData ->
                    []

        urlDataError =
            case loadingModel.urlData of
                Ok _ ->
                    []

                Err UrlData.InvalidUrlData ->
                    [ "Failed to load data from url." ]

                Err UrlData.NoUrlData ->
                    []

        spriteSheetError =
            case loadingModel.spriteSheetError of
                Just Texture.LoadError ->
                    [ "Failed to load spritesheet." ]

                Just (Texture.SizeError _ _) ->
                    [ "Spritesheet has invalid dimensions." ]

                Nothing ->
                    []

        spriteSheetDataError =
            case loadingModel.spriteSheetDataError of
                Just _ ->
                    [ "Failed to load spritesheet data." ]

                Nothing ->
                    []
    in
    Element.el
        [ Element.width Element.fill
        , Element.height Element.fill
        , Element.inFront buildInfo
        , Element.inFront <|
            Element.el
                [ Element.centerX
                , Element.alignBottom
                , Element.width Element.fill
                ]
                (Changelog.view loadingModel.windowSize)
        ]
    <|
        Element.column
            [ Element.width Element.fill
            , Element.centerY
            , Element.spacing 8
            , Element.above overwriteColorPalette
            ]
            [ amountLoaded loadingModel |> (\( a, b ) -> toFloat a / toFloat b) |> loadingBarContainer
            , Element.column
                [ Element.centerX
                , Font.color <| Element.rgb 0.8 0 0
                ]
                (saveDataError ++ urlDataError ++ soundErrors ++ spriteSheetError ++ spriteSheetDataError |> List.map Element.text)
            , loadingStartButton loadingModel
            ]


loadingBarContainer : Float -> Element msg
loadingBarContainer loadingRatio =
    Element.row
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        [ Element.el [ Element.width <| Element.fillPortion 1 ] Element.none
        , Element.el
            [ Element.width <| Element.fillPortion 3
            , Element.height <| Element.px 10
            ]
            (loadingBar loadingRatio)
        , Element.el [ Element.width <| Element.fillPortion 1 ] Element.none
        ]


loadingStartButton loadingModel =
    case ( loadingModel.assetState, soundsLoaded loadingModel ) of
        ( SpriteSheetDataLoaded data, Just sounds ) ->
            Input.button
                [ Element.centerX
                , Element.paddingXY 24 12
                , Font.size 40
                , mouseOverStyle
                , Background.color <| Element.rgb 0.9 0.9 0.9
                ]
                { onPress = StartGame sounds data.spriteSheet |> LoadingMsg |> Just, label = Element.text "Start!" }

        _ ->
            Element.el
                [ Element.centerX
                , Element.paddingXY 24 12
                , Font.size 40
                , Font.color <| Element.rgb 0.3 0.3 0.3
                ]
                (Element.text "Loading...")


buildInfo : Element msg
buildInfo =
    Element.column
        [ Font.color (Element.rgba 0 0 0 0.2)
        , Element.alignRight
        , Element.alignTop
        , Element.padding 10
        , Element.spacing 8
        ]
        [ Environment.buildTime
            |> DateFormat.format
                [ DateFormat.text "Last updated "
                , DateFormat.monthNameFull
                , DateFormat.text " "
                , DateFormat.dayOfMonthSuffix
                , DateFormat.text ", "
                , DateFormat.yearNumber
                ]
                Time.utc
            |> Element.text
            |> Element.el [ Element.alignRight ]
        , Element.el [ Element.alignRight ] (Element.text ("Build " ++ String.fromInt Environment.buildId))
        ]


viewBase : ModelBase -> Html MsgBase
viewBase modelBase =
    Element.layoutWith
        { options = [ Element.focusStyle { borderColor = Nothing, backgroundColor = Nothing, shadow = Nothing } ] }
        [ Element.htmlAttribute <| Html.Attributes.style "height" "100vh"
        , Element.clip
        , Element.htmlAttribute <| Html.Attributes.class "noselect"
        , Font.family [ Font.typeface "Monda", Font.sansSerif ]
        ]
    <|
        case modelBase of
            Loading loadingModel ->
                loadingView loadingModel

            Loaded model ->
                view model |> Element.map LoadedMsg

            InvalidFlags errorMessage ->
                Element.paragraph
                    [ Element.width Element.fill
                    , Element.padding 20
                    , Font.color (Element.rgb 0.9 0.1 0.1)
                    , Font.size 30
                    , Element.inFront buildInfo
                    ]
                    [ Element.text errorMessage ]


loadingBar : Float -> Element msg
loadingBar ratioComplete =
    Element.row
        [ Background.color <| Element.rgb 0.9 0.9 0.9
        , Element.width Element.fill
        , Element.height Element.fill
        ]
        [ Element.el
            [ Background.color <| Element.rgb 0.6 0.6 0.6
            , Element.width <| Element.fillPortion <| round <| ratioComplete * 100000
            , Element.height Element.fill
            ]
            Element.none
        , Element.el
            [ Element.width <| Element.fillPortion <| round <| (1 - ratioComplete) * 100000
            , Element.height Element.fill
            ]
            Element.none
        ]


mouseEvent : String -> (Point2d -> Msg) -> Element.Attribute Msg
mouseEvent eventName msg =
    Mouse.onWithOptions
        eventName
        { stopPropagation = False, preventDefault = False }
        (.offsetPos >> (\( x, y ) -> Point2d.fromCoordinates ( x, y )) >> msg)
        |> Element.htmlAttribute


framerateWarningView : Element Msg
framerateWarningView =
    let
        text =
            Element.paragraph [] [ Element.text "🐌 The game is lagging! Chrome seems to have the best luck at running this game smoothly so try that (unless you already are)." ]
    in
    Input.button
        [ Element.width <| Element.px 650
        , Element.moveUp 16
        , Element.centerX
        , Element.alignBottom
        , Background.color <| Element.rgb 0.9 0 0
        , Font.color <| Element.rgb 1 1 1
        , Border.rounded 6
        , Element.padding 16
        , Element.mouseOver [ Background.color <| Element.rgb 1 0.2 0.2 ]
        ]
        { onPress = Just PressedFramerateWarningPopup
        , label =
            Element.row
                [ Element.width Element.fill, Element.spacing 16 ]
                [ text, Element.el [ Element.alignRight, Element.alignTop ] (Element.text "✕") ]
        }


view : Model -> Element Msg
view model =
    Element.el
        [ Element.inFront <|
            if Environment.isDebug then
                viewFramerate model

            else
                Element.none
        , Element.inFront <|
            if model.framerateWarningStatus == Model.BadFramerate then
                framerateWarningView

            else
                Element.none
        , Element.width Element.fill
        , Element.height Element.fill
        , mouseEvent "mousemove" MouseMove
        , mouseEvent "mousedown" MouseDown
        , mouseEvent "mouseup" MouseUp
        ]
    <|
        case model.gameState of
            MainMenu mainMenu ->
                MainMenu.view model mainMenu

            Stage { stageId, inGameModel } ->
                InGame.view
                    (MainMenu.inGameMsgConfig stageId model.time)
                    model
                    False
                    (Just stageId)
                    (Input.fromKeyboard model)
                    True
                    True
                    InGame.PlayerControlledMap
                    inGameModel

            LevelEditor ->
                Editor.view model model.editorState |> Element.map EditorMsg

            ReplayViewer replayViewer ->
                ReplayViewer.view replayViewerMsgConfig model (Input.fromKeyboard model) replayViewer


replayViewerMsgConfig =
    { noOp = NoOp
    , back = PressedReplayBackButton
    , internalMsg = ReplayViewerMsg
    , loadReplay = PressedLoadReplayInReplayViewer
    }


subscriptions : ModelBase -> Sub MsgBase
subscriptions modelBase =
    case modelBase of
        Loading _ ->
            Sub.batch
                [ Sound.subscribe { soundLoaded = SoundLoaded, portError = PortError } |> Sub.map LoadingMsg
                , Keyboard.downs (KeyMsgLoading >> LoadingMsg)
                , Browser.Events.onResize
                    (\width height -> WindowResize { width = width, height = height } |> LoadingMsg)
                , onAnimationFrameDelta (UpdateCurrentTime >> LoadingMsg)
                ]

        Loaded model ->
            Sub.batch
                [ Sound.subscribe { soundLoaded = SoundLoaded_, portError = PortError_ }
                , Sub.map KeyMsg Keyboard.subscriptions
                , onAnimationFrameDelta Step
                , Browser.Events.onResize
                    (\width height -> WindowResize_ { width = width, height = height })
                , case model.gameState of
                    MainMenu _ ->
                        Sub.none

                    Stage _ ->
                        Sub.none

                    LevelEditor ->
                        Editor.subscriptions model.editorState |> Sub.map EditorMsg

                    ReplayViewer _ ->
                        Sub.none
                ]
                |> Sub.map LoadedMsg

        InvalidFlags _ ->
            Sub.none


onAnimationFrameDelta : (Quantity Float RealTime -> msg) -> Sub msg
onAnimationFrameDelta msg =
    Browser.Events.onAnimationFrameDelta (flip (/) 1000 >> RealTime.seconds >> msg)


main : Program JD.Value ModelBase MsgBase
main =
    Browser.element
        { init = init
        , view = viewBase
        , update = updateBase
        , subscriptions = subscriptions
        }
