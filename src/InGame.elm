module InGame exposing
    ( Config
    , InGameModel
    , InGameMsg
    , MsgConfig
    , Replayable
    , ShowMap(..)
    , cameraDefaultZ
    , cameraTransform
    , cameraWorldBounds
    , fastForwardButton
    , gameplayHud
    , inGameTopMenu
    , init
    , linePlaneIntersection
    , noOpMsgConfig
    , pointInConvexPolygon
    , replayableStep
    , restartReplayButton
    , startMission
    , step
    , update
    , useGlitchedIntro
    , view
    , webGLView
    )

import Agent exposing (AgentType(..), Player)
import Arc2d
import Array exposing (Array)
import Axis2d
import Axis3d
import Basics.Extra as Basics exposing (flip)
import Bitwise
import Cardinal exposing (Cardinal)
import Chip
import Circuit
    exposing
        ( Chip
        , ChipId(..)
        , Circuit
        , CircuitPosition(..)
        , Wire
        , WireDirection(..)
        , WireId(..)
        , WiresAndChips
        )
import Color exposing (Color)
import Color.Blending
import Color.Manipulate
import ColorPalette exposing (ColorPalette)
import Direction2d exposing (Direction2d)
import Direction3d
import Draw
import Element exposing (Element)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Environment
import Geometry.Interop.LinearAlgebra.Point2d as Point2d
import Geometry.Interop.LinearAlgebra.Point3d as Point3d
import Helper exposing (fillerPortion, ifElse)
import HighscoreTable
import Html.Attributes
import InGame.ChipMoveTime as ChipMoveTime
import InGame.Core as Core exposing (MissionStatus(..))
import InGame.CycleTime as CycleTime
import InGame.FrameNumber as FrameNumber exposing (FrameNumber)
import InGame.HackerMeter as HackerMeter
import InGame.HudTimer as HudTimer
import InGame.IntroAnimation as IntroAnimation
import InGame.Shaders as Shaders
import InGameTime exposing (InGameTime)
import Input exposing (Input)
import Keyboard
import KeyboardHelper
import Keyframe
import LineSegment2d exposing (LineSegment2d)
import List.Extra as List
import ListHelper as List
import Math.Matrix4 as Mat4 exposing (Mat4)
import Math.Vector2 as Vec2 exposing (Vec2)
import Math.Vector3 as Vec3 exposing (Vec3)
import Math.Vector4 as Vec4 exposing (Vec4)
import Maybe exposing (Maybe)
import Mesh exposing (ChipCapVertex, CircuitVertex, MapOverlayVertex, MeshPart, Vertex)
import Mission exposing (Mission(..))
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Point3d exposing (Point3d)
import Polygon2d exposing (Polygon2d)
import Quantity exposing (Quantity, Unitless)
import Random
import RandomEx as Random
import RealTime exposing (RealTime)
import Replay exposing (CompleteReplay, Replay)
import Set exposing (Set)
import SketchPlane3d exposing (SketchPlane3d)
import Sound exposing (Sound(..))
import SpriteSheet exposing (SpriteSheet)
import StageData exposing (StageId)
import StageScore exposing (ChipMoves, HealthPoints, StageScore)
import Style
import Svg
import Svg.Attributes
import Vector2d exposing (Vector2d)
import Vector2i exposing (Vector2i)
import Vector3d exposing (Vector3d)
import WebGL exposing (Entity, Mesh, Shader)
import WebGL.Settings as Setting
import WebGL.Settings.Blend as Blend
import WebGL.Settings.DepthTest as Depth
import Wire


type alias InGameModel =
    { circuitMesh : Mesh CircuitVertex
    , circuitMapMesh : Mesh MapOverlayVertex
    , chipCapsMesh : Mesh ChipCapVertex
    , replay : ReplayStatus
    , replayable : Replayable
    , initialState : Core.InitialState
    , introStartTime : Quantity Float RealTime
    , restartCount : Int
    , missionNumber : Int
    , showIntro : Bool
    , restartHotkey : Maybe (Quantity Float RealTime)
    , backToStageSelectHotkey : Maybe (Quantity Float RealTime)
    }


{-| Everything that resets when a replay starts.
-}
type alias Replayable =
    { coreState : Core.Model
    , cameraRotation : Direction2d
    , cameraOffset : Vector2d
    , cameraShakeVelocity : Vector2d
    , particles : List AgentParticle
    , chipLastEntered : Maybe (Quantity Float InGameTime)
    , chipLastExited : Maybe ( ChipId, Quantity Float InGameTime )
    , debugCameraLongitude : Float
    , debugCameraLatitude : Float
    , hackerMeter : HackerMeter.Model
    , mission : Mission
    }


type ReplayStatus
    = Recording Replay
    | Replaying Replaying_


type alias Replaying_ =
    { replay : CompleteReplay
    , isSuccess : Bool
    , replayCount : Int
    , fastForward : Bool
    , fastForwardClickTime : Quantity Float RealTime
    }


startMission : Mission -> Core.InitialState -> Replayable
startMission mission initialState =
    let
        coreState =
            Core.init initialState
    in
    { coreState = Core.init initialState
    , cameraOffset = Vector2d.zero
    , cameraShakeVelocity = Vector2d.zero
    , cameraRotation = Direction2d.fromAngle 0
    , debugCameraLongitude = 0
    , debugCameraLatitude = maxCameraLatitude
    , particles = []
    , chipLastEntered =
        case (Core.getCircuit coreState).playerStart of
            ChipPosition _ ->
                Just Quantity.zero

            WirePosition _ ->
                Nothing
    , chipLastExited = Nothing
    , hackerMeter = HackerMeter.init
    , mission = mission
    }
        |> (\m -> { m | cameraRotation = cameraTargetRotation m })


missionLength : Replaying_ -> Quantity Float InGameTime
missionLength replaying =
    Replay.replayLength replaying.replay


type alias AgentParticle =
    { startTime : Quantity Float InGameTime
    , color : AgentColor
    , position : Point2d
    }


type AgentColor
    = PlayerColor
    | AgentPassiveColor
    | AgentChaserColor


getAgentColor : AgentType -> AgentColor
getAgentColor agent =
    case agent of
        PassiveAgent _ ->
            AgentPassiveColor

        ChaserAgent _ ->
            AgentChaserColor


type alias Transform =
    { position : Point3d
    , rotation : Direction2d
    , lookAt : Point2d
    }


withZoom : Float -> Transform -> Transform
withZoom zoomBy transform =
    let
        offset =
            getLookAt3d transform |> Vector3d.from transform.position |> Vector3d.scaleBy -zoomBy
    in
    { position = getLookAt3d transform |> Point3d.translateBy offset
    , rotation = transform.rotation
    , lookAt = transform.lookAt
    }


getLookAt3d : Transform -> Point3d
getLookAt3d transform =
    let
        ( x, y ) =
            Point2d.coordinates transform.lookAt
    in
    Point3d.fromCoordinates ( x, y, 0 )


init : Bool -> Int -> Quantity Float RealTime -> Mission -> Core.InitialState -> InGameModel
init showIntro_ missionNumber currentTime mission initialState =
    let
        replayable =
            startMission mission initialState

        circuit =
            Core.getCircuit replayable.coreState
    in
    { circuitMesh = circuit |> Mesh.circuit |> Mesh.toMesh
    , circuitMapMesh = circuit |> Mesh.circuitOverlayMap |> Mesh.toMesh
    , chipCapsMesh = circuit |> Mesh.chipCaps |> Mesh.toMesh
    , replay = Replay.init initialState |> Recording
    , replayable = replayable
    , initialState = initialState
    , introStartTime = currentTime
    , missionNumber = missionNumber
    , showIntro = showIntro_
    , restartCount = 0
    , restartHotkey = Nothing
    , backToStageSelectHotkey = Nothing
    }


restartMission : InGameModel -> InGameModel
restartMission model =
    { model
        | replay = Replay.init model.initialState |> Recording
        , replayable = startMission model.replayable.mission model.initialState
        , restartCount = model.restartCount + 1
    }


type alias MsgConfig msg =
    { noOp : msg
    , backToStageSelect : msg
    , saveReplay : CompleteReplay -> msg
    , internalMsg : InGameMsg -> msg
    , typedHighscoreName : String -> msg
    , pressedAcceptedHighscoreName : HighscoreTable.PendingHighscore -> msg
    , pressedHighscoreSortBy : StageScore.SortBy -> msg
    , missionCompleted : Core.Model -> Replay.CompleteReplay -> msg
    }


noOpMsgConfig : MsgConfig ()
noOpMsgConfig =
    { noOp = ()
    , backToStageSelect = ()
    , saveReplay = always ()
    , internalMsg = always ()
    , typedHighscoreName = always ()
    , pressedAcceptedHighscoreName = always ()
    , pressedHighscoreSortBy = always ()
    , missionCompleted = \_ _ -> ()
    }


type InGameMsg
    = RestartMission
    | RestartReplay
    | ToggleFastForward


type alias Config a =
    { a
        | windowSize : Vector2i
        , time : Quantity Float RealTime
        , stepDelta : Quantity Float RealTime
        , mouseDown : Bool
        , previousMouseDown : Bool
        , mousePosition : Point2d
        , previousMousePosition : Point2d
        , spriteSheet : SpriteSheet
        , sounds : Sound.Model
        , antialias : Bool
        , highscoreState : HighscoreTable.Model
        , colorPalette : ColorPalette
        , keys : List Keyboard.Key
        , previousKeys : List Keyboard.Key
    }


mouseDelta : Config a -> Vector2d
mouseDelta config =
    Vector2d.from config.previousMousePosition config.mousePosition


particleAlive : Replayable -> AgentParticle -> Bool
particleAlive model particle =
    Core.getTime model.coreState |> Quantity.minus particle.startTime |> Quantity.lessThanOrEqualTo particleLifespan


particleLifespan : Quantity Float InGameTime
particleLifespan =
    InGameTime.seconds 0.2


update : Config a -> InGameMsg -> InGameModel -> InGameModel
update config msg model =
    case msg of
        RestartMission ->
            restartMission model

        RestartReplay ->
            case model.replay of
                Recording _ ->
                    model

                Replaying replaying ->
                    restartReplay model.replayable.mission replaying model

        ToggleFastForward ->
            case model.replay of
                Recording _ ->
                    model

                Replaying replaying ->
                    { model | replay = toggleFastForward config.time replaying |> Replaying }


toggleFastForward : Quantity Float RealTime -> Replaying_ -> Replaying_
toggleFastForward time replaying =
    { replaying | fastForward = not replaying.fastForward, fastForwardClickTime = time }


showIntro : InGameModel -> Bool
showIntro model =
    model.restartCount == 0 && model.showIntro


getCameraZ : Quantity Float RealTime -> InGameModel -> Float
getCameraZ realTime inGameModel =
    if showIntro inGameModel then
        Keyframe.startAt_ inGameModel.introStartTime 25
            |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 3) cameraDefaultZ
            |> Keyframe.valueAt_ realTime

    else
        cameraDefaultZ


cameraDefaultZ =
    20


beginPlaying : Config a -> InGameModel -> Bool
beginPlaying config inGameModel =
    if showIntro inGameModel then
        config.time
            |> Quantity.greaterThan
                (IntroAnimation.baseIntroStartTime (useGlitchedIntro inGameModel.replayable) inGameModel.introStartTime)

    else
        True


step : MsgConfig msg -> Config a -> Bool -> Input -> InGameModel -> { model : InGameModel, sounds : Sound.Model, msgs : List msg }
step msgConfig config showPlayer input inGameModel =
    let
        stepResult =
            case inGameModel.replay of
                Recording _ ->
                    if beginPlaying config inGameModel then
                        replayableStep config showPlayer config.sounds input inGameModel.replayable

                    else
                        { model = inGameModel.replayable, sounds = config.sounds }

                Replaying replaying ->
                    List.repeat (ifElse replaying.fastForward 3 1) ()
                        |> List.foldr
                            (\_ { model, sounds } ->
                                replayableStep config showPlayer sounds (Replay.getInput replaying.replay model.coreState) model
                            )
                            { model = inGameModel.replayable, sounds = config.sounds }

        ( newInGameModel, restartMsg ) =
            { inGameModel | replayable = stepResult.model } |> stepRecording config input |> stepRestartHotkey msgConfig config
    in
    { model = newInGameModel
    , sounds =
        stepResult.sounds
            |> IntroAnimation.soundUpdate
                inGameModel.missionNumber
                (useGlitchedIntro inGameModel.replayable)
                (showIntro inGameModel)
                config.time
                inGameModel.introStartTime
    , msgs =
        restartMsg
            :: (case ( newInGameModel.replay, inGameModel.replay ) of
                    ( Replaying replay, Recording _ ) ->
                        if replay.isSuccess then
                            [ msgConfig.missionCompleted newInGameModel.replayable.coreState replay.replay ]

                        else
                            []

                    _ ->
                        []
               )
    }


stepRestartHotkey : MsgConfig msg -> Config a -> InGameModel -> ( InGameModel, msg )
stepRestartHotkey msgConfig config inGameModel =
    let
        ( inGameModel2, msg ) =
            handleDelayedHotkey config (Keyboard.Character "R") .restartHotkey (\v a -> { a | restartHotkey = v }) inGameModel
                |> Tuple.mapSecond (\a -> ifElse a (msgConfig.internalMsg RestartMission) msgConfig.noOp)

        ( inGameModel3, msg2 ) =
            handleDelayedHotkey config Keyboard.Escape .backToStageSelectHotkey (\v a -> { a | backToStageSelectHotkey = v }) inGameModel2
                |> Tuple.mapSecond (\a -> ifElse a msgConfig.backToStageSelect msg)
    in
    ( inGameModel3, msg2 )


handleDelayedHotkey config key getHotkeyState setHotkeyState inGameModel =
    case ( KeyboardHelper.isDown config key, getHotkeyState inGameModel ) of
        ( True, Just startTime ) ->
            if config.time |> Quantity.greaterThan (Quantity.plus startTime restartHotkeyDelay) then
                ( setHotkeyState Nothing inGameModel, True )

            else
                ( inGameModel, False )

        ( False, _ ) ->
            ( setHotkeyState Nothing inGameModel, False )

        ( True, Nothing ) ->
            if KeyboardHelper.isPressed config key then
                ( setHotkeyState (Just config.time) inGameModel, False )

            else
                ( inGameModel, False )


restartHotkeyDelay : Quantity Float RealTime
restartHotkeyDelay =
    RealTime.seconds 0.5


{-| Did the user try to move too early while on a chip? (aka before the movement animation is complete)
-}
chipMovementEarly : Cardinal -> Input -> Core.Model -> Maybe Cardinal
chipMovementEarly currentDirection input model =
    case Core.chipDirectionPressed currentDirection (Core.getPreviousInput model) input of
        Just cardinal ->
            if Core.chipMoveTiming (Core.getTime model) model == ChipMoveTime.Early then
                Just cardinal

            else
                Nothing

        Nothing ->
            Nothing


useGlitchedIntro : Replayable -> Bool
useGlitchedIntro replayable =
    case replayable.mission of
        TutorialMission _ ->
            True

        _ ->
            False


randomVelocity : Random.Seed -> Vector2d
randomVelocity seed =
    Random.map
        (\dir -> Vector2d.withLength 1 (Direction2d.fromAngle dir))
        (Random.float -pi pi)
        |> flip Random.step seed
        |> Tuple.first


missionStep : Bool -> Replayable -> Replayable
missionStep showPlayer model =
    let
        time =
            Core.getTime model.coreState
    in
    case model.mission of
        TutorialMission _ ->
            model

        NormalMission ->
            model

        CustomMission ->
            model

        MenuMission menuMission ->
            if Quantity.plus menuMissionCameraPanDuration menuMission.startTime |> Quantity.lessThanOrEqualTo time then
                setMission
                    (Random.step
                        (Mission.randomMenuMission
                            False
                            time
                            (Core.getCircuit model.coreState)
                            (if showPlayer then
                                Core.getPlayer model.coreState |> .position |> Just

                             else
                                Nothing
                            )
                        )
                        (time |> InGameTime.inSeconds |> Random.initialSeed1)
                        |> Tuple.first
                        |> MenuMission
                    )
                    model

            else
                model


menuMissionCameraPanDuration : Quantity Float InGameTime
menuMissionCameraPanDuration =
    InGameTime.seconds 6


setMission : Mission -> Replayable -> Replayable
setMission mission replayable =
    { replayable | mission = mission }


replayableStep : Config a -> Bool -> Sound.Model -> Input -> Replayable -> { model : Replayable, sounds : Sound.Model }
replayableStep config showPlayer sounds input replayable =
    let
        inGameTime =
            Core.getTime coreState

        coreState =
            replayable.coreState

        chipMovementEarly_ : Maybe Cardinal
        chipMovementEarly_ =
            case (Core.getPlayer coreState).position of
                Circuit.WirePosition _ ->
                    Nothing

                Circuit.ChipPosition chipPosition ->
                    chipMovementEarly chipPosition.chipData.direction input replayable.coreState

        newCameraShakeVelocity =
            replayable.cameraShakeVelocity
                |> (\a ->
                        if isDamaged then
                            a |> Vector2d.sum (InGameTime.inSeconds inGameTime |> Random.initialSeed1 |> randomVelocity)

                        else
                            a
                   )
                |> (\a ->
                        case chipMovementEarly_ of
                            Just moveDirection ->
                                Cardinal.toVector2i moveDirection |> Vector2i.toVector2d |> Vector2d.scaleBy 0.5 |> Vector2d.sum a

                            Nothing ->
                                a
                   )
                |> Vector2d.scaleBy (0.1 ^ InGameTime.inSeconds Core.secondsPerFrame)

        cameraOffset =
            replayable.cameraOffset
                |> Vector2d.scaleBy (InGameTime.inSeconds Core.secondsPerFrame)
                |> Vector2d.sum newCameraShakeVelocity
                |> Vector2d.scaleBy (0.01 ^ InGameTime.inSeconds Core.secondsPerFrame)

        addParticlesDelay =
            InGameTime.seconds 0.05

        newParticles =
            if Helper.intervalElapsed addParticlesDelay inGameTime newTime then
                let
                    playerTail =
                        case Core.agentWorldPosition coreState newPlayer.position of
                            Just pos ->
                                [ { startTime = inGameTime
                                  , color = PlayerColor
                                  , position = pos
                                  }
                                ]

                            Nothing ->
                                []

                    agentTails =
                        List.filterMap
                            (\agent ->
                                Agent.getPosition agent
                                    |> Core.agentWorldPosition replayable.coreState
                                    |> Maybe.map (AgentParticle inGameTime (getAgentColor agent))
                            )
                            (Core.getAgents replayable.coreState)
                in
                playerTail ++ agentTails ++ replayable.particles

            else
                replayable.particles

        newCoreState =
            Core.step input replayable.coreState

        newTime =
            Core.getTime newCoreState

        player =
            Core.getPlayer coreState

        newPlayer =
            Core.getPlayer newCoreState

        sounds2 : Sound.Model
        sounds2 =
            sounds
                |> (\sounds_ ->
                        if Core.hackedChipCount newCoreState > Core.hackedChipCount coreState then
                            Sound.play Sound.ChipHacked config.time sounds_

                        else
                            config.sounds
                   )
                |> (\sounds_ ->
                        if isDamaged then
                            Sound.play Sound.Damage config.time sounds_

                        else
                            sounds_
                   )
                |> (\sounds_ ->
                        if chipMovementEarly_ == Nothing then
                            sounds_

                        else
                            Sound.play Sound.ChipMovementEarly config.time sounds_
                   )
                |> (\sounds_ ->
                        case ( newPlayer.position, Core.missionStatus coreState ) of
                            ( WirePosition { data }, MissionInProgress ) ->
                                case data.transitioning of
                                    Just _ ->
                                        Sound.stop Sound.Grind sounds_

                                    Nothing ->
                                        Sound.play Sound.Grind Quantity.zero sounds_
                                            |> Sound.setPlaybackRate (data.speedFactor ^ 0.3) Sound.Grind

                            ( _, _ ) ->
                                Sound.stop Sound.Grind sounds_
                   )

        isDamaged =
            Agent.healthCompare newPlayer.health player.health == LT

        replayable2 =
            { replayable
                | coreState = newCoreState
                , cameraRotation =
                    Direction2d.angleFrom replayable.cameraRotation (cameraTargetRotation replayable)
                        * 0.5
                        |> flip Direction2d.rotateBy replayable.cameraRotation
                , cameraShakeVelocity = newCameraShakeVelocity
                , cameraOffset = cameraOffset
                , particles = newParticles |> List.filter (particleAlive replayable)
                , chipLastEntered =
                    case ( newPlayer.position, player.position ) of
                        ( ChipPosition _, WirePosition _ ) ->
                            Just newTime

                        ( WirePosition _, ChipPosition _ ) ->
                            Nothing

                        _ ->
                            replayable.chipLastEntered
                , chipLastExited =
                    case ( newPlayer.position, player.position ) of
                        ( WirePosition _, ChipPosition { chipId } ) ->
                            Just ( chipId, newTime )

                        _ ->
                            replayable.chipLastExited
                , hackerMeter = HackerMeter.step newCoreState replayable.hackerMeter
            }
                |> missionStep showPlayer
    in
    { model = replayable2, sounds = sounds2 }


stepDebugCamera config replayable =
    if Environment.isDebug then
        { replayable
            | debugCameraLongitude =
                if config.mouseDownthen then
                    mouseDelta config
                        |> Vector2d.components
                        |> Tuple.first
                        |> (*) 0.01
                        |> (+) replayable.debugCameraLongitude

                else
                    replayable.debugCameraLongitude
            , debugCameraLatitude =
                if config.mouseDown then
                    mouseDelta config
                        |> Vector2d.components
                        |> Tuple.second
                        |> (*) 0.01
                        |> (+) replayable.debugCameraLatitude
                        |> clamp 0.1 maxCameraLatitude

                else
                    replayable.debugCameraLatitude
        }

    else
        replayable


stepRecording : Config a -> Input -> InGameModel -> InGameModel
stepRecording config currentInput inGameModel =
    let
        coreState =
            inGameModel.replayable.coreState

        playerAlive =
            Core.getPlayer coreState |> Agent.isAlive
    in
    case inGameModel.replay of
        Recording replay ->
            { inGameModel
                | replay =
                    if isMenuMission inGameModel.replayable || (beginPlaying config inGameModel |> not) then
                        inGameModel.replay

                    else if
                        (Core.missionStatus coreState == MissionSuccess)
                            || (Core.missionStatus coreState == MissionFailure)
                    then
                        Replaying
                            { replay = Replay.addInput currentInput replay |> Replay.endReplay config.colorPalette coreState
                            , isSuccess = playerAlive
                            , replayCount = 0
                            , fastForward = False
                            , fastForwardClickTime = Quantity.zero
                            }

                    else
                        Replay.addInput currentInput replay |> Recording
            }

        Replaying replaying ->
            let
                restartTime =
                    Quantity.plus restartReplayDelay (missionLength replaying)
            in
            if restartTime |> Quantity.lessThanOrEqualTo (Core.getTime coreState) then
                restartReplay inGameModel.replayable.mission replaying inGameModel

            else
                inGameModel


restartReplay : Mission -> Replaying_ -> InGameModel -> InGameModel
restartReplay mission replaying inGameModel =
    let
        { circuit } =
            Replay.getInitialState replaying.replay
    in
    { inGameModel
        | replayable =
            startMission
                mission
                { circuit = circuit
                , lookupTable = Core.getJumpLookup inGameModel.replayable.coreState |> Just
                }
        , replay = incrementReplayCount replaying |> Replaying
    }


restartReplayDelay : Quantity Float InGameTime
restartReplayDelay =
    InGameTime.seconds 3


nonLoopingInGameTime : Replaying_ -> Quantity Float InGameTime -> Quantity Float InGameTime
nonLoopingInGameTime replaying inGameTime =
    Quantity.plus restartReplayDelay (missionLength replaying)
        |> Quantity.multiplyBy (toFloat replaying.replayCount)
        |> flip Quantity.plus inGameTime


incrementReplayCount : Replaying_ -> Replaying_
incrementReplayCount replaying =
    { replaying | replayCount = replaying.replayCount + 1 }


maxCameraLatitude =
    pi / 2 - pi / 12


cameraTargetRotation : Replayable -> Direction2d
cameraTargetRotation inGameModel =
    Agent.playerDirection (Core.getCircuit inGameModel.coreState) (Core.getPlayer inGameModel.coreState)
        |> Maybe.withDefault (Direction2d.fromAngle 0)


drawPlayerIntroParticles : Point2d -> Replayable -> List Particle
drawPlayerIntroParticles playerPosition replayable =
    let
        angleAndTimeList : Random.Generator (List ( Direction2d, Float ))
        angleAndTimeList =
            Random.map2 Tuple.pair
                (Random.float 0 (2 * pi) |> Random.map Direction2d.fromAngle)
                (Random.float 0 0.5)
                |> Random.list 100

        inGameTime =
            Core.getTime replayable.coreState

        pointAlpha startTimeOffset =
            Keyframe.startAt_ (InGameTime.seconds startTimeOffset) 0
                |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 1) 1
                |> Keyframe.valueAt_ inGameTime
                |> sin

        pointPos : Direction2d -> Float -> Vector2d
        pointPos angle startTimeOffset =
            Keyframe.startAt_ (InGameTime.seconds startTimeOffset) 10
                |> Keyframe.add_ Keyframe.InExpo (InGameTime.seconds 1) 0
                |> Keyframe.valueAt_ inGameTime
                |> (\dist -> Vector2d.withLength dist angle)
    in
    Random.initialSeed 123123
        |> Random.step angleAndTimeList
        |> Tuple.first
        |> List.map
            (\( angle, startTimeOffset ) ->
                let
                    alpha =
                        pointAlpha startTimeOffset

                    pos =
                        Point2d.translateBy (pointPos angle startTimeOffset) playerPosition
                in
                { position = pos, alpha = alpha * 1.5, scale = 0.2 }
            )


glitchMatrix : Mat4 -> Float -> Mat4
glitchMatrix mat glitchAmount =
    let
        { m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42, m43, m44 } =
            Mat4.toRecord mat

        ( random0, seed0 ) =
            Random.step (Random.float -1 1) (Random.initialSeed1 glitchAmount)

        ( random1, seed1 ) =
            Random.step (Random.float -1 1) seed0

        ( random2, seed2 ) =
            Random.step (Random.float -1 1) seed1

        ( random3, seed3 ) =
            Random.step (Random.float -1 1) seed2

        ( random4, _ ) =
            Random.step (Random.float -1 1) seed3

        offset =
            Vec3.vec3 (glitchAmount * 0.2 * random0) (glitchAmount * 0.2 * random1) (glitchAmount * 0.2 * random2)
    in
    Mat4.makeRotate (0.01 * glitchAmount * random3) (Vec3.vec3 0 0 1)
        |> Mat4.rotate (0.01 * glitchAmount * random4) (Vec3.vec3 0 1 0)
        |> flip Mat4.mul mat
        |> Mat4.translate offset


type alias Meshes a =
    { a
        | circuitMesh : Mesh CircuitVertex
        , circuitMapMesh : Mesh MapOverlayVertex
        , chipCapsMesh : Mesh ChipCapVertex
    }


webGLView : Config a -> Input -> Transform -> Color -> Meshes b -> Replayable -> (Mat4 -> Mat4) -> Bool -> ShowMap -> Element msg
webGLView config input cameraTransform_ overlayColor meshes replayable perspectiveScrewery showPlayer showMap =
    let
        windowSize =
            config.windowSize

        coreState =
            replayable.coreState

        { red, green, blue, alpha } =
            Color.toRgba config.colorPalette.background

        antialias =
            if config.antialias then
                [ WebGL.antialias ]

            else
                []

        cameraMapTransform =
            cameraTransform_ |> withZoom 3

        {- As an optimization we compute the perspective matrices once here and then pass it to functions that need it. -}
        perspectiveMatrix =
            Mat4.mul (perspective windowSize) (cameraMatrix cameraTransform_)

        perspectiveMapMatrix =
            Mat4.mul (perspective windowSize) (cameraMatrix cameraMapTransform)

        renderConfig =
            { perspectiveMat = perspectiveScrewery perspectiveMatrix
            , cameraBounds =
                cameraWorldBounds
                    cameraTransform_.position
                    (Mat4.mul (perspective windowSize) (cameraMatrix cameraTransform_))
            , camera = cameraTransform_
            }

        mapRenderConfig =
            { perspectiveMat = perspectiveMapMatrix
            , cameraBounds =
                cameraWorldBounds
                    cameraMapTransform.position
                    (Mat4.mul (perspective windowSize) (cameraMatrix cameraMapTransform))
            , camera = cameraMapTransform
            }

        drawChipMovementArrows_ =
            case replayable.mission of
                MenuMission _ ->
                    []

                _ ->
                    drawChipMovementArrows config renderConfig replayable

        map =
            if (Input.showMap input && showMap == PlayerControlledMap) || (showMap == OverrideMap True) then
                drawEntity
                    False
                    Shaders.mapOverlayVS
                    Shaders.fragmentShader
                    meshes.circuitMapMesh
                    (mapOverlayUniforms config (Core.getCircuit replayable.coreState) mapRenderConfig)
                    :: drawAgents config agentMapOverlayInstances mapRenderConfig replayable

            else
                []
    in
    WebGL.toHtmlWith
        (WebGL.depth 1
            :: WebGL.alpha False
            :: antialias
        )
        [ Html.Attributes.width windowSize.width
        , Html.Attributes.height windowSize.height
        ]
        (drawBackground config.colorPalette.background
            :: drawEntity
                True
                Shaders.circuitVS
                Shaders.fragmentShader
                meshes.circuitMesh
                (circuitUniforms config renderConfig)
            :: drawChipTiles config renderConfig coreState
            ++ [ drawEntity
                    True
                    Shaders.chipCapsVS
                    Shaders.fragmentShader
                    meshes.chipCapsMesh
                    (chipCapsUniforms config renderConfig replayable showPlayer)
               ]
            ++ drawParticles config renderConfig replayable showPlayer
            ++ drawAgents config agentInstances renderConfig replayable
            ++ drawChipMovementArrows_
            ++ (if showPlayer then
                    drawPlayer config renderConfig replayable

                else
                    []
               )
            ++ drawHackSprites renderConfig replayable config.spriteSheet
            ++ map
            ++ [ drawOverlay overlayColor ]
        )
        |> Element.html


drawChipMovementArrows : Config a -> DrawConfig -> Replayable -> List Entity
drawChipMovementArrows config { perspectiveMat } replayable =
    let
        coreState =
            replayable.coreState

        circuit =
            Core.getCircuit coreState
    in
    case (Core.getPlayer coreState).position of
        ChipPosition chipPosition ->
            let
                showArrow : Int -> Float
                showArrow index =
                    if Core.isLeavingChip circuit chipPosition then
                        0

                    else
                        case Cardinal.all |> List.getAt index |> Core.moveOnChip_ circuit chipPosition of
                            Just _ ->
                                1

                            _ ->
                                0
            in
            if Core.missionStatus coreState == MissionInProgress then
                drawEntity
                    False
                    Shaders.chipMovementArrowsVS
                    Shaders.fragmentShader
                    Shaders.chipMovementArrows
                    { playerPosition =
                        Core.agentChipPosition coreState chipPosition
                            |> Maybe.withDefault (Point2d.fromCoordinates ( 9999, 9999 ))
                            |> Point2d.toVec2
                    , perspective = perspectiveMat
                    , showArrow0 = showArrow 0
                    , showArrow1 = showArrow 1
                    , showArrow2 = showArrow 2
                    , showArrow3 = showArrow 3
                    , color = config.colorPalette.movementArrows |> Mesh.colorToVec4
                    }
                    |> List.singleton

            else
                []

        WirePosition _ ->
            []


{-| A quad that spans the entire canvas.
-}
overlayMesh =
    Mesh.square
        |> Mesh.map
            (Point2d.scaleAbout (Point2d.fromCoordinates ( 1, 1 )) 2
                >> Point2d.mirrorAcross Axis2d.x
                >> Point2d.toVec2
                >> (\a -> { position = a })
            )
        |> Mesh.toMesh


drawOverlay : Color -> Entity
drawOverlay color =
    drawEntity False Shaders.overlayVS Shaders.overlayFS overlayMesh { color = Mesh.colorToVec4 color }


drawBackground : Color -> Entity
drawBackground color =
    WebGL.entityWith
        [ Setting.cullFace Setting.back
        , Depth.always { write = False, near = 0, far = 1 }
        , Blend.add Blend.one Blend.oneMinusSrcAlpha
        ]
        Shaders.overlayVS
        Shaders.overlayFS
        overlayMesh
        { color = Mesh.colorToVec4 color }


missionEndBackgroundAlpha : Replaying_ -> Quantity Float InGameTime -> Float
missionEndBackgroundAlpha replaying inGameTime =
    Keyframe.startAt_ (missionLength replaying) 0
        |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 1) 0.4
        |> Keyframe.valueAt_ (nonLoopingInGameTime replaying inGameTime)


missionEndView : MsgConfig msg -> Config a -> Maybe StageId -> Replaying_ -> Replayable -> Element msg
missionEndView msgConfig config stageId replaying replayable =
    let
        inGameTime =
            Core.getTime replayable.coreState

        endTime : Quantity Float InGameTime
        endTime =
            missionLength replaying

        currentTime : Quantity Float InGameTime
        currentTime =
            nonLoopingInGameTime replaying inGameTime

        replayToolbarAnim =
            Keyframe.startAt_ endTime 30
                |> Keyframe.addConstant (InGameTime.seconds 2)
                |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 0.3) 0
                |> Keyframe.valueAt_ currentTime

        replayToolbarFadeAnim =
            Keyframe.startAt_ endTime 0
                |> Keyframe.addConstant (InGameTime.seconds 2)
                |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 0.3) 1
                |> Keyframe.valueAt_ currentTime

        body =
            if replaying.isSuccess then
                missionSuccessView msgConfig config stageId endTime currentTime

            else
                missionFailureView msgConfig config endTime currentTime

        replayToolbar =
            Element.row
                [ Element.alignBottom
                , Element.centerX
                , Element.padding 5
                , Element.spacing 30
                , Element.moveDown replayToolbarAnim
                , Element.alpha replayToolbarFadeAnim
                ]
                [ restartReplayButton (msgConfig.internalMsg RestartReplay)
                , fastForwardButton (msgConfig.internalMsg ToggleFastForward) config.time replaying
                , saveReplayButton msgConfig replaying
                ]
    in
    Element.column
        [ Element.width Element.fill
        , Element.height Element.fill
        , Element.clip
        , Element.spacing 10
        , Element.inFront <|
            if replaying.isSuccess then
                HighscoreTable.newScoreNameOverlay
                    msgConfig
                    config
                    config.highscoreState

            else
                Element.none
        ]
        [ body
        , replayToolbar
        ]


missionFailureView : MsgConfig msg -> Config a -> Quantity Float InGameTime -> Quantity Float InGameTime -> Element msg
missionFailureView msgConfig config endTime currentTime =
    let
        gameOverAnim =
            Keyframe.startAt_ endTime (toFloat config.windowSize.height)
                |> Keyframe.add_ Keyframe.OutBounce (InGameTime.seconds 1) 0
                |> Keyframe.valueAt_ currentTime

        gameOverOffset =
            Keyframe.startAt_ endTime 20
                |> Keyframe.addConstant (InGameTime.seconds 1)
                |> Keyframe.add_ Keyframe.InOutQuart (InGameTime.seconds 1) 0
                |> Keyframe.valueAt_ currentTime

        tryAgainAnim =
            Keyframe.startAt_ endTime 0
                |> Keyframe.addConstant (InGameTime.seconds 1.2)
                |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 1) 1
                |> Keyframe.valueAt_ currentTime
    in
    Element.column
        [ Element.centerX
        , Element.centerY
        , Color.Manipulate.weightedMix Style.menuBackgroundColor (Color.rgba 0 0 0 0) tryAgainAnim
            |> Helper.colorToElement
            |> Background.color
        , Element.padding 20
        , Element.moveUp gameOverAnim
        ]
        [ Element.el
            [ Font.size 100
            , Font.bold
            , Element.moveDown gameOverOffset
            , Font.glow (Element.rgb 0 0 0) 30
            , Font.color (Element.rgb 0.8 0 0)
            ]
            (Element.text "GAME OVER")
        , Element.el [ Element.centerX, Element.alpha tryAgainAnim ] (missionEndOptions False msgConfig)
        ]


missionEndOptions : Bool -> MsgConfig msg -> Element msg
missionEndOptions missionSuccess msgConfig =
    Element.row
        []
        [ Input.button
            missionEndOptionsStyle
            { onPress = Just (msgConfig.internalMsg RestartMission), label = Element.text <| ifElse missionSuccess "Play again" "Try again" }
        , Input.button
            missionEndOptionsStyle
            { onPress = Just msgConfig.backToStageSelect, label = Element.text "Back to stage select" }
        ]


missionSuccessView : MsgConfig msg -> Config a -> Maybe StageId -> Quantity Float InGameTime -> Quantity Float InGameTime -> Element msg
missionSuccessView msgConfig config stageId endTime currentTime =
    let
        completeAnim =
            Keyframe.startAt_ endTime (toFloat config.windowSize.height)
                |> Keyframe.add_ Keyframe.OutQuart (InGameTime.seconds 1) 0
                |> Keyframe.valueAt_ currentTime

        title =
            Element.el
                [ Font.size 80
                , Element.moveUp completeAnim
                , Font.bold
                , Element.centerX
                , Font.color (Element.rgb 0 1 0)
                , Font.glow (Element.rgb 1 1 1) 15
                ]
                (Element.text "ACCESS GRANTED")
    in
    Element.column
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        [ title
        , Element.el
            [ Element.centerX, Element.centerY, Element.moveUp completeAnim ]
            (missionEndOptions True msgConfig)
        , case stageId of
            Just stageId_ ->
                HighscoreTable.view msgConfig config stageId_ endTime currentTime
                    |> Element.el [ Element.centerX, Element.alignBottom ]

            Nothing ->
                Element.none
        ]


missionEndOptionsStyle =
    [ Element.paddingXY 20 10
    , Element.mouseOver <| [ Background.color <| Element.rgba 1 1 1 0.5 ]
    , Font.color <| Element.rgb 1 1 1
    , Font.size 26
    ]


gameplayHud : Config a -> Replayable -> Element msg
gameplayHud config replayable =
    let
        badChipMove =
            Core.getAllChipMoves replayable.coreState
                |> List.filter (ChipMoveTime.chipMoveTiming >> (/=) ChipMoveTime.OnTime)

        timePenalties =
            badChipMove
                |> List.map (\chipMove -> { startTime = chipMove.moveTime, penaltyType = HudTimer.BadMovePenalty })
                |> (++)
                    (case Core.getPlayer replayable.coreState |> .health of
                        Agent.MaxHealth ->
                            []

                        Agent.Damaged { lastDamaged } ->
                            [ { startTime = lastDamaged
                              , penaltyType = HudTimer.DamagePenalty
                              }
                            ]
                    )
    in
    Element.el
        [ Element.width Element.fill
        , Element.height Element.fill
        , Element.inFront (clockCycleView replayable.coreState)
        , Element.inFront
            (HackerMeter.view
                (toFloat config.windowSize.width * 0.5 |> round)
                replayable.hackerMeter
                replayable.coreState
            )
        , Element.inFront <|
            Element.column
                [ Element.alignRight
                , Element.alignTop
                , Element.paddingXY 20 10
                , Element.spacing 20
                , Style.menuBackground
                , Border.roundEach { topLeft = 0, bottomRight = 0, bottomLeft = 10, topRight = 0 }
                , Font.color <| Element.rgb 1 1 1
                ]
                [ Element.el
                    [ Element.alignRight ]
                    (StageScore.penaltyTime
                        { health = Core.getPlayer replayable.coreState |> Agent.healthValue
                        , badChipMoves = List.length badChipMove |> StageScore.chipMoves
                        }
                        |> HudTimer.view (Core.getTime replayable.coreState) timePenalties
                    )
                , Element.el [ Element.alignRight ] (chipsRemainingView replayable)
                ]
        ]
        Element.none


type ShowMap
    = OverrideMap Bool
    | PlayerControlledMap


view : MsgConfig msg -> Config a -> Bool -> Maybe StageId -> Input -> Bool -> Bool -> ShowMap -> InGameModel -> Element msg
view msgConfig config firstFewSecondsOfGame stageId input showPlayer showHud showMap inGameModel =
    let
        coreState =
            inGameModel.replayable.coreState

        inGameTime =
            Core.getTime coreState

        mission =
            inGameModel.replayable.mission

        showTopMenu =
            case inGameModel.replay of
                Recording _ ->
                    showHud && not (IntroAnimation.showErrorScreen (useGlitchedIntro inGameModel.replayable) config.time inGameModel.introStartTime)

                Replaying _ ->
                    False

        hudOverlay =
            case inGameModel.replay of
                Recording _ ->
                    gameplayHud config inGameModel.replayable

                Replaying replaying ->
                    missionEndView msgConfig config stageId replaying inGameModel.replayable

        overlayColor =
            case mission of
                MenuMission menuMission ->
                    let
                        cameraTime =
                            inGameTime |> Quantity.minus menuMission.startTime |> InGameTime.inSeconds

                        alpha =
                            clamp 0 1 (1 - cameraTime) + clamp 0 1 (cameraTime - InGameTime.inSeconds menuMissionCameraPanDuration + 1)

                        introAlpha =
                            if menuMission.firstMenuIntro then
                                clamp 0 1 (1 - (InGameTime.inSeconds inGameTime * 0.5))

                            else
                                0

                        { red, green, blue } =
                            Color.toRgba config.colorPalette.background
                    in
                    Color.Manipulate.weightedMix Color.white (Color.rgba red green blue alpha) introAlpha

                _ ->
                    case inGameModel.replay of
                        Recording _ ->
                            Color.rgba 0 0 0 0

                        Replaying replaying ->
                            missionEndBackgroundAlpha replaying inGameTime |> Color.rgba 0.3 0.3 0.3

        firstAttemptIntro_ =
            if showIntro inGameModel then
                IntroAnimation.view
                    (useGlitchedIntro inGameModel.replayable)
                    inGameModel.missionNumber
                    config.windowSize
                    config.time
                    inGameModel.introStartTime

            else
                Element.none

        cameraTransform_ =
            cameraTransform (getCameraZ config.time inGameModel) inGameModel.replayable

        perspectiveMatrix mat =
            if useGlitchedIntro inGameModel.replayable && showIntro inGameModel then
                IntroAnimation.cameraWonkiness inGameModel.introStartTime
                    |> Keyframe.valueAt_ config.time
                    |> glitchMatrix mat

            else
                mat
    in
    Element.el
        [ Element.width Element.fill
        , Element.height Element.fill
        , Element.inFront <| ifElse showHud hudOverlay Element.none
        , Element.inFront <| ifElse showHud firstAttemptIntro_ Element.none
        , Element.inFront <| ifElse showTopMenu (inGameTopMenu msgConfig config inGameModel) Element.none
        , if firstFewSecondsOfGame then
            Helper.noneAttribute

          else
            Background.color <| Helper.colorToElement config.colorPalette.background
        ]
        (webGLView
            config
            input
            cameraTransform_
            overlayColor
            inGameModel
            inGameModel.replayable
            perspectiveMatrix
            showPlayer
            showMap
        )


restartReplayButton : msg -> Element msg
restartReplayButton restartReplayMsg =
    let
        height =
            70

        width =
            height * 7 / 8

        arc =
            Arc2d.sweptAround (Point2d.fromCoordinates ( width / 2, height / 2 )) (-pi * 1.7) (Point2d.fromCoordinates ( width * 0.77, height * 0.73 ))

        arcSvg =
            Draw.arc Color.black 10 arc

        arrow =
            Draw.arrow Color.black 0 15 (Point2d.fromCoordinates ( width * 0.1, height * 0.8 )) (Point2d.fromCoordinates ( width * 0.7, height * 0.8 ))
    in
    Input.button
        []
        { onPress = restartReplayMsg |> Just
        , label =
            Svg.svg
                [ width |> String.fromFloat |> flip (++) "px" |> Svg.Attributes.width
                , height |> String.fromFloat |> flip (++) "px" |> Svg.Attributes.height
                ]
                [ arcSvg, arrow ]
                |> Element.html
        }


saveReplayButton : MsgConfig msg -> Replaying_ -> Element msg
saveReplayButton msgConfig replaying =
    let
        height =
            60

        centerX =
            width / 2

        width =
            height * 7 / 8

        bottomLineThickness =
            6

        bottomLine =
            Draw.lineStrip
                Color.black
                bottomLineThickness
                [ Point2d.fromCoordinates ( 0, height - bottomLineThickness / 2 )
                , Point2d.fromCoordinates ( width, height - bottomLineThickness / 2 )
                ]

        arrow =
            Draw.arrow Color.black 10 centerX (Point2d.fromCoordinates ( centerX, 0 )) (Point2d.fromCoordinates ( centerX, height * 7 / 8 ))
    in
    Input.button
        []
        { onPress = msgConfig.saveReplay replaying.replay |> Just
        , label =
            Svg.svg
                [ width |> String.fromFloat |> flip (++) "px" |> Svg.Attributes.width
                , height |> String.fromFloat |> flip (++) "px" |> Svg.Attributes.height
                ]
                [ bottomLine, arrow ]
                |> Element.html
        }


fastForwardButton : msg -> Quantity Float RealTime -> { a | fastForward : Bool, fastForwardClickTime : Quantity Float RealTime } -> Element msg
fastForwardButton toggleFastForwardMsg time replaying =
    let
        height =
            60

        polygon xOffset =
            Polygon2d.singleLoop
                [ Point2d.fromCoordinates ( xOffset, 0 )
                , Point2d.fromCoordinates ( xOffset + height * 3 / 4, height / 2 )
                , Point2d.fromCoordinates ( xOffset, height )
                ]

        triangle xOffset =
            Draw.polygon
                Color.black
                (polygon xOffset)

        offsetAnimation from to =
            Keyframe.startAt_ replaying.fastForwardClickTime from
                |> Keyframe.add_ Keyframe.InOutExpo (RealTime.seconds 0.1) to
                |> Keyframe.valueAt_ time

        offset =
            height * (15 / 80)

        icon =
            if replaying.fastForward then
                [ triangle <| offsetAnimation 0 offset
                , triangle <| offsetAnimation (offset * 2) offset
                ]

            else
                [ triangle <| offsetAnimation offset 0
                , triangle <| offsetAnimation offset (offset * 2)
                ]
    in
    Input.button
        []
        { onPress = Just toggleFastForwardMsg
        , label =
            Svg.svg
                [ offset * 2 + height * 3 / 4 |> String.fromFloat |> flip (++) "px" |> Svg.Attributes.width
                , height |> String.fromFloat |> flip (++) "px" |> Svg.Attributes.height
                ]
                icon
                |> Element.html
        }


{-| Distance the player needs to travel along the current wire before it reaches a chip.
-}
chipEntryDistance : Core.Model -> Maybe Float
chipEntryDistance model =
    case (Core.getPlayer model).position of
        Circuit.WirePosition { wireId, t, data } ->
            let
                circuit =
                    Core.getCircuit model
            in
            case Circuit.getWire circuit wireId of
                Just wire ->
                    case ( wire.start, wire.end, data.direction ) of
                        ( _, Just _, Forward ) ->
                            Wire.tDistance circuit t (Wire.tMax circuit wire) wire |> Just

                        ( Just _, _, Backward ) ->
                            Wire.tDistance circuit Wire.tMin t wire |> Just

                        ( _, _, _ ) ->
                            Nothing

                Nothing ->
                    Nothing

        Circuit.ChipPosition _ ->
            Nothing


clockCycleView : Core.Model -> Element msg
clockCycleView coreModel =
    let
        width =
            140

        height =
            30

        secondsPerCycle =
            ChipMoveTime.cycleMaxLength |> InGameTime.inSeconds

        time =
            Core.getTime coreModel

        clockBar =
            Element.el
                [ Background.color <| Element.rgb 0 0 0
                , Element.width <| Element.px 3
                , Element.height <| Element.px height
                , Element.moveRight
                    ((Core.getMoveOnChip coreModel).frameNumber
                        |> FrameNumber.time Core.framesPerSecond
                        |> flip Quantity.minus time
                        |> InGameTime.inSeconds
                        |> (*) (width / secondsPerCycle)
                    )
                ]
                Element.none

        previousMoves : List (Element.Attribute msg)
        previousMoves =
            Core.getAllChipMoves coreModel
                |> List.take 10
                |> List.map
                    (\chipMove ->
                        let
                            alpha_ =
                                time |> Quantity.minus chipMove.moveTime |> InGameTime.inSeconds |> (*) 1.5 |> (-) 1
                        in
                        if alpha_ <= 0 then
                            Element.inFront Element.none

                        else
                            Element.el
                                [ Background.color <|
                                    if ChipMoveTime.chipMoveTiming chipMove == ChipMoveTime.OnTime then
                                        Element.rgb 0 1 0

                                    else
                                        Element.rgb 1 0 0
                                , Element.width <| Element.px 3
                                , Element.height <| Element.px height
                                , Element.alpha alpha_
                                , Element.moveRight
                                    (chipMove.relativeToCycle
                                        |> CycleTime.inSeconds
                                        |> (*) (width / secondsPerCycle)
                                    )
                                ]
                                Element.none
                                |> Element.inFront
                    )

        goodTimingMarkerColor =
            case Core.chipMoveTiming time coreModel of
                ChipMoveTime.OnTime ->
                    Element.rgb 0 1 0

                _ ->
                    Element.rgb 0 0.9 0

        goodTimingMarker =
            marker
                goodTimingMarkerColor
                (InGameTime.inSeconds ChipMoveTime.moveEarlyDuration / secondsPerCycle)
                ((ChipMoveTime.moveLateDuration |> Quantity.minus ChipMoveTime.moveEarlyDuration |> InGameTime.inSeconds) / secondsPerCycle)

        marker : Element.Color -> Float -> Float -> Element msg
        marker color offset size =
            Element.el
                [ Background.color color
                , Element.width <| Element.px <| round (width * size)
                , Element.height <| Element.px height
                , Element.moveRight (width * offset)
                ]
                Element.none

        alpha =
            case chipEntryDistance coreModel of
                Just distance ->
                    2 - distance |> clamp 0 1

                Nothing ->
                    case (Core.getPlayer coreModel).position of
                        ChipPosition _ ->
                            1

                        WirePosition _ ->
                            0

        bar =
            Element.el
                [ Background.color <| Element.rgb 1 1 1
                , Element.inFront goodTimingMarker
                , Element.width Element.fill
                , Element.height Element.fill
                , Element.alpha 0.5
                ]
                Element.none
    in
    Element.column
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        [ fillerPortion 6
        , Element.el
            ([ Element.width <| Element.px width
             , Element.height <| Element.px height
             , Element.alignBottom
             , Element.centerX
             , Element.inFront clockBar
             , Element.alpha alpha
             ]
                ++ previousMoves
            )
            bar
        , fillerPortion 4
        ]


isMenuMission : Replayable -> Bool
isMenuMission replayable =
    case replayable.mission of
        MenuMission _ ->
            True

        _ ->
            False


chipsRemainingView : Replayable -> Element msg
chipsRemainingView replayable =
    let
        chipsLeft =
            Core.chipsRemaining replayable.coreState

        chipsTotal =
            Circuit.getChips (Core.getCircuit replayable.coreState) |> List.length

        text =
            String.fromInt chipsLeft ++ "/" ++ String.fromInt chipsTotal
    in
    Element.row
        [ Element.spacing 15 ]
        [ Element.image [ Element.width <| Element.px 40 ] { src = "images/chips_left_icon.svg", description = "" }
        , Element.el
            [ Font.size 60
            , Element.alignTop
            ]
            (Element.text text)
        ]


inGameTopMenu : MsgConfig msg -> Config a -> InGameModel -> Element msg
inGameTopMenu msgConfig config inGameModel =
    Element.row
        [ Style.menuBackground
        , Element.alignTop
        , Element.alignLeft
        , Border.roundEach { topLeft = 0, bottomLeft = 0, bottomRight = 4, topRight = 0 }
        ]
        [ Input.button
            Style.menuFormButton
            { onPress = Just msgConfig.backToStageSelect
            , label =
                let
                    underscoreWidth =
                        case inGameModel.backToStageSelectHotkey of
                            Just startTime ->
                                Keyframe.startAt_ startTime 32
                                    |> Keyframe.add_ Keyframe.Linear restartHotkeyDelay 67
                                    |> Keyframe.valueAt_ config.time

                            Nothing ->
                                32
                in
                Element.el
                    [ Element.padding 4
                    , Element.inFront <|
                        Element.el
                            [ Element.width <| Element.px <| round underscoreWidth
                            , Element.height <| Element.px 2
                            , Background.color <| Element.rgb 1 1 1
                            , Element.moveRight 5
                            , Element.moveDown 23
                            ]
                            Element.none
                    ]
                <|
                    Element.text "Escape"
            }
        , Input.button
            (Border.roundEach { topLeft = 0, bottomLeft = 0, bottomRight = 4, topRight = 0 } :: Style.menuFormButton)
            { onPress = Just (msgConfig.internalMsg RestartMission)
            , label =
                let
                    underscoreWidth =
                        case inGameModel.restartHotkey of
                            Just startTime ->
                                Keyframe.startAt_ startTime 14
                                    |> Keyframe.add_ Keyframe.Linear restartHotkeyDelay 72
                                    |> Keyframe.valueAt_ config.time

                            Nothing ->
                                14
                in
                Element.el
                    [ Element.padding 4
                    , Element.inFront <|
                        Element.el
                            [ Element.width <| Element.px <| round underscoreWidth
                            , Element.height <| Element.px 2
                            , Background.color <| Element.rgb 1 1 1
                            , Element.moveRight 5
                            , Element.moveDown 23
                            ]
                            Element.none
                    ]
                <|
                    Element.text "Restart"
            }
        ]


arrowKeysView : Bool -> Bool -> Bool -> Bool -> Element msg
arrowKeysView highlightLeft highlightRight highlightUp highlightDown =
    let
        rectangle visible ( x, y ) =
            if visible then
                Element.el
                    [ Background.color <| Element.rgb 0.3 1 0.3
                    , Element.width <| Element.px 42
                    , Element.height <| Element.px 42
                    , Element.moveRight x
                    , Element.moveDown y
                    ]
                    Element.none

            else
                Element.none
    in
    Element.image
        [ Element.centerX
        , Element.alignBottom
        , Element.moveDown 10
        , Element.width <| Element.px 200
        , Element.behindContent <| rectangle highlightUp ( 78, 46 )
        , Element.behindContent <| rectangle highlightDown ( 78, 112 )
        , Element.behindContent <| rectangle highlightLeft ( 20, 112 )
        , Element.behindContent <| rectangle highlightRight ( 136, 112 )
        ]
        { src = "images/arrow_keys.svg", description = "" }


type alias Particle =
    { alpha : Float
    , scale : Float
    , position : Point2d
    }


drawParticles : Config a -> DrawConfig -> Replayable -> Bool -> List Entity
drawParticles config { perspectiveMat, cameraBounds } replayable showPlayer =
    let
        drawBatch : Color -> Array Particle -> Entity
        drawBatch color batch =
            drawEntity
                False
                Shaders.glowParticlesVS
                Shaders.fragmentShader
                glowParticles
                (glowParticlesUniforms perspectiveMat batch color)

        player =
            Core.getPlayer replayable.coreState

        introParticles : List Entity
        introParticles =
            case ( Core.agentWorldPosition replayable.coreState player.position, showPlayer ) of
                ( Just playerPos, True ) ->
                    drawPlayerIntroParticles playerPos replayable
                        |> List.greedyGroupsOf instanceCount
                        |> List.map Array.fromList
                        |> List.map (drawBatch <| Color.Manipulate.lighten 0.1 config.colorPalette.player)

                ( _, _ ) ->
                    []
    in
    replayable.particles
        |> List.filter
            (\{ position } ->
                case cameraBounds of
                    Just cameraBounds_ ->
                        pointInConvexPolygon cameraBounds_ position

                    Nothing ->
                        True
            )
        |> List.gatherEqualsBy .color
        |> List.concatMap
            (\( first, rest ) ->
                let
                    agentParticleToParticle : AgentParticle -> Particle
                    agentParticleToParticle { position, startTime, color } =
                        let
                            t =
                                (Core.getTime replayable.coreState |> Quantity.minus startTime |> InGameTime.inSeconds) / InGameTime.inSeconds particleLifespan

                            scale =
                                case color of
                                    PlayerColor ->
                                        if showPlayer then
                                            playerScale replayable

                                        else
                                            0

                                    _ ->
                                        1
                        in
                        { position = position, scale = (1 - t) * scale, alpha = t }

                    batches : List (Array Particle)
                    batches =
                        first
                            :: rest
                            |> List.map agentParticleToParticle
                            |> List.greedyGroupsOf instanceCount
                            |> List.map Array.fromList

                    agentColor =
                        case first.color of
                            PlayerColor ->
                                config.colorPalette.player

                            AgentPassiveColor ->
                                config.colorPalette.passiveAgent

                            AgentChaserColor ->
                                config.colorPalette.chaserAgent
                in
                List.map (drawBatch agentColor) batches
            )
        |> (++) introParticles


cameraWorldBounds : Point3d -> Mat4 -> Maybe Polygon2d
cameraWorldBounds cameraPosition perspectiveMat =
    let
        clipBounds =
            [ Vec3.vec3 -1 -1 0
            , Vec3.vec3 1 -1 0
            , Vec3.vec3 1 1 0
            , Vec3.vec3 -1 1 0
            ]

        plane =
            SketchPlane3d.throughPoints
                Point3d.origin
                (Point3d.fromCoordinates ( 1, 0, 0 ))
                (Point3d.fromCoordinates ( 0, 1, 0 ))
    in
    case ( Mat4.inverse perspectiveMat, plane ) of
        ( Just mat, Just plane_ ) ->
            let
                worldPoints =
                    clipBounds
                        |> List.map
                            (\v ->
                                linePlaneIntersection
                                    cameraPosition
                                    (Mat4.transform mat v |> Point3d.fromVec3 |> Vector3d.from cameraPosition)
                                    plane_
                            )
                        |> List.filterMap identity
            in
            case worldPoints of
                a :: b :: c :: d :: [] ->
                    Polygon2d.singleLoop [ a, b, c, d ] |> Just

                _ ->
                    Nothing

        ( _, _ ) ->
            Nothing


linePlaneIntersection : Point3d -> Vector3d -> SketchPlane3d -> Maybe Point2d
linePlaneIntersection rayOrigin rayVector plane =
    let
        normal =
            SketchPlane3d.normalDirection plane |> Direction3d.toVector

        rayVectorNormalized =
            Vector3d.normalize rayVector
    in
    if Vector3d.dotProduct normal rayVectorNormalized == 0 then
        Nothing

    else
        let
            origin =
                SketchPlane3d.originPoint plane |> Vector3d.from Point3d.origin

            d =
                Vector3d.dotProduct normal origin

            distance =
                (d - Vector3d.dotProduct normal (Vector3d.from Point3d.origin rayOrigin))
                    / Vector3d.dotProduct normal rayVectorNormalized
        in
        rayOrigin
            |> Point3d.translateBy (Vector3d.scaleBy distance rayVectorNormalized)
            |> Point3d.projectInto plane
            |> Just


drawChipTiles : Config a -> DrawConfig -> Core.Model -> List Entity
drawChipTiles config { perspectiveMat } coreState =
    let
        seed =
            Core.getTime coreState |> Helper.randomFromTime

        jitter : Point2d -> Random.Generator Point2d
        jitter point =
            Random.map2 (\x y -> Vector2d.fromComponents ( x, y ) |> flip Point2d.translateBy point)
                (Random.float -0.02 0.02)
                (Random.float -0.02 0.02)

        tiles =
            Core.getCircuit coreState
                |> Circuit.getChips
                |> List.concatMap
                    (\( _, chip ) ->
                        chip.hackedPoints
                            |> Set.toList
                            |> List.map (Point2i.fromTuple >> Chip.localToWorld chip)
                            |> List.foldr
                                (\tilePos ( tileList, currentSeed ) ->
                                    if Chip.isAlmostHacked chip then
                                        Random.step (jitter tilePos) currentSeed |> Tuple.mapFirst (\a -> a :: tileList)

                                    else
                                        ( tilePos :: tileList, currentSeed )
                                )
                                ( [], seed )
                            |> Tuple.first
                    )

        batches =
            tiles
                |> List.greedyGroupsOf instanceCount
                |> List.map Array.fromList

        drawBatch batch =
            drawEntity
                True
                Shaders.chipTileVS
                Shaders.fragmentShader
                chipTileInstances
                (chipTileUniforms config perspectiveMat coreState batch)
    in
    List.map drawBatch batches


drawHackSprites : DrawConfig -> Replayable -> SpriteSheet -> List Entity
drawHackSprites { perspectiveMat, camera } replayable spriteSheet =
    let
        rotateBy =
            Direction2d.angleFrom (Direction2d.fromAngle (pi / 2)) camera.rotation

        faceCameraMat =
            Mat4.identity
                |> Mat4.rotate rotateBy (Vec3.vec3 0 0 1)
                |> Mat4.rotate (-pi / 2 + replayable.debugCameraLatitude) (Vec3.vec3 -1 0 0)
                |> Mat4.scale (Vec3.vec3 3 3 3)

        hackedChips =
            Core.getCircuit replayable.coreState
                |> Circuit.getChips
                |> List.filter (Tuple.second >> Chip.isHacked)
                |> List.map
                    (\( _, chip ) ->
                        let
                            pos =
                                Vector2i.toVector2d chip.size
                                    |> Vector2d.scaleBy 0.5
                                    |> flip Point2d.translateBy Point2d.origin
                                    |> Chip.localToWorld_ chip
                                    |> (\p ->
                                            let
                                                ( x, y ) =
                                                    Point2d.coordinates p
                                            in
                                            Vec3.vec3 x y 0.5
                                       )
                        in
                        Mat4.mul (Mat4.makeTranslate pos) faceCameraMat
                    )

        batches =
            hackedChips
                |> List.greedyGroupsOf instanceCount
                |> List.map Array.fromList

        drawBatch batch =
            drawEntity
                False
                Shaders.hackSpriteVS
                Shaders.hackSpriteFS
                hackSpriteInstances
                (hackSpriteUniforms perspectiveMat replayable spriteSheet batch)
    in
    List.map drawBatch batches


playerScale : Replayable -> Float
playerScale inGameModel =
    let
        time =
            Core.getTime inGameModel.coreState
    in
    if time |> Quantity.lessThan (InGameTime.seconds 1.5) then
        Keyframe.startAt_ (InGameTime.seconds 1) 0
            |> Keyframe.add_ Keyframe.OutElastic (InGameTime.seconds 1.5) 1
            |> Keyframe.valueAt_ time

    else
        agentScaleAnim (Core.getChipMoveStart inGameModel.coreState) |> Keyframe.valueAt_ time


drawPlayer : Config a -> DrawConfig -> Replayable -> List Entity
drawPlayer config { perspectiveMat } replayable =
    let
        player =
            Core.getPlayer replayable.coreState

        playerPos =
            Core.agentWorldPosition replayable.coreState player.position

        playerDamageColor =
            Color.toRgba config.colorPalette.playerDamage

        playerColor =
            case player.health of
                Agent.Damaged { lastDamaged, health } ->
                    let
                        timeSinceDamage =
                            Core.getTime replayable.coreState |> Quantity.minus lastDamaged

                        (Quantity.Quantity rawTimeSinceDamage) =
                            timeSinceDamage
                    in
                    if
                        timeSinceDamage
                            |> Quantity.lessThan Agent.damagedRecentlyDuration
                            |> (&&) (Basics.fractionalModBy 0.4 rawTimeSinceDamage > 0.15)
                            |> (&&) (health > 0)
                    then
                        Color.rgba
                            playerDamageColor.red
                            playerDamageColor.green
                            playerDamageColor.blue
                            0.8

                    else
                        config.colorPalette.player

                Agent.MaxHealth ->
                    config.colorPalette.player
    in
    case playerPos of
        Just pos ->
            drawEntity
                False
                Shaders.playerVS
                Shaders.playerFS
                playerMesh
                { perspective = perspectiveMat
                , color = Mesh.colorToVec4 playerColor
                , offset =
                    Vec3.vec3
                        (Point2d.xCoordinate pos)
                        (Point2d.yCoordinate pos)
                        (playerScale replayable)
                , health = Agent.lazyHealth (Core.getTime replayable.coreState) player |> (*) 0.333
                , spriteSheet = config.spriteSheet.texture
                , uvTransform = SpriteSheet.uvMatrix "angle_gradient" config.spriteSheet
                }
                |> List.singleton

        Nothing ->
            []


pointInConvexPolygon : Polygon2d -> Point2d -> Bool
pointInConvexPolygon polygon point =
    case Polygon2d.outerLoop polygon of
        head :: rest ->
            (head :: rest ++ [ head ])
                |> List.pairwise
                |> List.all
                    (\( first, second ) ->
                        let
                            v =
                                Vector2d.from first second

                            vPerpendicular =
                                Vector2d.perpendicularTo v

                            relative =
                                Vector2d.from first point
                        in
                        Vector2d.dotProduct vPerpendicular relative > 0
                    )

        _ ->
            False


agentScaleAnim : Quantity Float InGameTime -> Keyframe.Animation InGameTime Unitless
agentScaleAnim startTime =
    Keyframe.startAt_ startTime 1.1
        |> Keyframe.add_ Keyframe.Linear (InGameTime.seconds 0.2) 1


drawAgents : Config a -> Mesh Shaders.GlowParticleVertex -> DrawConfig -> Replayable -> List Entity
drawAgents config mesh ({ cameraBounds } as drawConfig) replayable =
    let
        coreState =
            replayable.coreState

        agentScale =
            agentScaleAnim (Core.getChipMoveStart coreState) |> Keyframe.valueAt_ (Core.getTime coreState)
    in
    Core.getAgents coreState
        |> List.filterMap
            (\agent ->
                case Agent.getPosition agent |> Core.agentWorldPosition coreState of
                    Just position ->
                        if
                            cameraBounds
                                |> Maybe.map (\cameraBounds_ -> pointInConvexPolygon cameraBounds_ position)
                                |> Maybe.withDefault True
                        then
                            case agent of
                                PassiveAgent _ ->
                                    Just
                                        ( config.colorPalette.passiveAgent |> Color.Blending.multiply (Color.rgba 0.9 0.9 0.9 1)
                                        , position
                                        )

                                ChaserAgent _ ->
                                    Just
                                        ( config.colorPalette.chaserAgent |> Color.Blending.multiply (Color.rgba 0.9 0.9 0.9 1)
                                        , position
                                        )

                        else
                            Nothing

                    Nothing ->
                        Nothing
            )
        |> List.gatherEqualsBy Tuple.first
        |> List.concatMap
            (\( ( color, _ ) as first, rest ) ->
                let
                    batches : List (Array ( Point2d, Float ))
                    batches =
                        first
                            :: rest
                            |> List.map (\( _, pos ) -> ( pos, agentScale ))
                            |> List.greedyGroupsOf instanceCount
                            |> List.map Array.fromList

                    drawBatch batch =
                        drawEntity
                            False
                            Shaders.agentVS
                            Shaders.fragmentShader
                            mesh
                            (agentUniforms drawConfig batch color)
                in
                List.map drawBatch batches
            )


type alias DrawConfig =
    { perspectiveMat : Mat4
    , cameraBounds : Maybe Polygon2d
    , camera : Transform
    }


drawEntity depthTest =
    WebGL.entityWith
        [ Setting.cullFace Setting.back
        , if depthTest then
            Depth.lessOrEqual { write = True, near = 0, far = 1 }

          else
            Depth.always { write = True, near = 0, far = 1 }
        , Blend.add Blend.one Blend.oneMinusSrcAlpha
        ]


playerMesh : Mesh Shaders.PlayerVertex
playerMesh =
    Mesh.glow 12
        |> Mesh.map
            (\{ position, alpha, uvPosition } ->
                let
                    ( x, y ) =
                        Point2d.coordinates position

                    radius =
                        0.6
                in
                { position = Vec3.vec3 (x * radius) (y * radius) 0.2
                , alpha = alpha * 2
                , uv = Point2d.toVec2 uvPosition
                }
            )
        |> Mesh.toMesh


agentInstances : Mesh Shaders.GlowParticleVertex
agentInstances =
    List.repeat instanceCount (Mesh.glow 12)
        |> List.indexedMap
            (\index mesh ->
                Mesh.map
                    (\{ position, alpha } ->
                        let
                            ( x, y ) =
                                Point2d.coordinates position

                            radius =
                                0.6
                        in
                        { position = Vec3.vec3 (x * radius) (y * radius) 0.2
                        , instanceIndex = toFloat index
                        , alpha = alpha * 2
                        }
                    )
                    mesh
            )
        |> Mesh.Compound
        |> Mesh.toMesh


agentMapOverlayInstances : Mesh Shaders.GlowParticleVertex
agentMapOverlayInstances =
    List.repeat instanceCount (Mesh.circle 8)
        |> List.indexedMap
            (\index mesh ->
                Mesh.map
                    (\position ->
                        let
                            ( x, y ) =
                                Point2d.coordinates position

                            radius =
                                0.5
                        in
                        { position = Vec3.vec3 (x * radius) (y * radius) 0
                        , instanceIndex = toFloat index
                        , alpha = 1
                        }
                    )
                    mesh
            )
        |> Mesh.Compound
        |> Mesh.toMesh


glowParticles : Mesh Shaders.GlowParticleVertex
glowParticles =
    List.repeat instanceCount (Mesh.glow 12)
        |> List.indexedMap
            (\index mesh ->
                Mesh.map
                    (\{ position, alpha } ->
                        let
                            ( x, y ) =
                                Point2d.coordinates position

                            radius =
                                0.6
                        in
                        { position = Vec3.vec3 (x * radius) (y * radius) 0.1
                        , instanceIndex = toFloat index
                        , alpha = alpha
                        }
                    )
                    mesh
            )
        |> Mesh.Compound
        |> Mesh.toMesh


cameraTransform : Float -> Replayable -> Transform
cameraTransform cameraZ replayable =
    let
        inGameTime =
            Core.getTime replayable.coreState

        { position, rotationZ } =
            case replayable.mission of
                MenuMission menuMission ->
                    { position =
                        LineSegment2d.interpolate
                            menuMission.cameraPath
                            ((inGameTime |> Quantity.minus menuMission.startTime |> InGameTime.inSeconds) / 4)
                    , rotationZ = menuMission.cameraRotation
                    }

                _ ->
                    { position =
                        Core.agentWorldPosition replayable.coreState (Core.getPlayer replayable.coreState).position
                            |> Maybe.withDefault Point2d.origin
                            |> Point2d.translateBy replayable.cameraOffset
                    , rotationZ = replayable.cameraRotation
                    }

        rotateBy =
            Direction2d.rotateBy replayable.debugCameraLongitude rotationZ
                |> Direction2d.angleFrom (Direction2d.fromAngle 0)

        eye =
            let
                ( x, y ) =
                    Point2d.coordinates position
            in
            Point3d.fromCoordinates ( 0, 0, cameraZ )
                |> Point3d.rotateAround Axis3d.y (-pi / 2 + replayable.debugCameraLatitude)
                |> Point3d.rotateAround Axis3d.z (replayable.debugCameraLongitude + rotateBy)
                |> Point3d.translateBy (Vector3d.fromComponents ( x, y, 0 ))
    in
    { position = eye, rotation = rotationZ, lookAt = position }


cameraMatrix : Transform -> Mat4
cameraMatrix { position, lookAt } =
    Mat4.makeLookAt
        (Point3d.toVec3 position)
        (lookAt |> Point2d.coordinates |> (\( x, y ) -> Vec3.vec3 x y 0))
        (Vec3.vec3 0 0 1)


perspective : Vector2i -> Mat4
perspective windowSize =
    Mat4.mul
        (Mat4.makeScale (Vec3.vec3 -1 1 1))
        (Mat4.makePerspective
            60
            (Vector2i.aspectRatio windowSize |> Maybe.withDefault 1)
            0.1
            100
        )


circuitUniforms : Config a -> DrawConfig -> Shaders.CircuitUniforms
circuitUniforms config { perspectiveMat } =
    { perspective = perspectiveMat
    , chipColor = Mesh.colorToVec4 config.colorPalette.chip
    , chipLegColor = Mesh.colorToVec4 config.colorPalette.chipLegs
    , wireMiddleColor = Mesh.colorToVec4 config.colorPalette.wireMiddle
    , wireEdgeColor = Mesh.colorToVec4 config.colorPalette.wireEdge
    }


mapOverlayUniforms : Config a -> Circuit -> DrawConfig -> Shaders.MapOverlayUniforms
mapOverlayUniforms config circuit { perspectiveMat } =
    { perspective = perspectiveMat
    , hackedChips =
        Circuit.getChips circuit
            |> List.map (\( _, chip ) -> Chip.isHacked chip)
            |> List.foldr
                (\isHacked value ->
                    if isHacked then
                        value |> Bitwise.shiftLeftBy 1 |> Bitwise.or 1

                    else
                        value |> Bitwise.shiftLeftBy 1
                )
                0
    , chipColor = config.colorPalette.mapChip |> Helper.setAlpha 0.5 |> Mesh.colorToVec4
    , chipHackedColor = config.colorPalette.mapChipHacked |> Helper.setAlpha 0.5 |> Mesh.colorToVec4
    , wireColor = config.colorPalette.mapWire |> Mesh.colorToVec4
    }


agentUniforms : DrawConfig -> Array ( Point2d, Float ) -> Color -> Shaders.AgentUniforms
agentUniforms { perspectiveMat } offsets color =
    let
        getOffset index =
            Array.get index offsets
                |> Maybe.map (\( p, s ) -> Vec3.vec3 (Point2d.xCoordinate p) (Point2d.yCoordinate p) s)
                |> Maybe.withDefault (Vec3.vec3 -999999 -999999 0)
    in
    { perspective = perspectiveMat
    , color = Mesh.colorToVec4 color
    , instanceOffset0 = getOffset 0
    , instanceOffset1 = getOffset 1
    , instanceOffset2 = getOffset 2
    , instanceOffset3 = getOffset 3
    , instanceOffset4 = getOffset 4
    , instanceOffset5 = getOffset 5
    , instanceOffset6 = getOffset 6
    , instanceOffset7 = getOffset 7
    , instanceOffset8 = getOffset 8
    , instanceOffset9 = getOffset 9
    , instanceOffset10 = getOffset 10
    , instanceOffset11 = getOffset 11
    , instanceOffset12 = getOffset 12
    , instanceOffset13 = getOffset 13
    , instanceOffset14 = getOffset 14
    , instanceOffset15 = getOffset 15
    , instanceOffset16 = getOffset 16
    , instanceOffset17 = getOffset 17
    , instanceOffset18 = getOffset 18
    , instanceOffset19 = getOffset 19
    }


glowParticlesUniforms : Mat4 -> Array Particle -> Color -> Shaders.GlowParticlesUniforms
glowParticlesUniforms perspectiveMat particles color =
    let
        getOffset index =
            Array.get index particles
                |> Maybe.map
                    (\{ position, alpha, scale } ->
                        Vec4.vec4
                            (Point2d.xCoordinate position)
                            (Point2d.yCoordinate position)
                            scale
                            alpha
                    )
                |> Maybe.withDefault (Vec4.vec4 -999999 -999999 0 0)
    in
    { perspective = perspectiveMat
    , color = Mesh.colorToVec4 color
    , instanceOffset0 = getOffset 0
    , instanceOffset1 = getOffset 1
    , instanceOffset2 = getOffset 2
    , instanceOffset3 = getOffset 3
    , instanceOffset4 = getOffset 4
    , instanceOffset5 = getOffset 5
    , instanceOffset6 = getOffset 6
    , instanceOffset7 = getOffset 7
    , instanceOffset8 = getOffset 8
    , instanceOffset9 = getOffset 9
    , instanceOffset10 = getOffset 10
    , instanceOffset11 = getOffset 11
    , instanceOffset12 = getOffset 12
    , instanceOffset13 = getOffset 13
    , instanceOffset14 = getOffset 14
    , instanceOffset15 = getOffset 15
    , instanceOffset16 = getOffset 16
    , instanceOffset17 = getOffset 17
    , instanceOffset18 = getOffset 18
    , instanceOffset19 = getOffset 19
    }


chipCapsUniforms : Config a -> DrawConfig -> Replayable -> Bool -> Shaders.ChipCapsUniforms
chipCapsUniforms config { perspectiveMat } replayable showPlayer =
    let
        inGameTime =
            Core.getTime replayable.coreState

        ( prevId, prevTime ) =
            case replayable.chipLastExited of
                Just ( ChipId chipId, chipTime ) ->
                    ( chipId, InGameTime.inSeconds chipTime )

                Nothing ->
                    ( -1, -1 )
    in
    { perspective = perspectiveMat
    , previousChipId = ifElse showPlayer prevId -1
    , currentChipId =
        if showPlayer then
            case (Core.getPlayer replayable.coreState).position of
                ChipPosition { chipId } ->
                    chipId |> (\(ChipId a) -> a)

                WirePosition _ ->
                    -1

        else
            -1
    , chipEnterTime = replayable.chipLastEntered |> Maybe.withDefault inGameTime |> InGameTime.inSeconds
    , chipLeaveTime = prevTime
    , time = InGameTime.inSeconds inGameTime
    , color = config.colorPalette.chip |> Mesh.colorToVec4
    }


instanceCount =
    20


chipTileInstances : Mesh Shaders.ChipTileVertex
chipTileInstances =
    List.repeat instanceCount Mesh.square
        |> List.indexedMap
            (\index mesh ->
                Mesh.map
                    (\v ->
                        let
                            ( x, y ) =
                                Point2d.coordinates v
                        in
                        { position = Vec3.vec3 (x - 0.5) (y - 0.5) (Mesh.chipInsideHeight + 0.02)
                        , instanceIndex = toFloat index
                        }
                    )
                    mesh
            )
        |> Mesh.Compound
        |> Mesh.toMesh


chipTileUniforms : Config a -> Mat4 -> Core.Model -> Array Point2d -> Shaders.ChipTileUniforms
chipTileUniforms config perspectiveMat coreState particles =
    let
        getOffset index =
            Array.get index particles
                |> Maybe.map
                    (\position ->
                        Vec3.vec3 (Point2d.xCoordinate position) (Point2d.yCoordinate position) 0
                    )
                |> Maybe.withDefault (Vec3.vec3 -999999 -999999 0)
    in
    { perspective = perspectiveMat
    , time = Core.getTime coreState |> InGameTime.inSeconds
    , color = config.colorPalette.chipHacked |> Helper.setAlpha 0.5 |> Mesh.colorToVec4
    , instanceOffset0 = getOffset 0
    , instanceOffset1 = getOffset 1
    , instanceOffset2 = getOffset 2
    , instanceOffset3 = getOffset 3
    , instanceOffset4 = getOffset 4
    , instanceOffset5 = getOffset 5
    , instanceOffset6 = getOffset 6
    , instanceOffset7 = getOffset 7
    , instanceOffset8 = getOffset 8
    , instanceOffset9 = getOffset 9
    , instanceOffset10 = getOffset 10
    , instanceOffset11 = getOffset 11
    , instanceOffset12 = getOffset 12
    , instanceOffset13 = getOffset 13
    , instanceOffset14 = getOffset 14
    , instanceOffset15 = getOffset 15
    , instanceOffset16 = getOffset 16
    , instanceOffset17 = getOffset 17
    , instanceOffset18 = getOffset 18
    , instanceOffset19 = getOffset 19
    }


hackSpriteUniforms : Mat4 -> Replayable -> SpriteSheet -> Array Mat4 -> Shaders.HackSpriteUniforms
hackSpriteUniforms perspectiveMat replayable spriteSheet offsets =
    let
        getOffset index =
            Array.get index offsets
                |> Maybe.withDefault (Mat4.makeTranslate (Vec3.vec3 -999999 -999999 0))

        inGameTime =
            Core.getTime replayable.coreState

        spriteName =
            if Basics.fractionalModBy 3 (InGameTime.inSeconds inGameTime) > 2.3 then
                "chip_hacked_wink"

            else
                "chip_hacked"

        ( alphaOffset, _ ) =
            Random.step (Random.float -0.1 0.1) (Helper.randomFromTime inGameTime)
    in
    { perspective = perspectiveMat
    , spriteSheet = spriteSheet.texture
    , uvTransform = SpriteSheet.uvMatrix spriteName spriteSheet
    , alpha = 0.5 + alphaOffset
    , instanceOffset0 = getOffset 0
    , instanceOffset1 = getOffset 1
    , instanceOffset2 = getOffset 2
    , instanceOffset3 = getOffset 3
    , instanceOffset4 = getOffset 4
    , instanceOffset5 = getOffset 5
    , instanceOffset6 = getOffset 6
    , instanceOffset7 = getOffset 7
    , instanceOffset8 = getOffset 8
    , instanceOffset9 = getOffset 9
    , instanceOffset10 = getOffset 10
    , instanceOffset11 = getOffset 11
    , instanceOffset12 = getOffset 12
    , instanceOffset13 = getOffset 13
    , instanceOffset14 = getOffset 14
    , instanceOffset15 = getOffset 15
    , instanceOffset16 = getOffset 16
    , instanceOffset17 = getOffset 17
    , instanceOffset18 = getOffset 18
    , instanceOffset19 = getOffset 19
    }


hackSpriteInstances : Mesh Shaders.UvVertex
hackSpriteInstances =
    List.repeat instanceCount Mesh.square
        |> List.indexedMap
            (\index mesh ->
                Mesh.map
                    (\v ->
                        let
                            ( x, y ) =
                                Point2d.coordinates v
                        in
                        { position = Vec3.vec3 (x - 0.5) (y - 0.5) 0
                        , instanceIndex = toFloat index
                        , uv = Vec2.vec2 x (1 - y)
                        }
                    )
                    mesh
            )
        |> Mesh.Compound
        |> Mesh.toMesh
