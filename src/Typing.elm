module Typing exposing
    ( Typing
    , TypingAction(..)
    , backspaceAll
    , backspacePast
    , currentString
    , endTime
    , fastBackspaceAll
    , fastBackspacePast
    , fullString
    , instantTyping
    , normalTyping
    , pause
    , slice
    , startAt
    )

import List.Extra as List
import ListHelper as List
import Quantity exposing (Quantity)
import RealTime exposing (RealTime)


type TypingAction
    = TypeChar Char
    | Pause (Quantity Float RealTime)
    | Backspace


type alias Typing =
    List TypingAction


startAt : Quantity Float RealTime -> Typing
startAt time =
    Pause time |> List.singleton


normalTyping : String -> Typing -> Typing
normalTyping text typing =
    typing ++ (String.toList text |> List.concatMap (\c -> [ TypeChar c, Pause (RealTime.seconds 0.04) ]))


instantTyping : String -> Typing -> Typing
instantTyping text typing =
    typing ++ (String.toList text |> List.map TypeChar)


backspacePast : String -> Typing -> Typing
backspacePast text typing =
    backspacePast_ (RealTime.seconds 0.04) text typing


fastBackspacePast : String -> Typing -> Typing
fastBackspacePast text typing =
    backspacePast_ (RealTime.seconds 0.01) text typing


backspaceAll : Typing -> Typing
backspaceAll typing =
    backspaceAll_ (RealTime.seconds 0.04) typing


fastBackspaceAll : Typing -> Typing
fastBackspaceAll typing =
    backspaceAll_ (RealTime.seconds 0.01) typing


backspacePast_ : Quantity Float RealTime -> String -> Typing -> Typing
backspacePast_ secondsPerBackspace text typing =
    let
        previousText =
            fullString typing
    in
    case previousText |> String.indexes text |> List.last of
        Just index ->
            List.repeat (String.length previousText - index) [ Backspace, Pause secondsPerBackspace ]
                |> List.concat
                |> (++) typing

        Nothing ->
            Backspace :: typing


backspaceAll_ : Quantity Float RealTime -> Typing -> Typing
backspaceAll_ secondsPerBackspace typing =
    List.repeat (fullString typing |> String.length) [ Backspace, Pause secondsPerBackspace ]
        |> List.concat
        |> (++) typing


pause : Quantity Float RealTime -> Typing -> Typing
pause delay typing =
    typing ++ [ Pause delay ]


fullString : Typing -> String
fullString typing =
    currentString (RealTime.seconds 9999999999) typing



{- How long the typing takes between the start time and -}


endTime : Typing -> Quantity Float RealTime
endTime typing =
    List.foldr
        (\value time ->
            case value of
                Pause pauseLength ->
                    Quantity.plus pauseLength time

                TypeChar _ ->
                    time

                Backspace ->
                    time
        )
        Quantity.zero
        typing


currentString : Quantity Float RealTime -> Typing -> String
currentString elapsedTime typing =
    List.foldFast
        (\action ( timeLeft, text ) ->
            case ( Quantity.greaterThanOrEqualTo timeLeft elapsedTime, action ) of
                ( True, TypeChar c ) ->
                    Just ( timeLeft, c :: text )

                ( True, Backspace ) ->
                    Just ( timeLeft, List.drop 1 text )

                ( True, Pause pauseLength ) ->
                    Just ( Quantity.plus pauseLength timeLeft, text )

                ( False, _ ) ->
                    Nothing
        )
        ( Quantity.zero, [] )
        typing
        |> Tuple.second
        |> List.reverse
        |> String.fromList


slice : Quantity Float RealTime -> Quantity Float RealTime -> Typing -> Typing
slice startTime endTime_ typing =
    List.foldFast
        (\action ( totalTime, sliceList ) ->
            case ( Quantity.lessThan totalTime startTime, Quantity.lessThanOrEqualTo totalTime endTime_, action ) of
                ( True, False, TypeChar c ) ->
                    Just ( totalTime, TypeChar c :: sliceList )

                ( True, False, Backspace ) ->
                    Just ( totalTime, Backspace :: sliceList )

                ( True, False, Pause pauseLength ) ->
                    Just ( Quantity.plus pauseLength totalTime, Pause pauseLength :: sliceList )

                ( False, _, Pause pauseLength ) ->
                    Just ( Quantity.plus pauseLength totalTime, sliceList )

                ( False, _, _ ) ->
                    Just ( totalTime, sliceList )

                ( _, True, _ ) ->
                    Nothing
        )
        ( Quantity.zero, [] )
        typing
        |> Tuple.second
        |> List.reverse
