module MainMenu exposing (inGameMsgConfig, view)

import Basics.Extra exposing (flip)
import ColorPalette
import ColorSettingsPage
import Element exposing (Element)
import Element.Background as Background
import Element.Font as Font
import Element.Input as Input
import Element.Keyed
import Helper exposing (filler)
import Html
import Html.Attributes
import InGame
import Input
import Keyframe
import MenuState exposing (MainMenuIntro(..), MainMenuPage(..), MainMenu_, TitlePage_)
import Model exposing (Model, Msg(..))
import Quantity exposing (Quantity)
import RealTime exposing (RealTime)
import Replay
import SettingsPage
import Sound
import StageData exposing (StageId)
import StageSelect
import Style
import Typing


inGameMsgConfig : StageId -> Quantity Float RealTime -> InGame.MsgConfig Msg
inGameMsgConfig stageId currentTime =
    { noOp = NoOp
    , backToStageSelect = PressInGameBackToStageSelect
    , saveReplay = SaveReplay
    , internalMsg = InGameMsg
    , typedHighscoreName = TypedHighscoreName
    , pressedAcceptedHighscoreName = PressedAcceptHighscoreName
    , pressedHighscoreSortBy = PressedHighscoreSortBy
    , missionCompleted = \coreState replay -> Model.MissionCompleted_ stageId coreState replay currentTime |> MissionCompleted
    }


menuPageOffset : Model -> MainMenuPage -> Int
menuPageOffset model menuPage =
    case menuPage of
        SettingsPage ->
            model.windowSize.width

        TitlePage _ ->
            0

        StageSelectPage _ ->
            -model.windowSize.width

        ColorSettingsPage _ ->
            model.windowSize.width * 2


view : Model -> MainMenu_ -> Element Msg
view model mainMenu =
    let
        offset =
            case mainMenu.transitionStart of
                Just ( transitionStart, previousMenuPage ) ->
                    Keyframe.startAt_ transitionStart (menuPageOffset model previousMenuPage |> toFloat)
                        |> Keyframe.add_ Keyframe.InOutQuart MenuState.menuTransitionLength (menuPageOffset model mainMenu.menuPage |> toFloat)
                        |> Keyframe.valueAt_ model.time

                Nothing ->
                    menuPageOffset model mainMenu.menuPage |> toFloat
    in
    Element.el
        [ Element.width Element.fill
        , Element.height Element.fill
        , Element.clip
        , Element.behindContent <|
            case mainMenu.backgroundGameplay of
                Just { inGameModel, replay } ->
                    let
                        showMap =
                            case model.gameState of
                                MenuState.MainMenu menuState ->
                                    case menuState.menuPage of
                                        ColorSettingsPage { colorSelection } ->
                                            (colorSelection == Just ColorPalette.PaletteMapChip)
                                                || (colorSelection == Just ColorPalette.PaletteMapChipHacked)
                                                || (colorSelection == Just ColorPalette.PaletteMapWire)

                                        _ ->
                                            False

                                _ ->
                                    False

                        input =
                            replay
                                |> Maybe.map (flip Replay.getInput inGameModel.replayable.coreState)
                                |> Maybe.withDefault Input.noInput
                    in
                    InGame.view
                        InGame.noOpMsgConfig
                        model
                        (case mainMenu.mainMenuIntro of
                            MainMenuIntro startTime ->
                                model.time |> Quantity.minus startTime |> Quantity.lessThan (RealTime.seconds 1)

                            MainMenuIntroOver ->
                                False
                        )
                        Nothing
                        input
                        (replay /= Nothing)
                        False
                        (InGame.OverrideMap showMap)
                        inGameModel
                        |> Element.map (always NoOp)

                Nothing ->
                    Element.none
        , Element.inFront <|
            Element.el [ Element.centerX, Element.moveDown 30, Element.moveRight (min 0 offset) ] (titleView model mainMenu)
        , Element.inFront <| menuPageView model mainMenu mainMenu.menuPage offset
        , case mainMenu.transitionStart of
            Just ( _, previousMenuPage ) ->
                menuPageView model mainMenu previousMenuPage offset |> Element.map (always NoOp) |> Element.inFront

            Nothing ->
                Helper.noneAttribute
        , Element.inFront <|
            Element.el [ Element.alignRight, Element.alignBottom, Element.moveRight (min 0 offset) ] credits
        , Element.inFront <|
            if mainMenu.showTrailer then
                trailerView (min 1080 model.windowSize.width) PressedCloseTrailer

            else
                Element.none
        ]
        Element.none


menuPageView : Model -> MainMenu_ -> MainMenuPage -> Float -> Element Msg
menuPageView model mainMenu menuPage offset =
    let
        offset_ =
            offset - toFloat (menuPageOffset model menuPage)
    in
    case menuPage of
        TitlePage titlePage ->
            -- We use keying here as a fix to a bug where going from main menu to stage select causes all the stages to be highlighted.
            Element.Keyed.el
                [ Element.moveRight offset_
                , Element.width Element.fill
                , Element.height Element.fill
                , Element.paddingEach { top = 170, bottom = 100, left = 0, right = 0 }
                ]
                ( "MainMenu", mainMenuButtons titlePage )

        SettingsPage ->
            Element.Keyed.el
                [ Element.moveRight offset_
                , Element.width Element.fill
                , Element.centerY
                ]
                ( "Settings", SettingsPage.view model mainMenu )

        ColorSettingsPage colorSettings ->
            Element.Keyed.el
                [ Element.moveRight offset_
                , Element.width Element.fill
                , Element.height Element.fill
                ]
                ( "ColorSettings", ColorSettingsPage.view colorSettingsPageMsgConfig model colorSettings )

        StageSelectPage stageSelectPage ->
            Element.Keyed.el
                [ Element.moveRight offset_
                , Element.width Element.fill
                , Element.height Element.fill
                ]
                ( "StageSelect", StageSelect.view model stageSelectPage )


colorSettingsPageMsgConfig =
    { noOp = NoOp
    , userSelectedLinkText = SelectedColorSchemeLinkText
    , userPressedColorSelection = PressedColorSelection
    , userPressedBackButton = PressedColorSettingsPageBack
    , userPressedRestButton = PressedResetColorSettings
    , colorPickerMsg = ColorPickerMsg
    }


menuButton : Bool -> List (Element.Attr () msg)
menuButton highlight =
    [ Font.size 50
    , Font.center
    , Element.width Element.fill
    , Element.height (Element.fillPortion 5)
    , Font.color <| Element.rgb 1 1 1
    , if highlight then
        Background.color <| Element.rgba 1 1 1 0.5

      else
        Style.menuBackground
    ]


mainMenuButtons : TitlePage_ -> Element Msg
mainMenuButtons { selectedButton } =
    Element.column
        [ Element.width Element.fill
        , Element.height Element.fill
        ]
        [ Style.button
            (menuButton (selectedButton == MenuState.StartButton))
            { onPress = Batch [ PressedTitlePageStartButton, PlaySound Sound.ButtonClick ]
            , onMouseEnter = MouseOverTitlePageButton MenuState.StartButton |> Just
            , onMouseMove = MouseOverTitlePageButton MenuState.StartButton |> Just
            , onMouseLeave = Nothing
            , label = Element.text "Start"
            }
        , filler
        , Style.button
            (menuButton (selectedButton == MenuState.EditorButton))
            { onPress = Batch [ PressedTitlePageEditorButton, PlaySound Sound.ButtonClick ]
            , onMouseEnter = MouseOverTitlePageButton MenuState.EditorButton |> Just
            , onMouseMove = MouseOverTitlePageButton MenuState.EditorButton |> Just
            , onMouseLeave = Nothing
            , label = Element.text "Level Editor"
            }
        , filler
        , Style.button
            (menuButton (selectedButton == MenuState.SettingsButton))
            { onPress = Batch [ PressedTitlePageSettings, PlaySound Sound.ButtonClick ]
            , onMouseEnter = MouseOverTitlePageButton MenuState.SettingsButton |> Just
            , onMouseMove = MouseOverTitlePageButton MenuState.SettingsButton |> Just
            , onMouseLeave = Nothing
            , label = Element.text "Settings"
            }
        ]


titleView : Model -> MainMenu_ -> Element msg
titleView model mainMenu =
    let
        title =
            Element.el
                [ Font.size 70
                , Font.bold
                , Font.glow (Element.rgb 0 0 0) 20
                , Font.color (Element.rgb 1 1 1)
                , Element.centerX
                , Font.family [ Font.typeface "Turret Road", Font.sansSerif ]
                , Font.letterSpacing 4
                , Element.inFront <|
                    Element.el [ Font.glow (Element.rgb 0 0 0) 50 ] (Element.text "CIRCUIT BREAKER")
                ]
                (Element.text "CIRCUIT BREAKER")

        subtitleText_ =
            "The realistic hacking simulator"

        subtitleText =
            case mainMenu.mainMenuIntro of
                MainMenuIntro time ->
                    Typing.startAt time
                        |> Typing.pause (RealTime.seconds 3)
                        |> Typing.normalTyping subtitleText_
                        |> Typing.currentString model.time

                MainMenuIntroOver ->
                    subtitleText_

        subtitle =
            Element.el
                [ Font.size 30
                , Font.italic
                , Font.color (Element.rgba 1 1 1 0.9)
                , Element.centerX
                , Element.moveUp 6
                , Element.inFront (Element.text subtitleText)
                ]
                (Element.el [ Font.color (Element.rgba 1 1 1 0) ] (Element.text subtitleText_))
    in
    Element.column
        [ Element.spacing 10 ]
        [ title, subtitle ]


credits : Element msg
credits =
    Element.column
        [ Element.padding 5
        , Element.spacing 5
        , Font.size 24
        , Font.color (Element.rgb 0.9 0.9 0.9)
        ]
        [ Element.row [ Element.alignRight ]
            [ Element.text "Intro music by "
            , Element.newTabLink
                Style.menuLink
                { url = "https://soundcloud.com/lainvolta/lain-volta-ld-e011", label = Element.text "Lain Volta" }
            ]
        , Element.row [ Element.alignRight ]
            [ Element.text "Gameplay music by "
            , Element.newTabLink
                Style.menuLink
                { url = "https://meganeko.bandcamp.com/track/computer-blues", label = Element.text "Meganeko" }
            ]
        , Element.row [ Element.alignRight ]
            [ Element.newTabLink
                Style.menuLink
                { url = "https://gitlab.com/MartinSStewart/hackman/blob/master/credits.md", label = Element.text "Almost everything else" }
            , Element.text " by Martin Stewart"
            ]
        ]


trailerView : Int -> msg -> Element msg
trailerView videoWidth closeTrailerMsg =
    let
        video =
            Html.video [ Html.Attributes.width videoWidth, Html.Attributes.controls True ]
                [ Html.source
                    [ Html.Attributes.src "extras/circuit_breaker_trailer.mp4"
                    , Html.Attributes.type_ "video/mp4"
                    ]
                    []
                ]
                |> Element.html
                |> Element.el [ Element.centerX, Element.centerY ]
    in
    Element.el
        [ Element.width Element.fill
        , Element.height Element.fill
        , Background.color <| Element.rgba 0 0 0 0.9
        , Element.inFront <|
            Input.button
                [ Font.size 30
                , Font.color <| Element.rgb 1 1 1
                , Element.alignRight
                , Element.alignTop
                , Element.width <| Element.px 100
                , Element.height <| Element.px 100
                , Element.mouseOver <| [ Background.color <| Element.rgba 1 1 1 0.5 ]
                ]
                { onPress = Just closeTrailerMsg, label = Element.el [ Element.centerX, Element.centerY ] (Element.text "✕") }
        ]
        video
