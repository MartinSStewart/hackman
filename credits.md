# Credits

### Music 
- Computer Blues by [Meganeko](https://meganeko.bandcamp.com/track/computer-blues)
- LD - E011 (2013) Smooth Beats by [Lain Volta](https://soundcloud.com/lainvolta/lain-volta-ld-e011)

### Sound Effects
- "Mission ... go!" by [shawshank73 on freesound.org](https://freesound.org/people/shawshank73/sounds/78916/)
- Glass breaking sound for unlocking stages by [speedygonzo on freesound.org](https://freesound.org/people/speedygonzo/sounds/257642/)
- Chip hacked sound by... I lost their name. I'm really sorry whoever made that!

### Misc
- Computer chip icon by [Freepik on flaticon.com](https://www.flaticon.com/free-icon/cpu_1383655)
- Programming, remaining sound effects, and graphics by Martin Stewart
- Thank you friends, colleagues, Elm Slack folks, and especially my sister, who helped playtest the game!
- Also thanks to Ian Mackenzie elm-geometry and Matthew Griffith for elm-ui!