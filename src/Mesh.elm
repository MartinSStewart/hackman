module Mesh exposing
    ( ChipCapVertex
    , CircuitVertex
    , MapOverlayVertex
    , MeshPart(..)
    , Vertex
    , arrow
    , arrowMesh
    , chipCaps
    , chipInsideHeight
    , circle
    , circuit
    , circuitOverlayMap
    , colorToVec4
    , cube
    , glow
    , line
    , map
    , square
    , toMesh
    , wires
    )

import Array exposing (Array)
import Axis3d
import Basics.Extra exposing (flip)
import Bitwise
import BoundingBox2d exposing (BoundingBox2d)
import Cardinal
import Chip
import Circuit exposing (ChipId(..), Circuit, Wire, WireId)
import Color exposing (Color)
import Direction2d
import Geometry.Interop.LinearAlgebra.Point3d as Point3d
import LineSegment2d exposing (LineSegment2d)
import List.Extra as List
import ListHelper as List
import Math.Vector3 as Vec3 exposing (Vec3)
import Math.Vector4 as Vec4 exposing (Vec4)
import Point2d exposing (Point2d)
import Point3d exposing (Point3d)
import Polygon2d exposing (Polygon2d)
import TriangularMesh exposing (TriangularMesh)
import Vector2d exposing (Vector2d)
import Vector2i exposing (Vector2i)
import Vector3d exposing (Vector3d)
import WebGL exposing (Mesh)
import Wire


type MeshPart a
    = TriangleFan a a a (List a)
    | Triangles (List ( a, a, a ))
    | TriangleStrip Bool a a a (List a)
    | VerticesAndIndices (List a) (List ( Int, Int, Int ))
    | Compound (List (MeshPart a))


triangleStrip first second third rest =
    TriangleStrip False first second third rest


type alias Vertex =
    { color : Vec4
    , position : Vec3
    }


type alias CircuitVertex =
    { colorIndex : Float
    , colorLightness : Float
    , colorAlpha : Float
    , position : Vec3
    }


map : (a -> b) -> MeshPart a -> MeshPart b
map mapFunc meshPart =
    case meshPart of
        TriangleFan first second third rest ->
            TriangleFan (mapFunc first) (mapFunc second) (mapFunc third) (List.map mapFunc rest)

        Triangles list ->
            Triangles
                (List.map
                    (\( first, second, third ) -> ( mapFunc first, mapFunc second, mapFunc third ))
                    list
                )

        TriangleStrip flipped first second third rest ->
            TriangleStrip flipped (mapFunc first) (mapFunc second) (mapFunc third) (List.map mapFunc rest)

        VerticesAndIndices vertices indices ->
            VerticesAndIndices (List.map mapFunc vertices) indices

        Compound list ->
            List.map (map mapFunc) list |> Compound


flipFaces : MeshPart a -> MeshPart a
flipFaces meshPart =
    case meshPart of
        TriangleFan first second third rest ->
            case List.reverse (second :: third :: rest) of
                newSecond :: newThird :: newRest ->
                    TriangleFan first newSecond newThird newRest

                _ ->
                    TriangleFan first third second []

        Triangles list ->
            List.map (\( a, b, c ) -> ( c, b, a )) list |> Triangles

        TriangleStrip flipped first second third rest ->
            TriangleStrip (not flipped) first second third rest

        VerticesAndIndices vertices indices ->
            VerticesAndIndices
                vertices
                (List.map (\( a, b, c ) -> ( c, b, a )) indices)

        Compound list ->
            List.map flipFaces list |> Compound


toMesh : MeshPart a -> Mesh a
toMesh meshPart =
    let
        addTriangle vertices indices first second third =
            let
                newVertices =
                    [ first, second, third ] |> Array.fromList |> Array.append vertices

                vertexCount =
                    Array.length vertices

                newIndices =
                    ( vertexCount, vertexCount + 1, vertexCount + 2 ) :: indices
            in
            ( newVertices, newIndices )

        toMesh_ : Array a -> List ( Int, Int, Int ) -> List (MeshPart a) -> Mesh a
        toMesh_ vertices indices meshPartsLeft =
            case meshPartsLeft of
                head :: rest ->
                    case head of
                        TriangleFan first second third rest_ ->
                            let
                                ( newVertices, newIndices ) =
                                    addTriangle vertices indices first second third

                                vertexCount =
                                    Array.length newVertices

                                restIndices =
                                    List.range 0 (List.length rest_ - 1)
                                        |> List.map
                                            (\index ->
                                                ( vertexCount - 3
                                                , vertexCount + index - 1
                                                , vertexCount + index
                                                )
                                            )
                                        |> flip (++) newIndices

                                restVertices =
                                    rest_ |> Array.fromList |> Array.append newVertices
                            in
                            toMesh_ restVertices restIndices rest

                        Triangles triplets ->
                            let
                                newVertices =
                                    List.concatMap (\( v0, v1, v2 ) -> [ v0, v1, v2 ]) triplets
                                        |> Array.fromList
                                        |> Array.append vertices

                                vertexCount =
                                    Array.length vertices

                                newIndices =
                                    List.range 0 (List.length triplets)
                                        |> List.map
                                            (\index ->
                                                ( vertexCount + index * 3
                                                , vertexCount + index * 3 + 1
                                                , vertexCount + index * 3 + 2
                                                )
                                            )
                                        |> (++) indices
                            in
                            toMesh_ newVertices newIndices rest

                        TriangleStrip flipped first second third rest_ ->
                            let
                                ( newVertices, newIndices ) =
                                    if flipped then
                                        addTriangle vertices indices third second first

                                    else
                                        addTriangle vertices indices first second third

                                vertexCount =
                                    Array.length newVertices

                                restIndices =
                                    List.range 0 (List.length rest_ - 1)
                                        |> List.map
                                            (\index ->
                                                let
                                                    isEven =
                                                        modBy 2 index == 0
                                                in
                                                if isEven /= flipped then
                                                    ( vertexCount + index
                                                    , vertexCount + index - 1
                                                    , vertexCount + index - 2
                                                    )

                                                else
                                                    ( vertexCount + index - 2
                                                    , vertexCount + index - 1
                                                    , vertexCount + index
                                                    )
                                            )
                                        |> flip (++) newIndices

                                restVertices =
                                    rest_ |> Array.fromList |> Array.append newVertices
                            in
                            toMesh_ restVertices restIndices rest

                        VerticesAndIndices vertices_ indices_ ->
                            let
                                vertexCount =
                                    Array.length vertices

                                newVertices =
                                    vertices_ |> Array.fromList |> Array.append vertices

                                newIndices =
                                    indices
                                        ++ List.map
                                            (\( a, b, c ) -> ( a + vertexCount, b + vertexCount, c + vertexCount ))
                                            indices_
                            in
                            toMesh_ newVertices newIndices rest

                        Compound list ->
                            toMesh_ vertices indices (list ++ rest)

                [] ->
                    WebGL.indexedTriangles (Array.toList vertices) indices
    in
    toMesh_ Array.empty [] [ meshPart ]


circle : Int -> MeshPart Point2d
circle detail =
    let
        tau =
            pi * 2

        getPoint index =
            Point2d.fromCoordinates
                ( sin (tau * index / toFloat detail)
                , cos (tau * index / toFloat detail)
                )
    in
    TriangleFan 0 1 2 (List.range 3 (detail - 1))
        |> map (toFloat >> getPoint)


type alias GlowVertex =
    { position : Point2d
    , alpha : Float
    , uvPosition : Point2d
    }


glow : Int -> MeshPart GlowVertex
glow detail =
    let
        tau =
            pi * 2

        getPoint maybeIndex =
            case maybeIndex of
                Just index ->
                    { position =
                        Point2d.fromCoordinates
                            ( sin (tau * toFloat index / toFloat detail)
                            , cos (tau * toFloat index / toFloat detail)
                            )
                    , alpha = 0
                    , uvPosition =
                        Point2d.fromCoordinates
                            ( (sin (tau * toFloat index / toFloat detail) + 1) / 2
                            , (cos (tau * toFloat index / toFloat detail) + 1) / 2
                            )
                    }

                Nothing ->
                    { position = Point2d.origin
                    , alpha = 1
                    , uvPosition = Point2d.fromCoordinates ( 0.5, 0.5 )
                    }
    in
    TriangleFan Nothing (Just 0) (Just 1) (List.range 2 detail |> List.map Just)
        |> map getPoint


square : MeshPart Point2d
square =
    TriangleFan ( 0, 0 ) ( 0, 1 ) ( 1, 1 ) [ ( 1, 0 ) ]
        |> map Point2d.fromCoordinates


rectangle : BoundingBox2d -> MeshPart Point2d
rectangle boundingBox =
    let
        { minX, maxX, minY, maxY } =
            BoundingBox2d.extrema boundingBox
    in
    TriangleFan ( minX, minY ) ( minX, maxY ) ( maxX, maxY ) [ ( maxX, minY ) ]
        |> map Point2d.fromCoordinates


line : Float -> LineSegment2d -> MeshPart Point2d
line thickness lineSegment =
    let
        start =
            LineSegment2d.startPoint lineSegment

        end =
            LineSegment2d.endPoint lineSegment

        v0 =
            Vector2d.from start end
                |> Vector2d.perpendicularTo
                |> Vector2d.normalize
                |> Vector2d.scaleBy thickness

        v1 =
            Vector2d.reverse v0
    in
    TriangleFan
        (Point2d.translateBy v1 start)
        (Point2d.translateBy v0 start)
        (Point2d.translateBy v0 end)
        [ Point2d.translateBy v1 end ]


arrow : Float -> Float -> Point2d -> Point2d -> Polygon2d
arrow innerDiameter outerDiameter start end =
    let
        midpoint =
            Point2d.midpoint start end

        perpendicular =
            Vector2d.from end start
                |> Vector2d.perpendicularTo
                |> Vector2d.normalize
    in
    Polygon2d.singleLoop
        [ Point2d.translateBy (Vector2d.scaleBy outerDiameter perpendicular) midpoint
        , end
        , Point2d.translateBy (Vector2d.scaleBy -outerDiameter perpendicular) midpoint
        , Point2d.translateBy (Vector2d.scaleBy -innerDiameter perpendicular) midpoint
        , Point2d.translateBy (Vector2d.scaleBy -innerDiameter perpendicular) start
        , Point2d.translateBy (Vector2d.scaleBy innerDiameter perpendicular) start
        , Point2d.translateBy (Vector2d.scaleBy innerDiameter perpendicular) midpoint
        ]


arrowMesh : Float -> Float -> Point2d -> Point2d -> MeshPart Point2d
arrowMesh innerDiameter outerDiameter start end =
    let
        arrow_ : TriangularMesh Point2d
        arrow_ =
            arrow innerDiameter outerDiameter start end |> Polygon2d.triangulate
    in
    VerticesAndIndices
        (TriangularMesh.vertices arrow_ |> Array.toList)
        (TriangularMesh.faceIndices arrow_ |> List.map (\( a, b, c ) -> ( c, b, a )))


circuit : Circuit -> MeshPart CircuitVertex
circuit circuit_ =
    let
        wireForeground =
            wires 0.2 0.4 14 circuit_
                |> map
                    (\p ->
                        let
                            ( x, y ) =
                                Point2d.coordinates p
                        in
                        { colorIndex = wireMiddleColorIndex
                        , colorLightness = 1
                        , colorAlpha = 1
                        , position = Vec3.vec3 x y 0.01
                        }
                    )

        wireBackground =
            wires 1.1 1.4 20 circuit_
                |> map
                    (\p ->
                        let
                            ( x, y ) =
                                Point2d.coordinates p
                        in
                        { colorIndex = wireEdgeColorIndex
                        , colorLightness = 1
                        , colorAlpha = 1
                        , position = Vec3.vec3 x y 0
                        }
                    )
    in
    Compound [ wireForeground, wireBackground, chips circuit_ ]


type alias MapOverlayVertex =
    { position : Vec3
    , colorIndex : Float
    , chipId : Float
    }


circuitOverlayMap : Circuit -> MeshPart MapOverlayVertex
circuitOverlayMap circuit_ =
    let
        wireForeground =
            wires 0.1 0.2 8 circuit_
                |> map
                    (\p ->
                        { colorIndex = mapWireColorIndex
                        , position = pointToVec3 0.01 p
                        , chipId = 0
                        }
                    )
    in
    Compound
        [ wireForeground
        , Circuit.getChips circuit_
            |> List.indexedMap
                (\index ( _, chip ) ->
                    chipOverlayMap (Bitwise.shiftLeftBy index 1) chip.position chip.size
                )
            |> Compound
        ]


pointToVec3 : Float -> Point2d -> Vec3
pointToVec3 z p =
    let
        ( x, y ) =
            Point2d.coordinates p
    in
    Vec3.vec3 x y z


mapChipColorIndex =
    0


mapWireColorIndex =
    1


chipOverlayMap : Int -> Point2d -> Vector2i -> MeshPart MapOverlayVertex
chipOverlayMap id position size =
    rectangle (BoundingBox2d.from position (Point2d.translateBy (Vector2i.toVector2d size) position))
        |> map (\v -> { position = pointToVec3 0 v, colorIndex = mapChipColorIndex, chipId = toFloat id })


backgroundColor =
    Color.rgba (7 / 255) (62 / 255) (43 / 255) 1


wires : Float -> Float -> Int -> Circuit -> MeshPart Point2d
wires wireThickness endDiameter detail circuit_ =
    let
        wires_ =
            Circuit.getWires circuit_

        getWireEndpoints : ( WireId, Wire ) -> List (Maybe Point2d)
        getWireEndpoints ( _, wire ) =
            [ case wire.start of
                Just _ ->
                    Nothing

                Nothing ->
                    List.head wire.nodes
            , case wire.end of
                Just _ ->
                    Nothing

                Nothing ->
                    List.last wire.nodes
            ]

        endPoints : List (MeshPart Point2d)
        endPoints =
            wires_
                |> List.concatMap getWireEndpoints
                |> List.filterMap identity
                |> List.map
                    (\endpoint ->
                        circle detail
                            |> map (Point2d.scaleAbout Point2d.origin endDiameter)
                            |> map (Point2d.translateBy (Vector2d.from Point2d.origin endpoint))
                    )

        getLineStrip : Wire -> MeshPart Point2d
        getLineStrip wire =
            wire
                |> Wire.nodePoints circuit_
                |> lineStrip wireThickness detail
    in
    wires_
        |> List.map (\( _, wire ) -> getLineStrip wire)
        |> (++) endPoints
        |> Compound


lineStrip : Float -> Int -> List Point2d -> MeshPart Point2d
lineStrip thickness detail points =
    let
        bends : List (MeshPart Point2d)
        bends =
            points
                |> List.drop 1
                |> List.dropLast
                |> List.map
                    (\position ->
                        circle detail
                            |> map (Point2d.scaleAbout Point2d.origin thickness)
                            |> map (Point2d.translateBy (Vector2d.from Point2d.origin position))
                    )
    in
    List.pairwise points
        |> List.map (LineSegment2d.fromEndpoints >> line thickness)
        |> (++) bends
        |> Compound


type alias ChipVertex =
    { color : Vec3
    , position : Vec3
    , transparencyIndex : Float
    }


cube : MeshPart Point3d
cube =
    [ TriangleFan ( 0, 0, 0 ) ( 1, 0, 0 ) ( 1, 1, 0 ) [ ( 0, 1, 0 ) ] -- Bottom
    , TriangleFan ( 0, 0, 1 ) ( 0, 1, 1 ) ( 1, 1, 1 ) [ ( 1, 0, 1 ) ] -- Top
    , TriangleFan ( 0, 0, 0 ) ( 0, 0, 1 ) ( 1, 0, 1 ) [ ( 1, 0, 0 ) ] -- Back
    , TriangleFan ( 0, 1, 0 ) ( 1, 1, 0 ) ( 1, 1, 1 ) [ ( 0, 1, 1 ) ] -- Front
    , TriangleFan ( 0, 0, 0 ) ( 0, 1, 0 ) ( 0, 1, 1 ) [ ( 0, 0, 1 ) ] -- Left
    , TriangleFan ( 1, 0, 0 ) ( 1, 0, 1 ) ( 1, 1, 1 ) [ ( 1, 1, 0 ) ] -- Right
    ]
        |> Compound
        |> map Point3d.fromCoordinates


scaleBy : Vector3d -> Point3d -> Point3d
scaleBy scale point =
    let
        ( x, y, z ) =
            Point3d.coordinates point

        ( sx, sy, sz ) =
            Vector3d.components scale
    in
    Point3d.fromCoordinates ( x * sx, y * sy, z * sz )


chipInsideHeight =
    0.5


chipUndersideHeight =
    0.25


squareShadow : Vector2d -> Vector2d -> MeshPart CircuitVertex
squareShadow size penumbraSize =
    let
        ( sx, sy ) =
            Vector2d.components size

        ( px, py ) =
            Vector2d.components penumbraSize

        toVertex alpha =
            \( x, y ) ->
                { position = Vec3.vec3 x y 0
                , colorIndex = shadowColorIndex
                , colorLightness = 1
                , colorAlpha = alpha
                }

        vertices =
            List.map
                (toVertex 1)
                [ ( 0, 0 ), ( sx, 0 ), ( sx, sy ), ( 0, sy ) ]
                ++ List.map
                    (toVertex 0)
                    [ ( -px, -py ), ( sx + px, -py ), ( sx + px, sy + py ), ( -px, sy + py ) ]

        indices =
            [ ( 2, 1, 0 )
            , ( 3, 2, 0 )
            , ( 0, 1, 4 )
            , ( 1, 5, 4 )
            , ( 1, 2, 5 )
            , ( 6, 5, 2 )
            , ( 2, 3, 6 )
            , ( 3, 7, 6 )
            , ( 0, 7, 3 )
            , ( 4, 7, 0 )
            ]
    in
    VerticesAndIndices vertices indices


type alias ChipCapVertex =
    { position : Vec3
    , colorLightness : Float
    , chipId : Float
    }


chipCap : Vector2d -> MeshPart ( Float, Point3d )
chipCap size =
    let
        ( sx, sy ) =
            Vector2d.components size

        -- Top margin
        o =
            chipBaseMargin + 0.1

        -- Bottom margin
        b =
            chipBaseMargin

        height =
            0.2
    in
    [ -- Top
      ( 1.15
      , TriangleFan ( o, o, 1 ) ( o, sy - o, 1 ) ( sx - o, sy - o, 1 ) [ ( sx - o, o, 1 ) ]
      )

    -- Back
    , ( 1.06
      , TriangleFan ( b, b, 0 ) ( o, o, 1 ) ( sx - o, o, 1 ) [ ( sx - b, b, 0 ) ]
      )

    -- Front
    , ( 1.08
      , TriangleFan ( b, sy - b, 0 ) ( sx - b, sy - b, 0 ) ( sx - o, sy - o, 1 ) [ ( o, sy - o, 1 ) ]
      )

    -- Left
    , ( 1.12
      , TriangleFan ( b, b, 0 ) ( b, sy - b, 0 ) ( o, sy - o, 1 ) [ ( o, o, 1 ) ]
      )

    -- Right
    , ( 1.02
      , TriangleFan ( sx - b, b, 0 ) ( sx - o, o, 1 ) ( sx - o, sy - o, 1 ) [ ( sx - b, sy - b, 0 ) ]
      )
    ]
        |> List.map (\( colorLightness, mesh ) -> map (\v -> ( colorLightness, v )) mesh)
        |> Compound
        |> map
            (\( colorLightness, ( x, y, z ) ) ->
                ( colorLightness, Point3d.fromCoordinates ( x, y, z * height ) )
            )


chipCaps : Circuit -> MeshPart ChipCapVertex
chipCaps circuit_ =
    List.map
        (\( ChipId chipId, chip ) ->
            let
                offset =
                    Vector3d.fromComponents
                        ( Point2d.xCoordinate chip.position
                        , Point2d.yCoordinate chip.position
                        , chipInsideHeight
                        )
            in
            map
                (\( colorLightness, p ) ->
                    { position = Point3d.translateBy offset p |> Point3d.toVec3
                    , colorLightness = colorLightness
                    , chipId = toFloat chipId
                    }
                )
                (chipCap (Vector2i.toVector2d chip.size))
        )
        (Circuit.getChips circuit_)
        |> Compound


chipBaseMargin =
    0.2


chipColorIndex =
    0


chipLegColorIndex =
    1


wireMiddleColorIndex =
    2


wireEdgeColorIndex =
    3


shadowColorIndex =
    5


chipBase : Point2d -> Vector2d -> MeshPart CircuitVertex
chipBase pos size =
    let
        ( x, y ) =
            Point2d.coordinates pos

        ( sx, sy ) =
            Vector2d.components size

        shadowOffset =
            0.25
    in
    [ map
        (\p ->
            { position =
                scaleBy (Vector3d.fromComponents ( sx - chipBaseMargin * 2, sy - chipBaseMargin * 2, chipInsideHeight - chipUndersideHeight )) p
                    |> Point3d.translateBy (Vector3d.fromComponents ( x + chipBaseMargin, y + chipBaseMargin, chipUndersideHeight ))
                    |> Point3d.toVec3
            , colorIndex = chipColorIndex
            , colorLightness = 1
            , colorAlpha = 1
            }
        )
        cube
    , map
        (\p ->
            let
                ( px, py ) =
                    Point2d.coordinates p
            in
            { position =
                Point3d.fromCoordinates ( x + px * (sx - 1) + 0.5, y + py * (sy - 1) + 0.5, chipInsideHeight + 0.01 )
                    |> Point3d.toVec3
            , colorIndex = chipColorIndex
            , colorLightness = 4 / 3
            , colorAlpha = 1
            }
        )
        square
    , squareShadow
        (Vector2d.fromComponents ( sx - shadowOffset * 2, sy - shadowOffset * 2 ))
        (Vector2d.fromComponents ( 0.5, 0.5 ))
        |> map
            (\{ position, colorIndex, colorLightness, colorAlpha } ->
                { position =
                    Point3d.fromCoordinates ( x + shadowOffset, y + shadowOffset, 0.02 )
                        |> Point3d.toVec3
                        |> Vec3.add position
                , colorIndex = colorIndex
                , colorLightness = colorLightness
                , colorAlpha = colorAlpha * 0.2
                }
            )
    , map
        (\p ->
            let
                ( px, py ) =
                    Point2d.coordinates p
            in
            { position =
                Point3d.fromCoordinates ( x + px * (sx + 2) - 1, y + py * (sy + 2) - 1, 0 )
                    |> Point3d.toVec3
            , colorIndex = wireEdgeColorIndex
            , colorLightness = 1
            , colorAlpha = 1
            }
        )
        square
    ]
        |> Compound


colorToVec4 color =
    let
        { red, green, blue, alpha } =
            Color.toRgba color
    in
    Vec4.vec4 (red * alpha) (green * alpha) (blue * alpha) alpha


chips : Circuit -> MeshPart CircuitVertex
chips circuit_ =
    List.concatMap
        (\( chipId, chip ) ->
            chipBase
                chip.position
                (Vector2i.toVector2d chip.size)
                :: List.map
                    (\{ edgePoint } ->
                        let
                            ( x, y ) =
                                Chip.edgePointWorldPosition chip edgePoint |> Point2d.coordinates
                        in
                        map
                            (\( v, colorLightness ) ->
                                let
                                    rotateBy : Float
                                    rotateBy =
                                        Cardinal.direction edgePoint.side
                                            |> Direction2d.angleFrom (Direction2d.fromAngle 0)
                                in
                                { position =
                                    Point3d.rotateAround Axis3d.z rotateBy v
                                        |> Point3d.translateBy (Vector3d.fromComponents ( x, y, 0 ))
                                        |> Point3d.toVec3
                                , colorIndex = chipLegColorIndex
                                , colorLightness = colorLightness
                                , colorAlpha = 1
                                }
                            )
                            chipPin
                    )
                    (Chip.connectedWires circuit_ chipId)
        )
        (Circuit.getChips circuit_)
        |> Compound


chipPinPath : List { x : Float, y : Float, width : Float, colorLightness : Float }
chipPinPath =
    let
        height =
            chipInsideHeight + 0.01
    in
    [ { x = -0.5, y = height, width = 0.5, colorLightness = 1 }
    , { x = -chipBaseMargin, y = height, width = 0.5, colorLightness = 1 }
    , { x = -chipBaseMargin / 2, y = height, width = 0.5, colorLightness = 1 }
    , { x = 0, y = height - chipBaseMargin / 2, width = 0.5, colorLightness = 6 / 8 }
    , { x = 0, y = height - chipBaseMargin, width = 0.5, colorLightness = 6 / 8 }
    , { x = 0, y = height * (1 / 3), width = 0.5, colorLightness = 6 / 8 }
    , { x = 0, y = height * (1 / 3) - 0.1, width = 0.25, colorLightness = 6 / 8 }
    , { x = 0, y = 0, width = 0.25, colorLightness = 5.5 / 8 }
    ]


chipPin : MeshPart ( Point3d, Float )
chipPin =
    let
        vertices =
            List.concatMap
                (\{ x, y, width, colorLightness } ->
                    [ ( Point3d.fromCoordinates ( x, -width / 2, y ), colorLightness )
                    , ( Point3d.fromCoordinates ( x, width / 2, y ), colorLightness )
                    ]
                )
                chipPinPath
    in
    case vertices of
        first :: second :: third :: rest ->
            Compound
                [ triangleStrip first second third rest
                , triangleStrip first second third rest |> flipFaces
                ]

        _ ->
            Compound []
