module UrlData exposing (UrlData, UrlDataError(..), fromUrl, toUrl, urlQueryName)

import Codec.Serialize as Codec
import ColorPalette exposing (ColorPalette)
import Url exposing (Url)


type alias UrlData =
    { colorPalette : ColorPalette
    }


type UrlDataError
    = NoUrlData
    | InvalidUrlData


fromUrl : Url -> Result UrlDataError UrlData
fromUrl url =
    case url.query |> Maybe.withDefault "" |> String.split "=" of
        head :: saveDataFromUrl :: [] ->
            if String.endsWith urlQueryName head then
                Codec.fromString codec saveDataFromUrl |> Result.mapError (always InvalidUrlData)

            else
                Err InvalidUrlData

        _ ->
            Err NoUrlData


toUrl : Url -> UrlData -> String
toUrl url =
    Codec.toString codec >> (++) (Url.toString url ++ "?" ++ urlQueryName ++ "=")


type UrlDataVersion
    = UrlDataV0 UrlData


codec : Codec.Codec UrlData
codec =
    Codec.customType
        (\v0Encoder value ->
            case value of
                UrlDataV0 value_ ->
                    v0Encoder value_
        )
        |> Codec.variant1 UrlDataV0 urlDataCodecV0
        |> Codec.finishCustomType
        |> Codec.map
            (\value ->
                case value of
                    UrlDataV0 value_ ->
                        value_
            )
            UrlDataV0


urlDataCodecV0 : Codec.Codec UrlData
urlDataCodecV0 =
    Codec.record UrlData
        |> Codec.field .colorPalette ColorPalette.codec
        |> Codec.finishRecord


urlQueryName : String
urlQueryName =
    "a"
