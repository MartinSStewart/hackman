module Editor.Remap exposing (remapAssocList, remapDict)

import Array exposing (Array)
import AssocList
import Dict exposing (Dict)


getUnusedDictKeys : Int -> Dict Int a -> List Int
getUnusedDictKeys count dict =
    let
        getUnusedKeys_ list index =
            if List.length list == count then
                list

            else if Dict.member index dict then
                getUnusedKeys_ list (index + 1)

            else
                getUnusedKeys_ (index :: list) (index + 1)
    in
    getUnusedKeys_ [] 0


remapDict : Dict Int a -> Dict Int a -> Dict Int Int
remapDict source destination =
    let
        unusedKeys : Array Int
        unusedKeys =
            getUnusedDictKeys (Dict.size source) destination |> Array.fromList
    in
    Dict.toList source
        |> List.indexedMap (\index ( key, _ ) -> ( key, Array.get index unusedKeys |> Maybe.withDefault 0 ))
        |> Dict.fromList


getUnusedAssocListKeys : (Int -> a) -> Int -> AssocList.Dict a b -> List a
getUnusedAssocListKeys keyConstructor count dict =
    let
        getUnusedKeys_ list index =
            if List.length list == count then
                list

            else if AssocList.member (keyConstructor index) dict then
                getUnusedKeys_ list (index + 1)

            else
                getUnusedKeys_ (keyConstructor index :: list) (index + 1)
    in
    getUnusedKeys_ [] 0


remapAssocList : (Int -> a) -> AssocList.Dict a b -> AssocList.Dict a b -> AssocList.Dict a a
remapAssocList keyConstructor source destination =
    let
        unusedKeys =
            getUnusedAssocListKeys keyConstructor (AssocList.size source) destination |> Array.fromList
    in
    AssocList.toList source
        |> List.indexedMap (\index ( key, _ ) -> ( key, Array.get index unusedKeys |> Maybe.withDefault (keyConstructor 0) ))
        |> AssocList.fromList
