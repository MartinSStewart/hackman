module Agent exposing
    ( AgentType(..)
    , ChaserAgent_
    , ChipData
    , Config
    , Damaged_
    , Health(..)
    , PassiveAgent_
    , Player
    , WireData
    , WireTransition
    , chaserAgentBaseSpeed
    , chaserAgentMaxWireSpeedFactor
    , chaserAgentMinWireSpeedFactor
    , damagedRecently
    , damagedRecentlyDuration
    , deathStart
    , decrementHealth
    , fromStartPosition
    , getPosition
    , healthCompare
    , healthValue
    , isAlive
    , lazyHealth
    , passiveAgentSpeed
    , playerDirection
    , playerMaxHealth
    , playerMaxWireSpeedFactor
    , playerMinWireSpeedFactor
    , playerWireBaseSpeed
    , spawn
    , transitionStart
    , transitionStep
    , updatePosition
    , wirePosition
    , worldPositionOnChip
    )

import Cardinal exposing (Cardinal)
import Chip
import Circuit
    exposing
        ( AgentStartType(..)
        , Chip
        , ChipId
        , Circuit
        , CircuitPosition(..)
        , Wire
        , WireDirection
        , WireId
        , WiresAndChips
        )
import Direction2d exposing (Direction2d)
import Helper
import InGameTime exposing (InGameTime)
import Keyframe
import Point2d exposing (Point2d)
import Point2i exposing (Point2i)
import Quantity exposing (Quantity)
import StageScore
import T exposing (T(..))
import Wire


type alias WireTransition =
    { start : Point2d
    , t : Quantity Float T
    }


transitionStart : Point2d -> WireTransition
transitionStart startPos =
    { start = startPos
    , t = Quantity.zero
    }


transitionStep : Quantity Float T -> WireTransition -> Maybe WireTransition
transitionStep movement { start, t } =
    if Quantity.plus t movement |> Quantity.lessThan (T.t 1) then
        { start = start
        , t = Quantity.plus t movement
        }
            |> Just

    else
        Nothing


type alias WireData =
    { direction : Circuit.WireDirection
    , transitioning : Maybe WireTransition
    , speedFactor : Float
    }


type alias ChipData =
    { direction : Cardinal
    , previousPoint : Point2i
    }


type alias Player =
    { position : CircuitPosition WireData ChipData
    , health : Health
    }


type Health
    = MaxHealth
    | Damaged Damaged_


type alias Damaged_ =
    { health : Int, lastDamaged : Quantity Float InGameTime }


type alias Config =
    { baseSpeed : Float
    , boostFactor : Float
    , slowFactor : Float
    }


type AgentType
    = PassiveAgent PassiveAgent_
    | ChaserAgent ChaserAgent_


type alias PassiveAgent_ =
    { position : CircuitPosition WireData ChipData }


type alias ChaserAgent_ =
    { position : CircuitPosition WireData ChipData }


damagedRecently : Quantity Float InGameTime -> Player -> Bool
damagedRecently time player =
    case player.health of
        MaxHealth ->
            False

        Damaged { lastDamaged } ->
            Quantity.plus damagedRecentlyDuration lastDamaged |> Quantity.greaterThanOrEqualTo time


damagedRecentlyDuration : Quantity Float InGameTime
damagedRecentlyDuration =
    InGameTime.seconds 1.7


isAlive : Player -> Bool
isAlive player =
    case player.health of
        MaxHealth ->
            True

        Damaged { health } ->
            health > 0


decrementHealth : Quantity Float InGameTime -> Player -> Player
decrementHealth time player =
    case player.health of
        MaxHealth ->
            { player | health = Damaged { health = playerMaxHealth - 1, lastDamaged = time } }

        Damaged { health } ->
            { player | health = Damaged { health = health - 1, lastDamaged = time } }


{-| Player health with a transition effect from the previous health value.
-}
lazyHealth : Quantity Float InGameTime -> Player -> Float
lazyHealth time player =
    case player.health of
        MaxHealth ->
            3

        Damaged { health, lastDamaged } ->
            Keyframe.startAt_ lastDamaged (health + 1 |> toFloat)
                |> Keyframe.add_ Keyframe.Linear damagedRecentlyDuration (toFloat health)
                |> Keyframe.valueAt_ time


healthValue : Player -> Quantity Int StageScore.HealthPoints
healthValue player =
    case player.health of
        MaxHealth ->
            Quantity.Quantity 3

        Damaged { health, lastDamaged } ->
            Quantity.Quantity health


deathStart : Player -> Maybe (Quantity Float InGameTime)
deathStart player =
    case player.health of
        MaxHealth ->
            Nothing

        Damaged { health, lastDamaged } ->
            if health > 0 then
                Nothing

            else
                Just lastDamaged


healthCompare : Health -> Health -> Order
healthCompare health0 health1 =
    case ( health0, health1 ) of
        ( MaxHealth, MaxHealth ) ->
            EQ

        ( MaxHealth, Damaged _ ) ->
            GT

        ( Damaged _, MaxHealth ) ->
            LT

        ( Damaged damage0, Damaged damage1 ) ->
            compare damage0.health damage1.health


getPosition : AgentType -> CircuitPosition WireData ChipData
getPosition agentType =
    case agentType of
        PassiveAgent { position } ->
            position

        ChaserAgent { position } ->
            position


updatePosition : (CircuitPosition WireData ChipData -> CircuitPosition WireData ChipData) -> AgentType -> AgentType
updatePosition updateFunc agentType =
    case agentType of
        PassiveAgent { position } ->
            PassiveAgent { position = updateFunc position }

        ChaserAgent { position } ->
            ChaserAgent { position = updateFunc position }


spawn : AgentStartType -> AgentType
spawn agentStartType =
    case agentStartType of
        PassiveAgentStart { position } ->
            PassiveAgent { position = fromStartPosition position }

        ChaserAgentStart { position } ->
            ChaserAgent { position = fromStartPosition position }


fromStartPosition : CircuitPosition WireDirection Cardinal -> CircuitPosition WireData ChipData
fromStartPosition startPosition =
    case startPosition of
        WirePosition { wireId, t, data } ->
            WirePosition { wireId = wireId, t = t, data = { direction = data, transitioning = Nothing, speedFactor = 1 } }

        ChipPosition { chipId, position, chipData } ->
            Circuit.chipPosition chipId position { direction = chipData, previousPoint = position }


playerMaxHealth : number
playerMaxHealth =
    3


playerWireBaseSpeed : Float
playerWireBaseSpeed =
    9


playerMaxWireSpeedFactor : Float
playerMaxWireSpeedFactor =
    1.4


playerMinWireSpeedFactor : Float
playerMinWireSpeedFactor =
    0.9


passiveAgentSpeed : Float
passiveAgentSpeed =
    9


chaserAgentBaseSpeed : Float
chaserAgentBaseSpeed =
    playerWireBaseSpeed


chaserAgentMaxWireSpeedFactor : Float
chaserAgentMaxWireSpeedFactor =
    1.25


chaserAgentMinWireSpeedFactor : Float
chaserAgentMinWireSpeedFactor =
    0.7


wirePosition : WiresAndChips a -> Wire -> Quantity Float T -> Maybe Point2d
wirePosition circuit wire (Quantity.Quantity t) =
    let
        nodesBetween =
            Wire.nodePoints circuit wire
                |> List.drop (floor t)
                |> List.take 2
    in
    case nodesBetween of
        first :: last :: [] ->
            Helper.frac t |> Point2d.interpolateFrom first last |> Just

        first :: [] ->
            Just first

        _ ->
            Nothing


worldPositionOnChip : Chip -> Point2i -> Point2d
worldPositionOnChip chip localPosition =
    Chip.localToWorld chip localPosition


playerDirection : WiresAndChips a -> Player -> Maybe Direction2d
playerDirection circuit player =
    case player.position of
        WirePosition { wireId, t, data } ->
            case Circuit.getWire circuit wireId of
                Just wire ->
                    let
                        margin =
                            0.8

                        ( wire2, t2 ) =
                            case data.direction of
                                Circuit.Forward ->
                                    ( wire, t )

                                Circuit.Backward ->
                                    ( Wire.reverse wire, Wire.reverseT circuit wire t )

                        ( splitWire, _ ) =
                            Wire.moveT circuit margin wire2 t2 |> Wire.split circuit wire2

                        ( _, splitWire2 ) =
                            Wire.moveT circuit (-margin * 2) splitWire (Wire.tMax circuit splitWire) |> Wire.split circuit splitWire
                    in
                    splitWire2 |> Wire.nodePoints circuit |> Wire.averageDirection

                Nothing ->
                    Nothing

        ChipPosition { chipData } ->
            chipData.direction |> Cardinal.direction |> Just
