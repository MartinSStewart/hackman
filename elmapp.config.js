var path = require('path');

var SpritesmithPlugin = require('webpack-spritesmith');

module.exports = {
    homepage: "https://martinsstewart.gitlab.io/hackman",
    configureWebpack: (config, env) => {

        config.module.rules.push(
           {test: /\.styl$/, use: [
               'style-loader',
               'css-loader',
               'stylus-loader'
           ]});

        config.module.rules.push(
           {test: /\.png$/, use: [
               'file-loader?name=i/[hash].[ext]'
           ]});

        config.resolve.modules.push("spritesmith-generated");

        config.plugins.push(
            new SpritesmithPlugin({
                 src: {
                     cwd: path.resolve(__dirname, 'sprites'),
                     glob: '*.png'
                 },
                 target: {
                     image: path.resolve(__dirname, 'public/images/sprite_sheet.png'),
                     css: path.resolve(__dirname, 'public/images/sprite_sheet.styl')
                 },
                 apiOptions: {
                     cssImageRef: "~sprite.png"
                 }
             }));

        return config;
    }
}